#include "ShadowMapBuffer.hpp"
#include <Core/Macros.hpp>

ShadowMapBuffer::ShadowMapBuffer() {
  glGenFramebuffers(1, &m_id);
  glGenTextures(1, &texture);
}

void ShadowMapBuffer::init(int w, int h) {
  m_width = w;
  m_height = h;
  bind();
/*
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m_width, m_height, 0, GL_RED, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
  GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
  glDrawBuffers(1, DrawBuffers);
*/

  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, m_width, m_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture, 0);
  glDrawBuffer(GL_NONE);

  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
      Log(logError) << "Framebuffer is not complete !";
    }

  unbind();

}

Matrix4 ShadowMapBuffer::bias() {
  return Matrix4(
        0.5f, 0.f,  0.f,  0.f,
        0.f,  0.5f, 0.f,  0.f,
        0.f,  0.f,  0.5f, 0.f,
        0.5f, 0.5f, 0.5f, 1.f
        );
}

ShadowMapBuffer::~ShadowMapBuffer() {
  glDeleteFramebuffers(1, &m_id);
}

void ShadowMapBuffer::bind() {
  glBindFramebuffer(GL_FRAMEBUFFER, m_id);
  glViewport(0, 0, m_width, m_height);
}

void ShadowMapBuffer::unbind() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ShadowMapBuffer::unbindOther(unsigned int id){
  glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void ShadowMapBuffer::resize(int w, int h) {
  init(w,h);
}
