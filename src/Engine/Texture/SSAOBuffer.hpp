//
// Created by pierre on 21/10/18.
//

#ifndef ROGUE_SSAOBUFFER_HPP
#define ROGUE_SSAOBUFFER_HPP

/**
 * \file SSAOBuffer.hpp
 * \brief Frame buffer for SSAO
 */

class SSAOBuffer {
public:

    SSAOBuffer();
    ~SSAOBuffer();

    void init(int w, int h);

    void bind();
    void unbind();

    void resize(int w, int h);
    unsigned int texture;

private:

    int m_width;
    int m_height;

    unsigned int m_id;

};


#endif //ROGUE_SSAOBUFFER_HPP
