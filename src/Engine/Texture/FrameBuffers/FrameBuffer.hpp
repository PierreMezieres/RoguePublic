#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H
#include <Core/Macros.hpp>
#include <iostream>

class FrameBufferA {
public:
    void initFrameBuffer(int w, int h);
    virtual void bind();
    virtual void unbind();
    virtual void unbindOther(unsigned int id);

    virtual ~FrameBufferA();
    virtual void init(int w, int h);
    virtual void resize(int w, int h);

protected:
    FrameBufferA();
    int m_width;
    int m_height;
    unsigned int m_id;

};

#endif

