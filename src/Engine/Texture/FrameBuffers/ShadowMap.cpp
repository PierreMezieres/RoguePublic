#include "ShadowMap.hpp"

ShadowMap::ShadowMap() : FrameBufferA(){
  // attach the shadow map to framebuffer
  glGenTextures(1, &m_depthBuffer);

  m_biais = Matrix4(0.5f, 0.f , 0.f , 0.f,
                    0.f , 0.5f, 0.f , 0.f,
                    0.f , 0.f , 0.5f, 0.f,
                    0.5f, 0.5f, 0.5f, 1.f);
}

void ShadowMap::init(int w, int h) {
  initShadowMap(w,h);
}

void ShadowMap::resize(int w, int h) {
  initShadowMap(w,h);
}

void ShadowMap::initShadowMap(int w, int h){
  bind();
  // create the shadow map
  glBindTexture(GL_TEXTURE_2D, m_depthBuffer);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, w, h,0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_depthBuffer, 0);

  // depth is stored in z-buffer to which the shadow map is attached,
  // so there is no need for any color buffers
  glDrawBuffer(GL_NONE);

  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
      std::cerr << "Framebuffer is not complete !";
    }
  unbind();

}

void ShadowMap::bind() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_id);
}

void ShadowMap::unbind() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void ShadowMap::unbindOther(unsigned int id){
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id);
}
