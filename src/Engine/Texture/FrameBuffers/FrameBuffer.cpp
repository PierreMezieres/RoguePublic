#include <Engine/Texture/FrameBuffers/FrameBuffer.hpp>

FrameBufferA::FrameBufferA(){
    glGenFramebuffers(1, &m_id);
}

void FrameBufferA::init(int w, int h) {
    m_width = w;
    m_height = h;
}

void FrameBufferA::initFrameBuffer(int w, int h) {
  m_width = w;
  m_height = h;
}

FrameBufferA::~FrameBufferA() {
    glDeleteFramebuffers(1, &m_id);
}

void FrameBufferA::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
}

void FrameBufferA::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBufferA::unbindOther(unsigned int id){
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void FrameBufferA::resize(int w, int h) {
    initFrameBuffer(w,h);
}
