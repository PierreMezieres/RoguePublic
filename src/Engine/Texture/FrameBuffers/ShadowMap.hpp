#ifndef SHADOWMAP_H
#define SHADOWMAP_H

#include "FrameBuffer.hpp"
#include "Core/Macros.hpp"

class ShadowMap : public FrameBufferA
{
public:
  ShadowMap();
  virtual ~ShadowMap() override {}
  virtual void init(int w, int h) override;
  virtual void resize(int w, int h) override;
  virtual void bind() override;
  virtual void unbind() override;
  virtual void unbindOther(unsigned int id) override;
  inline GLuint buffer() { return m_depthBuffer;}
  inline Matrix4 biais() const { return m_biais;}
protected:
  unsigned int m_depthBuffer;
  Matrix4 m_biais;
  void initShadowMap(int w, int h);
};

#endif // SHADOWMAP_H
