//
// Created by jb on 03/02/19.
//

#ifndef ROGUE_SHADOWMAPBUFFERPOINTLIGHT_HPP
#define ROGUE_SHADOWMAPBUFFERPOINTLIGHT_HPP
#include <Core/Macros.hpp>

class ShadowMapBufferPointLight {

public:
    ShadowMapBufferPointLight();
    ~ShadowMapBufferPointLight();

    void init(int w, int h);

    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);
    unsigned int buffer() { return texture;}
    unsigned int texture;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
};


#endif //ROGUE_SHADOWMAPBUFFERPOINTLIGHT_HPP
