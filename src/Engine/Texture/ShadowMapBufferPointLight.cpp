//
// Created by jb on 03/02/19.
//

#include "ShadowMapBufferPointLight.hpp"


ShadowMapBufferPointLight::ShadowMapBufferPointLight() {
    glGenFramebuffers(1, &m_id);
    glGenTextures(1, &texture);
}

ShadowMapBufferPointLight::~ShadowMapBufferPointLight() {
    glDeleteFramebuffers(1, &m_id);
    glDeleteTextures(1, &texture);
}

void ShadowMapBufferPointLight::init(int w, int h) {
    m_width = w;
    m_height = h;
    bind();

    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    for (unsigned int i = 0; i < 6; ++i)
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT16, m_width, m_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);
    glDrawBuffer(GL_NONE);
    // glReadBuffer(GL_NONE);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        Log(logError) << "Framebuffer is not complete !";
    }

    unbind();
}

void ShadowMapBufferPointLight::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
    glViewport(0, 0, m_width, m_height);
}

void ShadowMapBufferPointLight::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ShadowMapBufferPointLight::unbindOther(unsigned int id){
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void ShadowMapBufferPointLight::resize(int w, int h) {
    init(w,h);
}
