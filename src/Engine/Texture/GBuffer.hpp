//
// Created by pierre on 20/10/18.
//

#ifndef ROGUE_GBUFFER_HPP
#define ROGUE_GBUFFER_HPP

/**
 * \file GBuffer.hpp
 * \brief Simple geometric buffer
 */


class GBuffer {
public:

    GBuffer();
    ~GBuffer();

    void init(int w, int h);

    void bind();
    void unbind();

    void resize(int w, int h);
    unsigned int gPosition, gNormal;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;

};

#endif //ROGUE_GBUFFER_HPP
