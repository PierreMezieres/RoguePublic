#ifndef TEXTURE_HPP
#define TEXTURE_HPP

/**
 * \file Texture.hpp
 * \brief Simple texture from file
 */

#include <string>

#include <Core/Log/Log.hpp>
#include <Core/Macros.hpp>

/* For the moment texture is only an image file
 * but later, texture will may be generated proceduraly */

 /**
  * \class Texture
  * \brief Manage texture from simple image
  */
class Texture
{
public:
    Texture(std::string filename);                                      ///< Construct texture from image
    Texture(std::string directory, std::vector<std::string> faces);     ///< Construct cubemap from images

    void bind(GLenum unit);         ///< Bind the texture if it's a simple image
    void bindCubemap(GLenum unit);  ///< Bind the cubemap

    bool isLoadingOk();             ///< Return true if the loading of texture is ok

    std::string m_filename;         ///< Filename of image used as texture

    unsigned int texture;           ///< OpenGL id for the texture
private:

    bool loadingOk = true;

};

#endif // TEXTURE_HPP
