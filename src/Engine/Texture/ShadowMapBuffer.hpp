#ifndef SHADOWMAPBUFFER_HPP
#define SHADOWMAPBUFFER_HPP
#include <Core/Macros.hpp>

class ShadowMapBuffer {

public :

  ShadowMapBuffer();
  ~ShadowMapBuffer();

  void init(int w, int h);

  void bind();
  void unbind();
  void unbindOther(unsigned int id);

  void resize(int w, int h);
  unsigned int buffer() { return texture;}
  unsigned int texture;
  Matrix4 bias();

  private:

  int m_width;
  int m_height;

  unsigned int m_id;
};

#endif
