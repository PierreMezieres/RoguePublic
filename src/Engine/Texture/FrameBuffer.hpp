//
// Created by pierre on 31/08/18.
//

#ifndef ROGUE_FRAMEBUFFER_HPP
#define ROGUE_FRAMEBUFFER_HPP

/**
 * \file FrameBuffer.hpp
 * \brief Simple frameBuffer
 */


class FrameBuffer {
public:

    FrameBuffer();
    ~FrameBuffer();

    void init(int w, int h);

    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);
    unsigned int texture;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;

};


#endif //ROGUE_FRAMEBUFFER_HPP
