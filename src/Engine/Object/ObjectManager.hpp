#ifndef OBJECTMANAGER_HPP
#define OBJECTMANAGER_HPP

/**
 * \file ObjectManager.hpp
 * \brief Simple object manager
 */

#include <Engine/Object/Object.hpp>
#include <vector>
#include <src/Core/Managers/Manager2List.hpp>
#include <Core/Macros.hpp>

struct Intersection{
    Object *obj;
    Element *elt;
};

/**
 * \class ObjectManager
 * \brief Manage object rendered
 *
 * ObjectManager is used as a list of Object to render
 */
class ObjectManager : public Manager2List<Object*>
{
public:
    ObjectManager();
    ~ObjectManager();

    void draw(DataForDrawing data);  ///< Draw all the object (Forward rendering)

    void setMeshTypes(GLenum t);                                ///< Set the mesh type of all the object in the list
    int addValue(Object *o, std::string name = "DEFAULT_NAME"); ///< Add an object to render, name is not necessary
    int addValue2(Object *o, std::string name = "DEFAULT_NAME"); ///< Add an object to render, name is not necessary
    void setNameLast(std::string name);                         ///< Set the name of the last object

    /* ********* AABB ******** */
    AABB getAABB();                                  ///< Get aabb of scene
    AABB getAABBObject(Object *o);                   ///< Get aab of object
    AABB getAABBElement(Element *e, Matrix4 model);  ///< Get aabb of Element

    /* ******* Ray intersection ******* */
    Object* rayIntersection(Ray &r);           ///< Return the object hit by the ray
    Element* rayIntersectionElement(Ray &r);   ///< Return the element hit by the ray
    Intersection rayIntersectionFull(Ray &r);  ///< Return the intersection (Object and element) hit by the ray

    void displayInfo();     ///< Display some info in the terminal
    void displayInfo2();    ///< Display some info in the terminal
};

#endif // OBJECTMANAGER_HPP
