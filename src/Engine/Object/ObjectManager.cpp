#include "ObjectManager.hpp"
#include <Core/Log/Log.hpp>
#include <Core/Math/GlmToEigen.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>

ObjectManager::ObjectManager(): Manager2List<Object*>("ObjectManager")
{
}

ObjectManager::~ObjectManager()
{
/*    for(auto i:listObject){
        delete i;
    }*/
}
/*
void ObjectManager::addObject(Object *object){
    listObject.push_back(object);
}

std::vector<Object*> ObjectManager::getListObject(){
    return listObject;
}*/

void ObjectManager::draw(DataForDrawing data){
    for(auto i:getValues()){
        i->draw(data);
    }
}

void ObjectManager::setMeshTypes(GLenum t){
    for(auto i:getValues()){
        i->setMeshTypes(t);
    }
}

int ObjectManager::addValue(Object *o, std::string name) {
    o->setName(name + std::to_string(getValues().size()+1));
    return Manager2List::addValue(o);
}

int ObjectManager::addValue2(Object *o, std::string name) {
    o->setName(name + std::to_string(getValues().size()+1));
    return Manager2List::addValue2(o);
}

void ObjectManager::setNameLast(std::string name) {
    if(getValues().size()>0){
        getLastValue()->setName(name + std::to_string(getValues().size()));
    }
}

/* ******************** AABB ******************* */

AABB ObjectManager::getAABB() {
    AABB aabb;
    //TODO improve ...
    Log(logWarning) << "Improve compute aabb";
    for(auto i: getValues()){
        Matrix4 model = i->getModel();
        for(auto j: i->getElements()){
            for(auto l: j->m_mesh.m_vertices){
                Vector4 vec = model *  Vector4(l.x,l.y,l.z,1);
                aabb.extend(GTE_VECTOR3(Vector3(vec.x, vec.y, vec.z)));
            }
        }
    }
    for(auto i: getValues2()){
        Matrix4 model = i->getModel();
        for(auto j: i->getElements()){
            for(auto l: j->m_mesh.m_vertices){
                Vector4 vec = model *  Vector4(l.x,l.y,l.z,1);
                aabb.extend(GTE_VECTOR3(Vector3(vec.x, vec.y, vec.z)));
            }
        }
    }
    return aabb;
}

AABB ObjectManager::getAABBObject(Object *o){
    AABB aabb;
    //TODO improve ...
    Matrix4 model = o->getModel();
    for(auto j: o->getElements()){
        for(auto l: j->m_mesh.m_vertices){
            Vector4 vec = model *  Vector4(l.x,l.y,l.z,1);
            aabb.extend(GTE_VECTOR3(Vector3(vec.x, vec.y, vec.z)));
        }
    }
    aabb.extend(GTE_VECTOR3(getPositionMat4(o->getModel())));
    return aabb;
}


AABB ObjectManager::getAABBElement(Element *e, Matrix4 model){
    AABB aabb;
    //TODO improve ...
    for(auto l: e->m_mesh.m_vertices){
        Vector4 vec = model *  Vector4(l.x,l.y,l.z,1);
        aabb.extend(GTE_VECTOR3(Vector3(vec.x, vec.y, vec.z)));
    }
    return aabb;
}


/* **************** Ray Intersection **************** */

Object* ObjectManager::rayIntersection(Ray &ray){
    float t = -1;
    Object *o = nullptr;
    for(auto i:getValues()){
        if(ray.intersectionObject(i,t)){
            o=i;
        }
    }
    for(auto i:getValues2()){
        if(ray.intersectionObject(i,t)){
            o=i;
        }
    }
    return o;

}

//TODO Verify if this is okay
Element* ObjectManager::rayIntersectionElement(Ray &ray){
    float t = -1;
    Element *o = nullptr;
    for(auto i:getValues()){
        for(auto j:i->getElements()){
            if(ray.intersectionElement(j,i->getModel(),t)){
                o=j;
            }
        }
    }
    for(auto i:getValues2()){
        for(auto j:i->getElements()){
            if(ray.intersectionElement(j,i->getModel(),t)){
                o=j;
            }
        }
    }
    return o;

}

Intersection ObjectManager::rayIntersectionFull(Ray &r) {
    float t=-1;
    Object *o = nullptr;
    Element *e = nullptr;
    Element *e2;
    for(auto i:getValues()){
        if((e2=r.intersectionObject2(i,t))){
            o=i;
            e=e2;
        }
    }
    for(auto i:getValues2()){
        if((e2=r.intersectionObject2(i,t))){
            o=i;
            e=e2;
        }
    }
    return {o,e};
}

void ObjectManager::displayInfo() {
    Log(logInfo) << "| - Object number (opaque): " << getValues().size();
    Log(logInfo) << "| - Object number (transparent): " << getValues2().size();
}

void ObjectManager::displayInfo2() {
    if(getValues().size()==0){
        Log(logInfo) << "|";
        Log(logInfo) << "| There are no opaque objects ...";
        Log(logInfo) << "|";
    }
    for(auto i:getValues()){
        i->displayInfo();
    }
    if(getValues2().size()==0){
        Log(logInfo) << "|";
        Log(logInfo) << "| There are no transparent objects ...";
        Log(logInfo) << "|";
    }
    for(auto i:getValues2()){
        i->displayInfo();
    }
}