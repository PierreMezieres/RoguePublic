#include <src/Core/Log/Log.hpp>
#include "Mesh.hpp"

Mesh::Mesh(VectorPoint3 vertices, VectorNormal normals, VectorPoint3 texCoord):
  m_vertices{vertices}, m_normals{normals}, m_texCoord{texCoord}
{
  toUpdate=true;
  m_vao=0;
  m_vbo = 0;
  m_nbo = 0;
  m_ebo = 0;
  m_tbo = 0;
}

void Mesh::setTriangle(Triangle t) {
  m_indices.push_back(t.x);
  m_indices.push_back(t.y);
  m_indices.push_back(t.z);
}

void Mesh::setLine(Line l) {
  m_indices.push_back(l.x);
  m_indices.push_back(l.y);
}


void Mesh::draw(){
  updateData();
  glBindVertexArray(m_vao);
  //Log(logInfo) << "Draw:" << m_vao;
  glDrawElements(mesh_types, casti(m_indices.size()*sizeof(uint)), GL_UNSIGNED_INT, nullptr);
  glBindVertexArray(0);
}

void Mesh::sendData(GLuint location, GLint n, GLuint *buffer, VectorPoint3 *data) {
  if(*buffer == 0){
      glGenBuffers(1,buffer);
      glBindBuffer(GL_ARRAY_BUFFER, *buffer);
      glVertexAttribPointer(location, n, GL_FLOAT, GL_FALSE, sizeof(Point3), static_cast<GLvoid *>(nullptr));
      glEnableVertexAttribArray(location);
    }

  glBindBuffer(GL_ARRAY_BUFFER, *buffer);
  glBufferData(GL_ARRAY_BUFFER, casti(data->size()*sizeof(Point3)), data->data(), GL_DYNAMIC_DRAW);


}

void Mesh::updateData(){
  if(toUpdate){
      // Initialize the geometry
      // 1. Generate geometry buffers
      if(m_vao == 0) glGenVertexArrays(1, &m_vao);

      glBindVertexArray(m_vao);
      sendData(0,3,&m_vbo,&m_vertices);
      sendData(1,3,&m_nbo,&m_normals);
      sendData(2,2,&m_tbo,&m_texCoord);



      if(m_ebo == 0){
          glGenBuffers(1, &m_ebo) ;
        }

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, casti(m_indices.size()*sizeof(uint)), m_indices.data(), GL_DYNAMIC_DRAW);


      glBindVertexArray(0);

      toUpdate=false;

    }
}

void Mesh::setUpdate() {
  toUpdate=true;
}

void Mesh::setMeshTypes(GLenum t) {
  mesh_types=t;
}

GLenum Mesh::getMeshTypes() {
  return mesh_types;
}

void Mesh::addLine(Point3 p1, Point3 p2){
  if(mesh_types != GL_LINES){
      Log(logWarning) << "We won't add a line to a mesh from other type than GL_POINT ! ";
      return;
    }
  m_vertices.push_back(p1);
  m_vertices.push_back(p2);
  m_normals.push_back(Normal(0,1,0));
  m_normals.push_back(Normal(0,1,0));
  m_texCoord.push_back(Point3(0,0,0));
  m_texCoord.push_back(Point3(0,0,0));
  m_indices.push_back(castui(m_vertices.size())-2);
  m_indices.push_back(castui(m_vertices.size())-1);

  toUpdate=true;
}

void Mesh::changeLastLine(Point3 p1, Point3 p2) {
  if(mesh_types != GL_LINES){
      Log(logWarning) << "We won't change a line to a mesh from other type than GL_LINES ! ";
      return;
    }
  unsigned int s = castui(m_vertices.size())-1;
  m_vertices[s-1] = p1;
  m_vertices[s] = p2;

  toUpdate=true;
}

void Mesh::autoNormals() {
  m_normals.resize(m_vertices.size(), Point3());


  for(unsigned long i=0;i<m_indices.size();i+=3){
      uint s1 = m_indices[i];
      uint s2 = m_indices[i+1];
      uint s3 = m_indices[i+2];
      Vector3 v1 = m_vertices[s1]-m_vertices[s2];
      Vector3 v2 = m_vertices[s1]-m_vertices[s3];
      Vector3 n = glm::cross(v2,v1);

      m_normals[s1] += n;
      m_normals[s2] += n;
      m_normals[s3] += n;

    }
  for(unsigned long i=0;i< m_normals.size(); i++){
      m_normals[i] = glm::normalize(m_normals[i]);
    }
  setUpdate();
}

void Mesh::invNormals() {
  for(unsigned long i=0;i<m_normals.size(); i++){
      Vector3 n = m_normals[i];
      m_normals[i] = Vector3(-n.x,-n.y,-n.z);
    }
  setUpdate();
}


MeshOM Mesh::toMeshOM() {

  if(mesh_types != GL_TRIANGLES){
      Log(logError) << "Convert mesh on openMesh format is not authorized with type different of triangles";
    }
  assert(mesh_types == GL_TRIANGLES);

  MeshOM mesh;
  std::vector<MeshOM::VertexHandle> vhandle;

  for(auto i : m_vertices) {
      vhandle.push_back(mesh.add_vertex(MeshOM::Point(i.x, i.y, i.z)));
    }

  std::vector<MeshOM::VertexHandle>  face_vhandles;
  for(unsigned long i = 0; i< m_indices.size(); i+=3){
      face_vhandles.clear();
      face_vhandles.push_back(vhandle[m_indices[i]]);
      face_vhandles.push_back(vhandle[m_indices[i+1]]);
      face_vhandles.push_back(vhandle[m_indices[i+2]]);
      mesh.add_face(face_vhandles);
    }


  // write mesh to output.obj
  try
  {
    if ( !OpenMesh::IO::write_mesh(mesh, "output.off") )
      {
        std::cerr << "Cannot write mesh to file 'output.off'" << std::endl;
      }
  }
  catch( std::exception& x )
  {
    std::cerr << x.what() << std::endl;
  }



  return mesh;
}

void Mesh::fromMeshOM(MeshOM mesh){

  m_vertices.clear();
  m_indices.clear();
  m_normals.clear();
  m_texCoord.clear();

  MeshOM::VertexIter v_it, v_end(mesh.vertices_end());
  MeshOM::FaceIter f_it, f_end(mesh.faces_end());
  MeshOM::Point point;

  for(v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it){
      point = mesh.point(*v_it);
      float *d = point.data();
      m_vertices.push_back(Point3(d[0],d[1],d[2]));
      m_texCoord.push_back(Vector3(0,0,0));
    }


  for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it){
      for(MeshOM::FaceVertexIter fvi = mesh.fv_iter(*f_it); fvi.is_valid(); ++fvi){
          m_indices.push_back(castui(fvi->idx()));
        }

    }



  autoNormals();
  setUpdate();
}
