#ifndef ELEMENT_HPP
#define ELEMENT_HPP

#include <src/Engine/Camera/Camera.hpp>
#include <src/Engine/Light/LightManager.hpp>
#include <Engine/Material/MaterialManager.hpp>
#include <src/Engine/Shader/ShaderManager.hpp>
#include "Mesh.hpp"

/**
 * \file Element.hpp
 * \brief Element to draw
 */

 /**
  * \struct DataForDrawing
  * \brief Usefull data to draw objects
  *
  */
struct DataForDrawing{
    LightManager *lightManager;
    ShaderManager *shaderManager;
    MaterialManager *materialManager;
    Camera *camera;

    Texture *skybox;
    unsigned int ssao;
    bool ssao_on;
    Matrix4 lightSpaceMatrix;
};


/**
 * \class Element
 * \brief Element to draw
 */
class Element
{
public:
    Element(Mesh mesh = Mesh());

    void draw(DataForDrawing data, Matrix4 model);
    void updateData();                                 ///< Update data of mesh

    void setMaterial(std::string nameMaterial);        ///< Set the material name used

    Mesh m_mesh;
    void setMeshTypes(GLenum t);                       ///< Set mesh type for the mesh

    std::string m_materialName = "default";            ///< Material name used

    void displayInfo();                                ///< Display information on element in terminal
};

#endif // ELEMENT_HPP
