#ifndef MESH_HPP
#define MESH_HPP

/**
 * \file Mesh.hpp
 * \brief Util data for mesh
 */

#include "src/Core/Macros.hpp"
#include "../Anim/Bone.hpp"

typedef std::vector<std::vector<std::pair<int,float>>> BoneWeight;
typedef std::vector<Bone*> BoneList;

class Mesh
{
public:
    Mesh(VectorPoint3 vertices = VectorPoint3(),
         VectorNormal normals = VectorNormal(),
                 VectorPoint3 texCoord = VectorPoint3());

    void draw();                             ///< Draw the mesh

    void setTriangle(Triangle t);            ///< Add the indices to create a triangle
    void setLine(Line l);                    ///< Add the indices to create a line
    void addLine(Point3 p1, Point3 p2);      ///< Add the vertices and indices to create a line
    void changeLastLine(Point3 p1, Point3 p2);
    void autoNormals();                      ///< Normals are calculated automatically
    void invNormals();                       ///< Invert normals

    VectorPoint3 m_vertices;                 ///< Vertices vector
    VectorNormal m_normals;                  ///< Normals vector
    VectorUInt m_indices;                    ///< Indices vector for Faces, Lines ...
    VectorPoint3 m_texCoord;                 ///< Texture coordinates vector

    /* Animation data */
    BoneWeight boneWeight;
    BoneList boneList;
    VectorPoint3 m_vertices_init; // TODO Better solution
    /* ************** */


    void setMeshTypes(GLenum t);             ///< Change the mesh types to draw
    GLenum getMeshTypes();                   ///< Get the mesh type used
    void setUpdate();                        ///< During the next draw, data will be updated
    void updateData();                       ///< Force update of data

    MeshOM toMeshOM();                       ///< Return the OpenMesh mesh version
    void fromMeshOM(MeshOM mesh);            ///< Construct the mesh with the OpenMesh version

private:

    bool toUpdate;

    GLuint m_vao;
    GLuint m_vbo; // Vertices
    GLuint m_nbo; // Normals
    GLuint m_ebo; // Edges
    GLuint m_tbo; // Texture coords

    void sendData(GLuint location, GLint n, GLuint *buffer, VectorPoint3* data);

    GLenum mesh_types = GL_TRIANGLES;

};

#endif // MESH_HPP
