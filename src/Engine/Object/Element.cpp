#include "Element.hpp"

Element::Element(Mesh mesh ) : m_mesh{mesh}
{

}

void Element::updateData(){
    m_mesh.updateData();
}

void Element::draw(DataForDrawing data, Matrix4 model) {

    Material *material = data.materialManager->getValue(m_materialName);

    Shader *shader = data.shaderManager->getValue(material->getNameShader());

    shader->use();

    glActiveTexture(GL_TEXTURE3);
    shader->setInt("texturessao",3);
    glBindTexture(GL_TEXTURE_2D,data.ssao);
    shader->setBool("ssao_on",data.ssao_on);
    //TODO Remove this if unecessary
/*
    glActiveTexture(GL_TEXTURE4);
    shader->setInt("shadowMap", 4);
    glBindTexture(GL_TEXTURE_2D, data.shadowMap);
    shader->setMat4("lightSpaceMatrix", data.lightSpaceMatrix);
*/
    shader->setMat4("model",model);
    shader->setMat4("proj",data.camera->GetProjMatrix());
    shader->setMat4("view",data.camera->GetViewMatrix());
    shader->setVec3("viewPos",data.camera->m_position);


    data.lightManager->bind(shader);
    material->bind(shader, data);


    m_mesh.draw();
}

void Element::setMeshTypes(GLenum t) {
    m_mesh.setMeshTypes(t);
}

void Element::setMaterial(std::string nameMaterial){
    m_materialName=nameMaterial;
}


void Element::displayInfo(){
    Log(logInfo) << "| - Material :" << m_materialName;
}
