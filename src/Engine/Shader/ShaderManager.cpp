#include "ShaderManager.hpp"

ShaderManager::ShaderManager() : ManagerMap<Shader*>("ShaderManager")
{

}

void ShaderManager::displayInfo() {
    Log(logInfo) << "| - Shader number: " << getValues().size();
}

void ShaderManager::reloadShaders(){
    Log(logInfo) << "";
    Log(logInfo) << "|-------- Reload shaders ---------";
    for(auto s:getValues()){
        Log(logInfo) << "| - Reload : " << s.first;
        s.second->reload();
    }
    Log(logInfo) << "|---------------------------------";
    Log(logInfo) << "";
}

/*
void ShaderManager::addShader(std::string name, Shader *shader){
    mapShader[name]=shader;
}

Shader * ShaderManager::getShader(std::string name){
    return mapShader[name];
}
*/
