#ifndef SHADER_HPP
#define SHADER_HPP

/**
 * \file Shader.hpp
 * \brief File to manage shader
 */

#include <Core/Macros.hpp>

/**
 * \class Shader
 * \brief Class to manage a shader
 */
class Shader{
public:
    enum Shader_type{VF = 0, VGF = 1};

    /**
     * @param vertexFile file path name to vertex Shader
     * @param fragmentFile file path name to fragment Shader
     * @param geometryFile file path name to geometry Shader
     */
    Shader(std::string vertexFile, std::string fragmentFile, std::string geometryFile = "DO.NOT.EXIST");


    void reload();

    void use(); ///< glUseProgram ...

    void loadDefaultShader(int num); ///< \deprecated will probably be no longer usable soon ...
    GLuint getProgram();


    void setBool(const std::string &name, const bool value) const;
    void setInt(const std::string &name, const int value) const;
    void setFloat(const std::string &name, const float value) const;
    void setVec2(const std::string &name, const Vector2 &vec) const;
    void setVec3(const std::string &name, const Vector3 &vec) const;
    void setVec4(const std::string &name, const Vector4 &vec) const;
    void setMat2(const std::string &name, const Matrix2 &mat) const;
    void setMat3(const std::string &name, const Matrix3 &mat) const;
    void setMat4(const std::string &name, const Matrix4 &mat) const;

private:
    GLuint m_program;
    Shader_type m_type;

    std::string nameVertex;
    std::string nameFragment;
    std::string nameGeometry;

    void preLoad();
    void loadShader(const char* vertexshader_source, const char* fragmentshader_source, const char* geometryshader_source);

    std::string preprocessIncludes(std::string& shader,std::string path);
};


#endif // SHADER_HPP
