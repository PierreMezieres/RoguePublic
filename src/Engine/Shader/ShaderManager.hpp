#ifndef SHADERMANAGER_HPP
#define SHADERMANAGER_HPP

/**
 * \file ShaderManager.hpp
 * \brief Simple shader manager
 */

#include <Engine/Shader/Shader.hpp>
#include <Core/Managers/ManagerMap.hpp>
#include <vector>
#include <map>
#include <string>


/**
 * \class ShaderManager
 * \brief Simple manager for shaders
 */
class ShaderManager: public ManagerMap<Shader*>
{
public:
    ShaderManager();

    void reloadShaders();

    void displayInfo(); ///< Display information about shader manger in terminal

};

#endif // SHADERMANAGER_HPP
