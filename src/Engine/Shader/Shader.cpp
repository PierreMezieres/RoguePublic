#include "Shader.hpp"
#include <Core/Util/FileReader.hpp>
#include <Core/Log/Log.hpp>
#include <Core/Util/StringUtils.hpp>

#include <iostream>
#include <regex>
#include <src/Core/Util/FileUtil.hpp>

//TODO improve default ...
std::string defaultVS ="#version 410 core\n\
        layout (location = 0) in vec3 position;\n\
        layout (location = 1) in vec3 inormal;\n\
        out vec3 normal;\n\
        void main()\n\
        {\n\
            normal=inormal;\n\
            gl_Position = vec4(position.x, position.y, position.z, 1.0);\n\
        }\n";

std::string defaultFS ="#version 410 core\n\
        in vec3 normal;\n\
        out vec4 color;\n\
        void main()\n\
        {\n\
            //color = vec4(vec3(clamp(dot(normalize(normal), vec3(0,0,1)), 0, 1)), 1.0);\n\
            color = vec4(0.5, 0.5, 0.5, 1.0);\n\
        }\n";


Shader::Shader(std::string vertexFile, std::string fragmentFile, std::string geometryFile) {
    nameVertex = vertexFile;
    nameFragment = fragmentFile;
    nameGeometry = geometryFile;

    if (nameGeometry == "DO.NOT.EXIST")
        m_type = VF;
    else
        m_type = VGF;

    preLoad();
}

void Shader::loadShader(const char* vertexshader_source, const char* fragmentshader_source, const char* geometryshader_source) {
    GLint success;
    GLchar infoLog[512]; // warning fixed size ... request for LOG_LENGTH!!!
    GLuint fragmentshader, vertexshader, geometryshader;

    // 1. Generate the shader VERTEX
    vertexshader = glCreateShader(GL_VERTEX_SHADER);
    // 2. set the source
    glShaderSource(vertexshader, 1, &vertexshader_source, nullptr);
    // 3. Compile
    glCompileShader(vertexshader);
    // 4. test for compile error
    glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertexshader, 512, NULL, infoLog);
        Log(logError) << "ERROR::SHADER::VERTEX::COMPILATION_FAILED : " << nameVertex << "\n" << infoLog;
    }

    //FRAGMENT
    fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentshader, 1, &fragmentshader_source, nullptr);
    glCompileShader(fragmentshader);
    glGetShaderiv(fragmentshader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragmentshader, 512, NULL, infoLog);
        Log(logError) << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED : " << nameFragment << "\n" << infoLog;
    }

    if (m_type == VGF) {
        //GEOMETRY
        geometryshader = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geometryshader, 1, &geometryshader_source, nullptr);
        glCompileShader(geometryshader);
        glGetShaderiv(geometryshader, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(geometryshader, 512, NULL, infoLog);
            Log(logError) << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED : " << nameGeometry << "\n" << infoLog;
        }
    }

    // 1. Generate the program
    m_program = glCreateProgram();
    // 2. Attach the shaders to the program
    glAttachShader(m_program, vertexshader);
    if (m_type == VGF)
        glAttachShader(m_program, geometryshader);
    glAttachShader(m_program, fragmentshader);
    // 3. Link the program
    glLinkProgram(m_program);
    // 4. Test for link errors
    glGetProgramiv(m_program, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(m_program, 512, nullptr, infoLog);
        Log(logError) << "ERROR::SHADER::LINK_FAILED\n" << infoLog;

    }
    glDeleteShader(fragmentshader);
    glDeleteShader(vertexshader);
    if (m_type == VGF)
        glDeleteShader(geometryshader);

}

void Shader::reload() {
    glDeleteProgram(m_program);
    Log(logInfo) << "|   " << nameVertex;
    Log(logInfo) << "|   " << nameFragment;
    if (m_type == VGF)
        Log(logInfo) << "|   " << nameGeometry;
    preLoad();
}


void Shader::preLoad() {
    std::string vs = FileReader::getFileWithString(nameVertex);
    std::string fs = FileReader::getFileWithString(nameFragment);

    if(vs.empty()) {
        Log(logWarning) << "Error open: " << nameVertex;
        vs = defaultVS;
    }
    if(fs.empty()) {
        Log(logWarning) << "Error open: " << nameFragment;
        fs = defaultFS;
    }



    vs = preprocessIncludes(vs,nameVertex);
    fs = preprocessIncludes(fs,nameFragment);

    std::string gs = "";
    if (m_type == VGF) {
        gs = FileReader::getFileWithString(nameGeometry);
        if (gs.empty()) {
            Log(logWarning) << "Error open: " << nameGeometry;
            gs = defaultVS;
        }
        gs = preprocessIncludes(gs, nameGeometry);
    }

    loadShader(vs.c_str(),fs.c_str(), gs.c_str());
}


void Shader::use(){
    glUseProgram(m_program);
}


void Shader::loadDefaultShader(int){
    //TODO Add shader with file and remove this
    //loadShader(vertexshader_source,fragmentshadernormal_source);
}

void Shader::setBool(const std::string &name, const bool value) const{
    glUniform1i(glGetUniformLocation(m_program, name.c_str()), casti(value));
}

void Shader::setInt(const std::string &name, const int value) const{
    glUniform1i(glGetUniformLocation(m_program, name.c_str()), value);
}

void Shader::setFloat(const std::string &name, const float value) const{
    glUniform1f(glGetUniformLocation(m_program, name.c_str()), value);
}

void Shader::setVec2(const std::string &name, const Vector2 &vec) const{
    glUniform2fv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setVec3(const std::string &name, const Vector3 &vec) const{
    glUniform3fv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setVec4(const std::string &name, const Vector4 &vec) const{
    glUniform4fv(glGetUniformLocation(m_program, name.c_str()), 1, &vec[0]);
}

void Shader::setMat2(const std::string &name, const Matrix2 &mat) const{
    glUniformMatrix2fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat3(const std::string &name, const Matrix3 &mat) const{
    glUniformMatrix3fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat4(const std::string &name, const Matrix4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(m_program, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

std::string Shader::preprocessIncludes(std::string& shader, std::string path)
{

    std::string result = "";
    std::vector<std::string> finalStrings;
    auto shaderLines = StringUtils::splitString(shader, '\n');
    finalStrings.reserve(shaderLines.size());
    path = path.substr(0, path.find_last_of('/'));

    static const std::regex reg("^[ ]*#[ ]*include[ ]+[\"<](.*)[\">].*");

    for (const auto& l : shaderLines)
    {
        std::string line = l;
        std::smatch match;
        if (std::regex_search(l, match, reg))
        {
            std::string new_path = path + '/' + match[1].str();
            std::string includeShader = FileReader::getFileWithString(FileUtil::AbsolutePath(new_path));

            if (includeShader.empty() )
            {
                Log(logError) << "Cannot open included file " <<  match[1].str();

            } else {
                line = preprocessIncludes(includeShader,new_path);

            }
        }

        finalStrings.push_back(line);
    }

    // Build final shader string
    for (const auto& l : finalStrings)
    {
        result.append(l);
        result.append("\n");
    }

    result.append("\0");


    return result;
}


GLuint Shader::getProgram() {
    return m_program;
}