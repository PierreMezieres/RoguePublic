//
// Created by pierre on 28/08/18.
//

#ifndef ROGUE_COMPUTEEACHDRAW_HPP
#define ROGUE_COMPUTEEACHDRAW_HPP

/**
 * \file ComputeEachDraw.hpp
 * \brief Simple mother class to compute something on each draw
 */


class ComputeEachDraw {
public:
    std::string id_computeEachDraw = "NO ID GIVER COMPUTEEACHDRAW";

    virtual ~ComputeEachDraw() {}

    virtual void computeEachDraw() = 0;
    virtual void cleanUp() = 0;

};


#endif //ROGUE_COMPUTEEACHDRAW_HPP
