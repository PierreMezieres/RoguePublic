#include "Renderer.hpp"
#include <Engine/Camera/EulerCamera.hpp>
#include <Core/Macros.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>
#include <random>
#include <src/Core/Math/Math.hpp>

Renderer::Renderer(std::string id): m_id{id}
{
    screenObj = new Object(createSimpleRec());
}

void Renderer::toggleDrawMode(){
    m_drawfill=!m_drawfill;
}

void Renderer::drawScreen() {
    screenObj->getMesh().draw();
}

bool Renderer::getDrawMode() {
    return m_drawfill;
}

std::string Renderer::getID() {
    return m_id;
}