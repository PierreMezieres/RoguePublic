//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_DEFAULTRENDERER_HPP
#define ROGUE_DEFAULTRENDERER_HPP


#include "Renderer.hpp"

class DefaultRenderer : public Renderer{
public:
    DefaultRenderer();
    virtual ~DefaultRenderer(){}

    void draw(DrawData &drawData);

    void resize(int w, int h);

    std::string changeShaderPost();
    std::string changeSkybox();
    void changeBufferFinal(int i);

    /* SSAO */
    std::string toggleSSAO();
    void changeRadiusSSAO(float radius);
    void changeBiasSSAO(float bias);

    /* FXAA */
    bool toggleFXAA();
    bool toggleFXAADebug();
    void changeMinContrastFXAA(float min);
    void changeMinThresholdFXAA(float t);

    /* HDR / BLOOM */
    void changeGammaHDR(float gamma);
    void changeExposureHDR(float exposure);
    bool toggleHDR();
    void changeAmountBloom(unsigned int amount);
    bool toggleBLOOM();


private:

    PostProcess postProcess;
    Skybox skybox;

    unsigned int idBufferFinal;


    Forward forward;
    Prepass prepass;
    Ssao ssao;
    Fxaa fxaa;
    Hdr hdr;
    Bloom bloom;

};


#endif //ROGUE_DEFAULTRENDERER_HPP
