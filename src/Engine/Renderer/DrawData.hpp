#ifndef DRAWDATA_HPP
#define DRAWDATA_HPP

/**
 * \file DrawData.hpp
 * \brief Usefull data to draw data
 */

#include <Engine/Camera/Camera.hpp>
#include <Engine/Shader/ShaderManager.hpp>
#include <Engine/Camera/CameraManager.hpp>
#include <src/Engine/Light/LightManager.hpp>
#include <src/Engine/Object/ObjectManager.hpp>
#include <Engine/Material/MaterialManager.hpp>
class FileLoaderManager;

/**
 * \class DrawData
 * \brief Contains all the data usefull to draw/render
 */
class DrawData
{
public:
    DrawData();

    CameraManager cameraManager;
    Camera *camera;                            ///< Current used camera

    ShaderManager shaderManager;
    LightManager lightManager;
    ObjectManager objectManager;
    MaterialManager materialManager;

    FileLoaderManager *fileLoaderManager;

    DataForDrawing getDataForDrawing();         ///< Get data usefull for a draw

    void cleanScene();                          ///< Remove the objects and lights of the scene

    void changeCamera(int i);                   ///< Change the current camera by i-th camera in camera manager
    Object *rayCast(GLfloat x, GLfloat y);      ///< Return the object hit by a ray on the pixel (x,y)

    void displayInfo();                         ///< Display information about drawData in terminal
private:

    void initShader();
    void initMaterial();
    void initData();
    
};

#endif // DRAWDATA_HPP
