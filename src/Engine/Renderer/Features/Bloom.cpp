//
// Created by pierre on 23/11/18.
//

#include <src/Core/Shapes/TriangleMesh.hpp>
#include "Bloom.hpp"

Bloom::Bloom() {

    shader = new Shader("../Shaders/Other/bloom.vs.glsl", "../Shaders/Other/bloom.fs.glsl");

    shader->use();
    shader->setInt("screenTexture",0);
    shaderBlur = new Shader("../Shaders/PostProcess/framebuffer_screen.vs.glsl","../Shaders/PostProcess/gaussian_blur_1pass.fs.glsl");
    shaderBlur->use();
    shaderBlur->setInt("screenTexture",0);
    amountBloom = 5;
    tog = true;
    horizontal = true;

    screenObj = new Object(createSimpleRec());
}

void Bloom::use(int texture){
    horizontal = true;
    if(tog){
        buffer.bind();
        glClearColor(1,1,1, 1.0f);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT );

        shader->use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, castui(texture));
        screenObj->getMesh().draw();

        bool first_iteration = true;
        shaderBlur->use();
        for(unsigned int i = 0; i < amountBloom; i++) {
            pingpongBuffers[horizontal].bind();
            shaderBlur->setBool("horizontal", horizontal);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D,
                          first_iteration ? buffer.texture : pingpongBuffers[!horizontal].texture);
            screenObj->getMesh().draw();
            horizontal = !horizontal;
            if (first_iteration) first_iteration = false;
        }
    }
}

void Bloom::resize(int w, int h) {
    buffer.resize(w,h);
    pingpongBuffers[0].resize(w,h);
    pingpongBuffers[1].resize(w,h);
}

int Bloom::getTexture() {
    if(amountBloom){
        return casti(pingpongBuffers[!horizontal].texture);
    }else{
        return casti(buffer.texture);
    }
}

bool Bloom::getActive() {
    return tog;
}

bool Bloom::toggle() {
    tog = !tog;
    return tog;
}

void Bloom::setAmountBloom(unsigned int a) {
    amountBloom = a;
}
