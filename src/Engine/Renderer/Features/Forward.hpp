//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_FORWARD_HPP
#define ROGUE_FORWARD_HPP


#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Engine/Texture/FrameBuffer.hpp>

class Forward {
public:
    Forward();

    void use(DrawData &drawData, Texture* skybox, int texSsao, bool ssaoActivate, bool drawfill = true);

    void resize(int w, int h);

    int getTexture();

private:

    FrameBuffer frameBuffer;

};


#endif //ROGUE_FORWARD_HPP
