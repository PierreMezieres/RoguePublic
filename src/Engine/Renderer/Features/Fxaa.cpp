//
// Created by pierre on 23/11/18.
//

#include <src/Core/Shapes/TriangleMesh.hpp>
#include "Fxaa.hpp"

Fxaa::Fxaa(){

    shader = new Shader("../Shaders/Other/fxaa.vs.glsl","../Shaders/Other/fxaa.fs.glsl");

    shader->use();
    shader->setInt("screenTexture",0);

    debug = false;
    tog = true;
    minContrast = 0.06f;
    minThrreshold = 0.03f;
    screenObj = new Object(createSimpleRec());
}

void Fxaa::use(Camera *camera, int texture){
    buffer.bind();
    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    shader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, castui(texture));
    shader->setBool("fxaaON",tog);
    shader->setBool("debug",debug);
    Vector2 step(1./camera->m_width,1./camera->m_height);
    shader->setVec2("texelStep",step);
    shader->setFloat("lumaThreshold",minThrreshold);
    shader->setFloat("lumaContrastMin",minContrast);
    screenObj->getMesh().draw();
}

void Fxaa::resize(int w, int h) {
    buffer.resize(w,h);
}

bool Fxaa::toggleDebug(){
    debug = !debug;
    return debug;
}

bool Fxaa::toggle() {
    tog = !tog;
    return tog;
}

void Fxaa::setMinContrast(float min){
    minContrast = min;
}

void Fxaa::setMinThreshold(float min){
    minThrreshold = min;
}

int Fxaa::getTexture() {
    return casti(buffer.texture);
}
