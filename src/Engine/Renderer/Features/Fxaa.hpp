//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_FXAA_HPP
#define ROGUE_FXAA_HPP


#include <src/Engine/Texture/FrameBuffer.hpp>
#include <src/Engine/Shader/Shader.hpp>
#include <src/Engine/Object/Object.hpp>

class Fxaa {
public:
    Fxaa();

    void use(Camera *camera, int texture);

    bool toggleDebug();
    bool toggle();

    void resize(int w, int h);

    void setMinContrast(float min);
    void setMinThreshold(float min);

    int getTexture();

private:

    FrameBuffer buffer;
    Shader *shader;
    bool debug;
    bool tog;
    float minContrast;
    float minThrreshold;

    Object *screenObj;
};


#endif //ROGUE_FXAA_HPP
