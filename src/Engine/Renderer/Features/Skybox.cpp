//
// Created by pierre on 23/11/18.
//

#include <src/Core/Shapes/TriangleMesh.hpp>
#include "Skybox.hpp"
Skybox::Skybox() {

    shader = new Shader("../Shaders/Other/skybox.vs.glsl","../Shaders/Other/skybox.fs.glsl");
    skyboxTextures.push_back({"no skybox", nullptr});
    skyboxTextures.push_back({"Ocean", new Texture("../Assets/Textures/Skybox/ocean", {"right.jpg","left.jpg","top.jpg","bottom.jpg","front.jpg","back.jpg"})});
    skyboxTextures.push_back({"Nebula", new Texture("../Assets/Textures/Skybox/ame_nebula", {"rt.tga","lf.tga","up.tga","dn.tga", "ft.tga", "bk.tga"})});
    skyboxTextures.push_back({"City", new Texture("../Assets/Textures/Skybox/hw_city", {"rt.jpg","lf.jpg","up.jpg","dn.jpg", "ft.jpg", "bk.jpg"})});


    cubemapTexture = nullptr;
    skyboxObj = new Object(createSimpleCube());

    shader->use();
    shader->setInt("skybox",0);

}

void Skybox::use(Camera *camera) {
    if(cubemapTexture != nullptr){
        glDepthFunc(GL_LEQUAL);
        shader->use();
        cubemapTexture->bindCubemap(GL_TEXTURE0);
        shader->setMat4("proj",camera->GetProjMatrix());
        shader->setMat4("view",Matrix4(Matrix3(camera->GetViewMatrix())));
        skyboxObj->getMesh().draw();
        glDepthFunc(GL_LESS);

    }
}

Texture* Skybox::getTexture() {
    return cubemapTexture;
}

std::string Skybox::changeSkybox() {
    numSkyBoxPP = (numSkyBoxPP+1)%skyboxTextures.size();
    auto p = skyboxTextures[numSkyBoxPP];
    cubemapTexture = p.second;
    return p.first;
}