//
// Created by pierre on 20/02/19.
//

#ifndef ROGUE_POSTPROCESSGAMMA_HPP
#define ROGUE_POSTPROCESSGAMMA_HPP


#include <Engine/Object/Object.hpp>
#include <Engine/Shader/Shader.hpp>

class PostProcessGamma {
public:
    PostProcessGamma();

    void use(int fb_display,int texture);

    Object *screenObj;
    Shader *screenShader;

};

#endif //ROGUE_POSTPROCESSGAMMA_HPP
