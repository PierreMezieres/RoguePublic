//
// Created by pierre on 23/11/18.
//

#include <src/Core/Shapes/TriangleMesh.hpp>
#include "PostProcess.hpp"

PostProcess::PostProcess() {

    std::string dir = "../Shaders/PostProcess/";
    shadersPostProcess.push_back({"default", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"framebuffer_screen.fs.glsl")});
    shadersPostProcess.push_back({"blur", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_blur.fs.glsl")});
    shadersPostProcess.push_back({"inversion", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_inversion.fs.glsl")});
    shadersPostProcess.push_back({"grayscale", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_grayscale.fs.glsl")});
    shadersPostProcess.push_back({"edgeDetection", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_edgeDetection.fs.glsl")});
    screenShader = shadersPostProcess[0].second;
    screenObj = new Object(createSimpleRec());

    screenShader->use();
    screenShader->setInt("screenTexture",0);
}

void PostProcess::use(int fb_display, int texture) {

    glBindFramebuffer(GL_FRAMEBUFFER, castui(fb_display));

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    screenShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, castui(texture));
    screenObj->getMesh().draw();
}

std::string PostProcess::changeShader() {
    numShaderPP = (numShaderPP+1)%shadersPostProcess.size();
    auto p = shadersPostProcess[numShaderPP];
    screenShader = p.second;
    return p.first;
}
