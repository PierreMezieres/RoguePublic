//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_PREPASS_HPP
#define ROGUE_PREPASS_HPP


#include <src/Engine/Texture/GBuffer.hpp>
#include <src/Engine/Shader/Shader.hpp>

class Prepass {
public:
    Prepass();

    void use(DrawData &drawData, bool drawfill = true);

    void resize(int w, int h);

    int getPositionTexture();
    int getNormalTexture();
private:

    Shader *shader;
    GBuffer gBuffer;

};


#endif //ROGUE_PREPASS_HPP
