//
// Created by pierre on 23/11/18.
//

#include "Forward.hpp"

Forward::Forward() {

}

void Forward::use(DrawData &drawData, Texture* skybox, int texSsao, bool ssaoActivate, bool drawfill) {

    frameBuffer.bind();

    DataForDrawing dfd = drawData.getDataForDrawing();

    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    if (drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


    dfd.skybox = skybox;
    dfd.ssao =  castui(texSsao);
    dfd.ssao_on = ssaoActivate;

    drawData.objectManager.draw(dfd);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Forward::resize(int w, int h) {
    frameBuffer.resize(w,h);
}

int Forward::getTexture() {
    return casti(frameBuffer.texture);
}
