//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_SKYBOX_HPP
#define ROGUE_SKYBOX_HPP

#include <Core/Macros.hpp>
#include <src/Engine/Texture/Texture.hpp>
#include <src/Engine/Shader/Shader.hpp>
#include <src/Engine/Object/Object.hpp>

class Skybox {
public:
    Skybox();

    void use(Camera *camera);

    std::string changeSkybox();

    Texture *getTexture();

    std::vector<std::pair<std::string,Texture *>> skyboxTextures;
    unsigned long numSkyBoxPP = 0;

private:

    Texture *cubemapTexture;
    Object *skyboxObj;
    Shader *shader;
};


#endif //ROGUE_SKYBOX_HPP
