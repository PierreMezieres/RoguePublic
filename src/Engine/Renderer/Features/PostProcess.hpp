//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_POSTPROCESS_HPP
#define ROGUE_POSTPROCESS_HPP

#include <Core/Macros.hpp>
#include <src/Engine/Shader/Shader.hpp>
#include <src/Engine/Object/Object.hpp>


class PostProcess {
public:
    PostProcess();

    void use(int fb_display,int texture);

    std::string changeShader();


    std::vector<std::pair<std::string,Shader *>> shadersPostProcess;
    unsigned int numShaderPP = 0;

    Object *screenObj;
    Shader *screenShader;

};


#endif //ROGUE_POSTPROCESS_HPP
