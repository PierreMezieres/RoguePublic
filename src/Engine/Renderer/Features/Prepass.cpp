//
// Created by pierre on 23/11/18.
//

#include <src/Engine/Renderer/DrawData.hpp>
#include "Prepass.hpp"

Prepass::Prepass() {

    shader = new Shader("../Shaders/Other/prepass.vs.glsl","../Shaders/Other/prepass.fs.glsl");
}

void Prepass::use(DrawData &drawData, bool drawfill) {
    if (drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    gBuffer.bind();
    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    shader->use();

    shader->setMat4("proj",drawData.camera->GetProjMatrix());
    shader->setMat4("view",drawData.camera->GetViewMatrix());

    for(auto obj : drawData.objectManager.getValues()){
        shader->setMat4("model",obj->getModel());
        for (auto e : obj->getElements()) {
            e->m_mesh.draw();
        }
    }
}

void Prepass::resize(int w, int h) {
    gBuffer.resize(w,h);
}

int Prepass::getNormalTexture() {
    return casti(gBuffer.gNormal);
}

int Prepass::getPositionTexture() {
    return casti(gBuffer.gPosition);
}
