//
// Created by pierre on 23/11/18.
//

#include <src/Core/Shapes/TriangleMesh.hpp>
#include "Hdr.hpp"

Hdr::Hdr() {

    shader = new Shader("../Shaders/Other/hdr.vs.glsl","../Shaders/Other/hdr.fs.glsl");

    shader->use();
    shader->setInt("screenTexture",0);
    shader->setInt("brightTexture",1);

    gamma = 1;
    exposure = 1;
    togHDR = true;
    screenObj = new Object(createSimpleRec());
}

void Hdr::use(int texture, bool bloomActive, int texBloom) {

    buffer.bind();
    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    shader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, castui(texture));
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, castui(texBloom));
    shader->setFloat("gamma",gamma);
    shader->setFloat("exposure",exposure);
    shader->setBool("togHDR",togHDR);
    shader->setBool("togBLOOM",bloomActive);
    screenObj->getMesh().draw();
}

int Hdr::getTexture() {
    return casti(buffer.texture);
}

void Hdr::resize(int w, int h) {
    buffer.resize(w,h);
}

void Hdr::setGamma(float g) {
    gamma = g;
}

void Hdr::setExposure(float e) {
    exposure = e;
}

bool Hdr::toggle() {
    togHDR = ! togHDR;
    return togHDR;
}
