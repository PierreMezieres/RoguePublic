//
// Created by pierre on 23/11/18.
//

#include <random>
#include <src/Core/Math/Math.hpp>
#include <src/Engine/Camera/Camera.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>
#include "Ssao.hpp"

Ssao::Ssao() {
    std::uniform_real_distribution<GLfloat> randomFloats(0.0,1.0);
    std::default_random_engine  generator;
    for(unsigned int i = 0; i<64; ++i){
        Vector3 sample(randomFloats(generator) * 2.0f - 1.0f, randomFloats(generator) * 2.0f - 1.0f, randomFloats(generator));
        sample = glm::normalize(sample);
        sample *= randomFloats(generator);
        float scale = float(i) / 64.0f;
        scale = lerp(0.1f, 1.0f, scale*scale);
        sample *= scale;
        kernel.push_back(sample);
    }

    for(unsigned int i=0; i<16; i++){
        Vector3 n(randomFloats(generator) *2.0f - 1.0f, randomFloats(generator) * 2.0f -1.0f, 0.0f);
        noise.push_back(n);
    }
    glGenTextures(1, & noiseTexture);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, & noise[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    shader = new Shader("../Shaders/Other/ssao.vs.glsl","../Shaders/Other/ssao.fs.glsl");
    shaderBlur = new Shader("../Shaders/Other/ssao.vs.glsl","../Shaders/Other/ssao_blur.fs.glsl");
    shader->use();
    shader->setInt("gPosition",0);
    shader->setInt("gNormal",1);
    shader->setInt("texNoise",2);
    shaderBlur->use();
    shaderBlur->setInt("ssaoInput",0);

    tog=true;
    screenObj = new Object(createSimpleRec());
}

void Ssao::use(Camera *camera, int gPosition, int gNormal){
    if(tog){
        buffer.bind();
        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(0.05f, 0.5f, 0.5f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        shader->use();

        for(unsigned int i=0; i < 64; i++){
            shader->setVec3("samples[" + std::to_string(i) + "]",  kernel[i]);
        }
        shader->setMat4("projection", camera->GetProjMatrix());
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, castui(gPosition));
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, castui(gNormal));
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, noiseTexture);
        shader->setFloat("radius",radius);
        shader->setFloat("bias",bias);
        Vector2 noiseScale(camera->m_width/4,camera->m_height/4);
        shader->setVec2("noiseScale", noiseScale);
        screenObj->getMesh().draw();


        // Blur
        bufferBlur.bind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        shader->use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, bufferBlur.texture);
        screenObj->getMesh().draw();

    }
}

int Ssao::getTexture() {
    return casti(buffer.texture);
}

int Ssao::getBlurryTexture() {
    return casti(bufferBlur.texture);
}

void Ssao::setBias(float b) {
    bias = b;
}

void Ssao::setRadius(float r) {
    radius = r;
}

void Ssao::resize(int w, int h) {
    buffer.resize(w,h);
    bufferBlur.resize(w,h);
}

bool Ssao::toggle(){
    tog = ! tog;
    return tog;
}

bool Ssao::activate() {
    return tog;
}
