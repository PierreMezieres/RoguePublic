//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_SSAO_HPP
#define ROGUE_SSAO_HPP


#include <src/Core/Macros.hpp>
#include <src/Engine/Object/Object.hpp>
#include <src/Engine/Shader/Shader.hpp>
#include <src/Engine/Texture/SSAOBuffer.hpp>

class Ssao {
public:
    Ssao();

    void use(Camera *camera, int gPosition, int gNormal);

    void resize(int w, int h);

    int getTexture();
    int getBlurryTexture();

    void setBias(float b);
    void setRadius(float r);
    bool toggle();
    bool activate();

private:
    VectorPoint3 kernel;
    VectorPoint3 noise;
    unsigned int noiseTexture;
    Shader *shader;
    Shader *shaderBlur;
    SSAOBuffer buffer;
    SSAOBuffer bufferBlur;
    float radius = 0.5f;
    float bias = 0.01f;
    bool tog;

    Object *screenObj;
};


#endif //ROGUE_SSAO_HPP
