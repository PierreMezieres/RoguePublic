//
// Created by pierre on 20/02/19.
//

#include "PostProcessGamma.hpp"
#include <src/Core/Shapes/TriangleMesh.hpp>
#include <Engine/Shader/Shader.hpp>

PostProcessGamma::PostProcessGamma() {

    screenShader = new Shader("../Shaders/PostProcess/framebuffer_screen.vs.glsl","../Shaders/PostProcess/fb_gamma.fs.glsl");
    screenObj = new Object(createSimpleRec());

    screenShader->use();
    screenShader->setInt("screenTexture",0);
}

void PostProcessGamma::use(int fb_display, int texture) {

    glBindFramebuffer(GL_FRAMEBUFFER, castui(fb_display));

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    screenShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, castui(texture));
    screenObj->getMesh().draw();
}