//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_HDR_HPP
#define ROGUE_HDR_HPP


#include <src/Engine/Texture/FrameBuffer.hpp>
#include <src/Engine/Shader/Shader.hpp>
#include <src/Engine/Object/Object.hpp>

class Hdr {
public:
    Hdr();

    void use(int texture, bool activeBloom = false, int texBloom = 0);

    void resize(int w, int h);

    int getTexture();

    void setGamma(float g);
    void setExposure(float e);
    bool toggle();

private:

    bool togHDR;
    FrameBuffer buffer;
    Shader *shader;
    float gamma;
    float exposure;

    Object *screenObj;

};


#endif //ROGUE_HDR_HPP
