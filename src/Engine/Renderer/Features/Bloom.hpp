//
// Created by pierre on 23/11/18.
//

#ifndef ROGUE_BLOOM_HPP
#define ROGUE_BLOOM_HPP


#include <src/Engine/Texture/FrameBuffer.hpp>
#include <src/Engine/Shader/Shader.hpp>
#include <src/Engine/Object/Object.hpp>

class Bloom {
public:
    Bloom();

    void use(int texture);

    void resize(int w, int h);

    int getTexture();
    bool getActive();

    bool toggle();
    void setAmountBloom(unsigned int a);

private:

    FrameBuffer buffer;
    Shader *shader;
    Shader *shaderBlur;
    FrameBuffer pingpongBuffers[2];
    unsigned int amountBloom;
    bool tog;
    bool horizontal;

    Object *screenObj;

};


#endif //ROGUE_BLOOM_HPP
