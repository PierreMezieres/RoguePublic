#ifndef RENDERER_HPP
#define RENDERER_HPP

/**
 * \file Renderer.hpp
 * \brief Simple forward renderer modified
 */

#include <Engine/Object/Object.hpp>
#include <Engine/Renderer/DrawData.hpp>
#include <src/Engine/Loader/FileLoaderManager.hpp>
#include <src/Engine/Texture/FrameBuffer.hpp>
#include <src/Engine/Texture/GBuffer.hpp>
#include <src/Engine/Texture/SSAOBuffer.hpp>
#include <src/Engine/Renderer/Features/Ssao.hpp>
#include <src/Engine/Renderer/Features/Fxaa.hpp>
#include <src/Engine/Renderer/Features/Prepass.hpp>
#include <src/Engine/Renderer/Features/Bloom.hpp>
#include <src/Engine/Renderer/Features/Hdr.hpp>
#include <src/Engine/Renderer/Features/PostProcess.hpp>
#include <src/Engine/Renderer/Features/Skybox.hpp>
#include <src/Engine/Renderer/Features/Forward.hpp>

class Renderer
{
public:
  Renderer(std::string id);
  virtual ~Renderer(){}

  virtual void draw(DrawData &drawData) = 0;

    void toggleDrawMode();
    void drawScreen();
    bool getDrawMode();

  virtual void resize(int w, int h) = 0;

  std::string getID();

protected:
  std::string m_id;
  bool m_drawfill = true;

    Object * screenObj;


};

#endif // RENDERER_HPP
