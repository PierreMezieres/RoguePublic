//
// Created by pierre on 23/11/18.
//

#include "DefaultRenderer.hpp"

DefaultRenderer::DefaultRenderer(): Renderer("Default Renderer")
{
    idBufferFinal = castui(hdr.getTexture());
    hdr.toggle();
    bloom.toggle();

}

void DefaultRenderer::draw(DrawData &drawData){

    // QT use his own default frame buffer ...
    GLint qt_buffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
    // On HDPI Display (Retina and co), Qt define the viewport according to pixel aspect Ratio, get it
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    prepass.use(drawData);
    ssao.use(drawData.camera, prepass.getPositionTexture(), prepass.getNormalTexture());
    forward.use(drawData, skybox.getTexture(), ssao.getBlurryTexture(), ssao.activate(), getDrawMode());
    skybox.use(drawData.camera);
    fxaa.use(drawData.camera,forward.getTexture());
    bloom.use(forward.getTexture());
    hdr.use(fxaa.getTexture(), bloom.getActive(), bloom.getTexture());

    // When rendering to the final buffer, use the Qt viewport so that rendering is correct on HDPI display
    glViewport(vp[0], vp[1], vp[2], vp[3]);
    postProcess.use(qt_buffer, casti(idBufferFinal));

}


void DefaultRenderer::changeBufferFinal(int i) {
    switch(i){
        case 0: idBufferFinal = castui(hdr.getTexture()); break;
        case 1: idBufferFinal = castui(prepass.getNormalTexture()); break;
        case 2: idBufferFinal =  castui(ssao.getTexture()); break;
        case 3: idBufferFinal =  castui(ssao.getBlurryTexture()); break;
        case 4: idBufferFinal = castui(bloom.getTexture()); break;
        default: break;
    }
}

void DefaultRenderer::resize(int w, int h) {
    forward.resize(w,h);
    ssao.resize(w,h);
    fxaa.resize(w,h);
    prepass.resize(w,h);
    hdr.resize(w,h);
    bloom.resize(w,h);
}

std::string DefaultRenderer::changeShaderPost(){
    return postProcess.changeShader();
}

std::string DefaultRenderer::changeSkybox(){
    return skybox.changeSkybox();
}

std::string DefaultRenderer::toggleSSAO() {
    bool togSSAO = ssao.toggle();
    if(togSSAO) return "SSAO ON";
    return "SSAO OFF";
}

void DefaultRenderer::changeRadiusSSAO(float radius) {
    ssao.setRadius(radius);
}

void DefaultRenderer::changeBiasSSAO(float bias) {
    ssao.setBias(bias);
}

bool DefaultRenderer::toggleFXAA() {
    return fxaa.toggle();
}

bool DefaultRenderer::toggleFXAADebug() {
    return fxaa.toggleDebug();
}

void DefaultRenderer::changeMinContrastFXAA(float min) {
    fxaa.setMinContrast(min);
}

void DefaultRenderer::changeMinThresholdFXAA(float t) {
    fxaa.setMinThreshold(t);
}

void DefaultRenderer::changeGammaHDR(float gamma) {
    hdr.setGamma(gamma);
}

void DefaultRenderer::changeExposureHDR(float exposure) {
    hdr.setExposure(exposure);
}

bool DefaultRenderer::toggleHDR() {
    return hdr.toggle();
}

void DefaultRenderer::changeAmountBloom(unsigned int amount) {
    bloom.setAmountBloom(amount);
}

bool DefaultRenderer::toggleBLOOM() {
    return bloom.toggle();
}
