//
// Created by pierre on 24/11/18.
//

#include "Bone.hpp"
#include "../Object/Mesh.hpp"
#include "../Object/Object.hpp"

Bone::Bone(Point3 _p1, Point3 _p2) {

    restPose = Matrix4(1.);
    pose = Matrix4(1.);

    p1 = _p1;
    p2 = _p2;

    Mesh m;
    m.setMeshTypes(GL_LINES);
    m.addLine(p1,p2);
    object = new Object(m);
    object->setMaterial("blue");

}

Bone::Bone(Matrix4 p) {
    restPose = p;
    pose = p;
}

void Bone::changeMesh() {
    Mesh m = object->getMesh();
    m.changeLastLine(getBlendPose()*Vector4(p1,1),getBlendPose()*Vector4(p2,1));
    object->setMesh(m);
}

void Bone::rotateRest(float angle, Vector3 axis) {
    pose = glm::rotate(restPose, glm::radians(angle), axis);
    changeMesh();
    for(auto i:childs){
        i->rotate(getBlendPose());
    }
}

void Bone::rotate(float angle, Vector3 axis) {
    pose = glm::rotate(pose, glm::radians(angle), axis);
    changeMesh();
    for(auto i:childs){
        i->rotate(getBlendPose());
    }
}

void Bone::rotate(Matrix4 rot) {
    pose = rot * pose;
    changeMesh();
}

void Bone::savePose() {
    restPose = pose;
}

void Bone::setPosition(Vector3 pos) {
    setPositionMat4(pose,pos);
    setPositionMat4(restPose,pos);
}

Matrix4 Bone::getBlendPose() {
    return pose * glm::inverse(restPose);
}

DualQuat Bone::getDualQuat() {
    return DualQuat(getBlendPose());
}
