//
// Created by pierre on 24/11/18.
//

#ifndef ROGUE_BONE_HPP
#define ROGUE_BONE_HPP


#include <src/Core/Macros.hpp>
#include <glm/gtx/dual_quaternion.hpp>
#include <src/Engine/Anim/DualQuat.hpp>

class Object;

class Bone {
public:
    Bone(Point3 _p1, Point3 _p2);
    Bone(Matrix4 p);

    Matrix4 restPose;
    Matrix4 pose;

    std::vector<Bone*> childs;

    Point3 p1,p2;
    Object *object;
    void changeMesh();

    void rotateRest(float angle, Vector3 axis);
    void rotate(float angle, Vector3 axis);
    void rotate(Matrix4 rot);
    void savePose();
    void setPosition(Vector3 pos);

    Matrix4 getBlendPose();
    DualQuat getDualQuat();

private:


};


#endif //ROGUE_BONE_HPP
