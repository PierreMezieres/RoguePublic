//
// Created by argo on 15/12/18.
//

#ifndef ROGUE_DUALQUAT_H
#define ROGUE_DUALQUAT_H

#include <glm/gtx/dual_quaternion.hpp>
#include <Core/Macros.hpp>

class DualQuat {
public:

    DualQuat();
    DualQuat(Matrix4 m);

    Vector3 transformPositionDQS( Vector3 position);
    void normalize();
    void blend(DualQuat q, float w);


    glm::quat quatFromMat4(Matrix4 m);
    glm::dualquat dualQuatFromMat4(Matrix4 m);


    glm::dualquat dq;
};


#endif //ROGUE_DUALQUAT_H
