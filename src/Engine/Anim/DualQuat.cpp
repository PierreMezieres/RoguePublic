//
// Created by argo on 15/12/18.
//

#include "DualQuat.hpp"

DualQuat::DualQuat() {
    dq.real *= 0;
    dq.dual *= 0;
}

DualQuat::DualQuat(Matrix4 m) {
    dq = dualQuatFromMat4(m);
}

void DualQuat::normalize() {
    float norm = glm::length(dq.real);
    dq.real = dq.real / norm;
    dq.dual = dq.dual / norm;
}

void DualQuat::blend(DualQuat q, float w){
    dq = dq + q.dq * w;
}

Vector3 DualQuat::transformPositionDQS( Vector3 position){
    Vector3 realxyz {dq.real.x, dq.real.y, dq.real.z};
    Vector3 dualxyz {dq.dual.x, dq.dual.y, dq.dual.z};
    Vector3 tr = (dualxyz*dq.real.w - realxyz*dq.dual.w + glm::cross(realxyz,dualxyz)) *2.f;
    Vector3 rot = position + glm::cross((realxyz*2.f), glm::cross( realxyz , position) + position * dq.real.w);

    return tr+rot;
}


glm::quat DualQuat::quatFromMat4(Matrix4 m){

    // Compute trace of matrix 't'
    float T = 1 + m[0][0] + m[1][1] + m[2][2];

    float S, X, Y, Z, W;

    if ( T > 0.00000001f ) // to avoid large distortions!
    {
        S = sqrt(T) * 2.f;
        X = ( m[2][1] - m[1][2] ) / S;
        Y = ( m[0][2] - m[2][0] ) / S;
        Z = ( m[1][0] - m[0][1] ) / S;
        W = 0.25f * S;
    }
    else
    {
        if ( m[0][0] > m[1][1] && m[0][0] > m[2][2] )
        {
            // Column 0 :
            S  = sqrt( 1.0f + m[0][0] - m[1][1] - m[2][2] ) * 2.f;
            X = 0.25f * S;
            Y = (m[1][0] + m[0][1] ) / S;
            Z = (m[0][2] + m[2][0] ) / S;
            W = (m[2][1] - m[1][2] ) / S;
        }
        else if ( m[1][1] > m[2][2] )
        {
            // Column 1 :
            S  = sqrt( 1.0f + m[1][1] - m[0][0] - m[2][2] ) * 2.f;
            X = (m[1][0] + m[0][1] ) / S;
            Y = 0.25f * S;
            Z = (m[2][1] + m[1][2] ) / S;
            W = (m[0][2] - m[2][0] ) / S;
        }
        else
        {   // Column 2 :
            S  = sqrt( 1.0f + m[2][2] - m[0][0] - m[1][1] ) * 2.f;
            X = (m[0][2] + m[2][0] ) / S;
            Y = (m[2][1] + m[1][2] ) / S;
            Z = 0.25f * S;
            W = (m[1][0] - m[0][1] ) / S;
        }
    }

    return glm::quat(W,-X,-Y,-Z);
}

glm::dualquat DualQuat::dualQuatFromMat4(Matrix4 m){

    glm::quat q= quatFromMat4(m);
    Vector3 t{m[3][0],m[3][1],m[3][2]};

    float w = -0.5f*( t.x * q.x + t.y * q.y + t.z * q.z);
    float i =  0.5f*( t.x * q.w + t.y * q.z - t.z * q.y);
    float j =  0.5f*(-t.x * q.z + t.y * q.w + t.z * q.x);
    float k =  0.5f*( t.x * q.y - t.y * q.x + t.z * q.w);

    return glm::dualquat(q, glm::quat(w,i,j,k));

}
