#include "Material.hpp"
#include <Engine/Object/Element.hpp>

Material::Material(Vector3 kd, Vector3 ks, float ns):
        m_kd{Vector4(kd.x,kd.y,kd.z,1)}, m_ks{ks}, m_ns{ns}{

}

Material::Material(Vector4 kd, Vector3 ks, float ns):
        m_kd{kd}, m_ks{ks}, m_ns{ns}{

}

Material::Material(std::string kd): m_kd{Vector4(0.8,0.8,0.2,1)}, m_ks{Vector3(1)}, m_ns{32}{
    setKdTexture(kd);
}

Material::Material(std::string kd, std::string ks): m_kd{Vector4(0.8,0.8,0.2,1)}, m_ks{Vector3(1)}, m_ns{32}{
    setKdTexture(kd);
    setKsTexture(ks);
}

void Material::setKd(Vector4 kd){
    m_kd = kd;
}

void Material::setKs(Vector3 ks){
    m_ks = ks;
}

void Material::setNs(float ns){
    m_ns = ns;
}

void Material::setKdTexture(Texture* tex){
    m_KdTexture = tex;
    m_hasKdTexture = m_KdTexture->isLoadingOk();
}

void Material::setKsTexture(Texture* tex){
    m_KsTexture = tex;
    m_hasKsTexture = m_KsTexture->isLoadingOk();
}

void Material::setNsTexture(Texture* tex){
    m_NsTexture = tex;
    m_hasNsTexture = m_NsTexture->isLoadingOk();
}

void Material::setKdTexture(std::string tex){
    m_KdTexture = new Texture(tex);
    m_hasKdTexture = m_KdTexture->isLoadingOk();
}

void Material::setKsTexture(std::string tex){
    m_KsTexture = new Texture(tex);
    m_hasKsTexture = m_KsTexture->isLoadingOk();
}

void Material::setNsTexture(std::string tex){
    m_NsTexture = new Texture(tex);
    m_hasNsTexture = m_NsTexture->isLoadingOk();
}

void Material::bind(Shader *shader, DataForDrawing){

    shader->setVec4("material.kd",m_kd);
    shader->setVec3("material.ks",m_ks);
    shader->setFloat("material.ns",m_ns);
    shader->setFloat("material.ambient",m_ambient);
    shader->setBool("material.hasKdTexture",m_hasKdTexture);
    shader->setBool("material.hasKsTexture",m_hasKsTexture);
    shader->setBool("material.hasNsTexture",m_hasNsTexture);

    if(m_hasKdTexture){
        m_KdTexture->bind(GL_TEXTURE0);
        shader->setInt("material.kdTexture",0);
    }
    if(m_hasKsTexture){
        m_KsTexture->bind(GL_TEXTURE1);
        shader->setInt("material.ksTexture",1);
    }
    if(m_hasNsTexture){
        m_NsTexture->bind(GL_TEXTURE2);
        shader->setInt("material.nsTexture",2);
    }

}

void Material::setShader(std::string str){
    m_nameShader=str;
}

std::string Material::getNameShader(){
    return m_nameShader;
}