//
// Created by pierre on 25/08/18.
//

#ifndef ROGUE_MATERIALGENERATOR_HPP
#define ROGUE_MATERIALGENERATOR_HPP

/**
 * \file MaterialGenerator.hpp
 * \brief Util function to generate material
 */

#include "MaterialManager.hpp"

/**
 * @param number Each number will generate a different material
 * @param mm The material manager in which you want to add the material
 * @return The name of the material
 */
std::string kdMaterialGeneratorN(int number, MaterialManager *mm);     ///< Generate a material

/**
 * @param mm The material manager in which you want to add the material
 * @return The name of the material
 */
std::string kdMaterialGenerator(MaterialManager *mm);

#endif //OGRE_MATERIALGENERATOR_HPP
