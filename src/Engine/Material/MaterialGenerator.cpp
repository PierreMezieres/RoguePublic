//
// Created by pierre on 25/08/18.
//

#include "MaterialGenerator.hpp"



std::string kdMaterialGeneratorN(int number, MaterialManager *mm){
    Vector3 col;
    std::string name;
    switch(number%11){
        case 0: col = Vector3(0.8,0,0); name = "red"; break;
        case 1: col = Vector3(0,0.8,0); name = "green"; break;
        case 2: col = Vector3(0,0,0.8); name = "blue"; break;
        case 3: col = Vector3(0.5,0.5,0.1); name = "yellow"; break;
        case 4: col = Vector3(0.5,0.1,0.5); name = "purple"; break;
        case 5: col = Vector3(0.1,0.5,0.5); name = "cyan"; break;
        case 6: col = Vector3(0.5,0.1,0.1); name = "red2"; break;
        case 7: col = Vector3(0.1,0.5,0.1); name = "green2"; break;
        case 8: col = Vector3(0.1,0.1,0.5); name = "blue2"; break;
        case 9: col = Vector3(0.5,0.5,0.5); name = "grey"; break;
        case 10: col = Vector3(0.9,0.9,0.9); name = "blank"; break;
        default: break;
    }

    Material * m = new Material(col);
    m->setShader("onlyKd");
    mm->addValue(name,m);
    return name;
}

int call = 0;

std::string kdMaterialGenerator(MaterialManager *mm){
    call++;
    return kdMaterialGeneratorN(call-1,mm);
}
