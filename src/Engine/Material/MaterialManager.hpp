//
// Created by pierre on 26/07/18.
//

#ifndef ROGUE_MATERIALMANAGER_HPP
#define ROGUE_MATERIALMANAGER_HPP

/**
 * \file MaterialManager.hpp
 * \brief Simple manager for material
 */

#include <Core/Managers/ManagerMap.hpp>
#include <Engine/Material/Material.hpp>

/**
 * \class MaterialManager
 * \brief Manager for materials
 */
class MaterialManager : public ManagerMap<Material*> {
public:
    MaterialManager();

    void displayInfo(); ///< Display information about material manger in terminal

};


#endif //OGRE_MATERIALMANAGER_HPP
