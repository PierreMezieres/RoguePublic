//
// Created by pierre on 01/09/18.
//

#include "MirrorMaterial.hpp"

MirrorMaterial::MirrorMaterial(): Material(Vector3(1)) {
    setShader("mirror");
}

void MirrorMaterial::bind(Shader *shader, DataForDrawing data){
    Material::bind(shader,data);

    if(data.skybox != nullptr){
        data.skybox->bindCubemap(GL_TEXTURE0);
    }
    shader->setInt("skybox",0);
}