//
// Created by pierre on 01/09/18.
//

#ifndef ROGUE_MIRRORMATERIAL_HPP
#define ROGUE_MIRRORMATERIAL_HPP

/**
 * \file MirrorMaterial.hpp
 * \brief Simple mirror material
 */


#include <src/Engine/Material/Material.hpp>
#include <src/Engine/Object/Element.hpp>

class MirrorMaterial : public Material {
public:

    MirrorMaterial();

    void bind(Shader * shader, DataForDrawing data);

    Texture *skybox;

};


#endif //ROGUE_MIRRORMATERIAL_HPP
