//
// Created by pierre on 01/09/18.
//

#ifndef ROGUE_GLASSMATERIAL_HPP
#define ROGUE_GLASSMATERIAL_HPP

/**
 * \file GlassMaterial.hpp
 * \brief Simple glass material
 */

#include <src/Engine/Material/Material.hpp>
#include <src/Engine/Object/Element.hpp>

class GlassMaterial : public Material {
public:

    GlassMaterial();

    void bind(Shader * shader, DataForDrawing data);

    Texture *skybox;

};


#endif //ROGUE_GLASSMATERIAL_HPP
