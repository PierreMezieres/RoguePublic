#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <Core/Macros.hpp>
#include <src/Core/Math/Ray.hpp>

/**
 * \file Camera.hpp
 * \brief Mother class for camera
 *
 * Mother class for camera
 *
 */

#define ZOOM 45.0f
#define SPEED 2.5f
#define SENSITIVITY 0.1f

enum Movement{FORWARD = 1, BACKWARD = 3, LEFT = 0, RIGHT = 2};

/**
 * \class Camera
 * \brief Mother class for camera
 */
class Camera
{
public:
  Camera(Vector3 position = Vector3(1,0,0), Vector3 front = Vector3(-1,0,0), Vector3 worldUp = Vector3(0,1,0));
  virtual ~Camera(){}

  virtual void mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint = true) = 0;
  virtual void mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) = 0;
  virtual void mouseScroll(GLfloat offset) = 0;
  virtual void keyboard(Movement movement, GLfloat deltatime) = 0;
  virtual void updateCameraVectors() = 0;

  virtual void fitScene(AABB aabb) = 0;
  Ray getRayFromScreen(GLfloat x, GLfloat y);


  void updateCameraVectorsInit();
  void setPoint(const Vector3& pos, const Vector3& front, const float fov, const Vector3& up);

  Matrix4 GetViewMatrix();
  Matrix4 GetProjMatrix();
  Matrix4 GetProjViewMatrix();
  float getNear();
  float getFar();


  std::string name_camera;
  Vector3 m_position;
  Vector3 m_front;
  Vector3 m_up;
  Vector3 m_right;
  Vector3 m_worldUp;
  float m_movementSpeed;
  float m_mouseSensitivity;
  float m_factorDistance;
  float m_zoom;

  void resizeCamera(int width, int height);
  int m_width, m_height;
  float getAspect();
  float getFOV();

  void displayInfo();

private:
  float m_zNear;
  float m_zFar;
};

#endif // CAMERA_HPP
