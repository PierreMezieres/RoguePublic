#include "CameraManager.hpp"

#include <Engine/Camera/EulerCamera.hpp>
#include <Engine/Camera/Camera2D.hpp>
#include <Engine/Camera/TrackBall.hpp>
#include <Core/Log/Log.hpp>

CameraManager::CameraManager()
{
  actualCamera = new TrackBall();
  listCamera.push_back(actualCamera);
  listCamera.push_back(new EulerCamera());
  listCamera.push_back(new Camera2D());
  localizedCamera = new LocalizedCamera();
  listCamera.push_back(localizedCamera);
  //addPoint(actualCamera->m_position, actualCamera->m_front, actualCamera->m_up);
  num_camera=0;
  actual_settings = 0;
  num_settings = 1;

}


Camera * CameraManager::getCamera(){
  return actualCamera;
}

Camera * CameraManager::nextCamera(){
  num_camera = (num_camera + 1) % listCamera.size();
  actualCamera = listCamera[num_camera];
  return actualCamera;
}

Camera * CameraManager::changeCamera(int i) {
  if(i >= 0){
      num_camera = castul(i);
      actualCamera = listCamera[num_camera];
    }
  Log(logInfo) << "Actual Camera: " << actualCamera->name_camera;
  return actualCamera;
}

void CameraManager::changeSize(int width, int height){
  for(auto i: listCamera){
      i->resizeCamera(width,height);
    }
}

void CameraManager::displayInfo(){
  Log(logInfo) << "| - Camera number: " << listCamera.size();
}

void CameraManager::setPoint(int){
  Log(logError) << "SetPoint camera manager";
  /*if(i >= 0){
      actualCamera->setPoint(m_position[castui(i)],m_front[castui(i)],m_fov[castui(i)],m_up[castui(i)]);
    }*/
}

void CameraManager::addPoint(Vector3& pos, Vector3& front, float fov){
  localizedCamera->addPoint(pos,front,fov);
}

void CameraManager::resetPoints(){
  localizedCamera->resetPoints();
}
