#ifndef CAMERA2D_HPP
#define CAMERA2D_HPP

/**
 * \file Camera2D.hpp
 * \brief Simple 2D Camera
 */

#include <Core/Macros.hpp>

#include <Engine/Camera/Camera.hpp>

class Camera2D: public Camera
{
public:
    Camera2D(Vector3 position = Vector3(0,0,5),
             Vector3 front = Vector3(0,0,-1),
             Vector3 worldUp = Vector3(0,1,0));

    void mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint = true) override;
    void mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) override;
    void mouseScroll(GLfloat offset) override;
    void keyboard(Movement movement, GLfloat deltatime) override;
    void updateCameraVectors() override;

    void fitScene(AABB aabb) override;
};

#endif // CAMERA2D_HPP
