#include <src/Core/Log/Log.hpp>
#include "TrackBall.hpp"
#include <Core/Math/EigenToGlm.hpp>

TrackBall::TrackBall(Vector3 pointLook, float distance):
    Camera(pointLook + distance * Vector3(0,0,1), Vector3(0,0,-1), Vector3(0,1,0)),
    m_pointLook{pointLook}, m_distance{distance}, m_yaw{YAW}, m_pitch{PITCH}
{
    updateCameraVectors();
    name_camera="TrackBall";
}

void TrackBall::mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint){
    xOffset *= m_mouseSensitivity;
    yOffset *= m_mouseSensitivity;

    m_yaw += xOffset;
    m_pitch += yOffset;

    if(constraint){
        if(m_pitch>89) m_pitch = 89;
        if(m_pitch<-89) m_pitch = -89;
    }

    updateCameraVectors();
}

void TrackBall::mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) {
    xOffset *= m_mouseSensitivity * 0.1f;
    yOffset *= m_mouseSensitivity * 0.1f;

    m_pointLook = m_pointLook + m_right * xOffset;
    m_pointLook = m_pointLook + m_worldUp * yOffset;
    m_position = m_pointLook - m_front*m_distance;

    updateCameraVectors();
}

void TrackBall::mouseScroll(GLfloat offset){
    m_distance -= (m_factorDistance/10)*offset;
    if(m_distance<0.2f) m_distance=0.2f;
    m_position=m_pointLook-m_front*m_distance;
}

void TrackBall::keyboard(Movement, GLfloat){
}

void TrackBall::updateCameraVectors(){
    Vector3 front;
    float yaw = glm::radians(m_yaw);
    float pitch = glm::radians(m_pitch);
    front.x = std::cos(yaw) * std::cos(pitch);
    front.y = std::sin(pitch);
    front.z = std::sin(yaw) * std::cos(pitch);
    m_front = glm::normalize(front);
    m_right = glm::normalize(glm::cross(m_front,m_worldUp));
    m_up = glm::normalize(glm::cross(m_right,m_front));

    m_position = m_pointLook - m_front*m_distance;
}

void TrackBall::fitScene(AABB aabb){
    m_pointLook = ETG_VECTOR3(aabb.center());

    m_yaw = YAW;
    m_pitch = PITCH;

    m_worldUp = Vector3(0,1,0);

    float fovH = degToRad( getAspect()* getFOV() );
    float fov = degToRad( getFOV() );

    float r = (aabb.max() - aabb.min()).norm()/2.0f;
    float x = r / std::tan(fov/2.0f);
    float y = r / std::tan(fovH/2.0f);

    m_distance = std::max(std::max(x,y), 0.001f);
    m_factorDistance = m_distance;

    updateCameraVectors();
}
