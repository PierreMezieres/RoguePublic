#include "Camera.hpp"

Camera::Camera(Vector3 position, Vector3 front, Vector3 worldUp):
    name_camera{"NO NAME"},  m_position{position}, m_front{front}, m_worldUp{worldUp},
    m_movementSpeed{SPEED}, m_mouseSensitivity{SENSITIVITY}, m_zoom{ZOOM}, m_zNear{0.1f}, m_zFar{500.f}
{
    updateCameraVectorsInit();
    m_factorDistance = glm::length(position);
}

#include <Eigen/Core>

Matrix4 Camera::GetViewMatrix(){
    return glm::lookAt(m_position, m_position + m_front, m_up);
}

Matrix4 Camera::GetProjMatrix(){
    return glm::perspective(glm::radians(m_zoom), castf(m_width) / castf(m_height), m_zNear, m_zFar);
}

Matrix4 Camera::GetProjViewMatrix(){
    return GetProjMatrix() * GetViewMatrix();
}

float Camera::getNear() {
    return m_zNear;
}

float Camera::getFar() {
    return m_zFar;
}

Ray Camera::getRayFromScreen(GLfloat x, GLfloat y) {
    Vector3 u = glm::unProject(Vector3(x,m_height-y,m_zNear),GetViewMatrix(),GetProjMatrix(),Vector4(0,0,m_width,m_height));
    Ray r(m_position,u-m_position);
    return r;
}

void Camera::updateCameraVectorsInit(){
    m_front = glm::normalize(m_front);
    m_right = glm::normalize(glm::cross(m_front,m_worldUp));
    m_up = glm::normalize(glm::cross(m_right,m_front));
}

void Camera::resizeCamera(int width, int height){
    m_width=width;
    m_height=height;
}

float Camera::getAspect() {
    return castf(m_width)/castf(m_height);
}

float Camera::getFOV() {
    return m_zoom;
}

void Camera::setPoint(const Vector3& pos, const Vector3& front, const float fov, const Vector3& up){
  m_position = pos;
  m_front = glm::normalize(front);
  m_worldUp = glm::normalize(up);
  updateCameraVectorsInit();
  m_factorDistance = glm::length(pos);
  m_zoom = fov;
}

void Camera::displayInfo() {
    Log(logInfo) << "";
    Log(logInfo) << "|-------------- Camera Info -------------";
    Log(logInfo) << "| - Name:     " << name_camera;
    Log(logInfo) << "| - Position: " << m_position.x << " / " << m_position.y << " / " << m_position.z;
    Log(logInfo) << "| - Front:    " << m_front.x << " / " << m_front.y << " / " << m_front.z;
    Log(logInfo) << "| - Up:       " << m_up.x << " / " << m_up.y << " / " << m_up.z;
    Log(logInfo) << "| - Right:    " << m_right.x << " / " << m_right.y << " / " << m_right.z;
    Log(logInfo) << "| - World up: " << m_worldUp.x << " / " << m_worldUp.y << " / " << m_worldUp.z;
    Log(logInfo) << "| - Zoom:     " << m_zoom;
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";
}
