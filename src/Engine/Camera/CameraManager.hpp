#ifndef CAMERAMANAGER_HPP
#define CAMERAMANAGER_HPP

/**
 * \file CameraManager.hpp
 * \brief Simple manager for camera
 */

#include <vector>

#include <Engine/Camera/Camera.hpp>
#include "LocalizedCamera.hpp"

#define TRACKBALL 0
#define PLANE2D 2
#define EULER 1

class CameraManager
{
public:
    CameraManager();

    Camera * getCamera();
    Camera * nextCamera();
    Camera * changeCamera(int i);


    void setPoint(int i);
    void addPoint(Vector3& pos, Vector3& front, float fov);
    void changeSize(int width, int height);
    void resetPoints();
    void displayInfo();

private:
    std::vector<Camera*> listCamera;
    std::vector<Vector3> m_position;
    std::vector<Vector3> m_front;
    std::vector<float> m_fov;
    std::vector<Vector3> m_up;
    unsigned long actual_settings;
    unsigned long num_settings;
    Camera *actualCamera;
    unsigned long num_camera;

    LocalizedCamera *localizedCamera;

};

#endif // CAMERAMANAGER_HPP
