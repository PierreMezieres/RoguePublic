//
// Created by pierre on 20/02/19.
//

#include "LocalizedCamera.hpp"

#include <Core/Log/Log.hpp>
#include <Core/Math/EigenToGlm.hpp>

LocalizedCamera::LocalizedCamera(Vector3 position, Vector3 front, Vector3 worldUp, float yaw, float pitch):
        Camera(position, front, worldUp),
        m_yaw{yaw}, m_pitch{pitch}
{
    resetPoints();
    //updateCameraVectors();
    name_camera="LocalizedCamera";
}

void LocalizedCamera::mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint){
    xOffset *= m_mouseSensitivity;
    yOffset *= m_mouseSensitivity;

    m_yaw += xOffset;
    m_pitch += yOffset;

    if(constraint){
        if(m_pitch>89) m_pitch = 89;
        if(m_pitch<-89) m_pitch = -89;
    }

    updateCameraVectors();
}

void LocalizedCamera::mouseMovementMiddle(GLfloat, GLfloat) {
}

void LocalizedCamera::mouseScroll(GLfloat offset){
    m_zoom -= offset;
    if(m_zoom <= 1) m_zoom = 1;
    if(m_zoom >= 90) m_zoom = 90;
}

void LocalizedCamera::keyboard(Movement movement, GLfloat){
    switch(movement){
        case LEFT: actual_settings--; break;
        case RIGHT: actual_settings++; break;
        default: return;
    }
    if(actual_settings>= num_settings) actual_settings--;
    if(actual_settings<0) actual_settings=0;

    Log(logInfo) << "Localized Camera place : " << actual_settings;

    setPoint(actual_settings);
}

void LocalizedCamera::updateCameraVectors(){
    Vector3 front;
    float yaw = glm::radians(m_yaw);
    float pitch = glm::radians(m_pitch);
    front.x = std::cos(yaw) * std::cos(pitch);
    front.y = std::sin(pitch);
    front.z = std::sin(yaw) * std::cos(pitch);
    m_front = glm::normalize(front);
    m_right = glm::normalize(glm::cross(m_front,m_worldUp));
    m_up = glm::normalize(glm::cross(m_right,m_front));
}

void LocalizedCamera::fitScene(AABB){
}

void LocalizedCamera::setPoint(int i){
    m_position = listPosition[i];
    m_front = glm::normalize(listFront[i]);
    //updateCameraVectorsInit();
    //m_factorDistance = glm::length(listPosition[i]);
    m_zoom = listFov[i];

    yawAndPitchFromFront(m_front);
    updateCameraVectors();
}

void LocalizedCamera::addPoint(Vector3& pos, Vector3& front, float fov){
    listPosition.emplace_back(pos);
    listFront.emplace_back(front);
    listFov.emplace_back(fov);
    ++num_settings;
}

unsigned long LocalizedCamera::getPointsCount(){
    return num_settings;
}
void LocalizedCamera::resetPoints(){

    listPosition.clear();
    listFront.clear();
    listFov.clear();

    m_position = Vector3(10,5,0);
    m_front = glm::normalize(Vector3(-1,-0.5,0));
    m_zoom = 45;
    listPosition.emplace_back(m_position);
    listFront.emplace_back(m_front);
    listFov.emplace_back(m_zoom);

    yawAndPitchFromFront(m_front);

    updateCameraVectors();

    num_settings = 1;
    actual_settings = 0;
}

void LocalizedCamera::yawAndPitchFromFront(Vector3 &front) {

    m_pitch = glm::degrees(std::asin(front.y));
    m_yaw = glm::degrees(std::atan2(front.z, front.x));
}