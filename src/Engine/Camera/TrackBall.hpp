#ifndef TRACKBALL_HPP
#define TRACKBALL_HPP

/**
 * \file TrackBall.hpp
 * \brief Simple trackball camera
 */

#include <Engine/Camera/Camera.hpp>

#define YAW -90
#define PITCH 0

class TrackBall : public Camera
{
public:
    TrackBall(Vector3 pointLook = Vector3(0,0,0), float distance = 5);

    void mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint = true) override;
    void mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) override;
    void mouseScroll(GLfloat offset) override;
    void keyboard(Movement movement, GLfloat deltatime) override;
    void updateCameraVectors() override;

    void fitScene(AABB aabb) override;

    Vector3 m_pointLook;
    float m_distance;
    float m_yaw, m_pitch;
};

#endif // TRACKBALL_HPP
