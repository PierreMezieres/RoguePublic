//
// Created by pierre on 20/02/19.
//

#ifndef ROGUE_LOCALIZEDCAMERA_HPP
#define ROGUE_LOCALIZEDCAMERA_HPP

#include "Camera.hpp"

#define YAW -90
#define PITCH 0

class LocalizedCamera : public Camera
{
public:
    LocalizedCamera(Vector3 position = Vector3(0,0,5),
                Vector3 front = Vector3(0,0,-1),
                Vector3 worldUp = Vector3(0,1,0),
                float yaw = YAW, float pitch = PITCH);
    virtual ~LocalizedCamera() override {}

    void mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint = true) override;
    void mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) override;
    void mouseScroll(GLfloat offset) override;
    void keyboard(Movement movement, GLfloat deltatime) override;
    void updateCameraVectors() override;

    void fitScene(AABB aabb) override;

    void setPoint(int i);
    void addPoint(Vector3& pos, Vector3& front, float fov);
    void changeSize(int width, int height);
    unsigned long getPointsCount();
    void resetPoints();

    float m_yaw, m_pitch;


private:

    std::vector<Vector3> listPosition;
    std::vector<Vector3> listFront;
    std::vector<float> listFov;
    int num_settings = 1;
    int actual_settings = 0;

    void yawAndPitchFromFront(Vector3& front);
};

#endif //ROGUE_LOCALIZEDCAMERA_HPP
