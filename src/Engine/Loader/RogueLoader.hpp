//
// Created by pierre on 03/08/18.
//

#ifndef ROGUE_OGRELOADER_HPP
#define ROGUE_OGRELOADER_HPP

/**
 * \file RogueLoader.hpp
 * \brief Parser for *.rogue file
 */

#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Gui/Scene.hpp>
#include "FileLoader.hpp"
#include "ParserRogue.hpp"


//TODO make RogueLoader much cleaner ... (way to define child class ...)

class RogueLoader : public FileLoader {
public:
    RogueLoader();


    virtual std::vector<std::string> getFormatManaged() override;
    virtual void loadModel(DrawData *d, std::string path, Mode_load mode_load = OPAQUE_MODE) override;

    // Have to be redefined for child class ...
    virtual void loadCustomData();
    virtual bool customToken(std::vector<std::string> line, int n);
    ParserRogue *m_parserRogue;
    void processFile(std::string path);
    void Error(std::string mes, int n);

    DrawData *m_drawData;

private:
    bool processLine(std::vector<std::string> line,int n);
    void processShape(std::vector<std::string> line, int n);
    void processLight(std::vector<std::string> line, int n);
    void processCamera(std::vector<std::string> line, int n);

    void cleanUp();

    std::string directory;
    std::string label = "";

};


#endif //ROGUE_OGRELOADER_HPP
