//
// Created by pierre on 08/08/18.
//

#include <QtCore/QString>
#include <src/Core/Shapes/Sphere.hpp>
#include <src/Core/Shapes/Cube.hpp>
#include <src/Core/Shapes/MacrosShapes.hpp>
#include "ParserRogue.hpp"
#include "FileLoaderManager.hpp"
#include <Engine/Light/DirLight.hpp>
#include <Engine/Light/SpotLight.hpp>
#include <Engine/Light/PointLight.hpp>
#include <Engine/Material/MaterialGenerator.hpp>
#include <src/Gui/Scene.hpp>
#include <Engine/Camera/TrackBall.hpp>
#include <Engine/Camera/Camera2D.hpp>
#include <Engine/Camera/EulerCamera.hpp>

class PSpring;



float getFloat(TOKENS list, unsigned int n, float d){
  if(n<list.size()){
      QString s = QString::fromStdString(list[n]);
      return s.toFloat();
    }
  return d;
}

Vector4 getVector4(TOKENS list, unsigned int n, Vector4 d){
    float x = getFloat(list,n,d.x);
    float y = getFloat(list,n+1,d.y);
    float z = getFloat(list,n+2,d.z);
    float a = getFloat(list,n+3,d.a);
    return Vector4(x,y,z,a);
}

Vector3 getVector3(TOKENS list, unsigned int n, Vector3 d){
  float x = getFloat(list,n,d.x);
  float y = getFloat(list,n+1,d.y);
  float z = getFloat(list,n+2,d.z);
  return Vector3(x,y,z);
}

Vector2 getVector2(TOKENS list, unsigned int n, Vector2 d){
    float x = getFloat(list,n,d.x);
    float y = getFloat(list,n+1,d.y);
    return Vector2(x,y);
}

std::string getString(TOKENS list, unsigned int n, std::string d){
  if(n<list.size()){
      return list[n];
    }
  return d;
}

ParserRogue::ParserRogue(DrawData *d): m_drawData{d} {
}

void ParserRogue::cleanUp(){
  current_object=nullptr;
}

void ParserRogue::processShapeSphere(TOKENS list){
    float radius = getFloat(list,2,1);
    current_object = new Object(Mesh(createSphereShape(radius)));

    addObject(current_object,"sphere");
    current_object->setMaterial(defaultMaterial);

}

void ParserRogue::processShapeCube(TOKENS list){

  Vector3 c = getVector3(list,2,Vector3(1));
    current_object = new Object(Mesh(createCubeShape(c.x, c.y, c.z)));
    addObject(current_object,"cube");
    current_object->setMaterial(defaultMaterial);
}

void ParserRogue::processShapeGrid(TOKENS list){
  float l = getFloat(list,2,10);
  float L = getFloat(list,3,10);
  float x = getFloat(list,4,10);
  float y = getFloat(list,5,10);

    Object *o = new Object(Mesh(createGridShape(l,L,x,y)));
    o->setMaterial("blank");
    addObject(o,"grid");
    current_object = o;
}

void ParserRogue::processShapeTriangleShape(TOKENS list) {
    int points = getFloat(list,2,0);
    int vertices = getFloat(list,3,0);

    int cpt = 4;
    Mesh m;
    for(int i=0;i<points;i++){
        Vector3 pts = getVector3(list,cpt);
        cpt += 3;
        m.m_vertices.push_back(pts);
    }
    for(int i=0;i<vertices;i++){
        Vector3 ind = getVector3(list,cpt);
        cpt += 3;
        m.setTriangle(ind);
    }
    for(int i=0; i<points;i++){
        Vector2 tex = getVector2(list,cpt);
        cpt+=2;
        m.m_texCoord.push_back(Vector3(tex.x,tex.y,0));
    }
    m.autoNormals();

    Object *o = new Object(m);
    addObject(o,"grid");
    current_object = o;
}

void ParserRogue::processAttenuation(TOKENS list) {
    attenuation_default = getVector3(list,1,Point3(1,0.0009,0.000032));
}

void ParserRogue::processLightDir(TOKENS list){
  Vector3 dir = getVector3(list,2,Vector3(-1));
  Vector3 color = getVector3(list,5,Vector3(1));
  m_drawData->lightManager.addValue(new DirLight(dir,color));
}

void ParserRogue::processLightSpot(TOKENS list){

    Vector3 point = getVector3(list,2,Vector3(1));
    Vector3 dir = getVector3(list,5,Vector3(1));
    float inner = getFloat(list,8,20);
    float outer = getFloat(list,9,25);
    Vector3 color = getVector3(list,10,Vector3(1));
    Point3 att = getVector3(list,13,attenuation_default);
    Attenuation a{att.x,att.y,att.z};
    m_drawData->lightManager.addValue(new SpotLight(point,dir,inner,outer,color,a));

}

void ParserRogue::processLightPoint(TOKENS list){

    Vector3 point = getVector3(list,2,Vector3(1));
    Vector3 color = getVector3(list,5,Vector3(1));
    Point3 att = getVector3(list,8,attenuation_default);
    Attenuation a{att.x,att.y,att.z};
    m_drawData->lightManager.addValue(new PointLight(point,color,a));
}

void ParserRogue::processMaterial(TOKENS list){
  std::string m = getString(list,1);
  if(current_object!= nullptr){
      current_object->setMaterial(m);
    }
}

void ParserRogue::processTranslate(TOKENS list) {
  Vector3 t = getVector3(list,1,Vector3(0));
  if(current_object!= nullptr){
      current_object->translate(t);
    }
}

void ParserRogue::processRotate(TOKENS list){
  float a = getFloat(list,1,0);
  Vector3 t = getVector3(list,2,Vector3(0));
  if(current_object!= nullptr){
      current_object->rotate(a,t);
    }
}

void ParserRogue::processScale(TOKENS list){
  Vector3 t = getVector3(list,1,Vector3(0));
  if(current_object!= nullptr){
      current_object->scale(t);
    }
}

void ParserRogue::processFileLoader(TOKENS list, std::string path){

    std::string f = getString(list,1);
    Log(logInfo) << "[RogueLoader] Load file: " << path + "/" + f;
    m_drawData->fileLoaderManager->loadFile(m_drawData , path + "/" + f,mode_load);
    if(mode_load == OPAQUE_MODE){
        if(m_drawData->objectManager.getValues().size() > 0) current_object = m_drawData->objectManager.getLastValue();
    }else{
        if(m_drawData->objectManager.getValues2().size() > 0) current_object = m_drawData->objectManager.getLastValue2();
    }
}

void ParserRogue::processDefaultMaterial(TOKENS list) {
  defaultMaterial = getString(list,1,"default");
}

void ParserRogue::processCameraSettings(TOKENS list) {
  Vector3 position;
  Vector3 lookAt;
  float fov;

  position = getVector3(list,1,Vector3(0,0,5));
  lookAt = getVector3(list,4,Vector3(0,0,-1));
  fov = getFloat(list,7,45);

  m_drawData->cameraManager.addPoint(position,lookAt,fov);

}

void ParserRogue::processCreateMaterial(TOKENS list, std::string path) {
    Log(logWarning) << "TOKENS : createMaterial, this version of the token is temporary, it will soon be modified";

    std::string id = getString(list,1);
    std::string mode = getString(list,2,"Kd");
    std::string texKd;
    Vector4 Kd;
    Material *mat;
    int nbArg = 3;
    if(mode=="Kd"){
        Kd = getVector4(list,3,Vector4(0.5,0.5,0.5,1));
        mat = new Material(Kd);
        nbArg += 3;
    }else{
        texKd = path + "/" + getString(list,3);
        mat = new Material(texKd);
        nbArg++;
    }
    std::string shader = getString(list,nbArg);
    mat->setShader(shader);
    m_drawData->materialManager.addValue(id,mat);

}

void ParserRogue::processCreateShader(TOKENS list, std::string path) {
    std::string id = getString(list,1);
    std::string vs = path + "/" + getString(list,2);
    std::string fs = path + "/" + getString(list,3);

    m_drawData->shaderManager.addValue(id,new Shader(vs,fs));

}

void ParserRogue::processOpaqueObject() {
    mode_load = OPAQUE_MODE;
}

void ParserRogue::processTransparentObject() {
    mode_load = TRANSPARENT_MODE;
}

void ParserRogue::addObject(Object *o, std::string id) {
    if(mode_load == OPAQUE_MODE){
        m_drawData->objectManager.addValue(o,id);
    }else{
        m_drawData->objectManager.addValue2(o,id);
    }
}
