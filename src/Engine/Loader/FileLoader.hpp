//
// Created by pierre on 03/08/18.
//

#ifndef ROGUE_FILELOADER_HPP
#define ROGUE_FILELOADER_HPP

/**
 * \file FileLoader.hpp
 * \brief Mother class for a file loader
 */

#include <vector>
#include <string>
#include <src/Engine/Renderer/DrawData.hpp>

class Scene;
enum Mode_load{OPAQUE_MODE, TRANSPARENT_MODE};

class FileLoader {
public:


    FileLoader();
    virtual ~FileLoader(){}

    virtual std::vector<std::string> getFormatManaged() = 0;
    virtual void loadModel(DrawData *d, std::string path, Mode_load mode_load = OPAQUE_MODE) = 0;

};


#endif //ROGUE_FILELOADER_HPP
