//
// Created by pierre on 29/07/18.
//

#include "AssimpLoader.hpp"

#include <Core/Log/Log.hpp>
#include <src/Core/Util/StringUtils.hpp>
#include <src/Gui/Scene.hpp>

#define IDA "[AssimpLoader] "

AssimpLoader::AssimpLoader(): FileLoader(){
    Log(logInfo) << IDA << "AssimpLoader initialization ";
}


std::vector<std::string> AssimpLoader::getFormatManaged(){
    std::string format;
    std::vector<std::string> formats;
    Assimp::Importer importer;
    importer.GetExtensionList(format);
    formats = StringUtils::splitString(format, ';');
    return formats;
}

void AssimpLoader::loadModel(DrawData *d, std::string path, Mode_load mode_load) {
    m_DrawData=d;
    directory = path.substr(0, path.find_last_of('/'));
    std::string name = path.substr(path.find_last_of('/')+1);
    fileType = path.substr(path.find_last_of('.'));
    Log(logDebug) << "Loading model ..." << fileType;
    Assimp::Importer import;
    const aiScene *scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode){
        Log(logError) << IDA << import.GetErrorString();
        return;
    }
    processNode(scene->mRootNode, scene);

    if(mode_load == OPAQUE_MODE){
        m_DrawData->objectManager.addValue(getObject(),name);
    }else{
        m_DrawData->objectManager.addValue2(getObject(),name);
    }
    m_elements.clear();
    Log(logInfo) << IDA << "Done !";
}

void AssimpLoader::processNode(aiNode *node, const aiScene *scene){
    for(unsigned int i = 0; i < node->mNumMeshes; i++){

        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];


        aiMatrix4x4 transform = scene->mRootNode->mTransformation * node->mTransformation;

        m_elements.push_back(processElement(mesh, scene, transform));

    }

    for(unsigned int i = 0; i < node->mNumChildren; i++){
        processNode(node->mChildren[i], scene);
    }
}


Element* AssimpLoader::processElement(aiMesh *mesh, const aiScene *scene, aiMatrix4x4 transform) {

    Mesh m = processMesh(mesh,transform);

    m.m_vertices_init = m.m_vertices;//TODO Better solution
    processBones(mesh,m);
    Element *e = new Element(m);

    std::string nameMaterial = processMaterial(mesh,scene);
    e->setMaterial(nameMaterial);



    return e;
}

Mesh AssimpLoader::processMesh(aiMesh *mesh, aiMatrix4x4 transform) {

    Mesh m;
    VectorPoint3 vertices;
    VectorNormal normals;
    VectorTriangles triangles;
    VectorPoint3 texCoord;

    std::map<Triplet, unsigned int> uniqueMap;
    std::vector<unsigned int> duplicateVec;
    bool texture = false;

    for(unsigned int i = 0; i < mesh->mNumVertices; i++){

        aiVector3D v = transform*mesh->mVertices[i];
        //m.m_vertices.push_back(setVec3(mesh->mVertices[i].x,mesh->mVertices[i].y,mesh->mVertices[i].z));
        Point3 p(v.x,v.y,v.z);

        texture = mesh->mTextureCoords[0];

        if(!texture){

            auto it = uniqueMap.find(p);

            if(it == uniqueMap.end()){
                duplicateVec.push_back(castui(m.m_vertices.size()));
                uniqueMap[p] = castui(m.m_vertices.size());
                m.m_vertices.push_back(p);

                if(mesh->mNormals){
                    m.m_normals.push_back(setVec3(mesh->mNormals[i].x,mesh->mNormals[i].y,mesh->mNormals[i].z));
                }

                m.m_texCoord.push_back(Point3(0));

            }else{
                duplicateVec.push_back(it->second);
            }

        }else{
            m.m_vertices.push_back(p);

            if(mesh->mNormals){
                m.m_normals.push_back(setVec3(mesh->mNormals[i].x,mesh->mNormals[i].y,mesh->mNormals[i].z));
            }
            m.m_texCoord.push_back(Point3(mesh->mTextureCoords[0][i].x,mesh->mTextureCoords[0][i].y,0));
        }

    }
    for(unsigned int i = 0; i < mesh->mNumFaces; i++){
        aiFace face = mesh->mFaces[i];
        for(unsigned j = 0; j < face.mNumIndices;j+=3){ //TODO improve ...
            if(!texture){
                unsigned int
                        a=duplicateVec[face.mIndices[j]],
                        b=duplicateVec[face.mIndices[j+2]],
                        c=duplicateVec[face.mIndices[j+1]];
                m.setTriangle((setVec3(a,b,c)));
            }else{
                m.setTriangle(setVec3(face.mIndices[j],face.mIndices[j+2],face.mIndices[j+1]));
            }
        }
    }

    if(!mesh->mNormals) m.autoNormals();
    return m;
}

Matrix4 toMatrix4(aiMatrix4x4 m){
    Matrix4 matrix;
    matrix[0][0] = m.a1; matrix[0][1]=m.b2; matrix[0][2] = m.c3; matrix[0][3]=m.d4;
    matrix[1][0] = m.a1; matrix[1][1]=m.b2; matrix[1][2] = m.c3; matrix[1][3]=m.d4;
    matrix[2][0] = m.a1; matrix[2][1]=m.b2; matrix[2][2] = m.c3; matrix[2][3]=m.d4;
    matrix[3][0] = m.a1; matrix[3][1]=m.b2; matrix[3][2] = m.c3; matrix[3][3]=m.d4;
    return matrix;
}

void AssimpLoader::processBones(aiMesh *, Mesh &) {
    /*m.boneWeight.resize(m.m_vertices.size());

    for(uint i = 0; i < mesh->mNumBones; i++){
        Bone *bone=new Bone;
        displayMat4(bone->restPose);
        m.boneList.push_back(bone);
        std::vector<std::pair<int,int>> weightForBones;

        for(uint j = 0; j < mesh->mBones[i]->mNumWeights; j++){
            uint vertex = mesh->mBones[i]->mWeights[j].mVertexId;
            float weight = mesh->mBones[i]->mWeights[j].mWeight;
            m.boneWeight[vertex].push_back(std::pair<int,float>(i,weight));
        }
    }*/


}

std::string AssimpLoader::processMaterial(aiMesh *mesh, const aiScene *scene){
    aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
    std::vector<Texture*> map;
    Material *m = new Material(Vector3(0.5,0.5,0.5));

    map = loadMaterialTextures(material, aiTextureType_DIFFUSE);
    if(map.size()>=1) m->setKdTexture(map[0]);

    map = loadMaterialTextures(material, aiTextureType_SPECULAR);
    if(map.size()>=1) m->setKsTexture(map[0]);

    std::string nameMaterial = material->GetName().C_Str();
    nameMaterial = m_DrawData->materialManager.addValueForce(nameMaterial,m);

    return nameMaterial;
}

std::vector<Texture*> AssimpLoader::loadMaterialTextures(aiMaterial *mat, aiTextureType type) {

    std::vector<Texture*> textures;
    for(unsigned int i=0; i<mat->GetTextureCount(type); i++){
        aiString str;
        mat->GetTexture(type,i,&str);
        bool skip = false;

        std::string s = str.C_Str();
        s = directory+'/'+s.substr(s.find_last_of('\\')+1,s.size());
        for(unsigned int j=0; j<textures_loaded.size(); j++){
            if(textures_loaded[j]->m_filename == s){
                textures.push_back(textures_loaded[j]);
                skip=true;
                break;
            }
        }

        if(!skip){
            Texture *texture = new Texture(s);

            textures.push_back(texture);
            textures_loaded.push_back(texture);
        }

    }

    return textures;

}

Object * AssimpLoader::getObject(){
    if(m_elements.size()==0) Log(logWarning) << IDA << "Trying to get an object with no element ...";
    return new Object(m_elements);
}

void AssimpLoader::displayNote() {
    Log(logInfo) << "(AssimpLoader take only the first texture by type (Diffuse, Specular ...))\n" <<
                    "(And take only Diffuse ans specular texture ...)";
}

// Taken from radium
Triplet::Triplet(const Vector3 &v) : m_v(v)
{}

bool Triplet::operator==(const Triplet &t) const
{
    float d = glm::length(m_v - t.m_v);
    return d<0.00001f; //TODO see if better solution ...
}

bool Triplet::operator<(const Triplet &t) const
{
    if (m_v[0] < t.m_v[0])
    {
        return true;
    }
    if (abs(m_v[0] - t.m_v[0]) < EPSILON)
    {
        if (m_v[1] < t.m_v[1])
        {
            return true;
        }
        if (abs(m_v[1]-t.m_v[1]) < EPSILON)
        {
            return (m_v[2] < t.m_v[2]);
        }
    }
    return false;
}
