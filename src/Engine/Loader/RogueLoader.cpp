//
// Created by pierre on 03/08/18.
//

#include <src/Core/Util/FileReader.hpp>
#include <src/Core/Log/Log.hpp>
#include <src/Core/Util/StringUtils.hpp>
#include "RogueLoader.hpp"
#include "ParserRogue.hpp"

#define IDL "[RogueLoader] "

RogueLoader::RogueLoader() : FileLoader(){
}


std::vector<std::string> RogueLoader::getFormatManaged(){
  std::vector<std::string> formats;
  formats.push_back("*.rogue");
  return formats;
}

void RogueLoader::Error(std::string mes, int n){
  Log(logError) << IDL << "(l." << n << ") " << mes;
}

void RogueLoader::loadModel(DrawData *d, std::string path, Mode_load) {
  m_drawData=d;
  m_parserRogue=new ParserRogue(d);
  loadCustomData();
  processFile(path);
}

void RogueLoader::processFile(std::string path) {
  Log(logInfo) << IDL << "Try to parse :" << path;
  directory = path.substr(0, path.find_last_of('/'));
  std::string content = FileReader::getFileWithString(path);
  auto lines = StringUtils::splitString(content,'\n');
  int number_line = 1;
  bool again = true;
  bool first = true;
  std::string token;
  for(auto line : lines){
      auto dec = StringUtils::splitStringForce(line,';');               // Use ';' as '/n', separate each "line"
      for(auto d:dec) {
          auto dec2 = StringUtils::splitStringForce(d,',');             // Use "," as a separator but use same token as previous line
          first = true;
          for(auto d2:dec2){
              auto words = StringUtils::splitStringForce(d2, ' ','\t');      // Separate token and parameters
              if(first){
                  if(words.size()>0) token = words[0];
                  first=false;
                }else{
                  words.insert(words.begin(), token);
                }

              if(words.size()>0) again = processLine(words,number_line);
            }
        }
      if(!again) break;
      number_line++;
    }
  if(label!="") Log(logError) << IDL << "label \"" << label << "\" was not found";
  Log(logInfo) << IDL << "Parsing Done!";
  cleanUp();
}

void RogueLoader::cleanUp() {
  m_parserRogue->cleanUp();
  label="";
  directory="";
}


void RogueLoader::processShape(std::vector<std::string> line, int n){
  std::string shape;
  if(line.size()<=1) shape = "No shape given ...";
  else shape = line[1];

  if(shape == "sphere"){
      m_parserRogue->processShapeSphere(line);
    }else if(shape == "cube") {
        m_parserRogue->processShapeCube(line);
    }else if(shape == "grid") {
        m_parserRogue->processShapeGrid(line);
    }else if(shape== "triangleShape"){
        m_parserRogue->processShapeTriangleShape(line);
    }else{
      Error("Shape unknown :" + shape, n);
    }
}

void RogueLoader::processLight(std::vector<std::string> line, int n){
  std::string light;
  if(line.size()<=1) light = "No light given ...";
  else light = line[1];
  if(light == "dir"){
      m_parserRogue->processLightDir(line);
    }else if(light == "point"){
      m_parserRogue->processLightPoint(line);
    }else if(light == "spot") {
      m_parserRogue->processLightSpot(line);
    }else{
      Error("Light unknown :" + light, n);
    }
}

bool RogueLoader::processLine(std::vector<std::string> line, int n){

  std::string token = line[0];
  if(label!=""){
      if(label + ":" == token) label = "";
      return true;
    }
    bool token_unknown = false;
    switch(token.c_str()[0]){
        case 'a':
            if(token == "attenuation"){
                m_parserRogue->processAttenuation(line);
            }else token_unknown = true;
            break;
        case 'c':
            if(token == "createMaterial"){
                m_parserRogue->processCreateMaterial(line,directory);
            }else if(token == "createShader") {
                m_parserRogue->processCreateShader(line,directory);
            }else if(token == "camera"){
                m_parserRogue->processCameraSettings(line);
            }else token_unknown = true;
            break;
        case 'd':
            if(token == "defaultMaterial"){
                m_parserRogue->processDefaultMaterial(line);
            }else token_unknown = true;
            break;
        case 'E':
            if(token == "END"){
                return false;
            }else token_unknown=true;
            break;
        case 'G':
            if(token == "GOTO"){
                if(line.size()<2){
                    Error("GOTO without label",n);
                    label = "";
                } else label = line[1];
            }else token_unknown=true;
            break;
        case 'l':
            if(token == "loadfile"){
                m_parserRogue->processFileLoader(line,directory);
            }else if(token == "light"){
                processLight(line,n);
            }else token_unknown=true;
            break;
        case 'm':
            if(token == "material"){
                m_parserRogue->processMaterial(line);
            }else token_unknown=true;
            break;
        case 'o':
            if(token == "opaqueObject"){
                m_parserRogue->processOpaqueObject();
            }else token_unknown = true;
            break;
        case 'r':
            if(token == "rotate"){
                m_parserRogue->processRotate(line);
            }else token_unknown=true;
            break;
        case 's':
            if(token == "shape"){
                processShape(line,n);
            }else if(token == "scale"){
                m_parserRogue->processScale(line);
            }else token_unknown=true;
            break;
        case 't':
            if(token == "translate"){
                m_parserRogue->processTranslate(line);
            }else if(token == "transparentObject") {
                m_parserRogue->processTransparentObject();
            }else token_unknown=true;
            break;
        case '/': return true;
        default: token_unknown=true; break;
    }
  if(token_unknown) token_unknown = customToken(line,n);
  if(token_unknown){
      Error("Token unknown :" + token , n);
    }
  return true;
}

void RogueLoader::processCamera(std::vector<std::string> line, int){
  std::string camera;
  if(line.size()<=1) camera = "No shape given ...";
  else camera = line[1];
  m_parserRogue->processCameraSettings(line);
}
bool RogueLoader::customToken(std::vector<std::string>, int) {
  // Redefine this on child class
  return true;
}

void RogueLoader::loadCustomData(){
  // Redefine this on child class
}

