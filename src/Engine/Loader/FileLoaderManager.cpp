//
// Created by pierre on 03/08/18.
//

#include "FileLoaderManager.hpp"
#include "AssimpLoader.hpp"
#include "RogueLoader.hpp"
#include <regex>

FileLoaderManager::FileLoaderManager() : ManagerList<FileLoader*>("FileLoaderManager") {

    addValue(new AssimpLoader());
    addValue(new RogueLoader());
}


void FileLoaderManager::loadFile(DrawData *d, std::string file, Mode_load mode_load){

    Log(logInfo) << IDM << "Try to load :" << file;
    for(auto i:getValues()){
        auto formats = i->getFormatManaged();
        for(auto format:formats){
            format.erase(0,1);
            format.insert(1,".*\\.");
            if(std::regex_match(file,std::regex(format))){
                i->loadModel(d,file, mode_load);
                return;
            }
        }
    }
    Log(logError) << "We can't load file (format not managed) : " << file;
}