//
// Created by pierre on 03/08/18.
//

#ifndef ROGUE_FILELOADERMANAGER_HPP
#define ROGUE_FILELOADERMANAGER_HPP

/**
 * \file FileLoaderManager.hpp
 * \brief Manager for the different file loader
 */


#include "FileLoader.hpp"
#include <Core/Managers/ManagerList.hpp>

class FileLoaderManager : public ManagerList<FileLoader*> {
public:
    FileLoaderManager();

    void loadFile(DrawData *d, std::string file, Mode_load mode_load = OPAQUE_MODE);

};


#endif //ROGUE_FILELOADERMANAGER_HPP
