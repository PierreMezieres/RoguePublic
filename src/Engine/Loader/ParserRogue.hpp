//
// Created by pierre on 08/08/18.
//

#ifndef ROGUE_PARSER_HPP
#define ROGUE_PARSER_HPP

/**
 * \file ParserRogue.hpp
 * \brief Token action for *.rogue file
 */

#include <string>
#include <vector>
#include <map>
#include <Core/Macros.hpp>
#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Gui/Scene.hpp>

using TOKENS = std::vector<std::string>;

float getFloat(TOKENS list, unsigned int n, float d = 0.f);
Vector4 getVector4(TOKENS list, unsigned int n, Vector4 d = Vector4(0));
Vector3 getVector3(TOKENS list, unsigned int n, Vector3 d = Vector3(0));
Vector2 getVector2(TOKENS list, unsigned int n, Vector2 d = Vector2(0));
std::string getString(TOKENS list, unsigned int n, std::string d = "default");

class ParserRogue
{
public:
    ParserRogue(DrawData *d);

    void processShapeSphere(TOKENS list);
    void processShapeCube(TOKENS list);
    void processShapeGrid(TOKENS list);
    void processShapeTriangleShape(TOKENS list);

    void processAttenuation(TOKENS list);
    void processLightDir(TOKENS list);
    void processLightSpot(TOKENS list);
    void processLightPoint(TOKENS list);
    void processMaterial(TOKENS list);
    void processTranslate(TOKENS list);
    void processRotate(TOKENS list);
    void processScale(TOKENS list);
    void processFileLoader(TOKENS list, std::string path);
    void processDefaultMaterial(TOKENS list);

    void processCameraSettings(TOKENS list);


    void processCreateMaterial(TOKENS list, std::string path);
    void processCreateShader(TOKENS list, std::string path);

    void processOpaqueObject();
    void processTransparentObject();

    Mode_load mode_load = OPAQUE_MODE;


    DrawData *m_drawData;

    Object *current_object=nullptr;
    std::string defaultMaterial = "default";
    Vector3 attenuation_default = Vector3(1,0.0009,0.000032);

    void cleanUp();

private:
    void addObject(Object *o, std::string id);
};


#endif //ROGUE_PARSER_HPP
