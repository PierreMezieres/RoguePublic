//
// Created by pierre on 24/07/18.
//

#include "PointLight.hpp"

PointLight::PointLight(Point3 position, Color color, Attenuation attenuation) : Light(POINT_LIGHT, color), m_attenuation{attenuation}, m_position{position}
{

}

void PointLight::bind(Shader *shader, int number){
    Light::bind(shader,number);
    std::string num = Construct_number(number);
    shader->setVec3(num + "pointLight.position",m_position);
    shader->setFloat(num + "pointLight.attenuation.constant",m_attenuation.constant);
    shader->setFloat(num + "pointLight.attenuation.linear",m_attenuation.linear);
    shader->setFloat(num + "pointLight.attenuation.quadratic",m_attenuation.quadratic);
}

void PointLight::bindSimple(Shader* shader) {
    Light::bindSimple(shader);
    shader->setVec3("light.pointLight.position",m_position);
    shader->setFloat("light.pointLight.attenuation.constant",m_attenuation.constant);
    shader->setFloat("light.pointLight.attenuation.linear",m_attenuation.linear);
    shader->setFloat("light.pointLight.attenuation.quadratic",m_attenuation.quadratic);
}

void PointLight::bindSingle(Shader *shader){
    Light::bindSingle(shader);
    shader->setVec3("light.pointLight.position",m_position);
    shader->setFloat("light.pointLight.attenuation.constant",m_attenuation.constant);
    shader->setFloat("light.pointLight.attenuation.linear",m_attenuation.linear);
    shader->setFloat("light.pointLight.attenuation.quadratic",m_attenuation.quadratic);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_shadowMap.texture);
    shader->setInt("shadowMap", 3);
}

void PointLight::displayInfo() {
    Log(logInfo) << "| ---------------- Light ----------------";
    Log(logInfo) << "| - Type : PointLight";
    Log(logInfo) << "| - Color: " << m_color.x << " " << m_color.y << " " << m_color.z;
    Log(logInfo) << "| - Position: " << m_position.x << " " << m_position.y << " " << m_position.z;
}

Point3 PointLight::getPosition() {
    return m_position;
}
