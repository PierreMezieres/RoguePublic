//
// Created by pierre on 24/07/18.
//

#include <src/Core/Log/Log.hpp>
#include "SpotLight.hpp"


SpotLight::SpotLight(Point3 position,Vector3 direction, float innerAngle, float outerAngle, Color color, Attenuation attenuation):
        Light(SPOT_LIGHT, color), m_attenuation{attenuation}, m_position{position}, m_direction{direction},
        m_innerAngle{innerAngle}, m_outerAngle{outerAngle} {

}

/*SpotLight::SpotLight(Point3 position,Point3 from, Point3 to, Color color):
        Light(SPOT_LIGHT, color), m_position{position}, m_direction{glm::normalize(to-from)}{

}*/

void SpotLight::bind(Shader *shader, int number) {
    Light::bind(shader,number);
    std::string num = Construct_number(number);
    shader->setVec3(num + "spotLight.position",m_position);
    shader->setVec3(num + "spotLight.direction",m_direction);
    shader->setFloat(num + "spotLight.innerAngle",glm::cos(glm::radians(m_innerAngle)));
    shader->setFloat(num + "spotLight.outerAngle",glm::cos(glm::radians(m_outerAngle)));
    shader->setFloat(num + "spotLight.attenuation.constant",m_attenuation.constant);
    shader->setFloat(num + "spotLight.attenuation.linear",m_attenuation.linear);
    shader->setFloat(num + "spotLight.attenuation.quadratic",m_attenuation.quadratic);
}

void SpotLight::bindSimple(Shader* shader) {
    Light::bindSingle(shader);
    shader->setVec3("light.spotLight.position",m_position);
    shader->setVec3("light.spotLight.direction",m_direction);
    shader->setFloat("light.spotLight.innerAngle",glm::cos(glm::radians(m_innerAngle)));
    shader->setFloat("light.spotLight.outerAngle",glm::cos(glm::radians(m_outerAngle)));
    shader->setFloat("light.spotLight.attenuation.constant",m_attenuation.constant);
    shader->setFloat("light.spotLight.attenuation.linear",m_attenuation.linear);
    shader->setFloat("light.spotLight.attenuation.quadratic",m_attenuation.quadratic);
}

void SpotLight::bindSingle(Shader * shader){
  Light::bindSingle(shader);
  shader->setVec3("light.spotLight.position",m_position);
  shader->setVec3("light.spotLight.direction",m_direction);
  shader->setFloat("light.spotLight.innerAngle",glm::cos(glm::radians(m_innerAngle)));
  shader->setFloat("light.spotLight.outerAngle",glm::cos(glm::radians(m_outerAngle)));
  shader->setFloat("light.spotLight.attenuation.constant",m_attenuation.constant);
  shader->setFloat("light.spotLight.attenuation.linear",m_attenuation.linear);
  shader->setFloat("light.spotLight.attenuation.quadratic",m_attenuation.quadratic);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, m_shadowMap.texture);
    shader->setInt("shadowMap", 3);
}

void SpotLight::displayInfo() {
    Log(logInfo) << "| ---------------- Light ----------------";
    Log(logInfo) << "| - Type : SpotLight";
    Log(logInfo) << "| - Color: " << m_color.x << " " << m_color.y << " " << m_color.z;
    Log(logInfo) << "| - Position: " << m_position.x << " " << m_position.y << " " << m_position.z;
    Log(logInfo) << "| - Direction: " << m_direction.x << " " << m_direction.y << " " << m_direction.z;
}

Point3 SpotLight::getPosition() {
    return m_position;
}

Vector3 SpotLight::getDirection() {
    return m_direction;
}

float SpotLight::getOuterAngle() {
    return m_outerAngle;
}

float SpotLight::getInnerAngle() {
    return m_innerAngle;
}
