//
// Created by pierre on 24/07/18.
//

#ifndef ROGUE_DIRLIGHT_HPP
#define ROGUE_DIRLIGHT_HPP

/**
 * \file DirLight.hpp
 * \brief Simple directionnal light
 */


#include "Light.hpp"
#include <Engine/Texture/ShadowMapBuffer.hpp>

class DirLight : public Light {
public:

    DirLight(Vector3 direction = Vector3(1,1,1), Color color = Color(1,1,1));
    //DirLight(Point3 from = Point3(1,1,1), Point3 to = Point3(0,0,0), Color color = Color(1,1,1));

    void bind(Shader *shader, int number) override;
    void bindSingle(Shader * shader) override;
    void bindSimple(Shader* shader) override;

        void displayInfo() override;

    Vector3 getDirection();

    ShadowMapBuffer  m_shadowMap;
private:
    Vector3 m_direction;
    int m_flag;

};


#endif //ROGUE_DIRLIGHT_HPP
