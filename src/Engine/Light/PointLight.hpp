//
// Created by pierre on 24/07/18.
//

#ifndef ROGUE_POINTLIGHT_HPP
#define ROGUE_POINTLIGHT_HPP

/**
 * \file PointLight.hpp
 * \brief Simple point light
 */

#include "Light.hpp"
#include <./Engine/Texture/ShadowMapBufferPointLight.hpp>

class PointLight : public Light {
public:
    PointLight(Point3 position = Point3(0,0,0), Color color = Color(1,1,1), Attenuation attenuation = Attenuation{1,0.09,0.032});


    void bind(Shader *shader, int number) override;
    void bindSingle(Shader * shader) override;
    void bindSimple(Shader* shader) override;

    void displayInfo();
    Point3 getPosition();

    ShadowMapBufferPointLight m_shadowMap;
    Matrix4 m_lightSpaceMats[6];

private:
    Attenuation m_attenuation;
    Point3 m_position;


};




#endif //ROGUE_POINTLIGHT_HPP
