//
// Created by pierre on 22/07/18.
//

#ifndef ROGUE_LIGHT_HPP
#define ROGUE_LIGHT_HPP

/**
 * \file Light.hpp
 * \brief Mother class for light
 */

#include <Core/Macros.hpp>
#include <src/Engine/Shader/Shader.hpp>

/**
 * \struct Attenuation
 * \brief Attenuation of light
 */
struct Attenuation{
    float constant;
    float linear;
    float quadratic;
};


/**
 * \class Light
 * \brief Mother class for light
 */
class Light {
public:

    virtual ~Light(){}

    enum light_type{POINT_LIGHT = 0, SPOT_LIGHT = 1, DIR_LIGHT = 2};

    Light(light_type type, Color color = Color(1,1,1));

    virtual void bind(Shader *shader, int number) = 0;
    virtual void bindSingle(Shader *shader) = 0;
    virtual void bindSimple(Shader* shader) = 0;

    /* Temporary function, until the rendering is done in an other way
     * Not just give to shader all the light in one time */
    std::string Construct_number(int n);

    void setLightSpace(Matrix4 lsm);
    Matrix4 * getLightSpace(void);

    light_type m_type;
    Color m_color;

    virtual void displayInfo() = 0;
private:
    Matrix4 m_lightSpace;
};


#endif //ROGUE_LIGHT_HPP
