//
// Created by pierre on 22/07/18.
//

#include "LightManager.hpp"
#include "DirLight.hpp"

LightManager::LightManager(): ManagerList<Light*>("LightManager"){
    defaultLight = new DirLight(Vector3(-1));
}

void LightManager::bind(Shader *shader) {
    int cpt = 0;
    for(auto l: getValues()){
        l->bind(shader,cpt);
        cpt++;
    }
    if(cpt==0){
        defaultLight->bind(shader,cpt);
        cpt++;
    }
    shader->setInt("numberLights",cpt);
}

void LightManager::displayInfo() {
    Log(logInfo) << "| - Lights number: " << getValues().size();
}

void LightManager::displayInfo2() {
    if(getValues().size()==0){
        Log(logInfo) << "|";
        Log(logInfo) << "| There are no lights ...";
        Log(logInfo) << "|";
    }
    for(auto i:getValues()){
        i->displayInfo();
    }
}

/*
std::vector<Light*> LightManager::getLights(){
    return lightList;
}

void LightManager::addLight(Light *light) {
    lightList.push_back(light);
}*/