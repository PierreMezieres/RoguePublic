//
// Created by pierre on 24/07/18.
//

#ifndef ROGUE_SPOTLIGHT_HPP
#define ROGUE_SPOTLIGHT_HPP

/**
 * \file SpotLight.hpp
 * \brief Simple spot light
 */


#include "Light.hpp"
#include <./Engine/Texture/ShadowMapBuffer.hpp>

class SpotLight : public Light {
public:
    SpotLight(Point3 position = Point3(0,0,0),Vector3 direction = Vector3(1,1,1),
            float innerAngle = 20, float outerAngle = 25, Color color = Color(1,1,1), Attenuation attenuation = Attenuation{1,0.09,0.032});

    //SpotLight(Point3 position = Point3(0,0,0),Point3 from = Point3(1,1,1), Point3 to = Point3(0,0,0), Color color = Color(1,1,1));

    void bind(Shader *shader, int number) override;
    void bindSingle(Shader * shader) override;
    void bindSimple(Shader* shader) override;


    void displayInfo();
    Point3 getPosition();
    Vector3 getDirection();
    float getOuterAngle();
    float getInnerAngle();

    ShadowMapBuffer  m_shadowMap;


private:
    Attenuation m_attenuation;
    Point3 m_position;
    Vector3 m_direction;

    float m_innerAngle;
    float m_outerAngle;

    int m_flag;

};



#endif //ROGUE_SPOTLIGHT_HPP
