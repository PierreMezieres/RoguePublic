//
// Created by pierre on 27/01/19.
//

#ifndef ROGUE_MANAGER2LIST_HPP
#define ROGUE_MANAGER2LIST_HPP

#include <Core/Managers/ManagerBoss.hpp>
template<typename T>
class Manager2List : public ManagerBoss<T> {
public:
    Manager2List(std::string nameM);

    int addValue(T value);
    void remValue(int index);
    void remValue(T value);
    void remAll();
    T getValue(int index);
    T getLastValue();
    int addValue2(T value);
    void remValue2(int index);
    void remValue2(T value);
    void remAll2();
    T getValue2(int index);
    T getLastValue2();

    std::vector<T> getValues();
    std::vector<T> getValues2();

private:


    std::vector<T> m_listValue;
    std::vector<T> m_listValue2;
};

template<typename T>
Manager2List<T>::Manager2List(std::string nameM): ManagerBoss<T>(MODE_INDEX,nameM) {

}



template<typename T>
int Manager2List<T>::addValue(T value){
    m_listValue.push_back(value);
    return m_listValue.size()-1;
}

template<typename T>
void Manager2List<T>::remValue(int index) {
    if (index >= (int)m_listValue.size()) {
        Log(logWarning) << IDM << "Can't remove value with index: " << index << " (size:" << m_listValue.size() <<")";
    } else {
        m_listValue.erase(m_listValue.begin() + index, m_listValue.begin() + index + 1);
    }
}

template<typename T>
void Manager2List<T>::remValue(T value) {
    //TODO improve this function
    int j=0;
    for(auto i:m_listValue){
        if(i==value) remValue(j);
        j++;
    }
}

template<typename T>
void Manager2List<T>::remAll() {
    m_listValue.erase(m_listValue.begin(),m_listValue.end());
}

template<typename T>
T Manager2List<T>::getValue(int index){
    if (index >= (int)m_listValue.size()) {
        Log(logWarning) << IDM << "Can't get value with index: " << index << " (size:" << m_listValue.size() <<")";
    } else {
        return m_listValue[index];
    }
    return m_listValue[0];
}

template<typename T>
T Manager2List<T>::getLastValue(){
    if (0 == (int)m_listValue.size()) {
        Log(logWarning) << IDM << "Can't get last value, because list is empty ";
    } else {
        return m_listValue[m_listValue.size()-1];
    }
    return m_listValue[0];
}

template<typename T>
std::vector<T> Manager2List<T>::getValues(){
    return m_listValue;
}

template<typename T>
int Manager2List<T>::addValue2(T value){
    m_listValue2.push_back(value);
    return m_listValue2.size()-1;
}

template<typename T>
void Manager2List<T>::remValue2(int index) {
    if (index >= (int)m_listValue2.size()) {
        Log(logWarning) << IDM << "Can't remove value with index: " << index << " (size:" << m_listValue2.size() <<")";
    } else {
        m_listValue2.erase(m_listValue2.begin() + index, m_listValue2.begin() + index + 1);
    }
}

template<typename T>
void Manager2List<T>::remValue2(T value) {
    //TODO improve this function
    int j=0;
    for(auto i:m_listValue2){
        if(i==value) remValue2(j);
        j++;
    }
}

template<typename T>
void Manager2List<T>::remAll2() {
    m_listValue2.erase(m_listValue2.begin(),m_listValue2.end());
}

template<typename T>
T Manager2List<T>::getValue2(int index){
    if (index >= (int)m_listValue2.size()) {
        Log(logWarning) << IDM << "Can't get value with index: " << index << " (size:" << m_listValue2.size() <<")";
    } else {
        return m_listValue2[index];
    }
    return m_listValue2[0];
}

template<typename T>
T Manager2List<T>::getLastValue2(){
    if (0 == (int)m_listValue2.size()) {
        Log(logWarning) << IDM << "Can't get last value, because list is empty ";
    } else {
        return m_listValue2[m_listValue2.size()-1];
    }
    return m_listValue2[0];
}

template<typename T>
std::vector<T> Manager2List<T>::getValues2(){
    return m_listValue2;
}
#endif //ROGUE_MANAGER2LIST_HPP
