//
// Created by pierre on 26/07/18.
//

#ifndef ROGUE_MANAGERLIST_HPP
#define ROGUE_MANAGERLIST_HPP

#include <Core/Managers/ManagerBoss.hpp>

/**
 *
 * \file ManagerList.hpp
 * \brief Manage data as a list
 *
 */

template<typename T>
class ManagerList : public ManagerBoss<T> {
public:
    ManagerList(std::string nameM);

    int addValue(T value);
    void remValue(int index);
    void remValue(T value);
    void remAll();
    T getValue(int index);
    T getLastValue();

    std::vector<T> getValues();

private:


    std::vector<T> m_listValue;
};

template<typename T>
ManagerList<T>::ManagerList(std::string nameM): ManagerBoss<T>(MODE_INDEX,nameM) {

}



template<typename T>
int ManagerList<T>::addValue(T value){
    m_listValue.push_back(value);
    return m_listValue.size()-1;
}

template<typename T>
void ManagerList<T>::remValue(int index) {
    if (index >= casti(m_listValue.size())) {
        Log(logWarning) << IDM << "Can't remove value with index: " << index << " (size:" << m_listValue.size() <<")";
    } else {
        m_listValue.erase(m_listValue.begin() + index, m_listValue.begin() + index + 1);
    }
}

template<typename T>
void ManagerList<T>::remValue(T value) {
    //TODO improve this function
    int j=0;
    for(auto i:m_listValue){
        if(i==value) remValue(j);
        j++;
    }
}

template<typename T>
void ManagerList<T>::remAll() {
    m_listValue.erase(m_listValue.begin(),m_listValue.end());
}

template<typename T>
T ManagerList<T>::getValue(int index){
    if (index >= casti(m_listValue.size())) {
        Log(logWarning) << IDM << "Can't get value with index: " << index << " (size:" << m_listValue.size() <<")";
    } else {
        return m_listValue[index];
    }
    return m_listValue[0];
}

template<typename T>
T ManagerList<T>::getLastValue(){
    if (0 == casti(m_listValue.size())) {
        Log(logWarning) << IDM << "Can't get last value, because list is empty ";
    } else {
        return m_listValue[m_listValue.size()-1];
    }
    return m_listValue[0];
}

template<typename T>
std::vector<T> ManagerList<T>::getValues(){
    return m_listValue;
}

#endif //ROGUE_MANAGERLIST_HPP
