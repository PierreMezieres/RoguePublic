//
// Created by pierre on 26/07/18.
//

#ifndef ROGUE_MANAGERMAP_HPP
#define ROGUE_MANAGERMAP_HPP

#include <Core/Managers/ManagerBoss.hpp>

/**
 *
 * \file ManagerMap.hpp
 * \brief Manage data as a map
 *
 */

template<typename T>
class ManagerMap : public ManagerBoss<T>{
public:
    ManagerMap(std::string nameM);

    void addValue(std::string name, T value);
    std::string addValueForce(std::string name, T value);
    void remValue(std::string name);
    T getValue(std::string name);

    std::map<std::string, T> getValues();

private:
    std::map<std::string, T> m_mapValue;

};


template<typename T>
ManagerMap<T>::ManagerMap(std::string nameM) : ManagerBoss<T>(MODE_NAME,nameM) {

}


template<typename T>
void ManagerMap<T>::addValue(std::string name, T value){
    if(m_mapValue.find(name) != m_mapValue.end()){
        Log(logWarning) << IDM << "Value already bind with that name: " << name;
    }else{
        m_mapValue[name] = value;
    }
}

template<typename T>
std::string ManagerMap<T>::addValueForce(std::string name, T value) {
    int i=1;
    std::string new_name = name;
    if(m_mapValue.find(name) != m_mapValue.end()){
        new_name = name + std::to_string(i);
        while(m_mapValue.find(new_name) != m_mapValue.end()){
            i++;
            new_name = name + std::to_string(i);
        }
    }
    m_mapValue[new_name] = value;
    return new_name;
}

template<typename T>
void ManagerMap<T>::remValue(std::string name){
    m_mapValue.erase(name);
}

template<typename T>
T ManagerMap<T>::getValue(std::string name){
    if(m_mapValue.find(name) == m_mapValue.end()){
        Log(logError) << IDM << "Can't find value with that name: " << name;
    }else{
        return m_mapValue[name];
    }
    return nullptr;
}

template<typename T>
std::map<std::string, T> ManagerMap<T>::getValues(){
    return m_mapValue;
}


#endif //ROGUE_MANAGERMAP_HPP
