#include "Macros.hpp"
#include <Core/Log/Log.hpp>
#include <iomanip>

float degToRad(float t){
    return (Math_Pi * 2.0f) * (t / 360.f);
}

void displayMat4(Matrix4& mat, std::string say){
    std::stringstream ss;
    ss << "\n   ";
    for(int i=0; i!=4; i++){
        for(int j=0; j!=4; j++){
            ss << std::right << std::setw(13)  << mat[i][j];
        }
        ss << "\n   ";
    }
    Log(logInfo) << say << ss.str();
}

void displayVec3(Vector3& vec){
    Log(logInfo) << "Vec x:" << vec.x << " y:" << vec.y << " z:" << vec.z;
}

std::string Vec3AsString(Vector3 &vec, std::string separator){
    return std::to_string(vec.x) + separator + std::to_string(vec.y) + separator + std::to_string(vec.z);
}

void displayVec4(Vector4& vec){
  Log(logInfo) << "Vec x:" << vec.x << " y:" << vec.y << " z:" << vec.z << " w:"<<vec.w;
}

Vector3 getPositionMat4(Matrix4 mat){
    return Vector3{mat[3][0], mat[3][1], mat[3][2]};
}

void setPositionMat4(Matrix4 &mat, Vector3 vec){
    mat[3][0] = vec.x;
    mat[3][1] = vec.y;
    mat[3][2] = vec.z;
}

void GL_CHECK_ERROR(){
    GLenum err;
    Log(logInfo) << "Check Error OpenGL:";
    while((err = glGetError()) != GL_NO_ERROR){
        Log(logError) << "OpenGL Error:" << err;
    }
}
