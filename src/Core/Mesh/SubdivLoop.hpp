//
// Created by pierre on 25/10/18.
//

#ifndef ROGUE_UTILS_HPP
#define ROGUE_UTILS_HPP

/**
 *
 * \file SubdivLoop.hpp
 * \brief Subdivision loop
 *
 */

#include <Core/Macros.hpp>
#include <src/Engine/Object/Mesh.hpp>

void subdivLoop(MeshOM &mesh, unsigned int n = 1);
void subdivLoop(Mesh &mesh, unsigned int n =1);


#endif //ROGUE_UTILS_HPP
