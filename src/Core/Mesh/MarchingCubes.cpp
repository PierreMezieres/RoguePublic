//
// Created by pierre on 01/11/18.
//

#include <src/Core/Shapes/Grid.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>
#include "MarchingCubes.hpp"

MarchingCubes::MarchingCubes(bool normalPoint,int nx,int ny,int nz):
  m_normalPoint{normalPoint}, NX{castui(nx)}, NY{castui(ny)}, NZ{castui(nz)} {

}

void MarchingCubes::Polygonise(GRIDCELL grid, double isolevel, Mesh &mesh) {
    int i;
    int cubeindex;
    Vector3 vertlist[12];

    cubeindex = 0;
    if (castd(grid.val[0]) < isolevel) cubeindex |= 1;
    if (castd(grid.val[1]) < isolevel) cubeindex |= 2;
    if (castd(grid.val[2]) < isolevel) cubeindex |= 4;
    if (castd(grid.val[3]) < isolevel) cubeindex |= 8;
    if (castd(grid.val[4]) < isolevel) cubeindex |= 16;
    if (castd(grid.val[5]) < isolevel) cubeindex |= 32;
    if (castd(grid.val[6]) < isolevel) cubeindex |= 64;
    if (castd(grid.val[7]) < isolevel) cubeindex |= 128;

/* Cube is entirely in/out of the surface */
    if (edgeTable[cubeindex] == 0)
        return;

/* Find the vertices where the surface intersects the cube */
    if (edgeTable[cubeindex] & 1)
        vertlist[0] =
                VertexInterp(isolevel,grid.p[0],grid.p[1],castd(grid.val[0]),castd(grid.val[1]));
    if (edgeTable[cubeindex] & 2)
        vertlist[1] =
                VertexInterp(isolevel,grid.p[1],grid.p[2],castd(grid.val[1]),castd(grid.val[2]));
    if (edgeTable[cubeindex] & 4)
        vertlist[2] =
                VertexInterp(isolevel,grid.p[2],grid.p[3],castd(grid.val[2]),castd(grid.val[3]));
    if (edgeTable[cubeindex] & 8)
        vertlist[3] =
                VertexInterp(isolevel,grid.p[3],grid.p[0],castd(grid.val[3]),castd(grid.val[0]));
    if (edgeTable[cubeindex] & 16)
        vertlist[4] =
                VertexInterp(isolevel,grid.p[4],grid.p[5],castd(grid.val[4]),castd(grid.val[5]));
    if (edgeTable[cubeindex] & 32)
        vertlist[5] =
                VertexInterp(isolevel,grid.p[5],grid.p[6],castd(grid.val[5]),castd(grid.val[6]));
    if (edgeTable[cubeindex] & 64)
        vertlist[6] =
                VertexInterp(isolevel,grid.p[6],grid.p[7],castd(grid.val[6]),castd(grid.val[7]));
    if (edgeTable[cubeindex] & 128)
        vertlist[7] =
                VertexInterp(isolevel,grid.p[7],grid.p[4],castd(grid.val[7]),castd(grid.val[4]));
    if (edgeTable[cubeindex] & 256)
        vertlist[8] =
                VertexInterp(isolevel,grid.p[0],grid.p[4],castd(grid.val[0]),castd(grid.val[4]));
    if (edgeTable[cubeindex] & 512)
        vertlist[9] =
                VertexInterp(isolevel,grid.p[1],grid.p[5],castd(grid.val[1]),castd(grid.val[5]));
    if (edgeTable[cubeindex] & 1024)
        vertlist[10] =
                VertexInterp(isolevel,grid.p[2],grid.p[6],castd(grid.val[2]),castd(grid.val[6]));
    if (edgeTable[cubeindex] & 2048)
        vertlist[11] =
                VertexInterp(isolevel,grid.p[3],grid.p[7],castd(grid.val[3]),castd(grid.val[7]));

/* Create the triangle */
    for (i=0;triTable[cubeindex][i]!=-1;i+=3) {
        if(m_normalPoint){
            int i1 = addPoint(vertlist[triTable[cubeindex][i  ]],mesh);
            int i2 = addPoint(vertlist[triTable[cubeindex][i+1]],mesh);
            int i3 = addPoint(vertlist[triTable[cubeindex][i+2]],mesh);
            mesh.setTriangle(Triangle(i1,i3,i2));
        }else{
            int s = casti(mesh.m_vertices.size());
            mesh.m_vertices.push_back(vertlist[triTable[cubeindex][i  ]]);
            mesh.m_vertices.push_back(vertlist[triTable[cubeindex][i+1]]);
            mesh.m_vertices.push_back(vertlist[triTable[cubeindex][i+2]]);
            mesh.setTriangle(Triangle(s,s+1,s+2));
            mesh.m_texCoord.push_back(Vector3(0,0,0));
            mesh.m_texCoord.push_back(Vector3(0,0,0));
            mesh.m_texCoord.push_back(Vector3(0,0,0));
        }
    }

}


Isosurface MarchingCubes::createIsosurface(std::vector<Point3> sphereCenter){
    Isosurface data;
    data.resize(castui(NX));
    for (uint i=0;i<NX;i++) {
        data[i].resize(castui(NY));
        for (uint j = 0; j < NY; j++) {
            data[i][j].resize(castui(NZ));
            for (uint k = 0; k < NZ; k ++) {
                data[i][j][k]=0;
                for(auto c : sphereCenter){
                    float x = i - c.x;
                    float y = j - c.y;
                    float z = k - c.z;
                    data[i][j][k] += 1/(x*x+y*y+z*z);
                }
            }
        }
    }
    return data;
}

Mesh MarchingCubes::getMesh(double isolevel,std::vector<Point3> points) {
    Mesh m;
    uniqueMap.clear();
    GRIDCELL grid;
    Isosurface data = createIsosurface(points);
    for (uint i=0;i<NX-1;i++) {
        for (uint j=0;j<NY-1;j++) {
            for (uint k=0;k<NZ-1;k++) {
                grid.p[0].x = i;
                grid.p[0].y = j;
                grid.p[0].z = k;
                grid.val[0] = data[i][j][k];
                grid.p[1].x = i+1;
                grid.p[1].y = j;
                grid.p[1].z = k;
                grid.val[1] = data[i+1][j][k];
                grid.p[2].x = i+1;
                grid.p[2].y = j+1;
                grid.p[2].z = k;
                grid.val[2] = data[i+1][j+1][k];
                grid.p[3].x = i;
                grid.p[3].y = j+1;
                grid.p[3].z = k;
                grid.val[3] = data[i][j+1][k];
                grid.p[4].x = i;
                grid.p[4].y = j;
                grid.p[4].z = k+1;
                grid.val[4] = data[i][j][k+1];
                grid.p[5].x = i+1;
                grid.p[5].y = j;
                grid.p[5].z = k+1;
                grid.val[5] = data[i+1][j][k+1];
                grid.p[6].x = i+1;
                grid.p[6].y = j+1;
                grid.p[6].z = k+1;
                grid.val[6] = data[i+1][j+1][k+1];
                grid.p[7].x = i;
                grid.p[7].y = j+1;
                grid.p[7].z = k+1;
                grid.val[7] = data[i][j+1][k+1];
                Polygonise(grid,isolevel,m);
            }
        }
    }
    m.autoNormals();
    return m;
}

Object* MarchingCubes::getCage() {
    Mesh m = createCage(NX-1,NZ-1,NY-1);
    Object *obj = new Object(m);
    obj->setMaterial("blue");
    return obj;
}

int MarchingCubes::addPoint(Point3 p, Mesh &m) {
    auto it = uniqueMap.find(p);
    if(it == uniqueMap.end()){
        //duplicateVec.push_back(m.m_vertices.size());
        uniqueMap[p] = castui(m.m_vertices.size());
        m.m_vertices.push_back(p);
        m.m_texCoord.push_back(Point3(0));
        return casti(uniqueMap[p]);
    }else{
        return casti(it->second);
    }
}

Vector3 MarchingCubes::VertexInterp(double isolevel,Vector3 p1,Vector3 p2,double valp1,double valp2){
    double mu;
    Vector3 p;

    if (glm::abs(isolevel-valp1) < 0.00001)
        return(p1);
    if (glm::abs(isolevel-valp2) < 0.00001)
        return(p2);
    if (glm::abs(valp1-valp2) < 0.00001)
        return(p1);
    mu = (isolevel - valp1) / (valp2 - valp1);
    p.x = p1.x + castf(mu) * (p2.x - p1.x);
    p.y = p1.y + castf(mu) * (p2.y - p1.y);
    p.z = p1.z + castf(mu) * (p2.z - p1.z);

    return(p);
}
