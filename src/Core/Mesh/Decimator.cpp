//
// Created by pierre on 13/11/18.
//

#include "Decimator.hpp"

struct DataDecimate{
  MeshOM::VertexHandle vertexToDecimate;
  MeshOM::VertexHandle vertex;
  float error;
  bool dirty;

};

typedef std::map<MeshOM::EdgeHandle, DataDecimate> MapData;
typedef std::map<MeshOM::FaceHandle, std::vector<float>> ErrorFace;
typedef std::vector<MeshOM::EdgeHandle> VectorData;

MapData mapData;
ErrorFace errorFace;
VectorData vectorData;

void displayData(){
  Log(logInfo) << "MAP DATA:";
  for(auto i : mapData){
      Log(logInfo) << "Edge : " << i.first.idx() << "   .Dirty:" << i.second.dirty << "  Error:" << i.second.error;
    }
  Log(logInfo) << "ERROR FACE:";
  /*for(auto i : errorFace){
        Log(logInfo) << "Face : " << i.first.idx() << "   .Error:" << i.second[0];
    }*/
  Log(logInfo) << "VECTOR DATA";
  for(auto i:vectorData){
      Log(logInfo) << "Edge : " << i.idx();
    }
}


/* **** */

void insertVectorData(MeshOM::EdgeHandle edge){
  DataDecimate d = mapData[edge];
  for(auto it=vectorData.begin(); it != vectorData.end(); it++){
      if(d.error < mapData[*it].error){
          vectorData.insert(it,edge);
          return;
        }
    }
  vectorData.push_back(edge);
}

/* **** */

/**
 *
 * @param point
 * @param faces
 * @return The error between the point and normals faces
 */
float computeError(MeshOM::Point point, std::vector<MeshOM::FaceHandle> faces){
  std::vector<float> Q;
  for(unsigned int i=0;i<10;i++) Q.push_back(0);
  for(auto it :faces){
      for(unsigned int i=0;i<10;i++) Q[i] += errorFace[it][i];
    }
  float *data = point.data();
  float a = data[0];
  float b = data[1];
  float c = data[2];

  float e1 = a * Q[0] + b * Q[1] + c * Q[2] + Q[3];
  float e2 = a * Q[1] + b * Q[4] + c * Q[5] + Q[6];
  float e3 = a * Q[2] + b * Q[5] + c * Q[7] + Q[8];
  float e4 = a * Q[3] + b * Q[6] + c * Q[8] + Q[9];

  return a * e1 + b * e2 + c * e3 + e4;
}

DataDecimate collapseError(MeshOM& mesh, const MeshOM::EdgeHandle edge){

  DataDecimate dd;
  dd.dirty=false;

  MeshOM::HalfedgeHandle heh = mesh.halfedge_handle(edge,0);
  MeshOM::VertexHandle v1 = mesh.to_vertex_handle(heh);
  MeshOM::VertexHandle v2 = mesh.from_vertex_handle(heh);
  MeshOM::Point p1 = mesh.point(v1);
  MeshOM::Point p2 = mesh.point(v2);

  std::vector<MeshOM::FaceHandle> faces;

  for(auto it = mesh.vf_iter(v1); it.is_valid(); ++it){
      faces.push_back(*it);
    }
  for(auto it = mesh.vf_iter(v2); it.is_valid(); ++it){
      faces.push_back(*it);
    }


  float e1 = computeError(p1, faces);
  float e2 = computeError(p2, faces);

  if(e1<e2){
      dd.error = e1;
      dd.vertex = v1;
      dd.vertexToDecimate = v2;
    }else{
      dd.error = e2;
      dd.vertex = v2;
      dd.vertexToDecimate = v1;
    }
  //mapData[edge] = dd;
  //insertVectorData(dd);
  return dd;
}

void computeErrorFace(MeshOM& mesh, const MeshOM::FaceHandle face){
  MeshOM::Normal n = mesh.calc_face_normal(face);
  MeshOM::Point point = mesh.point(*mesh.fv_iter(face));

  std::vector<float> q;
  float *dat = n.data();
  float x = dat[0];
  float y = dat[1];
  float z = dat[2];
  float *datP = point.data();
  float d = x * datP[0] + y * datP[1] + z * datP[2];

  q.clear();
  q.push_back(x*x);
  q.push_back(x*y);
  q.push_back(x*z);
  q.push_back(x*d);
  q.push_back(y*y);
  q.push_back(y*z);
  q.push_back(y*d);
  q.push_back(z*z);
  q.push_back(z*d);
  q.push_back(d*d);
  errorFace[face] = q;
}

void preprocess(MeshOM &mesh){

  mapData.clear();
  errorFace.clear();
  vectorData.clear();

  // Compute the error for face
  for(auto it = mesh.faces_begin(); it != mesh.faces_end(); it++){
      computeErrorFace(mesh,*it);

    }

  //INIT QUEUE
  for(auto it = mesh.edges_begin(); it != mesh.edges_end(); it++){
      mapData[*it] = collapseError(mesh,*it);
      insertVectorData(*it);
    }

}

/* ********** PROCESS *********** */

void collapseEdge(MeshOM &mesh, DataDecimate dd){
  MeshOM::VertexHandle vDeci = dd.vertexToDecimate;
  for(auto it = mesh.cvih_iter
      (vDeci); it.is_valid(); it++){
      mesh.set_vertex_handle(*it,dd.vertex);
    }

  /* Compute new normal for faces */
  for(auto it = mesh.vf_iter(vDeci); it.is_valid(); it++){
      computeErrorFace(mesh,*it);
    }
}

void process(MeshOM &mesh){

  MeshOM::EdgeHandle edge = vectorData[0];
  DataDecimate dd = mapData[edge];

  if(dd.dirty){
      Log(logDebug) << "DIRTY";
      mapData[edge] = collapseError(mesh,edge);
      vectorData.erase(vectorData.begin());
      insertVectorData(edge);
      process(mesh);
    }else{
      // Dirty data ...
      for(auto it = mesh.cve_begin(dd.vertex); it.is_valid(); it++){
          mapData[*it].dirty=true;
        }
      for(auto it=mesh.cve_begin(dd.vertexToDecimate); it.is_valid(); it++){
          mapData[*it].dirty=true;
        }

      collapseEdge(mesh,dd);
      vectorData.erase(vectorData.begin());
    }

}

void clean(){
  mapData.clear();
  errorFace.clear();
  vectorData.clear();
}

void decimate(MeshOM &mesh, int edges){


  Log(logInfo) << "Decimate ... " << edges;

  Log(logDebug) << "PREPROCESS";
  preprocess(mesh);

  int collapsed = 0;

  Log(logDebug) << "PROCESS";
  while( (collapsed < edges) && (!vectorData.empty()) ){
      Log(logInfo) << "DECIMATION " << collapsed;
      process(mesh);

      collapsed++;
      //displayData();
    }
  Log(logInfo) << "DECIMATE DONE";
  clean();
}

void decimate(Mesh &mesh, int edges){
  MeshOM m = mesh.toMeshOM();
  decimate(m,edges);
  mesh.fromMeshOM(m);
}
