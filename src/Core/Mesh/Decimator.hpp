//
// Created by pierre on 13/11/18.
//

#ifndef ROGUE_DECIMATOR_HPP
#define ROGUE_DECIMATOR_HPP

/**
 * \file Decimator.hpp
 * \brief Decimate a mesh
 */


#include <src/Core/Macros.hpp>
#include <src/Engine/Object/Mesh.hpp>

void decimate(MeshOM &mesh, int edges);
void decimate(Mesh &mesh, int edges);


#endif //ROGUE_DECIMATOR_HPP
