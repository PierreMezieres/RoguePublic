//
// Created by pierre on 25/10/18.
//

#include "SubdivLoop.hpp"


#define MAX_VALENCE 50
// Struct to save new point before adding it in mesh ...
OpenMesh::VPropHandleT< MeshOM::Point> vp_pos;
OpenMesh::EPropHandleT< MeshOM::Point> ep_pos;

#define PairFF std::pair<float,float>
std::vector<PairFF> weights;



// Compute weights to use when compute new vertices position
void compute_weight(){
    weights.push_back(PairFF(0,0));
    float iv,t,a;
    for(int v=1;v<MAX_VALENCE;v++){
        iv = 1.f / v;
        t = (3.f + 2.f * std::cos(2.f * castf(M_PI) * iv));
        a = (40.f - t * t)/64.f;

        weights.push_back(PairFF(1-a,iv*a));
    }
}

void computeOldPoint(MeshOM &mesh, const MeshOM::VertexHandle & vh){
    MeshOM::Point point(0,0,0);

    if(mesh.is_boundary(vh)){

        MeshOM::HalfedgeHandle heh, pheh;
        heh = mesh.halfedge_handle(vh);

        if(!heh.is_valid()) return;

        pheh = mesh.prev_halfedge_handle(heh);

        MeshOM::VertexHandle to_vh = mesh.to_vertex_handle(heh);
        MeshOM::VertexHandle from_vh = mesh.from_vertex_handle(pheh);

        // Weights used 1 6 1 (boundary)
        point = mesh.point(vh) * 6.;
        point += mesh.point(to_vh) + mesh.point(from_vh);
        point *= 1.0 / 8.0;

    }else{

        uint valence=0;

        MeshOM::VertexVertexIter vvi;
        for(vvi = mesh.vv_iter(vh); vvi.is_valid(); ++vvi){
            valence++;
            point += mesh.point(*vvi);
        }

        point *= weights[valence].second;
        point += weights[valence].first * (mesh.point(vh));
    }

    mesh.property( vp_pos, vh) = point; // Save the point for later
}

void computeNewPoint(MeshOM &mesh, const MeshOM::EdgeHandle & eh){

    MeshOM::HalfedgeHandle heh, opp;

    heh = mesh.halfedge_handle(eh, 0);
    opp = mesh.halfedge_handle(eh, 1);

    MeshOM::Point point(mesh.point(mesh.to_vertex_handle(heh)));

    point += mesh.point(mesh.to_vertex_handle(opp));

    if(mesh.is_boundary(eh)){
        // Boundary -> midPoint between vertex
        point *= 0.5;
    }else{
        /*    1 ------ 3
         *     \      / \
         *      \  point \
         *       \  /     \
         *        3 ------ 1  */
        point *= 3.0;
        point += mesh.point(mesh.to_vertex_handle(mesh.next_halfedge_handle(heh)));
        point += mesh.point(mesh.to_vertex_handle(mesh.next_halfedge_handle(opp)));
        point *= 1. / 8.;
    }

    mesh.property( ep_pos, eh ) = point; // Save the point for later

}

void splitEdges(MeshOM &mesh, const MeshOM::EdgeHandle &eh){


    /*       <------------- eh ----------->            */
    /*                      vh             vh1         */
    /*     * -------------- * ------------- *          */
    /*       -----------------------------> heh        */
    /*       <----------------------------- opp        */
    /*     *    old edge          new edge             */



    MeshOM::HalfedgeHandle heh = mesh.halfedge_handle(eh, 0);
    MeshOM::HalfedgeHandle opp = mesh.halfedge_handle(eh, 1);

    MeshOM::Point point(mesh.point(mesh.to_vertex_handle(heh)));
    point += mesh.point(mesh.to_vertex_handle(opp));
    point *= 0.5;
    MeshOM::VertexHandle vh = mesh.new_vertex(point);

    mesh.property(vp_pos, vh) = mesh.property(ep_pos,eh);

    MeshOM::HalfedgeHandle new_heh, opp_new, t_heh;
    if(mesh.is_boundary(eh)){
        for(t_heh = heh; mesh.next_halfedge_handle(t_heh) != opp; t_heh = mesh.opposite_halfedge_handle(mesh.next_halfedge_handle(t_heh))){}
    }else{
        for(t_heh = mesh.next_halfedge_handle(opp); mesh.next_halfedge_handle(t_heh) != opp; t_heh = mesh.next_halfedge_handle(t_heh)){}
    }

    MeshOM::VertexHandle vh1 = mesh.to_vertex_handle(heh);

    new_heh = mesh.new_edge(vh,vh1);
    opp_new = mesh.opposite_halfedge_handle(new_heh);
    mesh.set_vertex_handle(heh,vh);

    mesh.set_next_halfedge_handle(t_heh, opp_new);
    mesh.set_next_halfedge_handle(new_heh, mesh.next_halfedge_handle(heh));
    mesh.set_next_halfedge_handle(heh, new_heh);
    mesh.set_next_halfedge_handle(opp_new, opp);

    if(mesh.face_handle(opp).is_valid()){
        mesh.set_face_handle(opp_new, mesh.face_handle(opp));
        mesh.set_halfedge_handle(mesh.face_handle(opp_new), opp_new);
    }

    mesh.set_face_handle(new_heh, mesh.face_handle(heh));
    mesh.set_halfedge_handle(vh, new_heh);
    mesh.set_halfedge_handle(mesh.face_handle(heh),heh);
    mesh.set_halfedge_handle(vh1, opp_new);

    mesh.adjust_outgoing_halfedge(vh);
    mesh.adjust_outgoing_halfedge(vh1);
}

void splitCorner(MeshOM &mesh, const MeshOM::HalfedgeHandle &he){
    MeshOM::HalfedgeHandle heh1(he);
    MeshOM::HalfedgeHandle heh5(heh1);
    MeshOM::HalfedgeHandle heh6(mesh.next_halfedge_handle(heh1));

    for(; mesh.next_halfedge_handle(mesh.next_halfedge_handle(heh5)) != heh1; heh5 = mesh.next_halfedge_handle(heh5)) {}

    MeshOM::VertexHandle vh1 = mesh.to_vertex_handle(heh1);
    MeshOM::VertexHandle vh2 = mesh.to_vertex_handle(heh5);

    MeshOM::HalfedgeHandle heh2(mesh.next_halfedge_handle(heh5));
    MeshOM::HalfedgeHandle heh3(mesh.new_edge(vh1,vh2));
    MeshOM::HalfedgeHandle heh4(mesh.opposite_halfedge_handle(heh3));

    MeshOM::FaceHandle fh_old(mesh.face_handle(heh6));
    MeshOM::FaceHandle fh_new(mesh.new_face());

    mesh.set_next_halfedge_handle(heh4, heh6);
    mesh.set_next_halfedge_handle(heh5, heh4);
    mesh.set_face_handle(heh4, fh_old);
    mesh.set_face_handle(heh5, fh_old);
    mesh.set_face_handle(heh6, fh_old);
    mesh.set_halfedge_handle(fh_old, heh4);

    mesh.set_next_halfedge_handle(heh1, heh3);
    mesh.set_next_halfedge_handle(heh3, heh2);

    mesh.set_face_handle(heh1, fh_new);
    mesh.set_face_handle(heh2, fh_new);
    mesh.set_face_handle(heh3, fh_new);

    mesh.set_halfedge_handle(fh_new, heh1);

}

void splitFaces(MeshOM &mesh, const MeshOM::FaceHandle &fh){
    MeshOM::HalfedgeHandle heh1(mesh.halfedge_handle(fh));
    MeshOM::HalfedgeHandle heh2(mesh.next_halfedge_handle(mesh.next_halfedge_handle(heh1)));
    MeshOM::HalfedgeHandle heh3(mesh.next_halfedge_handle(mesh.next_halfedge_handle(heh2)));

    // We want to "split" each corner of a triangle
    splitCorner(mesh, heh1);
    splitCorner(mesh, heh2);
    splitCorner(mesh, heh3);
}

void subdivLoop(MeshOM &mesh, unsigned int n){

    MeshOM::VertexIter vi;
    MeshOM::EdgeIter ei, ei2;
    MeshOM::FaceIter fi, fi2;

    compute_weight();

    mesh.add_property( vp_pos );
    mesh.add_property( ep_pos );

    for(unsigned int i = 0; i< n; ++i){

        // Point displacement
        for(vi = mesh.vertices_begin(); vi != mesh.vertices_end(); ++vi){
            computeOldPoint(mesh, *vi);
        }

        // New point
        for(ei = mesh.edges_begin(); ei != mesh.edges_end(); ++ei){
            computeNewPoint(mesh, *ei);
        }

        // Split edges
        ei2 = mesh.edges_end();
        for(ei = mesh.edges_begin(); ei != ei2; ++ei){
            splitEdges(mesh, *ei);
        }

        // Split faces
        fi2 = mesh.faces_end();
        for(fi = mesh.faces_begin(); fi != fi2; ++fi){
            splitFaces(mesh, *fi);
        }

        // Reload geometry
        for(vi = mesh.vertices_begin(); vi != mesh.vertices_end(); ++vi){
            mesh.set_point(*vi, mesh.property( vp_pos, *vi));
        }

    }

}

void subdivLoop(Mesh &mesh, unsigned int n){
    MeshOM m = mesh.toMeshOM();
    subdivLoop(m,n);
    mesh.fromMeshOM(m);
}
