#ifndef MACROS_HPP
#define MACROS_HPP

/**
 * \file Macros.hpp
 * \brief Some typedef ...
 */

#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Eigen/Eigen>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#ifdef __APPLE__
  #include <OpenGL/gl3.h>
  #include <OpenGL/gl3ext.h>
#else
  #define GL_GLEXT_PROTOTYPES 1
  #include <GL/gl.h>
  #include <GL/glext.h>
#endif
#include <string>
#include <build/submodule/include/Eigen/src/Core/Matrix.h>

typedef glm::vec3 Vector3;
typedef glm::vec2 Vector2;
typedef glm::vec4 Vector4;
typedef glm::vec3 Point3;
typedef glm::vec2 Point2;
typedef glm::vec3 Normal;
typedef glm::uvec3 Triangle;
typedef glm::uvec2 Line;
typedef glm::vec3 Color;

typedef Eigen::Vector3f Eigen_Vector3f;

typedef std::vector<Point3> VectorPoint3;
typedef std::vector<Point2> VectorPoint2;
typedef std::vector<Normal> VectorNormal;
typedef std::vector<GLfloat> VectorFloat;
typedef std::vector<GLuint> VectorUInt;
typedef std::vector<Triangle> VectorTriangles;

typedef glm::mat4 Matrix4;
typedef glm::mat3 Matrix3;
typedef glm::mat2 Matrix2;

#include <Eigen/Geometry>
#include <src/Core/Log/Log.hpp>

typedef Eigen::AlignedBox<float, 3> AABB;
typedef OpenMesh::PolyMesh_ArrayKernelT<> MeshOM;

#define Math_Pi 3.141592653

float degToRad(float t);

template <typename T>
Vector3 setVec3(T x, T y, T z){
    return {x,y,z};
}

template <typename T>
Vector2 setVec2(T x, T y){
    return{x,y};
}

void displayMat4(Matrix4& mat, std::string say="");
void displayVec3(Vector3& vec);
std::string Vec3AsString(Vector3 &vec, std::string separator = " ");
void displayVec4(Vector4& vec);
Vector3 getPositionMat4(Matrix4 mat);
void setPositionMat4(Matrix4 &mat, Vector3 vec);

void GL_CHECK_ERROR();

#define casti(x) static_cast<int>(x)
#define castui(x) static_cast<unsigned int>(x)
#define castul(x) static_cast<unsigned long>(x)
#define castf(x) static_cast<float>(x)
#define castd(x) static_cast<double>(x)

#define EPSILON 0.0000001f //Defined from AssimpLoader.cpp line 242

#endif // MACROS_HPP
