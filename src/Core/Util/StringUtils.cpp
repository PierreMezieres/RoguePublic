//
// Created by pierre on 24/07/18.
//

#include <sstream>
#include <Core/Log/Log.hpp>
#include "StringUtils.hpp"


std::vector<std::string> StringUtils::splitString(std::string &str, char c){
    std::stringstream ss{str};
    std::string one;
    std::vector<std::string> res;

    while(std::getline(ss,one,c))
        res.push_back(one);

    return res;
}

std::vector<std::string> StringUtils::splitStringForce(std::string &str, char c){
    std::stringstream ss{str};
    std::string one;
    std::vector<std::string> res;

    while(std::getline(ss,one,c)){
        if(one!="") res.push_back(one);
    }

    return res;
}

std::vector<std::string> StringUtils::splitStringForce(std::string &str, char c1, char c2){
    std::stringstream ss{str};
    std::string one;
    std::vector<std::string> res;
    std::vector<std::string> aux;
    std::vector<std::string> aux2;

    aux = splitString(str,c1);

    for(auto i: aux){
        aux2 = splitStringForce(i,c2);
        for(auto j:aux2){
            if(j!="") res.push_back(j);
        }
    }

    return res;
}

bool StringUtils::stringWithChar(std::string &str){
    for(char c:str){
        Log(logSuccess) << c;
        if((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9')) return true;
    }
    return false;
}