#ifndef FILEREADER_HPP
#define FILEREADER_HPP

/**
 * \file FileReader.hpp
 * \brief Util function to read in a file
 */

#include <string>
#include <vector>

/**
 * \class FileReader
 * \brief Useful function to read file
 */
class FileReader
{
public:
    FileReader();

    /* Get the content of the file */
    static const char* getFileWithCharStar(const char* name);         ///< Get the content of the file with a char *
    static std::string getFileWithString(const char* name);           ///< Get the content of the file with a string
    static const char* getFileWithCharStar(const std::string &name);  ///< Get the content of the file with a char *
    static std::string getFileWithString(const std::string &name);    ///< Get the content of the file with a string

};

#endif // FILEREADER_HPP
