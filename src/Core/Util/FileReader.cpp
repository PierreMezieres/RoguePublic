#include "FileReader.hpp"

#include <istream>
#include <fstream>
#include <sstream>

const char* FileReader::getFileWithCharStar(const char* name){
    std::ifstream file;
    file.open(name);
    std::stringstream stream;
    stream << file.rdbuf();
    file.close();
    return stream.str().c_str();
}

std::string FileReader::getFileWithString(const char* name){
    std::ifstream file;
    file.open(name);
    std::stringstream stream;
    stream << file.rdbuf();
    file.close();
    return stream.str();
}

const char* FileReader::getFileWithCharStar(const std::string &name){
    std::ifstream file;
    file.open(name);
    std::stringstream stream;
    stream << file.rdbuf();
    file.close();
    return stream.str().c_str();
}

std::string FileReader::getFileWithString(const std::string &name){
    std::ifstream file;
    file.open(name);
    std::stringstream stream;
    stream << file.rdbuf();
    file.close();
    return stream.str();
}