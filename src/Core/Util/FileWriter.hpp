//
// Created by pierre on 11/11/18.
//

#ifndef ROGUE_FILEWRITER_HPP
#define ROGUE_FILEWRITER_HPP

/**
 * \file FileWriter.hpp
 * \brief Usefull fonction to write in file
 */

 /**
  *
  * @param width of image
  * @param height of image
  * @param pixels of image
  */
void createPPM(int width, int height, GLubyte *pixels);


#endif //ROGUE_FILEWRITER_HPP
