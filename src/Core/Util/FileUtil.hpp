#ifndef FILEUTIL_HPP
#define FILEUTIL_HPP

/**
 * \file FileUtil.hpp
 * \brief Util function to navigate through directory and file
 */

#include <string>
#include <cctype>
#include <vector>
#include <QtCore/QString>

/* Basic functions to manage file and directory */

/**
 * \class FileUtil
 * \brief Useful function to manipulate file
 */
class FileUtil
{
public:

    static bool IsAbsolutePath(const std::string &filename);
    static std::string AbsolutePath(const std::string &filename);
    static std::string ResolveFilename(const std::string &filename);
    //static std::string DirectoryContaining(const std::string &filename);
    static void SetSearchDirectory(const std::string &dirname);
    static bool fileExist(QString fileName);

    static std::string searchDirectory;


};

#endif // FILEUTIL_HPP
