//
// Created by pierre on 24/07/18.
//

#ifndef ROGUE_STRINGUTILS_HPP
#define ROGUE_STRINGUTILS_HPP

/**
 * \file StringUtils.hpp
 * \brief Util function to manipulate String
 */


#include <vector>
#include <string>

/**
 * \class StringUtils
 * \brief Useful function to manipulate string
 */
class StringUtils {
public:

    // Split the string with character 'c', but if 2 'c' are following, the second will not be removed
    static std::vector<std::string> splitString(std::string &str, char c);          ///< Split the string with a char, don't remove 2 consecutive occurences
    // Split the string with character 'c', remove all 'c' in the string
    static std::vector<std::string> splitStringForce(std::string &str, char c);     ///< Split the string with a char, remove all occurences
    static std::vector<std::string> splitStringForce(std::string &str, char c1, char c2);     ///< Split the string with a char, remove all occurences

    static bool stringWithChar(std::string &str);
};


#endif //ROGUE_STRINGUTILS_HPP
