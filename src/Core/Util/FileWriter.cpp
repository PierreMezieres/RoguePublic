//
// Created by pierre on 11/11/18.
//

#include <fstream>
#include "../Macros.hpp"
#include "FileWriter.hpp"

void createPPM(int width, int height, GLubyte *pixels){

    std::ofstream file("screen.ppm", std::ofstream::out);
    file << "P3"     << "\n"
        << width       << " "
        << height      << "\n"
        << 255   << "\n";
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            int cur = 4 * ((height - i - 1) * width + j);
            int r = pixels[cur];
            int g = pixels[cur+1];
            int b = pixels[cur+2];
            file << r << " " << g << " " << b << " ";
        }
        file << "\n";
    }
    file.close();
}