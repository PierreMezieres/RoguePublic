//
// Created by pierre on 14/02/19.
//

#ifndef ROGUE_COLOR_HPP
#define ROGUE_COLOR_HPP

#include <Core/Macros.hpp>
#include <QtGui/QColor>
Color qcolorToColor(QColor c);
QColor colorToQColor(Color c);

Color rgbToXyz(Color c);
Color xyzToLab(Color col);
//Color xyzToRgb(Color &c);

Color rgbToLab(Color c);
//Color labToRgb(Color &c);

/* Each of theses function have to return a result between 0 and 1 */
float gapColorHSVhue(QColor &q1, QColor &q2);
float gapColorHSVsaturation(QColor &q1, QColor &q2);
float gapColorHSVvalue(QColor &q1, QColor &q2);
float gapColorHSV(QColor &q1, QColor &q2);
float deltaE76lab(QColor &qc1, QColor &qc2);

float toGrayLightness(QColor rgb);
float toGrayAverage(QColor rgb);
float toGrayLuminosity(QColor rgb);




#endif //ROGUE_COLOR_HPP
