//
// Created by pierre on 14/02/19.
//

#include "GLChronometer.hpp"

GLChronometer::GLChronometer(){
    glGenQueries(2, queryID);
}

void GLChronometer::tic(){
    glQueryCounter(queryID[0], GL_TIMESTAMP);
}

void GLChronometer::toc(){
    glQueryCounter(queryID[1], GL_TIMESTAMP);
}

float GLChronometer::getTime(){
    GLint stopTimerAvailable = 0;
    while(!stopTimerAvailable){
        glGetQueryObjectiv(queryID[1], GL_QUERY_RESULT_AVAILABLE, &stopTimerAvailable);
    }
    glGetQueryObjectui64v(queryID[0], GL_QUERY_RESULT, &startTime);
    glGetQueryObjectui64v(queryID[1], GL_QUERY_RESULT, &stopTime);
    return (stopTime - startTime) / 1000000.0;
}