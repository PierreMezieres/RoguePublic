//
// Created by pierre on 21/10/18.
//

#ifndef ROGUE_MATH_HPP
#define ROGUE_MATH_HPP

#include <Core/Macros.hpp>

/**
 *
 *  \file Math.hpp
 *  \brief Simple function for math application
 *
 */

float lerp(float a, float b, float f); ///< Linear interpolation betweeen a and b
double legendre(int l,int m,double x); ///< Associated Legendre Polynomials


void sphericalTOcartesian(double rho, double theta, double phi, double &x, double &y, double &z);
void cartesianTOspherical(double x, double y, double z, double &rho, double &theta, double &phi);
void sphericalTOcartesian(Vector3 sph, Vector3 &car);
void cartesianTOspherical(Vector3 car, Vector3 &sph);

#endif //ROGUE_MATH_HPP
