//
// Created by pierre on 02/08/18.
//

#ifndef ROGUE_GLMTOEIGEN_HPP
#define ROGUE_GLMTOEIGEN_HPP

/**
 *
 * \file GlmToEigen.hpp
 * \brief Simple functions to convert glm format to Eigen
 */

#include <Core/Macros.hpp>
#include <Eigen/Eigen>


Eigen_Vector3f GTE_VECTOR3(Vector3 vec);


#endif //ROGUE_GLMTOEIGEN_HPP
