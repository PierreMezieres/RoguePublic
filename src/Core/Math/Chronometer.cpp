//
// Created by pierre on 20/08/18.
//

#include <ctime>
#include "Chronometer.hpp"

typedef std::chrono::high_resolution_clock Clock;

Chronometer::Chronometer(){

}

void Chronometer::start(){
    active = true;
    t1 = Clock::now();
}

void Chronometer::pause(){
    active = false;
}

float Chronometer::getDeltaMark(){
    if(active){
        t2 = Clock::now();
        auto d = std::chrono::duration<float>(t2-t1);
        t1 = t2;
        return d.count();
    }
    return 0;
}