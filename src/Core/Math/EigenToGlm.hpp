//
// Created by pierre on 03/08/18.
//

#ifndef ROGUE_EIGENTOGLM_HPP
#define ROGUE_EIGENTOGLM_HPP

#include <Core/Macros.hpp>

/**
 *
 * \file EigenToGlm.hpp
 * \brief Simple functions to convert Eigen format to glm
 *
 */

Vector3 ETG_VECTOR3(Eigen_Vector3f vec);


#endif //ROGUE_EIGENTOGLM_HPP
