//
// Created by pierre on 29/08/18.
//

#ifndef ROGUE_RAY_HPP
#define ROGUE_RAY_HPP

/**
 *
 * \file Ray.hpp
 * \brief Manage Ray
 */


#include <src/Core/Macros.hpp>


class Object;
class Mesh;
class Element;

/**
 * \class Ray
 * \brief Manage a ray
 */
class Ray {
public:
    Ray(Vector3 orig, Vector3 dir);

    bool updateT(float &t, float &r);

    bool intersectionTriangle(Vector3 &v1, Vector3 &v2, Vector3 &v3, float &t);
    bool intersectionPlane(Vector3 pt, Normal normal, Vector3 &res);

    bool intersectionMesh(Mesh *m, Matrix4 model, float &t);
    bool intersectionElement(Element *e, Matrix4 model, float &t);
    bool intersectionObject(Object *o, float &t);
    Element * intersectionObject2(Object *o, float &t);

    void displayInfo();

    Vector3 getOrig();
    Vector3 getDir();

private:
    Vector3 m_orig;
    Vector3 m_dir;

};


#endif //ROGUE_RAY_HPP
