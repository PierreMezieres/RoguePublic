//
// Created by pierre on 14/02/19.
//

#ifndef ROGUE_GLCHRONOMETER_HPP
#define ROGUE_GLCHRONOMETER_HPP

#include <Core/Macros.hpp>

class GLChronometer {
public:
    GLChronometer();

    void tic();
    void toc();
    float getTime();

private:
    GLuint64 startTime,stopTime;
    unsigned int queryID[2];

};


#endif //ROGUE_GLCHRONOMETER_HPP
