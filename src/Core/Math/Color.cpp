//
// Created by pierre on 14/02/19.
//

#include "Color.hpp"




Color qcolorToColor(QColor c){
    Color res;
    res.x = c.redF();
    res.y = c.greenF();
    res.z = c.blueF();
    return res;
}

QColor colorToQColor(Color c){
    return QColor(c.x,c.y,c.z);
}

Color rgbToXyz(Color c){
    float x = (c.x * 0.4124 + c.y * 0.3575 + c.z * 0.1804);
    float y = (c.x * 0.2126 + c.y * 0.7152 + c.z * 0.0722);
    float z = (c.x * 0.0193 + c.y * 0.1192 + c.z * 0.9505);
    //Log(logDebug) << "rgbToXyz " << c.x << " " << c.y << " " << c.z << " -> " << x << " " << y << " " << z;
    return Color(x,y,z);
}

Color xyzToLab(Color col){

    float x = col.x / 0.95047;
    float y = col.y / 1.00000;
    float z = col.z / 1.08883;
    float a = (x > 0.008856) ? pow(x, 1.0/3.0) : (7.787 * x) + 16/116;
    float b = (y > 0.008856) ? pow(y, 1.0/3.0) : (7.787 * y) + 16/116;
    float c = (z > 0.008856) ? pow(z, 1.0/3.0) : (7.787 * z) + 16/116;
    //Log(logDebug) << "xyzToLab " << col.x << " " << col.y << " " << col.z << " -> " << (116 * b) - 16 << " " << 500 * (a - b) << " " << 200 * (b - c);
    return Color((116 * b) - 16, 500 * (a - b), 200 * (b - c));
}

Color rgbToLab(Color c) {
    Color aux = rgbToXyz(c);
    aux = xyzToLab(aux);
    //Log(logInfo) << "rgbToLab " << c.x << " " << c.y << " " << c.z << " -> " << aux.x << " " << aux.y << " " << aux.z;
    return aux;
}



float gapColorHSVhue(QColor &q1, QColor &q2){
    float h1=q1.hsvHue();
    float h2=q2.hsvHue();
    float res = std::min(std::abs(h2-h1),360-std::abs(h2-h1)) / 180.0;
    return res;
}

float gapColorHSVsaturation(QColor &q1, QColor &q2){
    float h1=q1.hsvSaturationF();
    float h2=q2.hsvSaturationF();
    return std::abs(h2-h1);
}

float gapColorHSVvalue(QColor &q1, QColor &q2){
    float h1=q1.valueF();
    float h2=q2.valueF();
    return abs(h2-h1);
}

float gapColorHSV(QColor &q1, QColor &q2){
    float dh = gapColorHSVhue(q1,q2);
    float ds = gapColorHSVsaturation(q1,q2);
    float dv = gapColorHSVvalue(q1,q2);
    return std::sqrt(dh*dh+ds*ds+dv*dv) / std::sqrt(3);
}

float deltaE76lab(QColor &qc1, QColor &qc2){
    Color c1 = qcolorToColor(qc1);
    Color c2 = qcolorToColor(qc2);
    c1 = rgbToLab(c1);
    c2 = rgbToLab(c2);
    float x = c1.x-c2.x;
    float y = c1.y-c2.y;
    float z = c1.z-c2.z;
    return std::sqrt(x*x+y*y+z*z)/std::sqrt(100 + 184.439*184.439 + 202.345*202.345);
}

float toGrayLightness(QColor rgb) {
    float max = std::max(rgb.red(),std::max(rgb.blue(),rgb.green()));
    float min = std::min(rgb.red(),std::min(rgb.blue(),rgb.green()));
    return (max+min)/2.0;
}

float toGrayAverage(QColor rgb) {
    return (rgb.red()+rgb.green()+rgb.blue())/3.0;
}

float toGrayLuminosity(QColor rgb) {
    return 0.21*rgb.red() + 0.72*rgb.green() + 0.07*rgb.blue();
}