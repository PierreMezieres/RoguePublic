//
// Created by pierre on 21/10/18.
//

#include "Math.hpp"
#include <Core/Macros.hpp>

float lerp(float a, float b, float f){
    return a + f * (b - a);
}


double legendre(int l,int m,double x)
{
    // evaluate an Associated Legendre Polynomial P(l,m,x) at x
    double pmm = 1.0;
    if(m>0){
        double  somx2  =  std::sqrt((1.0-x)*(1.0+x));
        double  fact  =  1.0;
        for(int  i=1;  i<=m;  i++)  {
            pmm  *=  (-fact)  *  somx2;
            fact  +=  2.0;
        }
    }
    if(l==m) return pmm;
    double pmmp1 = x * (2.0*m+1.0) * pmm;
    if(l==m+1) return pmmp1;
    double pll = 0.0;
    for(int ll=m+2; ll<=l; ++ll) {
        pll = ( (2.0*ll-1.0)*x*pmmp1-(ll+m-1.0)*pmm ) / (ll-m);
        pmm = pmmp1;
        pmmp1 = pll;
    }
    return pll;
}


void sphericalTOcartesian(double rho, double theta, double phi, double &x, double &y, double &z){
    x=rho*cos(theta)*sin(phi);
    y=rho*sin(theta)*cos(phi);
    z=rho*cos(phi);
}

void cartesianTOspherical(double x, double y, double z, double &rho, double &theta, double &phi){
    rho = sqrt(x*x+y*y+z*z);
    theta = acos(z/rho);
    phi = atan2(y,x);
}

void sphericalTOcartesian(Vector3 sph, Vector3 &car){
    double x,y,z;
    sphericalTOcartesian(sph.x,sph.y,sph.z,x,y,z);
    car = Vector3(x,y,z);
}

void cartesianTOspherical(Vector3 car, Vector3 &sph){
    double r,t,p;
    cartesianTOspherical(car.x,car.y,car.z,r,t,p);
    sph = Vector3(r,t,p);
}