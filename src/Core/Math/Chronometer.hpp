//
// Created by pierre on 20/08/18.
//

#ifndef ROGUE_CHRONOMETER_HPP
#define ROGUE_CHRONOMETER_HPP

#include <chrono>

/**
 *
 * \file Chronometer.hpp
 * \brief Use a simple chronometer
 *
 */

class Chronometer {
public:

    Chronometer();

    void start();           ///< Start the chronometer
    void pause();           ///< Pause the chronometer
    float getDeltaMark();   ///< Get the time elapsed since the last getDeltaMark()

private:
    std::chrono::high_resolution_clock::time_point t1,t2;

    bool active = false;
};


#endif //ROGUE_CHRONOMETER_HPP
