//
// Created by pmeziere on 3/13/19.
//

#include "SphericalHarmonic.hpp"
#include "Math.hpp"

SphericalHarmonic::SphericalHarmonic(int nbands): m_nbands{nbands} {
    precomputeFactorials();

}

SphericalHarmonic::~SphericalHarmonic() {

}

void SphericalHarmonic::SH_setup_spherical_samples(SHSample *samples, int sqrt_n_samples) {
    int i=0;
    double oneoverN = 1.0/sqrt_n_samples;
    for(int a=0;a<sqrt_n_samples;a++){
        for(int b=0;b<sqrt_n_samples;b++){
            double x = (a + random()) * oneoverN;
            double y = (b + random()) * oneoverN;
            double theta = 2.0 * std::acos(sqrt(1.0 - x));
            double phi = 2.0 * Math_Pi * y;
            samples[i].sph = Vector3(theta,phi,1.0);
            Vector3 vec(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));
            samples[i].vec = vec;

            for(int l=0; l<m_nbands;++l){
                for(int m=-l; m<=l;++m){
                    int index = l*(l+1)+m;
                    samples[i].coeff[index] = SH(l,m,theta,phi);
                }
            }
            ++i;
        }
    }

}

double SphericalHarmonic::K(int l, int m)
{
    // renormalisation constant for SH function
    double temp = ((2.0*l+1.0)*factorial[l-m]) / (4.0*Math_Pi*factorial[l+m]);
    return  sqrt(temp);
}
double SphericalHarmonic::SH(int l, int m, double theta, double phi)
{
    // return a point sample of a Spherical Harmonic basis function
    // l is the band, range [0..N]
    // m in the range [-l..l]
    // theta in the range [0..Pi]
    // phi in the range [0..2*Pi]
    const double sqrt2 = sqrt(2.0);
    if(m==0)  return  K(l,0)*legendre(l,m,cos(theta));
    else if(m>0) return sqrt2*K(l,m)*cos(m*phi)*legendre(l,m,cos(theta));
    else  return  sqrt2*K(l,-m)*sin(-m*phi)*legendre(l,-m,cos(theta));
}

void SphericalHarmonic::precomputeFactorials() {
    factorial[0]=1;
    factorial[1]=1;
    int accum = 1;
    for(int i=2;i<33;i++){
        accum *= i;
        factorial[i] = accum;
    }
}