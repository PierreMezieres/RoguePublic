//
// Created by pmeziere on 3/13/19.
//

#ifndef ROGUE_SPHERICALHARMONIC_H
#define ROGUE_SPHERICALHARMONIC_H

#include <Core/Macros.hpp>

struct SHSample{
    Vector3 sph;
    Vector3 vec;
    double *coeff;
};

class SphericalHarmonic {
public:
    SphericalHarmonic(int nbands);
    ~SphericalHarmonic();

    void SH_setup_spherical_samples(SHSample samples[], int sqrt_n_samples);

    double K(int l, int m);
    double SH(int l, int m, double theta, double phi);

private:

    void precomputeFactorials();
    int factorial[33];

    int m_nbands;

};


#endif //ROGUE_SPHERICALHARMONIC_H
