//
// Created by pierre on 23/08/18.
//

#ifndef ROGUE_GROD_HPP
#define ROGUE_GROD_HPP


/**
 * \file Grid.hpp
 * \brief Create mesh of a grid or cage
 */

#include "src/Engine/Object/Mesh.hpp"

Mesh createGridShape(float l = 10, float L = 10, float x = 10, float y = 10);
Mesh createCage(float l = 10, float L = 10, float p = 10);


#endif //ROGUE_GROD_HPP
