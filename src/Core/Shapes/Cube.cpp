//
// Created by pierre on 26/07/18.
//

#include "Cube.hpp"
#include <Core/Log/Log.hpp>

Mesh createCubeShape(float length, float width, float depth){

    float l = length / 2;
    float w = width / 2;
    float d = depth / 2;


    Mesh m;

    for(int i = 0; i < 4*6; i+=4){
        m.setTriangle(Triangle(i,i+1,i+3));
        m.setTriangle(Triangle(i+1,i+2,i+3));
        m.m_texCoord.push_back(Point3(0,1,0));
        m.m_texCoord.push_back(Point3(0,0,0));
        m.m_texCoord.push_back(Point3(1,0,0));
        m.m_texCoord.push_back(Point3(1,1,0));
    }


    m.m_vertices.push_back(Point3(-l,-w,d));
    m.m_vertices.push_back(Point3(-l,w,d));
    m.m_vertices.push_back(Point3(l,w,d));
    m.m_vertices.push_back(Point3(l,-w,d));

    m.m_vertices.push_back(Point3(l,-w,d));
    m.m_vertices.push_back(Point3(l,w,d));
    m.m_vertices.push_back(Point3(l,w,-d));
    m.m_vertices.push_back(Point3(l,-w,-d));

    m.m_vertices.push_back(Point3(l,-w,-d));
    m.m_vertices.push_back(Point3(l,w,-d));
    m.m_vertices.push_back(Point3(-l,w,-d));
    m.m_vertices.push_back(Point3(-l,-w,-d));

    m.m_vertices.push_back(Point3(-l,-w,-d));
    m.m_vertices.push_back(Point3(-l,w,-d));
    m.m_vertices.push_back(Point3(-l,w,d));
    m.m_vertices.push_back(Point3(-l,-w,d));

    m.m_vertices.push_back(Point3(-l,w,d));
    m.m_vertices.push_back(Point3(-l,w,-d));
    m.m_vertices.push_back(Point3(l,w,-d));
    m.m_vertices.push_back(Point3(l,w,d));

    m.m_vertices.push_back(Point3(-l,-w,-d));
    m.m_vertices.push_back(Point3(-l,-w,d));
    m.m_vertices.push_back(Point3(l,-w,d));
    m.m_vertices.push_back(Point3(l,-w,-d));

    for(int i= 0; i<4; i++) m.m_normals.push_back(Normal(0,0,1));
    for(int i= 0; i<4; i++) m.m_normals.push_back(Normal(1,0,0));
    for(int i= 0; i<4; i++) m.m_normals.push_back(Normal(0,0,-1));
    for(int i= 0; i<4; i++) m.m_normals.push_back(Normal(-1,0,0));
    for(int i= 0; i<4; i++) m.m_normals.push_back(Normal(0,1,0));
    for(int i= 0; i<4; i++) m.m_normals.push_back(Normal(0,-1,0));

    return m;
}