//
// Created by pierre on 20/08/18.
//

#include "Cylinder.hpp"

Mesh createCylinderShape(float length, float radius, int ech){

    Mesh m;

    const Vector3 a{0,0,0};
    const Vector3 b{0,length,0};
    int nFaces=32;

    Vector3 xPlane(1,0,0);
    Vector3 yPlane(0,0,1);

    //Vector3 c = Vector3(0.5) * ( a + b );

    const float thetaInc( Math_Pi*2 / nFaces );
    for ( int i = 0; i <= nFaces; ++i )
    {
        float theta = i * thetaInc;

        for(int j=0; j<ech; j++){
            Vector3 p = Vector3(j/(float)ech) * (a+b);
            m.m_vertices.push_back(
                    p + radius * (std::cos( theta ) * xPlane + std::sin( theta ) * yPlane )
                    );
        }

        for( int j =0; j<ech  ; ++j){

            m.m_normals.push_back( Vector3(std::cos(theta), 0, std::sin(theta)));
        }

        theta = i*thetaInc;

        for(auto v=0;v<ech;++v){
            const float phi = v  * Math_Pi / (ech-1);
            m.m_texCoord.push_back({0.5 * theta / Math_Pi, phi / Math_Pi, 0});
        }
    }

    for ( int i = 0; i < nFaces ; ++i){
        for ( int j = 0; j < ech-1; ++j )
        {
            // Outer face.
            uint o1 = i * ech + j;
            uint o2 = (i+1) * ech + j;
            uint o3 = o1 + 1;
            uint o4 = o2 + 1;

            m.setTriangle( Triangle( o1, o2, o3  ) );
            m.setTriangle( Triangle( o2, o4, o3 ) );
        }
    }

    return m;
}
