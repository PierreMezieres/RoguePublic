//
// Created by pierre on 26/07/18.
//

#ifndef ROGUE_MACROSSHAPES_HPP
#define ROGUE_MACROSSHAPES_HPP

/**
 * \file MacrosShapes.hpp
 * \brief Macros to include all shape creation source
 */

#include "Sphere.hpp"
#include "Cube.hpp"
#include "Cylinder.hpp"
#include "Grid.hpp"
#include "TriangleMesh.hpp"



#endif //ROGUE_MACROSSHAPES_HPP
