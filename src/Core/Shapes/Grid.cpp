//
// Created by pierre on 23/08/18.
//

#include "Grid.hpp"

Mesh createGridShape(float l, float L, float x, float y){

    Mesh m;

    float stepX = l/x;
    float stepY = L/y;

    float startX = -l/2;
    float startY = -L/2;
    x++;
    y++;

    for(int i = 0; i < x; i++){
        for(int j = 0; j < y; j++){
            m.m_vertices.push_back(Vector3(startX+i*stepX,0,startY+j*stepY));
            m.m_texCoord.push_back(Vector3(0));
            m.m_normals.push_back(Vector3(0,1,0));
        }
    }

    for(int i = 0; i < x; i++){
        for(int j = 0; j < y-1; j++){
            m.setLine( Line( i*x+j,i*x+j+1) );
        }
    }

    for(int i = 0; i < x-1; i++){
        for(int j = 0; j < y; j++){
            m.setLine( Line( i*x+j,i*x+x+j ) );
        }
    }

    m.setMeshTypes(GL_LINES);

    return m;
}

Mesh createCage(float l, float L, float p){

    Mesh m;


    m.m_vertices.push_back(Point3(0));
    m.m_vertices.push_back(Point3(0,0,L));
    m.m_vertices.push_back(Point3(0,p,L));
    m.m_vertices.push_back(Point3(0,p,0));
    m.m_vertices.push_back(Point3(l,0,0));
    m.m_vertices.push_back(Point3(l,0,L));
    m.m_vertices.push_back(Point3(l,p,L));
    m.m_vertices.push_back(Point3(l,p,0));

    for(int i=0; i<8; i++){
        m.m_normals.push_back(Point3(0));
        m.m_texCoord.push_back(Point3(0));
    }

    m.setLine(Line(0,1));
    m.setLine(Line(1,2));
    m.setLine(Line(2,3));
    m.setLine(Line(3,0));
    m.setLine(Line(4,5));
    m.setLine(Line(5,6));
    m.setLine(Line(6,7));
    m.setLine(Line(7,4));
    m.setLine(Line(0,4));
    m.setLine(Line(1,5));
    m.setLine(Line(2,6));
    m.setLine(Line(3,7));

    m.setMeshTypes(GL_LINES);
    return m;
}