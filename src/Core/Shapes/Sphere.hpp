#ifndef SPHERE_HPP
#define SPHERE_HPP

/**
 * \file Sphere.hpp
 * \brief Create mesh of a sphere
 */

#include "src/Engine/Object/Mesh.hpp"

Mesh createSphereShape(float radius=1, uint slices=20, uint stacks=20);

#endif // SPHERE_HPP
