//
// Created by pierre on 31/08/18.
//

#ifndef ROGUE_SIMPLEMESH_HPP
#define ROGUE_SIMPLEMESH_HPP

/**
 * \file TriangleMesh.hpp
 * \brief Create mesh with simple triangle mesh
 */

#include "src/Engine/Object/Mesh.hpp"

Mesh createTriangleMeshShape(
        VectorPoint3 vertices,
        VectorUInt indices,
        VectorPoint3 texCoord = VectorPoint3(),
        VectorNormal normal = VectorNormal());

Mesh createSimpleRec();
Mesh createSimpleCube();

#endif //ROGUE_SIMPLEMESH_HPP