//
// Created by pierre on 26/07/18.
//

#ifndef ROGUE_CUBE_HPP
#define ROGUE_CUBE_HPP

/**
 * \file Cube.hpp
 * \brief Create mesh of a cube
 */

#include "src/Engine/Object/Mesh.hpp"

Mesh createCubeShape(float length = 1, float width = 1, float depth = 1);

#endif //ROGUE_CUBE_HPP
