//
// Created by pierre on 20/08/18.
//

#ifndef ROGUE_CYLINDER_HPP
#define ROGUE_CYLINDER_HPP

/**
 * \file Cylinder.hpp
 * \brief Create mesh of a cylinder
 */

#include "src/Engine/Object/Mesh.hpp"

Mesh createCylinderShape(float length = 1, float radius = 1, int ech = 32);


#endif //ROGUE_CYLINDER_HPP
