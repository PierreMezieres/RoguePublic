//
// Created by pmeziere on 3/13/19.
//

#ifndef ROGUE_PLUGINTEST_HPP
#define ROGUE_PLUGINTEST_HPP

#include <src/Plugin/PluginInterface.hpp>
#include <src/Plugin/Test/PluginCore/TestWidget.hpp>

/** \class PluginTest
 * \brief [Plugin Test]
 */
class PluginTest : public PluginInterface {
public:

    PluginTest();
    void loadData();

    TestWidget *testWidget;

};

#endif //ROGUE_PLUGINTEST_HPP
