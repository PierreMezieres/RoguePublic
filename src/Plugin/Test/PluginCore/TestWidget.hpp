#ifndef TESTWIDGET_HPP
#define TESTWIDGET_HPP

#include <QWidget>
#include <Plugin/PluginInterface.hpp>
#include <QtWidgets/QSpinBox>

namespace Ui {
class TestWidget;
}

class TestWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TestWidget(ContextPlugin contextPlugin,QWidget *parent = nullptr);
    ~TestWidget();

    void updateSphericalHarmonic();

private slots:
    void on_pushButton_clicked();

    void on_spinBox_valueChanged(int arg1);

    void on_spinBox_2_valueChanged(int arg1);

private:
    Ui::TestWidget *ui;
    ContextPlugin m_context;

    Object *objSpherical;

    int l = 0, m = 0;
    QSpinBox *spinBoxM;
    QSpinBox *spinBoxL;
};

#endif // TESTWIDGET_HPP
