#include <Plugin/PluginInterface.hpp>
#include "TestWidget.hpp"
#include "ui_TestWideget.h"
#include <Core/Shapes/Sphere.hpp>
#include <Core/Math/Math.hpp>
#include <Core/Math/SphericalHarmonic.hpp>

TestWidget::TestWidget(ContextPlugin contextPlugin, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestWidget)
{
    ui->setupUi(this);
    spinBoxM = ui->spinBox_2;
    spinBoxL = ui->spinBox;

    m_context = contextPlugin;

    Mesh mesh = createSphereShape(1,100,100);
    objSpherical = new Object(mesh);
    objSpherical->setMaterial("texCoord");
}

TestWidget::~TestWidget()
{
    delete ui;
}

void TestWidget::on_pushButton_clicked()
{
    m_context.scene->cleanScene();
    updateSphericalHarmonic();
    m_context.scene->drawData.objectManager.addValue(objSpherical,"SphereHarmonic");
    m_context.openGLWidget->update();
    m_context.openGLWidget->fitScene();


}

void TestWidget::updateSphericalHarmonic() {

    Mesh mesh = createSphereShape(1,100,100);

    int i=0;
    SphericalHarmonic sphericalHarmonic(4);

    for(auto v:mesh.m_vertices){
        //Log(logInfo) << "Vector:";
        //displayVec3(v);
        Vector3 spherical;
        Vector3 vaux{v.x,v.z,v.y};
        cartesianTOspherical(vaux,spherical);
        //displayVec3(spherical);

        double sh = sphericalHarmonic.SH(l,m,spherical.y,spherical.z);
        //Log(logDebug) << l << " " << m << " "<< sh;

        double red=0,green=0;
        if(sh>0) red = sh;
        if(sh<0) green =-sh;

        mesh.m_texCoord[i]= Vector3(red,green,0);
        i++;
    }

    objSpherical->setMesh(mesh);
}

void TestWidget::on_spinBox_valueChanged(int arg1)
{
    l = arg1;
    if(m>l) spinBoxM->setValue(l);
    else{
        updateSphericalHarmonic();
        m_context.openGLWidget->update();
    }
}

void TestWidget::on_spinBox_2_valueChanged(int arg1)
{
    m = arg1;
    if(m>l) spinBoxL->setValue(m);
    else{
        updateSphericalHarmonic();
        m_context.openGLWidget->update();
    };
}
