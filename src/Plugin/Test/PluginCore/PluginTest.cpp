//
// Created by pmeziere on 3/13/19.
//

#include "PluginTest.hpp"

PluginTest::PluginTest() {
    m_name = "PluginTest";

}

void PluginTest::loadData(){
    testWidget = new TestWidget(m_context);
    registerWidget(testWidget,"Test");
}