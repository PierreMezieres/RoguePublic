//
// Created by pierre on 04/02/19.
//

#ifndef ROGUE_RENDERERMBOIT_HPP
#define ROGUE_RENDERERMBOIT_HPP


#include <Engine/Renderer/Renderer.hpp>
#include <Engine/Renderer/DrawData.hpp>
#include "FrameBuffer/MBufferExtra.hpp"
#include "FrameBuffer/ZBuffer.hpp"
#include <cmath>

/*! Circle constant.*/
#ifndef M_PI
#define M_PI 3.14159265358979323f
#endif

/**
 * \file RendererMBOIT
 * \brief Moment Based Order Independant Transparency
 */

/**
 * \class RendererMBOIT
 * \brief Moment Based Order Independant Transparency
 */
class RendererMBOIT : public Renderer {
public:
    RendererMBOIT();

    /**
     * @brief Compute and display the scene
     * @param drawData
     */
    void draw(DrawData &drawData);

    /**
     * @brief Resize all of the buffers with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    /**
     * @brief Set the biais
     * @param v
     */
    void setBiasMoment(float v);

    /**
     * @brief Set the max over estimation
     * @param v
     */
    void setOverestimation(float v);

    /**
     * @brief True : Compute MBOIT with trigonometrics moments
     *        False : Compute MBOIT with exponentials moments
     * @param b
     */
    void setTrigonometric(bool b);

    /**
     * @brief Set the amount of moment to be used
     * @param number
     */
    void setMomentNumber(int number);



private:


    float biasMoment = 5*std::pow(10,-5);
    float overestimation = 0.5;
    bool trigonometric = false;
    int moment_number = 4;

    Shader *shader;
    Shader *shaderCompositing;
    Shader *shaderOpaque;
    Shader *shaderCompositingFinal;
    MBufferExtra mbuffer;
    FrameBuffer frameBuffer;

    ZBuffer zbufferOpaque;

    float circleToParameter(float angle, float* pOutMaxParameter = nullptr);
    void computeWrappingZoneParameters(Vector4 p_out_wrapping_zone_parameters,float new_wrapping_zone_angle = 0.1f * M_PI);
    Vector4 wrapping_parameters;

};

#endif //ROGUE_RENDERERMBOIT_HPP
