//
// Created by pierre on 11/02/19.
//

#include "RendererWBOIT.hpp"

RendererWBOIT::RendererWBOIT() : Renderer("WBOIT"){

}

void RendererWBOIT::draw(DrawData &drawData) {
    shader = drawData.shaderManager.getValue("WBOIT");
    shaderCompositing = drawData.shaderManager.getValue("WBOITcompositing");
    shaderOpaque = drawData.shaderManager.getValue("WBOITopaque");



    GLint qt_buffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    unsigned int attach1[1] = {GL_COLOR_ATTACHMENT0};
    unsigned int attach2[1] = {GL_COLOR_ATTACHMENT1};
    unsigned int attach3[2] = {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1};
    float CLEAR_DEPTH = -99999.0;

    DataForDrawing dfd = drawData.getDataForDrawing();

    /* ******** Opaque objects ******** */

    shaderOpaque->use();
    zbufferOpaque.bind();

    zbufferOpaque.bind();
    glDrawBuffers(1,attach1);
    glClearColor(-CLEAR_DEPTH,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbufferOpaque.bind();
    glDrawBuffers(1,attach2);
    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbufferOpaque.bind();
    glDrawBuffers(2,attach3);
    glEnable(GL_DEPTH_TEST);

    drawData.lightManager.bind(shaderOpaque);
    for(auto o:drawData.objectManager.getValues()){
        shaderOpaque->setMat4("model",o->getModel());

        for(auto e:o->getElements()){

            Material *material = drawData.materialManager.getValue(e->m_materialName);

            shaderOpaque->setMat4("proj",drawData.camera->GetProjMatrix());
            shaderOpaque->setMat4("view",drawData.camera->GetViewMatrix());
            shaderOpaque->setVec3("viewPos",drawData.camera->m_position);

            material->bind(shaderOpaque, dfd);

            e->m_mesh.draw();
        }
    }


    /* ******** Transparent object ******** */

    mbuffer.bind();
    shader->use();
    shader->setMat4("proj",drawData.camera->GetProjMatrix());
    shader->setMat4("view",drawData.camera->GetViewMatrix());
    shader->setVec3("viewPos",drawData.camera->m_position);
    shader->setInt("weightFunction",weightFunction);
    shader->setFloat("colorResistance",colorResistance);
    shader->setFloat("depthRange",depthRange);
    shader->setFloat("orderingStrength",orderingStrength);

    drawData.lightManager.bind(shader);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);

    mbuffer.bind();
    glDrawBuffers(1,attach1);
    glClearColor(1,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mbuffer.bind();
    glDrawBuffers(1,attach2);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mbuffer.bind();
    glDrawBuffers(2,attach3);


    glDisable(GL_DEPTH_TEST);
    //glDisable(GL_CULL_FACE);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunci(1,GL_ONE,GL_ONE);
    glBlendFunci(0,GL_ZERO,GL_ONE_MINUS_SRC_COLOR);
    //glBlendFunc(GL_ONE,GL_ONE);
    //glBlendFuncSeparate(GL_ONE, GL_ONE, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);

    shader->setInt("depthOpaque",9);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, zbufferOpaque.depth);

    for(auto o:drawData.objectManager.getValues2()){
        shader->setMat4("model",o->getModel());
        for(auto e:o->getElements()){

            Material *material = drawData.materialManager.getValue(e->m_materialName);

            material->bind(shader, dfd);

            e->m_mesh.draw();
        }
    }


    /* ******** Final Compositing ******** */

    glDisable(GL_BLEND);

    glViewport(vp[0], vp[1], vp[2], vp[3]);

    shaderCompositing->use();
    shaderCompositing->setInt("opaqueLayer",8);
    shaderCompositing->setInt("revealTexture",9);
    shaderCompositing->setInt("accumTexture",10);

    glBindFramebuffer(GL_FRAMEBUFFER, qt_buffer);

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, zbufferOpaque.rgba);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, mbuffer.b0);
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, mbuffer.b);

    drawScreen();

}

void RendererWBOIT::resize(int w, int h){
    mbuffer.resize(w,h);
    frameBuffer.resize(w,h);
    zbufferOpaque.resize(w,h);
}

void RendererWBOIT::setWeightFunction(int v) {
    weightFunction = v;
}

void RendererWBOIT::setColorResistance(float value){
    colorResistance = value;
}

void RendererWBOIT::setDepthRange(float value){
    depthRange = value;
}

void RendererWBOIT::setOrderingStrength(float value){
    orderingStrength = value;
}
