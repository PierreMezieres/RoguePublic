//
// Created by pierre on 04/02/19.
//

#include "RendererMBOIT.hpp"
RendererMBOIT::RendererMBOIT() : Renderer("MBOIT"){
    computeWrappingZoneParameters(wrapping_parameters,M_PI/10);
}

void RendererMBOIT::draw(DrawData &drawData) {
    shader = drawData.shaderManager.getValue("MBOIT");
    shaderCompositing = drawData.shaderManager.getValue("MBOITcompositing");
    shaderOpaque = drawData.shaderManager.getValue("MBOITopaque");
    shaderCompositingFinal = drawData.shaderManager.getValue("MBOITcompositingFinal");



    GLint qt_buffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    unsigned int attach1[1] = {GL_COLOR_ATTACHMENT0};
    unsigned int attach2[1] = {GL_COLOR_ATTACHMENT1};
    unsigned int attach3[2] = {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1};
    float CLEAR_DEPTH = -99999.0;

    DataForDrawing dfd = drawData.getDataForDrawing();

    /* ******** Opaque objects ******** */

    shaderOpaque->use();
    zbufferOpaque.bind();

    zbufferOpaque.bind();
    glDrawBuffers(1,attach1);
    glClearColor(-CLEAR_DEPTH,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbufferOpaque.bind();
    glDrawBuffers(1,attach2);
    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbufferOpaque.bind();
    glDrawBuffers(2,attach3);
    glEnable(GL_DEPTH_TEST);

    drawData.lightManager.bind(shaderOpaque);
    for(auto o:drawData.objectManager.getValues()){
        shaderOpaque->setMat4("model",o->getModel());

        for(auto e:o->getElements()){

            Material *material = drawData.materialManager.getValue(e->m_materialName);

            shaderOpaque->setMat4("proj",drawData.camera->GetProjMatrix());
            shaderOpaque->setMat4("view",drawData.camera->GetViewMatrix());
            shaderOpaque->setVec3("viewPos",drawData.camera->m_position);

            material->bind(shaderOpaque, dfd);

            e->m_mesh.draw();
        }
    }


    /* ******** Compute moments ******** */

    mbuffer.bind();
    shader->use();
    shader->setMat4("proj",drawData.camera->GetProjMatrix());
    shader->setMat4("view",drawData.camera->GetViewMatrix());
    shader->setBool("trigonometric",trigonometric);
    shader->setInt("moment_number",moment_number);
    shader->setVec4("wrapping_parameters",wrapping_parameters);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    //glDisable(GL_CULL_FACE);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE,GL_ONE);

    shader->setInt("depthOpaque",9);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, zbufferOpaque.depth);

    for(auto o:drawData.objectManager.getValues2()){
        shader->setMat4("model",o->getModel());
        for(auto e:o->getElements()){

            Material *material = drawData.materialManager.getValue(e->m_materialName);

            material->bind(shader, dfd);

            e->m_mesh.draw();
        }
    }



    /* ******** Resolve moments ******** */

    frameBuffer.bind();

    shaderCompositing->use();
    shaderCompositing->setMat4("proj",drawData.camera->GetProjMatrix());
    shaderCompositing->setMat4("view",drawData.camera->GetViewMatrix());
    shaderCompositing->setVec3("viewPos",drawData.camera->m_position);
    shaderCompositing->setBool("trigonometric",trigonometric);
    shaderCompositing->setInt("moment_number",moment_number);

    shaderCompositing->setInt("momentsTexture1",9);
    shaderCompositing->setInt("b0Texture",10);
    shaderCompositing->setInt("depthOpaque",11);
    shaderCompositing->setInt("momentsTexture2",12);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D,mbuffer.b1);
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D,mbuffer.b0);
    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_2D,zbufferOpaque.depth);
    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_2D,mbuffer.b2);

    shaderCompositing->setFloat("momentOIT.overestimation",overestimation);
    shaderCompositing->setFloat("momentOIT.moment_bias",biasMoment);
    shaderCompositing->setVec4("momentOIT.wrapping_zone_parameters",wrapping_parameters);

    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    //glEnable(GL_DEPTH_TEST);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE,GL_ONE);
    //glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    drawData.lightManager.bind(shaderCompositing);


    for(auto o:drawData.objectManager.getValues2()){
        shaderCompositing->setMat4("model",o->getModel());
        for(auto e:o->getElements()){

            Material *material = drawData.materialManager.getValue(e->m_materialName);

            material->bind(shaderCompositing, dfd);

            e->m_mesh.draw();
        }
    }


    /* ******** Final Compositing ******** */

    glDisable(GL_BLEND);

    glViewport(vp[0], vp[1], vp[2], vp[3]);

    shaderCompositingFinal->use();
    shaderCompositingFinal->setInt("opaqueLayer",8);
    shaderCompositingFinal->setInt("transparentLayer",9);
    shaderCompositingFinal->setInt("totalTransmittance",10);

    glBindFramebuffer(GL_FRAMEBUFFER, qt_buffer);

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, zbufferOpaque.rgba);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, frameBuffer.texture);
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, mbuffer.b0);

    drawScreen();
}

void RendererMBOIT::resize(int w, int h){
    mbuffer.resize(w,h);
    frameBuffer.resize(w,h);
    zbufferOpaque.resize(w,h);
}

void RendererMBOIT::setBiasMoment(float v) {
    biasMoment = v;
}

void RendererMBOIT::setOverestimation(float v) {
    overestimation = v;
}

void RendererMBOIT::setTrigonometric(bool b) {
    trigonometric = b;
}

void RendererMBOIT::setMomentNumber(int number) {
    moment_number = number;
}

/* http://momentsingraphics.de/?page_id=210 */
float RendererMBOIT::circleToParameter(float angle, float* pOutMaxParameter) {
    float x = std::cos(angle);
    float y = std::sin(angle);
    float result = std::abs(y) - std::abs(x);
    result = (x<0.0f) ? (2.0f - result) : result;
    result = (y<0.0f) ? (6.0f - result) : result;
    result += (angle >= 2.0f * M_PI) ? 8.0f : 0.0f;
    if (pOutMaxParameter) {
        (*pOutMaxParameter) = 7.0f;
    }
    return result;
}

void RendererMBOIT::computeWrappingZoneParameters(Vector4 p_out_wrapping_zone_parameters,float new_wrapping_zone_angle) {
    p_out_wrapping_zone_parameters.x = new_wrapping_zone_angle;
    p_out_wrapping_zone_parameters.y = M_PI - 0.5f * new_wrapping_zone_angle;
    if (new_wrapping_zone_angle <= 0.0f) {
        p_out_wrapping_zone_parameters.z = 0.0f;
        p_out_wrapping_zone_parameters.w = 0.0f;
    }
    else {
        float zone_end_parameter;
        float zone_begin_parameter = circleToParameter(2.0f * M_PI - new_wrapping_zone_angle, &zone_end_parameter);
        p_out_wrapping_zone_parameters.z = 1.0f / (zone_end_parameter - zone_begin_parameter);
        p_out_wrapping_zone_parameters.w = 1.0f - zone_end_parameter * p_out_wrapping_zone_parameters[2];
    }
}