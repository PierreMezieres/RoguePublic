//
// Created by pierre on 14/01/19.
//

#ifndef ROGUE_RENDERERTRANSPARENCY_HPP
#define ROGUE_RENDERERTRANSPARENCY_HPP

#include <Engine/Renderer/Renderer.hpp>
#include <Plugin/CO/OIT/FrameBuffer/ZBuffer.hpp>

/**
 * \file RendererDepthPeeling
 * \brief Depth peeling front to back
 */

/**
 * \file RendererDepthPeeling
 * \brief Depth peeling front to back
 */

class RendererDepthPeeling : public Renderer {
public:
    RendererDepthPeeling();

    /**
     * @brief Compute and display the scene
     * @param drawData
     */
    void draw(DrawData &drawData);

    /**
     * @brief Resize all of the buffers with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    /**
     * @brief Set the number of pass to compute the DepthPeeling algorithm
     * @param v
     */
    void setNumPasses(int v);
private:

    Forward forward;
    Prepass prepass;
    PostProcess postProcess;


    FrameBuffer frameBuffer;

    int numPasses = 5;

    Shader *shader;
    Shader *shaderOpaque;
    Shader *shaderCompositing;

    ZBuffer zbuffer[2];
    ZBuffer zbufferOpaque;
};


#endif //ROGUE_RENDERERTRANSPARENCY_HPP
