//
// Created by pierre on 14/01/19.
//

#include "RendererDepthPeeling.hpp"

RendererDepthPeeling::RendererDepthPeeling() : Renderer("Depth Peeling"){
}

void RendererDepthPeeling::draw(DrawData &drawData) {
    shader = drawData.shaderManager.getValue("depthPeeling");
    shaderOpaque = drawData.shaderManager.getValue("depthPeelingOpaque");
    shaderCompositing = drawData.shaderManager.getValue("depthPeelingCompositing");

    GLint qt_buffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    unsigned int attach1[1] = {GL_COLOR_ATTACHMENT0};
    unsigned int attach2[1] = {GL_COLOR_ATTACHMENT1};
    unsigned int attach3[2] = {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1};
    float CLEAR_DEPTH = -99999.0;

    /* *** Opaque Object *** */

    shaderOpaque->use();
    zbufferOpaque.bind();

    DataForDrawing dfd = drawData.getDataForDrawing();

    zbufferOpaque.bind();
    glDrawBuffers(1,attach1);
    glClearColor(-CLEAR_DEPTH,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbufferOpaque.bind();
    glDrawBuffers(1,attach2);
    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbufferOpaque.bind();
    glDrawBuffers(2,attach3);
    glEnable(GL_DEPTH_TEST);

    drawData.lightManager.bind(shaderOpaque);
    for(auto o:drawData.objectManager.getValues()){
        shaderOpaque->setMat4("model",o->getModel());

        for(auto e:o->getElements()){

            Material *material = drawData.materialManager.getValue(e->m_materialName);

            shaderOpaque->setMat4("proj",drawData.camera->GetProjMatrix());
            shaderOpaque->setMat4("view",drawData.camera->GetViewMatrix());
            shaderOpaque->setVec3("viewPos",drawData.camera->m_position);

            material->bind(shaderOpaque, dfd);

            e->m_mesh.draw();
        }
    }

    /* *** Transparent Object *** */

    shader->use();
    shader->setInt("posZ",9);
    shader->setInt("color",10);
    shader->setInt("opaqueDepth",11);
    shader->setMat4("proj",drawData.camera->GetProjMatrix());
    shader->setMat4("view",drawData.camera->GetViewMatrix());
    shader->setVec3("viewPos",drawData.camera->m_position);
    drawData.lightManager.bind(shader);
    glDepthMask(GL_FALSE);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);

    zbuffer[0].bind();
    glDrawBuffers(1,attach1);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbuffer[0].bind();
    glDrawBuffers(1,attach2);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawBuffers(2,attach3);

    zbuffer[1].bind();
    glDrawBuffers(1,attach1);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    zbuffer[1].bind();
    glDrawBuffers(1,attach2);
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawBuffers(2,attach3);

    for(int i=0; i<numPasses+1; i++){
        zbuffer[i%2].bind();
        glDrawBuffers(1,attach1);
        glClearColor(CLEAR_DEPTH,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT);

        zbuffer[i%2].bind();
        glDrawBuffers(1,attach2);
        glClearColor(0,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT);

        zbuffer[i%2].bind();
        glDrawBuffers(2,attach3);
        glBlendEquation(GL_MAX);

        glActiveTexture(GL_TEXTURE9);
        glBindTexture(GL_TEXTURE_2D,zbuffer[(i+1)%2].depth);
        glActiveTexture(GL_TEXTURE10);
        glBindTexture(GL_TEXTURE_2D,zbuffer[(i+1)%2].rgba);
        glActiveTexture(GL_TEXTURE11);
        glBindTexture(GL_TEXTURE_2D,zbufferOpaque.depth);


        for(const auto o:drawData.objectManager.getValues2()){
            shader->setMat4("model",o->getModel());
            for(const auto e:o->getElements()){

                Material *material = drawData.materialManager.getValue(e->m_materialName);

                material->bind(shader, dfd);

                e->m_mesh.draw();
            }
        }
    }
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
    //glEnable(GL_CULL_FACE);

    glViewport(vp[0], vp[1], vp[2], vp[3]);

    shaderCompositing->use();
    shaderCompositing->setInt("opaqueLayer",8);
    shaderCompositing->setInt("transparentLayer",9);

    glBindFramebuffer(GL_FRAMEBUFFER, qt_buffer);

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, zbufferOpaque.rgba);
    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, zbuffer[numPasses%2].rgba);

    drawScreen();
}

void RendererDepthPeeling::resize(int w, int h){
    forward.resize(w,h);
    prepass.resize(w,h);
    zbuffer[0].resize(w,h);
    zbuffer[1].resize(w,h);
    zbufferOpaque.resize(w,h);
}

void RendererDepthPeeling::setNumPasses(int v) {
    numPasses = v;
}
