//
// Created by pierre on 11/02/19.
//

#ifndef ROGUE_RENDERERWBOIT_HPP
#define ROGUE_RENDERERWBOIT_HPP

#include <Engine/Renderer/Renderer.hpp>
#include <Engine/Renderer/DrawData.hpp>
#include "FrameBuffer/MBuffer.hpp"
#include "FrameBuffer/ZBuffer.hpp"

/**
 * \file RendererWBOIT.hpp
 * \brief weighted blended order-independent transparency
 */

/**
 * \class RendererWBOIT
 * \brief weighted blended order-independent transparency
 */
class RendererWBOIT : public Renderer {
public:
    RendererWBOIT();
    /**
     * @brief Compute and display the scene
     * @param drawData
     */
    void draw(DrawData &drawData);

    /**
     * @brief Resize all of the buffers with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    /**
     * @brief Select the weight function to use during rendring
     * @param v : ID of the weight function
     */
    void setWeightFunction(int v);

    /**
     * @brief Set the color resistance
     * @param value
     */
    void setColorResistance(float value);

    /**
     * @brief Set the depth range
     * @param value
     */
    void setDepthRange(float value);

    /**
     * @brief Set the ordering strength
     * @param value
     */
    void setOrderingStrength(float value);

private:

    Forward forward;
    Prepass prepass;
    PostProcess postProcess;

    Shader *shader;
    Shader *shaderCompositing;
    Shader *shaderOpaque;
    MBuffer mbuffer;
    FrameBuffer frameBuffer;
    ZBuffer zbufferOpaque;

    int weightFunction = 0;

    /* Weight function design parameters */
    float colorResistance = 1.0f;
    float depthRange = 200.f;
    float orderingStrength = 4.0f;



};


#endif //ROGUE_RENDERERWBOIT_HPP
