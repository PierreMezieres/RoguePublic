//
// Created by pierre on 04/02/19.
//

#include "MBuffer.hpp"
MBuffer::MBuffer() {
    glGenFramebuffers(1, &m_id);
    glGenTextures(1, &b0);
    glGenTextures(1, &b);
    glGenRenderbuffers(1, &rbo);
}

void MBuffer::init(int w, int h) {
    m_width = w;
    m_height = h;
    bind();

    glBindTexture(GL_TEXTURE_2D, b0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, m_width, m_height, 0, GL_RED, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, b0, 0);

    glBindTexture(GL_TEXTURE_2D, b);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, b, 0);

    unsigned int attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, attachments);

    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_width, m_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        Log(logError) << "Framebuffer is not complete !";
    }

    unbind();
}

MBuffer::~MBuffer() {
    glDeleteFramebuffers(1, &m_id);
    glDeleteTextures(1, &b0);
    glDeleteTextures(1, &b);
    glDeleteRenderbuffers(1, &rbo);
}

void MBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
    // Due to Qt HighDPI Display management, always set the right viewport
    glViewport(0, 0, m_width, m_height);
}

void MBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void MBuffer::resize(int w, int h) {
    init(w,h);
}
