//
// Created by pierre on 28/01/19.
//

#ifndef ROGUE_ZBUFFER_HPP
#define ROGUE_ZBUFFER_HPP

#include <Core/Macros.hpp>


/**
 * \file ZBuffer
 * \brief Z buffer to save depth and color (GL_RED, GL_RGBA)
 */

/**
 * \class ZBuffer
 * \brief Z buffer to save depth and color (GL_RED, GL_RGBA)
 */
class ZBuffer {
public:

    ZBuffer();
    ~ZBuffer();

    /**
     * @brief Init the ZBuffer with the given width and heig
     * @param w
     * @param h
     */
    void init(int w, int h);

    /**
     * @brief Bind the ZBuffer in the current OpenGL state
     */
    void bind();

    /**
     * @brief Unbind the ZBuffer from the current OpenGL stats
     */
    void unbind();

    /**
     * @brief Unbind the given buffer from the current OpenGL state
     * @param id
     */
    void unbindOther(unsigned int id);

    /**
     * @brief Resize the ZBuffer with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);
    unsigned int depth,rgba;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;
};


#endif //ROGUE_ZBUFFER_HPP
