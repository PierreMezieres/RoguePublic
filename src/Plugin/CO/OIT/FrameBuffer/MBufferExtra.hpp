//
// Created by pierre on 13/02/19.
//

#ifndef ROGUE_MBUFFEREXTRA_HPP
#define ROGUE_MBUFFEREXTRA_HPP

#include <Core/Macros.hpp>

/**
 * \file MBufferExtra
 * \brief Moment buffer (GL_RED, GL_RGBA et GL_RGBA)
 */

/**
 * \class MBufferExtra
 * \brief Moment buffer (GL_RED, GL_RGBA et GL_RGBA)
 */

class MBufferExtra {
public:
    MBufferExtra();
    ~MBufferExtra();

    /**
     * @brief Init the MufferExtra with the given width and heig
     * @param w
     * @param h
     */
    void init(int w, int h);

    /**
     * @brief Bind the MBufferExtra in the current OpenGL state
     */
    void bind();

    /**
     * @brief Unbind the MBufferExtra from the current OpenGL stats
     */
    void unbind();

    /**
     * @brief Unbind the given buffer from the current OpenGL state
     * @param id
     */
    void unbindOther(unsigned int id);

    /**
     * @brief Resize the MBufferExtra with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);
    unsigned int b0,b1,b2;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;
};


#endif //ROGUE_MBUFFEREXTRA_HPP
