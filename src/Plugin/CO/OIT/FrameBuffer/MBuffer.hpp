//
// Created by pierre on 04/02/19.
//

#ifndef ROGUE_MBUFFER_HPP
#define ROGUE_MBUFFER_HPP

#include <Core/Macros.hpp>

/**
 * \file MBuffer
 * \brief Moment buffer (GL_RED et GL_RGBA)
 */

/**
 * \class MBuffer
 * \brief Moment buffer (GL_RED et GL_RGBA)
 */
class MBuffer {
public:

    MBuffer();
    ~MBuffer();

    /**
     * @brief Init the Muffer with the given width and heig
     * @param w
     * @param h
     */
    void init(int w, int h);

    /**
     * @brief Bind the MBuffer in the current OpenGL state
     */
    void bind();

    /**
     * @brief Unbind the MBuffer from the current OpenGL stats
     */
    void unbind();

    /**
     * @brief Unbind the given buffer from the current OpenGL state
     * @param id
     */
    void unbindOther(unsigned int id);

    /**
     * @brief Resize the MBuffer with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    unsigned int b0,b;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;
};


#endif //ROGUE_MBUFFER_HPP
