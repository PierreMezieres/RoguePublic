//
// Created by pierre on 20/11/18.
//

#ifndef ROGUE_COSHORTCUT_HPP
#define ROGUE_COSHORTCUT_HPP


#include <src/Gui/ShortcutInterface.hpp>
#include <src/Engine/Object/Object.hpp>

class COShortcut : public ShortcutInterface {
public:
    COShortcut();

    enum mode_move{FREE = 0, XY = 1, YZ =2, XZ =3};

    bool keyboard(unsigned char k) override;
    void mouseclick(ButtonMouse button, float xpos, float ypos) override;
    void mousemove(ButtonMouse button, float xpos, float ypos) override;


};


#endif //ROGUE_COSHORTCUT_HPP
