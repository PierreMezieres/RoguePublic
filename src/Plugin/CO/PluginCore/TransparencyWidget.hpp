#ifndef TRANSPARENCYWIDGET_HPP
#define TRANSPARENCYWIDGET_HPP

#include <QWidget>
#include <Plugin/CO/OIT/RendererDepthPeeling.hpp>
#include <Plugin/PluginInterface.hpp>
#include <Plugin/CO/OIT/RendererMBOIT.hpp>
#include <Plugin/CO/Comparison/MainComparison.hpp>
#include <Plugin/CO/OIT/RendererWBOIT.hpp>
#include "ComparisonWidget.hpp"

namespace Ui {
class TransparencyWidget;
}

class TransparencyWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TransparencyWidget(ContextPlugin contextPlugin, RendererDepthPeeling *rendererDepthPeeling, RendererMBOIT *rendererMBOIT, RendererWBOIT* rendererWBOIT, ComparisonWidget *comparisonWidget, QWidget *parent = 0);
    ~TransparencyWidget();

private slots:

    void on_orderingStrengthSpinBox_valueChanged(double arg1);
    void on_depthRangeSpinBox_valueChanged(double arg1);
    void on_colorResistSpinBox_valueChanged(double arg1);
    void on_savepngButton_clicked();
    void on_comboBox_4_activated(int index);
    void on_comboBox_activated(int index);
    void on_saveButton_clicked();
    void on_biasBspinBox_valueChanged(int arg1);
    void on_biasAspinBox_valueChanged(int arg1);
    void on_overestimationSpinBox_valueChanged(double arg1);
    void on_passeNumberspinBox_valueChanged(int arg1);

private:
    Ui::TransparencyWidget *ui;


    ContextPlugin      m_context;
    ObjectManager *    objectManager;
    MaterialManager *  materialManager;
    ShaderManager *    shaderManager;

    RendererDepthPeeling *m_rendererDepthPeeling;
    RendererMBOIT *m_rendererMBOIT;
    RendererWBOIT *m_rendererWBOIT;
    ContextPlugin m_contextPlugin;
    ComparisonWidget *m_comparisonWidget;

    int biasA = 5;
    int biasB = 5;
    void updateMomentBias();

};

#endif // TRANSPARENCYWIDGET_HPP
