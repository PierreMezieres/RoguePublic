#include <Core/Log/Log.hpp>
#include <Core/Math/GLChronometer.hpp>
#include "ComparisonWidget.hpp"
#include "ui_ComparisonWidget.h"

#define ID "[ComparisonModule] "

ComparisonWidget::ComparisonWidget(ContextPlugin contextPlugin, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ComparisonWidget)
{
    ui->setupUi(this);

    QComboBox *img1CB = ui->img1ComboBox;
    QComboBox *img2CB = ui->img2ComboBox;
    m_mainComparison = new MainComparison(img1CB,img2CB);
    m_contextPlugin = contextPlugin;

    QPixmap pixmap("../../src/Plugin/CO/Icon/tv.png");
    QIcon ButtonIcon(pixmap);
    ui->pushButton_3->setIcon(ButtonIcon);
    ui->pushButton->setIcon(ButtonIcon);
    //ui->pushButton_3->setIconSize(pixmap.rect().size());
}

ComparisonWidget::~ComparisonWidget()
{
    delete m_mainComparison;
    delete ui;
}

MainComparison *ComparisonWidget::getMainComparison() {
    return m_mainComparison;
}

void ComparisonWidget::on_launchComparisonButton_clicked()
{
    m_mainComparison->LaunchComparison();
}

void ComparisonWidget::on_img1ComboBox_activated(const QString &arg1)
{
    m_mainComparison->setImg1(arg1.toStdString());
}

void ComparisonWidget::on_img2ComboBox_activated(const QString &arg1)
{
    m_mainComparison->setImg2(arg1.toStdString());
}

void ComparisonWidget::on_pushButton_clicked()
{
    m_mainComparison->displayImg1();
}

void ComparisonWidget::on_pushButton_3_clicked()
{
    m_mainComparison->displayImg2();
}

void ComparisonWidget::on_operatorComboBox_activated(int index)
{
    m_mainComparison->setNumberOperator(index);
}

void ComparisonWidget::on_saveActualButton_clicked()
{
    m_mainComparison->saveImg(m_contextPlugin.openGLWidget->grabFramebuffer(),m_contextPlugin.scene->renderer->getID());
}

void ComparisonWidget::on_pushButton_4_clicked()
{
    m_mainComparison->saveImgPng(m_contextPlugin.openGLWidget->grabFramebuffer(),m_contextPlugin.scene->renderer->getID());
}

void ComparisonWidget::on_generateButton_clicked()
{
    m_contextPlugin.openGLWidget->update();
    glFinish();
    std::int64_t last = m_contextPlugin.openGLWidget->getLastTime();
    ui->label->setText(QString::fromStdString("Execution time (ms):" + std::to_string(last)));
}

void ComparisonWidget::on_pushButton_2_clicked()
{
    m_mainComparison->saveResult();
}

void ComparisonWidget::on_spinBox_valueChanged(int arg1)
{
    m_mainComparison->setMultiplicator(arg1);
}
