#include "TransparencyWidget.hpp"
#include "ui_TransparencyWidget.h"
#include <src/Core/Macros.hpp>

#define ID "[TransparencyModule] "

TransparencyWidget::TransparencyWidget(ContextPlugin contextPlugin, RendererDepthPeeling *rendererDepthPeeling,RendererMBOIT *rendererMBOIT, RendererWBOIT *rendererWBOIT, ComparisonWidget *comparisonWidget, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TransparencyWidget),
    m_rendererDepthPeeling{rendererDepthPeeling},
    m_rendererMBOIT{rendererMBOIT},
    m_rendererWBOIT{rendererWBOIT},
    m_contextPlugin{contextPlugin},
    m_comparisonWidget{comparisonWidget}
{
    ui->setupUi(this);

    m_context = contextPlugin;
    objectManager = &m_context.scene->drawData.objectManager;
    materialManager = &m_context.scene->drawData.materialManager;
    shaderManager = &m_context.scene->drawData.shaderManager;

    shaderManager->addValue("depthPeeling",new Shader("../Shaders/OIT/DepthPeeling/DepthPeeling.vs.glsl","../Shaders/OIT/DepthPeeling/DepthPeeling.fs.glsl"));
    shaderManager->addValue("depthPeelingOpaque",new Shader("../Shaders/OIT/DepthPeeling/DepthPeelingOpaque.vs.glsl","../Shaders/OIT/DepthPeeling/DepthPeelingOpaque.fs.glsl"));
    shaderManager->addValue("depthPeelingCompositing", new Shader("../Shaders/OIT/DepthPeeling/DepthPeelingCompositing.vs.glsl","../Shaders/OIT/DepthPeeling/DepthPeelingCompositing.fs.glsl"));

    shaderManager->addValue("MBOIT", new Shader("../Shaders/OIT/MBOIT/MBOIT.vs.glsl","../Shaders/OIT/MBOIT/MBOIT.fs.glsl"));
    shaderManager->addValue("MBOITcompositing", new Shader("../Shaders/OIT/MBOIT/MBOITcompositing.vs.glsl","../Shaders/OIT/MBOIT/MBOITcompositing.fs.glsl"));
    shaderManager->addValue("MBOITopaque", new Shader("../Shaders/OIT/MBOIT/MBOITopaque.vs.glsl","../Shaders/OIT/MBOIT/MBOITopaque.fs.glsl"));
    shaderManager->addValue("MBOITcompositingFinal", new Shader("../Shaders/OIT/MBOIT/MBOITfinalCompositing.vs.glsl","../Shaders/OIT/MBOIT/MBOITfinalCompositing.fs.glsl"));

    shaderManager->addValue("WBOIT", new Shader("../Shaders/OIT/WBOIT/WBOIT.vs.glsl","../Shaders/OIT/WBOIT/WBOIT.fs.glsl"));
    shaderManager->addValue("WBOITopaque", new Shader("../Shaders/OIT/WBOIT/WBOITopaque.vs.glsl","../Shaders/OIT/WBOIT/WBOITopaque.fs.glsl"));
    shaderManager->addValue("WBOITcompositing", new Shader("../Shaders/OIT/WBOIT/WBOITcompositing.vs.glsl","../Shaders/OIT/WBOIT/WBOITcompositing.fs.glsl"));

    ui->colorResistSpinBox->setToolTip(QString("Increase if low-coverage foreground transparents are affecting background transparent color."));
    ui->depthRangeSpinBox->setToolTip(QString("Depth range over which significant ordering discrimination is required. Decrease if high-opacity surfaces seem \"too transparent\", increase if distant transparents are blending together too much."));
    ui->orderingStrengthSpinBox->setToolTip(QString("Increase if background is showing through foreground too much."));

    ui->colorResistSpinBox->setDisabled(true);
    ui->depthRangeSpinBox->setDisabled(true);
    ui->orderingStrengthSpinBox->setDisabled(true);



}

TransparencyWidget::~TransparencyWidget()
{
    delete ui;
}

void TransparencyWidget::on_passeNumberspinBox_valueChanged(int arg1)
{
    m_rendererDepthPeeling->setNumPasses(arg1);
    m_contextPlugin.openGLWidget->update();
}

void TransparencyWidget::updateMomentBias(){
    float bias = biasA * std::pow(10,-biasB);
    m_rendererMBOIT->setBiasMoment(bias);
    m_contextPlugin.openGLWidget->update();
}

void TransparencyWidget::on_overestimationSpinBox_valueChanged(double arg1)
{
    m_rendererMBOIT->setOverestimation(arg1);
    m_contextPlugin.openGLWidget->update();
}

void TransparencyWidget::on_biasAspinBox_valueChanged(int arg1)
{
    biasA = arg1;
    updateMomentBias();
}

void TransparencyWidget::on_biasBspinBox_valueChanged(int arg1)
{
    biasB = arg1;
    updateMomentBias();
}

void TransparencyWidget::on_saveButton_clicked()
{
    m_comparisonWidget->getMainComparison()->saveImg(m_contextPlugin.openGLWidget->grabFramebuffer(),m_contextPlugin.scene->renderer->getID());
}

void TransparencyWidget::on_comboBox_activated(int index)
{
    m_rendererWBOIT->setWeightFunction(index);
    bool enable = true;

    if (index != 5)
        enable = false;

    ui->colorResistSpinBox->setEnabled(enable);
    ui->depthRangeSpinBox->setEnabled(enable);
    ui->orderingStrengthSpinBox->setEnabled(enable);

    m_contextPlugin.openGLWidget->update();
}

void TransparencyWidget::on_comboBox_4_activated(int index)
{

    switch(index){
        case 0: m_rendererMBOIT->setMomentNumber(4); break;
        case 1: m_rendererMBOIT->setMomentNumber(6); break;
        case 2: m_rendererMBOIT->setMomentNumber(8); break;
        case 3: m_rendererMBOIT->setMomentNumber(2); break;
        case 4: m_rendererMBOIT->setMomentNumber(3); break;
        case 5: m_rendererMBOIT->setMomentNumber(4); break;
    }
    m_rendererMBOIT->setTrigonometric((index/3)!=0);
    m_contextPlugin.openGLWidget->update();
}

void TransparencyWidget::on_savepngButton_clicked()
{
    m_comparisonWidget->getMainComparison()->saveImgPng(m_contextPlugin.openGLWidget->grabFramebuffer(),m_contextPlugin.scene->renderer->getID());
}

void TransparencyWidget::on_colorResistSpinBox_valueChanged(double arg1){
    m_rendererWBOIT->setColorResistance(arg1);
    m_contextPlugin.openGLWidget->update();
}



void TransparencyWidget::on_depthRangeSpinBox_valueChanged(double arg1){
    m_rendererWBOIT->setDepthRange(arg1);
    m_contextPlugin.openGLWidget->update();
}

void TransparencyWidget::on_orderingStrengthSpinBox_valueChanged(double arg1){
    m_rendererWBOIT->setOrderingStrength(arg1);
    m_contextPlugin.openGLWidget->update();
}
