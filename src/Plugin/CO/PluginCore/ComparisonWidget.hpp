#ifndef COMPARISONWIDGET_HPP
#define COMPARISONWIDGET_HPP

#include <QWidget>
#include <Plugin/CO/Comparison/MainComparison.hpp>
#include <Plugin/PluginInterface.hpp>

namespace Ui {
class ComparisonWidget;
}

class ComparisonWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ComparisonWidget(ContextPlugin contextPlugin, QWidget *parent = 0);
    ~ComparisonWidget();

    MainComparison *getMainComparison();

private slots:
    void on_spinBox_valueChanged(int arg1);
    void on_pushButton_2_clicked();
    void on_generateButton_clicked();
    void on_pushButton_4_clicked();
    void on_saveActualButton_clicked();
    void on_operatorComboBox_activated(int index);
    void on_pushButton_3_clicked();
    void on_pushButton_clicked();
    void on_img2ComboBox_activated(const QString &arg1);
    void on_img1ComboBox_activated(const QString &arg1);
    void on_launchComparisonButton_clicked();

private:
    Ui::ComparisonWidget *ui;

    MainComparison *m_mainComparison;
    ContextPlugin m_contextPlugin;



};

#endif // COMPARISONWIDGET_HPP
