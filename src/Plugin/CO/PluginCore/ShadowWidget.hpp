#ifndef SHADOWWIDGET_HPP
#define SHADOWWIDGET_HPP

#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <src/Plugin/PluginInterface.hpp>
#include <src/Plugin/CO/Shadow/RendererPCF.hpp>
#include <src/Plugin/CO/Shadow/RendererMSM.hpp>
#include <src/Plugin/CO/Shadow/RendererVSM.hpp>
#include "ComparisonWidget.hpp"


namespace Ui {
class ShadowWidget;
}

class ShadowWidget : public QWidget
{
    Q_OBJECT

public:
    ShadowWidget(ContextPlugin contextPlugin, RendererPCF *rendererPCF, RendererMSM *rendererMSM, RendererVSM *rendererVSM, ComparisonWidget *comparisonWidget, QWidget *parent = 0);
    ~ShadowWidget();

private slots:
    void on_saveButton_clicked();
    void on_savepngButton_clicked();
    void on_momentBiasA_valueChanged(int arg1);

    void on_momentBaisB_valueChanged(int arg1);

    void on_depthBias_valueChanged(double arg1);

    void on_KernelSize_valueChanged(int kernelSize);

    void on_PCFStandardDeviation_valueChanged(double arg1);

    void on_PCFDepthBias_valueChanged(double arg1);

    void on_kernelSizeMSM_valueChanged(int arg1);

    void on_standardDeviationMSM_valueChanged(double arg1);

    void on_KernelSizeVSM_valueChanged(int arg1);

    void on_standardDeviationVSM_valueChanged(double arg1);

    void on_MinVarVSM_valueChanged(double arg1);

private:
    Ui::ShadowWidget * ui;

    RendererPCF* m_rendererPCF;
    RendererMSM* m_rendererMSM;
    RendererVSM* m_rendererVSM;
    ComparisonWidget * m_comparisonWidget;

    ContextPlugin      m_context;
    ObjectManager *    objectManager;
    MaterialManager *  materialManager;
    ShaderManager *    shaderManager;
    ContextPlugin m_contextPlugin;

    QPushButton *      m_saveImg;
    QPushButton *      m_saveAsPNG;
    QSpinBox*          m_momentBiasA;
    QSpinBox*          m_momentBiasB;
    QDoubleSpinBox*    m_depthBias;



};

#endif // SHADOWWIDGET_HPP
