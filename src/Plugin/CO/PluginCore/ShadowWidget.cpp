#include "ShadowWidget.hpp"
#include "ui_ShadowWidget.h"

ShadowWidget::ShadowWidget(ContextPlugin contextPlugin, RendererPCF *rendererPCF, RendererMSM *rendererMSM, RendererVSM* rendererVSM, ComparisonWidget *comparisonWidget, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ShadowWidget)
{
  ui->setupUi(this);
  m_saveImg = ui->saveButton;
  m_saveAsPNG = ui->savepngButton;
  m_momentBiasA = ui->momentBiasA;
  m_momentBiasB = ui->momentBaisB;
  m_depthBias = ui->depthBias;

  m_context = contextPlugin;
  objectManager = &m_context.scene->drawData.objectManager;
  materialManager = &m_context.scene->drawData.materialManager;
  shaderManager = &m_context.scene->drawData.shaderManager;

  shaderManager->addValue("mapDepthLight",new Shader("../Shaders/Shadow/ShadowMap/depthMap.vs.glsl",
                                                        "../Shaders/Shadow/ShadowMap/depthMap.fs.glsl"));
  shaderManager->addValue("screenDepthDebug",new Shader("../Shaders/Shadow/Debug/screenDepthDebug.vs.glsl", "../Shaders/Shadow/Debug/screenDepthDebug.fs.glsl"));
  shaderManager->addValue("MSMMapDepthLight", new Shader("../Shaders/Shadow/ShadowMap/MSM/momentMap.vs.glsl", "../Shaders/Shadow/ShadowMap/MSM/momentMap.fs.glsl"));
  shaderManager->addValue("VSMMapDepthLight", new Shader("../Shaders/Shadow/ShadowMap/VSM/varMap.vs.glsl", "../Shaders/Shadow/ShadowMap/VSM/varMap.fs.glsl"));

  m_rendererPCF = rendererPCF;
  m_rendererMSM = rendererMSM;
  m_rendererVSM = rendererVSM;
  m_comparisonWidget = comparisonWidget;
  m_contextPlugin = contextPlugin;


}

ShadowWidget::~ShadowWidget()
{
  delete ui;
}

void ShadowWidget::on_momentBiasA_valueChanged(int val) {
  m_rendererMSM->setMomentBias(float(val) * float(std::pow(10, float(-m_momentBiasB->value()))));
  m_contextPlugin.openGLWidget->update();
}

void ShadowWidget::on_momentBaisB_valueChanged(int val) {
  m_rendererMSM->setMomentBias(float(m_momentBiasA->value()) * float(std::pow(10, -val)));
  m_contextPlugin.openGLWidget->update();
}

void ShadowWidget::on_depthBias_valueChanged(double val) {
  m_rendererMSM->setDepthBias(float(val));
  m_contextPlugin.openGLWidget->update();
}

void ShadowWidget::on_KernelSize_valueChanged(int kernelSize) {
    m_rendererPCF->setKernelSize(kernelSize);
    m_contextPlugin.openGLWidget->update();
}

void ShadowWidget::on_PCFStandardDeviation_valueChanged(double sd) {
    m_rendererPCF->setStandardDeviation(float(sd));
    m_contextPlugin.openGLWidget->update();
}

void ShadowWidget::on_PCFDepthBias_valueChanged(double depthBias) {
    m_rendererPCF->setDepthBias(float(depthBias));
    m_contextPlugin.openGLWidget->update();
}

void ShadowWidget::on_kernelSizeMSM_valueChanged(int kernelSize) {
    m_rendererMSM->setKernelSize(kernelSize);
    m_contextPlugin.openGLWidget->update();

}

void ShadowWidget::on_standardDeviationMSM_valueChanged(double sd) {
    m_rendererMSM->setStandardDeviation(float(sd));
    m_contextPlugin.openGLWidget->update();

}

void ShadowWidget::on_KernelSizeVSM_valueChanged(int kernelSize) {
    m_rendererVSM->setKernelSize(uint(kernelSize));
    m_contextPlugin.openGLWidget->update();

}

void ShadowWidget::on_standardDeviationVSM_valueChanged(double sd) {
    m_rendererVSM->setStandardDeviation(float(sd));
    m_contextPlugin.openGLWidget->update();

}

void ShadowWidget::on_MinVarVSM_valueChanged(double minVar){
    m_rendererVSM->setVarClamp(float(minVar));
    m_contextPlugin.openGLWidget->update();

}

void ShadowWidget::on_savepngButton_clicked()
{
    m_comparisonWidget->getMainComparison()->saveImgPng(m_contextPlugin.openGLWidget->grabFramebuffer(),m_contextPlugin.scene->renderer->getID());
}

void ShadowWidget::on_saveButton_clicked()
{
    m_comparisonWidget->getMainComparison()->saveImg(m_contextPlugin.openGLWidget->grabFramebuffer(),m_contextPlugin.scene->renderer->getID());
}
