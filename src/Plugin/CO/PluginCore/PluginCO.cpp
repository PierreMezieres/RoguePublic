//
// Created by pierre on 20/11/18.
//

#include <Plugin/CO/OIT/RendererDepthPeeling.hpp>
#include <Plugin/CO/Shadow/RendererPCF.hpp>
#include <Plugin/CO/OIT/RendererMBOIT.hpp>
#include <Plugin/CO/OIT/RendererWBOIT.hpp>
#include "PluginCO.hpp"

PluginCO::PluginCO() {
    m_name = "PluginCO";
}
void PluginCO::loadData(){

    RendererDepthPeeling* rendererDepthPeeling = new RendererDepthPeeling();
    RendererMBOIT * rendererMBOIT = new RendererMBOIT();
    RendererWBOIT * rendererWBOIT = new RendererWBOIT();

    RendererPCF* rendererPCF = new RendererPCF();
    RendererMSM* rendererMSM = new RendererMSM();
    RendererVSM* rendererVSM = new RendererVSM();



    comparisonWidget = new ComparisonWidget(m_context);
    transparencyWidget = new TransparencyWidget(m_context, rendererDepthPeeling, rendererMBOIT, rendererWBOIT, comparisonWidget);
    shadowWidget = new ShadowWidget(m_context, rendererPCF, rendererMSM, rendererVSM, comparisonWidget);



    coShortcut = new COShortcut();

    registerWidget(shadowWidget,"Shadow");
    registerWidget(transparencyWidget,"Transparency");
    registerWidget(comparisonWidget,"Comparison");

    registerShortcut(coShortcut);

    registerRenderer(rendererPCF);
    registerRenderer(rendererMSM);
    registerRenderer(rendererVSM);

    registerRenderer(rendererMBOIT);
    registerRenderer(rendererDepthPeeling);
    registerRenderer(rendererWBOIT);
}
