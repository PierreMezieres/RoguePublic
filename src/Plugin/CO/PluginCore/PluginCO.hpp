//
// Created by pierre on 20/11/18.
//

#ifndef ROGUE_PLUGINCO_HPP
#define ROGUE_PLUGINCO_HPP


#include <src/Plugin/PluginInterface.hpp>
#include "ShadowWidget.hpp"
#include "TransparencyWidget.hpp"
#include "ComparisonWidget.hpp"
#include "COShortcut.hpp"
#include <./Plugin/CO/Shadow/RendererMSM.hpp>
#include <./Plugin/CO/Shadow/RendererVSM.hpp>


/** \class PluginCO
 * \brief [Plugin CO]
 */
class PluginCO : public PluginInterface {
public:

    PluginCO();
    void loadData();

    ShadowWidget *shadowWidget;
    TransparencyWidget *transparencyWidget;
    ComparisonWidget *comparisonWidget;
    COShortcut *coShortcut;

};


#endif //ROGUE_PLUGINCO_HPP
