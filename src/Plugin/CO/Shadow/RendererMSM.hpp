//
// Created by jb on 11/02/19.
//

#ifndef ROGUE_RENDERERMSM_HPP
#define ROGUE_RENDERERMSM_HPP
#include <./Engine/Renderer/Renderer.hpp>
#include <./Engine/Texture/FrameBuffers/ShadowMap.hpp>
#include <Plugin/CO/Shadow/Features/ShadowMSM.hpp>
#include <Plugin/CO/Shadow/Features/GaussianBlur.hpp>
#include <Engine/Renderer/Features/PostProcessGamma.hpp>

/**
 * @file RendererMSM.hpp
 * @class RendererMSM
 * @brief Render the given scene using MSM method to render shadows
*/

class RendererMSM : public Renderer {

    //Functions
public:
    RendererMSM();
    /**
     * @brief Compute and display the scene
     * @param drawData
     */
    void draw(DrawData &drawData);

    /**
     * @brief Resize all of the buffers with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    /**
     * @brief Set the moment biais
     * @param momentBias
     */
    void setMomentBias(float momentBias);

    /**
     * @brief Set the depth biais
     * @param depthBias
     */
    void setDepthBias(float depthBias);

    /**
     * @brief Set the standard deviation
     * @param sd
     */
    void setStandardDeviation(float sd);

    /**
     * @brief Set the size of the gaussian kernel
     * @param ks
     */
    void setKernelSize(int ks);

private:
    void computeShadowMaps(DrawData& drawData);
    Vector3 getLightPosition(Light * light);

    void debugPostProcess(DrawData* drawData, int bufferId);

//Member variables
private:

    Prepass m_prepass;
    ShadowMSM m_shadow;
    PostProcessGamma m_postProcess;

    bool m_recomputeShadowMap;

    ShadowMapBuffer m_shadowMap;

    Object *m_screenObj;
    Shader *m_screenShader;
    Shader *m_shadowMapShader;
    Shader *m_shadowMapPointShader;

    Shader *screenShader;

    std::vector<GaussianBlur*> gaussianBlurPostProcess;

    float m_momentBias;
    float m_depthBias;
    int m_kernelSize;
    float m_standardDeviation;

    int m_sm_height = 1024;
    int m_sm_width = 1024;

};


#endif //ROGUE_RENDERERMSM_HPP
