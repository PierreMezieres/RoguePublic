//
// Created by jb on 17/02/19.
//

#ifndef ROGUE_VSMBUFFER_HPP
#define ROGUE_VSMBUFFER_HPP

#include <Core/Macros.hpp>

/**
 * @file VSMShadowMapBuffer.hpp
 * @class VSMShadowMapBuffer
 * @brief A texture that can store up to 2 moments (multisampled)
 */

class VSMBuffer {
public:

    VSMBuffer();
    ~VSMBuffer();

    /**
     * @brief Init the Muffer with the given width and heig
     * @param w
     * @param h
     */
    void init(int w, int h);

    /**
     * @brief Bind the MBuffer in the current OpenGL state
     */
    void bind();

    /**
     * @brief Unbind the MBuffer from the current OpenGL stats
     */
    void unbind();

    /**
     * @brief Unbind the given buffer from the current OpenGL state
     * @param id
     */
    void unbindOther(unsigned int id);

    /**
     * @brief Resize the MBuffer with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    /**
     * @brief Create a non multisampled texture from the shadowmap
     */
    void blit();
    unsigned int texture;
    /**
     * @brief Change the texture
     * @param texture
     */
    void changeTexture(unsigned int texture);

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    uint rbo;
    uint intermediateFBO;
    uint textureColorBufferMultiSampled;
};


#endif //ROGUE_VSMBUFFER_HPP
