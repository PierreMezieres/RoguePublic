//
// Created by jb on 17/02/19.
//

#ifndef ROGUE_BLURVSMTEXTURE_HPP
#define ROGUE_BLURVSMTEXTURE_HPP

/**
 * @file BlurVSMTexture.hpp
 * @class BlurVSMTexture
 * @brief Texture to blur when using VSM method
 */

class BlurVSMTexture {
public:
    BlurVSMTexture();
    ~BlurVSMTexture();

    /**
     * @brief Init the Muffer with the given width and heig
     * @param w
     * @param h
     */
    void init(int w, int h);

    /**
     * @brief Bind the MBuffer in the current OpenGL state
     */
    void bind();

    /**
     * @brief Unbind the MBuffer from the current OpenGL stats
     */
    void unbind();

    /**
     * @brief Unbind the given buffer from the current OpenGL state
     * @param id
     */
    void unbindOther(unsigned int id);

    /**
     * @brief Resize the MBuffer with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);
    unsigned int texture;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;
};


#endif //ROGUE_BLURVSMTEXTURE_HPP
