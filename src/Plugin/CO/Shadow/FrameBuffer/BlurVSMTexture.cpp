//
// Created by jb on 17/02/19.
//

#include "BlurVSMTexture.hpp"
#include <Core/Macros.hpp>


BlurVSMTexture::BlurVSMTexture() {
    glGenFramebuffers(1, &m_id);
    glGenTextures(1, &texture);
    glGenRenderbuffers(1, &rbo);
}

void BlurVSMTexture::init(int w, int h) {
    m_width = w;
    m_height = h;
    bind();

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, m_width, m_height, 0, GL_RG, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
        Log(logError) << "Framebuffer is not complete !";
    }

    unbind();
}

BlurVSMTexture::~BlurVSMTexture() {
    glDeleteFramebuffers(1, &m_id);
    glDeleteTextures(1, &texture);
    glDeleteRenderbuffers(1, &rbo);
}

void BlurVSMTexture::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
    // Due to Qt HighDPI Display management, always set the right viewport
    glViewport(0, 0, m_width, m_height);
}

void BlurVSMTexture::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void BlurVSMTexture::unbindOther(unsigned int id){
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void BlurVSMTexture::resize(int w, int h) {
    init(w,h);
}
