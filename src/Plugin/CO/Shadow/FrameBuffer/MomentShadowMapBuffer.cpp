//
// Created by jb on 11/02/19.
//

#include "MomentShadowMapBuffer.hpp"


MomentShadowMapBuffer::MomentShadowMapBuffer() {
    glGenFramebuffers(1, &m_id);
    glGenFramebuffers(1, &intermediateFBO);
    glGenTextures(1, &texture);
    glGenTextures(1, &textureColorBufferMultiSampled);
    glGenRenderbuffers(1, &rbo);
}

void MomentShadowMapBuffer::init(int w, int h) {
    m_width = w;
    m_height = h;
    bind();

    // create a multisampled color attachment texture
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, textureColorBufferMultiSampled);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA32F, m_width, m_height, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, textureColorBufferMultiSampled, 0);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    // create a (also multisampled) renderbuffer object for depth and stencil attachments

    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH24_STENCIL8/*GL_DEPTH32F_STENCIL8*/, m_width, m_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        Log() << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!";
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // configure second post-processing framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, intermediateFBO);
    // create a color attachment texture

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    float borderColor[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        Log() << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!";
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    unbind();

}

MomentShadowMapBuffer::~MomentShadowMapBuffer() {
    glDeleteFramebuffers(1, &m_id);
    glDeleteFramebuffers(1, &intermediateFBO);
    glDeleteTextures(1, &textureColorBufferMultiSampled);
    glDeleteTextures(1, &texture);
    glDeleteRenderbuffers(1, &rbo);
}

void MomentShadowMapBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
    // Due to Qt HighDPI Display management, always set the right viewport
    glViewport(0, 0, m_width, m_height);
}

void MomentShadowMapBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void MomentShadowMapBuffer::unbindOther(unsigned int id){
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void MomentShadowMapBuffer::resize(int w, int h) {
    init(w,h);
}

void MomentShadowMapBuffer::blit() {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, m_id);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, intermediateFBO);
    glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, m_width, m_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void MomentShadowMapBuffer::changeTexture(unsigned int tex) {
    glDeleteTextures(1, &texture);
    texture = tex;
}
