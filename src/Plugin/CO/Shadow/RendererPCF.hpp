//
// Created by pierre on 09/01/19.
//

#ifndef ROGUE_RENDERERPCF_HPP
#define ROGUE_RENDERERPCF_HPP


#include <./Engine/Renderer/Renderer.hpp>
#include <./Engine/Texture/FrameBuffers/ShadowMap.hpp>
#include <./Engine/Texture/ShadowMapBuffer.hpp>
#include <Plugin/CO/Shadow/Features/Shadow.hpp>
#include <Engine/Renderer/Features/PostProcessGamma.hpp>


/**
 * @file RendererPCF.hpp
 * @class RendererPCF
 * @brief Render the given scene using PCF method to render shadows
*/

class RendererPCF : public Renderer {

  //Functions
public:
    RendererPCF();
    /**
     * @brief Compute and display the scene
     * @param drawData
     */
    void draw(DrawData &drawData);

    /**
     * @brief Resize all of the buffers with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);


    /**
     * @brief Set the depth biais
     * @param depthBias
     */
    void setDepthBias(float depthBias);

    /**
     * @brief Set the standard deviation
     * @param sd
     */
    void setStandardDeviation(float sd);

    /**
     * @brief Set the size of the gaussian kernel
     * @param ks
     */
    void setKernelSize(int ks);

private:
  void computeShadowMaps(DrawData& drawData);
  Vector3 getLightPosition(Light * light);

  void debugPostProcess(DrawData* drawData, int bufferId);

//Member variables
private:

  Prepass m_prepass;
  Shadow m_shadow;
  PostProcessGamma m_postProcess;

  bool m_recomputeShadowMap;

  ShadowMapBuffer m_shadowMap;

  Object *m_screenObj;
  Shader *m_screenShader;
  Shader *m_shadowMapShader;
  Shader *m_shadowMapPointShader;

  Shader *screenShader;


  int m_sm_height = 1024;
  int m_sm_width = 1024;

};


#endif //ROGUE_RENDEREREXAMPLE_HPP
