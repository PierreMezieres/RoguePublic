//
// Created by jb on 11/02/19.
//

#include "RendererMSM.hpp"
#include <Engine/Light/DirLight.hpp>
#include <Engine/Light/PointLight.hpp>
#include <Engine/Light/SpotLight.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>

RendererMSM::RendererMSM()
        : Renderer("MSM") {
    m_momentBias = 3.f*std::pow(10, -5);
    m_depthBias = 0.001;
    m_kernelSize = 9;
    m_standardDeviation = 2.4f;

    m_shadow.setDepthBias(m_depthBias);
    m_shadow.setMomentBias(m_momentBias);
}

void RendererMSM::draw(DrawData &drawData) {

    m_screenShader = drawData.shaderManager.getValue("screenDepthDebug");
    m_shadowMapShader = drawData.shaderManager.getValue("MSMMapDepthLight");
    //m_shadowMapShader = drawData.shaderManager.getValue("TMSMMapDepthLight");
    //m_shadowMapPointShader = drawData.shaderManager.getValue("cubeMapDepthLight");

    GLint qt_buffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    /*
     ==============================================================
                              Shadow pass
     ==============================================================
    */
    computeShadowMaps(drawData);

    /*
     ==============================================================
                              Light pass
     ==============================================================
    */
    m_shadow.use(drawData,m_drawfill);

    /*
     ==============================================================
                              Post process pass
     ==============================================================
    */
    glViewport(vp[0], vp[1], vp[2], vp[3]);
    m_postProcess.use(qt_buffer, m_shadow.getTexture());
    //debugPostProcess(&drawData, qt_buffer);

    for (GaussianBlur* g : gaussianBlurPostProcess)
        delete g;

    gaussianBlurPostProcess.clear();
    m_shadow.clearShadowMaps();
}

void RendererMSM::resize(int w, int h){
    m_shadow.resize(w,h);
    m_prepass.resize(w,h);
    m_shadowMap.resize(w,h);
}

void RendererMSM::computeShadowMaps(DrawData& drawData) {
    glCullFace(GL_FRONT);
    glEnable(GL_MULTISAMPLE);

    Matrix4 lightProjection, lightView;
    std::vector<Matrix4> lightViews;

    for(const auto& light : drawData.lightManager.getValues()) {

        lightViews.clear();
        lightView = glm::lookAt(Vector3(0.5f,2,2), Vector3(0.f), Vector3(0.f, 1.f, 0.f));
        Vector3 position;

        switch(light->m_type){
            case Light::POINT_LIGHT:{
                auto *pLight = dynamic_cast<PointLight*>(light);
                //pLight->m_shadowMap.resize(m_sm_width, m_sm_height);
                //pLight->m_shadowMap.bind();
                m_shadow.addCubeMap();

                position = pLight->getPosition();
                lightProjection = glm::perspective<float>(glm::radians(90.f), float(m_sm_width)/float(m_sm_height), 1.f, 20.f);

                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3( 1.f, 0.f, 0.f), Vector3(0.f, -1.f, 0.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(-1.f, 0.f, 0.f), Vector3(0.f, -1.f, 0.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 1.f, 0.f), Vector3(0.f, 0.f, 1.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, -1.f, 0.f), Vector3(0.f, 0.f, -1.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 0.f, 1.f), Vector3(0.f, -1.f, 0.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 0.f,-1.f), Vector3(0.f,-1.f, 0.f)));

                for (uint i = 0; i < 6; i++)
                    pLight->m_lightSpaceMats[i] = lightViews[i];

            }
                break;
            case Light::SPOT_LIGHT:{
                auto *sLight = dynamic_cast<SpotLight*>(light);
                //sLight->m_shadowMap.resize(m_sm_width,m_sm_height);
                //sLight->m_shadowMap.bind();
                m_shadow.addShadowMap();
                m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->resize(m_sm_width,m_sm_height);
                m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->bind();

                position = sLight->getPosition();
                lightProjection = glm::perspective<float>(glm::radians(sLight->getOuterAngle()*2.f), 1.0f, 1.f, 20.f);
                lightView = glm::lookAt(position, position + glm::normalize(sLight->getDirection()), drawData.camera->m_worldUp);
            }
                break;
            case Light::DIR_LIGHT:{
                auto *dLight = dynamic_cast<DirLight*>(light);
                //dLight->m_shadowMap.resize(m_sm_width,m_sm_height);
                //dLight->m_shadowMap.bind();
                m_shadow.addShadowMap();
                m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->resize(m_sm_width, m_sm_height);
                m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->bind();

                position = -10.f * glm::normalize(dLight->getDirection());
                lightProjection = glm::ortho(-10.f, 10.f, -10.f, 10.f, 1.f, 20.f);
                lightView = glm::lookAt(position, position + glm::normalize(dLight->getDirection()), drawData.camera->m_worldUp);
            }
                break;
        }
        if (light->m_type == Light::DIR_LIGHT || light->m_type == Light::SPOT_LIGHT) {
            m_shadowMapShader->use();
            Matrix4 lsm = lightProjection * lightView;
            light->setLightSpace(lsm);
            m_shadowMapShader->setMat4("lightSpaceMatrix", lsm);

            glClearColor(1,1,1,1);
            glClear(GL_DEPTH_BUFFER_BIT);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);
            glClear(GL_COLOR_BUFFER_BIT);
            glEnable(GL_MULTISAMPLE);

            for(auto& obj : drawData.objectManager.getValues()) {
                m_shadowMapShader->setMat4("model",obj->getModel());
                for (auto& e : obj->getElements()) {
                    e->m_mesh.draw();
                }
            }
            m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->blit();
            gaussianBlurPostProcess.emplace_back(new GaussianBlur());
            gaussianBlurPostProcess[gaussianBlurPostProcess.size()-1]->resize(m_sm_width, m_sm_height);
            gaussianBlurPostProcess[gaussianBlurPostProcess.size()-1]->use(m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->texture, m_kernelSize, m_standardDeviation);
            m_shadow.m_MSMBuffers[m_shadow.m_MSMBuffers.size()-1]->changeTexture(gaussianBlurPostProcess[gaussianBlurPostProcess.size()-1]->getTexture());
        }
        else if (light->m_type == Light::POINT_LIGHT) {
            m_shadowMapShader->use();
            m_shadowMapShader->setVec3("lightPos", position);
            m_shadowMapShader->setFloat("far_plane", 20.f);
            for (uint i = 0; i < 6; i++) {
                m_shadow.m_MSMCubeMaps[m_shadow.m_MSMCubeMaps.size()-1][i].resize(m_sm_width, m_sm_height);
                m_shadow.m_MSMCubeMaps[m_shadow.m_MSMCubeMaps.size()-1][i].bind();
                glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
                glClear(GL_DEPTH_BUFFER_BIT);
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                glEnable(GL_DEPTH_TEST);
                glClear(GL_COLOR_BUFFER_BIT);
                glEnable(GL_MULTISAMPLE);
                m_shadowMapShader->setMat4("lightSpaceMatrix", lightViews[i]);
                for (auto &obj : drawData.objectManager.getValues()) {
                    m_shadowMapShader->setMat4("model", obj->getModel());
                    for (auto &e : obj->getElements()) {
                        e->m_mesh.draw();
                    }
                }
                m_shadow.m_MSMCubeMaps[m_shadow.m_MSMCubeMaps.size()-1][i].blit();
            }
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    glCullFace(GL_BACK);
}

void RendererMSM::debugPostProcess(DrawData*, int bufferId) {

    glBindFramebuffer(GL_FRAMEBUFFER, castui(bufferId));

    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);

    m_screenShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gaussianBlurPostProcess[0]->getTexture());
    //glBindTexture(GL_TEXTURE_2D, m_shadow.m_MSMCubeMaps[0][1].texture);
    m_screenShader->setInt("screenTexture", 0);
    m_screenShader->setFloat("near_plane", 1.f);
    m_screenShader->setFloat("far_plane", 20.f);
    drawScreen();

}

void RendererMSM::setMomentBias(float momentBias) {
    m_momentBias = momentBias;
    m_shadow.setMomentBias(m_momentBias);
}

void RendererMSM::setDepthBias(float depthBias) {
    m_depthBias = depthBias;
    m_shadow.setDepthBias(m_depthBias);
}

void RendererMSM::setStandardDeviation(float sd) {
    m_standardDeviation = sd;
}

void RendererMSM::setKernelSize(int ks) {
    m_kernelSize = ks;
}
