#ifndef ROGUE_RENDERERVSM_HPP
#define ROGUE_RENDERERVSM_HPP
#include <./Engine/Renderer/Renderer.hpp>
#include <./Engine/Texture/FrameBuffers/ShadowMap.hpp>
#include <./Engine/Texture/ShadowMapBuffer.hpp>
#include <Plugin/CO/Shadow/Features/ShadowVSM.hpp>
#include <Plugin/CO/Shadow/Features/GaussianBlurVSMOpt.hpp>
#include <Plugin/CO/Shadow/FrameBuffer/BlurVSMTexture.hpp>
#include <Engine/Renderer/Features/PostProcessGamma.hpp>

/**
 * @file RendererVSM.hpp
 * @class RendererVSM
 * @brief Render the given scene using VSM method to render shadows
*/

class RendererVSM : public Renderer
{
public:
  RendererVSM();
  //Functions

  /**
   * @brief Compute and display the scene
   * @param drawData
   */
  void draw(DrawData &drawData);

  /**
   * @brief Resize all of the buffers with the given width and height
   * @param w
   * @param h
   */
  void resize(int w, int h);

  /**
   * @brief Set the minnimum variance
   * @param momentBias
   */
  void setVarClamp(float momentBias);

  /**
   * @brief Set the depth biais
   * @param depthBias
   */
  void setDepthBias(float depthBias);

  /**
   * @brief Set the standard deviation
   * @param sd
   */
  void setStandardDeviation(float sd);

  /**
   * @brief Set the size of the gaussian kernel
   * @param ks
   */
  void setKernelSize(uint ks);

private:
  void computeShadowMaps(DrawData& drawData);
  void blurShadowMaps(DrawData& drawData);
  Vector3 getLightPosition(Light * light);

  void debugPostProcess(DrawData* drawData, int bufferId);

  //Member variables
private:

  Prepass m_prepass;
  ShadowVSM m_shadow;

  PostProcessGamma m_postProcess;

  bool m_recomputeShadowMap;

  ShadowMapBuffer m_shadowMap;

  std::vector<GaussianBlurVSMOpt*> gaussianBlurPostProcess;

  Object *m_screenObj;
  Shader *m_screenShader;
  Shader *m_shadowMapShader;
  Shader *m_shadowMapPointShader;

  Shader *screenShader;

  float m_varMin;
  uint m_kernelSize;
  float m_standardDeviation;

  int m_sm_height = 1024;
  int m_sm_width = 1024;

};

#endif // RENDERERVSM_H
