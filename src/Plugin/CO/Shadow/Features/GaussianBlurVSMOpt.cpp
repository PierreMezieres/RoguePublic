//
// Created by jb on 17/02/19.
//

#include "GaussianBlurVSMOpt.hpp"


GaussianBlurVSMOpt::GaussianBlurVSMOpt() {
    horizontalPass = new BlurVSMTexture();
    verticalPass = new BlurVSMTexture();
    shader = new Shader("../Shaders/Shadow/ShadowMap/VSM/screen.vs.glsl", "../Shaders/Shadow/ShadowMap/VSM/gaussianBlur.fs.glsl");
    screenObj = new Object(createSimpleRec());
}

GaussianBlurVSMOpt::~GaussianBlurVSMOpt() {
    delete shader;
    delete screenObj;
    delete verticalPass;
    delete horizontalPass;
}

void GaussianBlurVSMOpt::use(unsigned int texture, int kernelSize, float standardDeviation) {
    /*
     Horizontal pass
    */
    horizontalPass->bind();
    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    shader->use();
    shader->setInt("blurSize", kernelSize);
    shader->setFloat("sigma", standardDeviation);
    shader->setInt("horizontalPass", 1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    shader->setInt("screenTexture", 0);
    screenObj->getMesh().draw();

    /*
     Vertical pass
    */
    verticalPass->bind();

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);

    shader->use();
    shader->setInt("blurSize", kernelSize);
    shader->setFloat("sigma", standardDeviation);
    shader->setInt("horizontalPass", 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, horizontalPass->texture);
    shader->setInt("screenTexture", 0);
    screenObj->getMesh().draw();

    //delete horizontalPass;
}

unsigned int GaussianBlurVSMOpt::getTexture() {
    return verticalPass->texture;
}

void GaussianBlurVSMOpt::resize(int w, int h) {
    verticalPass->resize(w, h);
    horizontalPass->resize(w, h);
}