//
// Created by jb on 11/02/19.
//

#ifndef ROGUE_SHADOWMSM_HPP
#define ROGUE_SHADOWMSM_HPP
#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Engine/Texture/FrameBuffer.hpp>
#include "./Engine/Texture/FrameBuffers/ShadowMap.hpp"
#include <./Engine/Texture/ShadowMapBuffer.hpp>
#include <./Engine/Texture/SSAOBuffer.hpp>
#include <Plugin/CO/Shadow/FrameBuffer/MomentShadowMapBuffer.hpp>
#include <./Engine/Light/PointLight.hpp>

/**
 * @file ShadowMSM.hpp
 * @brief Compute lightning taking into acount Shadowmaps witht the MSM method
 */
/**
 * @class ShadowMSM
 * @brief Compute
 */
typedef MomentShadowMapBuffer CubeMapMSM[6];

class ShadowMSM {


public:
  ShadowMSM();

  //TODO Change that to support numerous shadowMap
  /**
         * @brief Setup the pipeline and render the scene using the computed shadowmap
         * @param drawData
         * @param drawfill
         */
  void use(DrawData &drawData, bool drawfill = true);

  /**
         * @brief Resize the framebuffer used with the given width and height
         * @param w
         * @param h
         */
  void resize(int w, int h);

  /**
         * @brief Return the computed texture. DON4T FORGET TO CALL USE BEFORE
         * @return
         */
  int getTexture();

  /**
   * @brief Clear all of the computed shadow maps
   */
  void clearShadowMaps();

  /**
   * @brief Add a new shadowmap to the list
   */
  void addShadowMap();

  /**
   * @brief Add a new cube map to the list
   */
  void addCubeMap();

  /**
   * @brief Bind the transformation matrix from camera to lightspace
   * @param light
   */
  void bindLightSpaceLights(Light *light);

  /**
   * @brief Set the biais for the moments
   * @param val
   */
  void setMomentBias(float val);

  /**
   * @brief Set the depth biais
   * @param val
   */
  void setDepthBias(float val);

public:
  /**
   * @brief m_MSMBuffers : Shadowmaps of the spot and directionnal lights
   */
  std::vector<MomentShadowMapBuffer*> m_MSMBuffers;

  /**
   * @brief m_MSMCubeMaps : ShadowMaps of the point lights
   */
  std::vector<MomentShadowMapBuffer*> m_MSMCubeMaps;

private:
  void clearPingPongBuffer(bool drawfill);
  void bindShadowMap(Light* light, uint lightNum);
  void selectShader(Light* light);

private:

  FrameBuffer frameBuffer[2];
  Shader* m_currentShader;
  Shader* m_shaderBasic;
  Shader* m_shaderPoint;

  int m_currentBuffer;
  bool m_drawFill = true;

  float m_momentBias;
  float m_depthBias;
};


#endif //ROGUE_SHADOWMSM_HPP
