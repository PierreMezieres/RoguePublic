//
// Created by jb on 17/02/19.
//

#include "GaussianBlur.hpp"

GaussianBlur::GaussianBlur() {
    horizontalPass = new BlurMSMTexture();
    verticalPass = new BlurMSMTexture();
    shader = new Shader("../Shaders/Shadow/ShadowMap/MSM/screen.vs.glsl", "../Shaders/Shadow/ShadowMap/MSM/gaussianBlur.fs.glsl");
    screenObj = new Object(createSimpleRec());
}

GaussianBlur::~GaussianBlur() {
    delete shader;
    delete screenObj;
    delete verticalPass;
    delete horizontalPass;
}

void GaussianBlur::use(unsigned int texture, int kernelSize, float standardDeviation) {
    /*
     Horizontal pass
    */
    horizontalPass->bind();
    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );

    shader->use();
    shader->setInt("blurSize", kernelSize);
    shader->setFloat("sigma", standardDeviation);
    shader->setInt("horizontalPass", 1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    shader->setInt("screenTexture", 0);
    screenObj->getMesh().draw();

    /*
     Vertical pass
    */
    verticalPass->bind();

    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);

    shader->use();
    shader->setInt("blurSize", kernelSize);
    shader->setFloat("sigma", standardDeviation);
    shader->setInt("horizontalPass", 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, horizontalPass->texture);
    shader->setInt("screenTexture", 0);
    screenObj->getMesh().draw();

    //delete horizontalPass;
}

unsigned int GaussianBlur::getTexture() {
    return verticalPass->texture;
}

void GaussianBlur::resize(int w, int h) {
    verticalPass->resize(w, h);
    horizontalPass->resize(w, h);
}