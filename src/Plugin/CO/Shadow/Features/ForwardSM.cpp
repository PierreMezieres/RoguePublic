#include "ForwardSM.hpp"

ForwardSM::ForwardSM() {
    shader = new Shader("../Shaders/ShadowMapTest/vertexShader.vs.glsl", "../Shaders/ShadowMapTest/BasicLight.fs.glsl");

}

void ForwardSM::use(DrawData &drawData, Texture* skybox, ShadowMap shadowMap, bool drawfill) {
    frameBuffer.bind();

    DataForDrawing dfd = drawData.getDataForDrawing();

    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    if (drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    dfd.skybox = skybox;

    //drawData.objectManager.draw(dfd);
    std::cout << "Nombre d'objets à dessiner : " << drawData.objectManager.getValues().size() << std::endl;

    for (auto o : drawData.objectManager.getValues())
        for (auto e : o->getElements())
            drawElement(dfd, o->getModel(), shadowMap, e);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void ForwardSM::drawElement(DataForDrawing data, Matrix4 model, ShadowMap shadowMap, Element *e) {


    Material *material = data.materialManager->getValue(e->m_materialName);
    //Shader *shader = data.shaderManager->getValue(material->getNameShader());

    shader->use();

    glActiveTexture(GL_TEXTURE4);
    shader->setInt("shadowMap", 4);
    shadowMap.bind();
    shader->setMat4("lightSpaceMatrix", data.lightSpaceMatrix);

    shader->setMat4("model", model);
    shader->setMat4("proj",data.camera->GetProjMatrix());
    shader->setMat4("view",data.camera->GetViewMatrix());
    shader->setVec3("viewPos",data.camera->m_position);


    data.lightManager->bind(shader);
    material->bind(shader, data);
    e->draw(data, model);
}
