#include "Shadow.hpp"

Shadow::Shadow()
{
  m_shaderBasic = new Shader("../Shaders/Shadow/ShadowMap/PCF/BasicLights/vertexShader.vs.glsl","../Shaders/Shadow/ShadowMap/PCF/BasicLights/BasicLight.fs.glsl");
  //m_shader->setInt("shadowMap", 3);
  m_currentBuffer = 0;
  m_depthBias = 0.005;
  m_kernelSize = 9;
  m_standardDeviation = 2.4;
}

void Shadow::use(DrawData &drawData, bool drawfill) {
  clearPingPongBuffer(drawfill);
  DataForDrawing dfd = drawData.getDataForDrawing();
  m_currentBuffer = 0;
  for(auto& light : dfd.lightManager->getValues()) {
      //Only while point lights are not implemented
      //if (light->m_type == Light::POINT_LIGHT)
          //continue;
      selectShader(light);
      m_currentShader->use();

      frameBuffer[m_currentBuffer].bind();
      glClear(GL_DEPTH_BUFFER_BIT);
      light->bindSingle(m_currentShader);
      bindParameters();
      for (auto& obj : drawData.objectManager.getValues()) {
          for (auto& elem : obj->getElements()) {
              glActiveTexture(GL_TEXTURE4);
              glBindTexture(GL_TEXTURE_2D, frameBuffer[(m_currentBuffer + 1)%2].texture);
              m_currentShader->setInt("previousDrawCall", 4);
              Material *material = dfd.materialManager->getValue(elem->m_materialName);
              m_currentShader->setMat4("lightSpaceMatrix", *(light->getLightSpace()));
              m_currentShader->setMat4("model",obj->getModel());
              m_currentShader->setMat4("proj",dfd.camera->GetProjMatrix());
              m_currentShader->setMat4("view",dfd.camera->GetViewMatrix());
              m_currentShader->setVec3("viewPos",dfd.camera->m_position);
              m_currentShader->setFloat("far_plane", 20.f);
              material->bind(m_currentShader, dfd);
              elem->m_mesh.draw();

            }
        }
      frameBuffer[m_currentBuffer].unbind();
      m_currentBuffer = (m_currentBuffer + 1) % 2;
    }

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Shadow::resize(int w, int h) {
  frameBuffer[0].resize(w,h);
  frameBuffer[1].resize(w,h);
}

int Shadow::getTexture() {
  return casti(frameBuffer[(m_currentBuffer+1) % 2].texture);
}

void Shadow::clearPingPongBuffer(bool drawfill){
  for(int i = 0; i < 2; ++i){
      frameBuffer[i].bind();
      if(i==0) glClearColor(0.05f, 0.1f, 0.1f, 0);
      else glClearColor(0,0,0,0);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glEnable(GL_DEPTH_TEST);
    }

  if (drawfill)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  else
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void Shadow::selectShader(Light* light) {
    if (light->m_type == Light::DIR_LIGHT || light->m_type == Light::SPOT_LIGHT)
        m_currentShader = m_shaderBasic;
    else if (light->m_type == Light::POINT_LIGHT)
        m_currentShader = m_shaderPointLight;
    else
        Log(logError) << "Error::Shadow maps don't work with this type of lights yet";
    m_currentShader = m_shaderBasic;

}

void Shadow::setStandardDeviation(float sd) {
    m_standardDeviation = sd;
}

void Shadow::setKernelSize(int ks) {
    m_kernelSize = ks;
}

void Shadow::setDepthBias(float db) {
    m_depthBias = db;
}

void Shadow::bindParameters() {
    m_currentShader->setInt("u_kernelSize", m_kernelSize);
    m_currentShader->setFloat("u_standardDeviation", m_standardDeviation);
    m_currentShader->setFloat("u_depthBias", m_depthBias);
}