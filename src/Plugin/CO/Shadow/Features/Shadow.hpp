#ifndef SHADOW_H
#define SHADOW_H

#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Engine/Texture/FrameBuffer.hpp>
#include "./Engine/Texture/FrameBuffers/ShadowMap.hpp"
#include <./Engine/Texture/ShadowMapBuffer.hpp>
#include <./Engine/Texture/SSAOBuffer.hpp>

/**
 * @file Shadow.hpp
 * @brief The Shadow class
 */

/**
 * @class Shadow
 * @brief Compute lightning taking into acount Shadowmaps with the PCF method
 */

class Shadow {
public:
    Shadow();

    /**
     * @brief Setup the pipeline and render the scene using the computed shadowmap
     * @param drawData
     * @param drawfill
     */
    void use(DrawData &drawData, bool drawfill = true);

    /**
     * @brief Resize the framebuffer used with the given width and height
     * @param w
     * @param h
     */
    void resize(int w, int h);

    /**
     * @brief Return the computed texture. DON4T FORGET TO CALL USE BEFORE
     * @return
     */
    int getTexture();

    /**
     * @brief Set the current standard deviation to compute a gaussian blur
     */
    void setStandardDeviation(float);

    /**
     * @brief Set the size of the kernel to be used with the gaussian blur : Set to 0 for no blur.
     */
    void setKernelSize(int);

    /**
     * @brief Set the depth biais. Will in the next versions
     */
    void setDepthBias(float);

private:
    void clearPingPongBuffer(bool drawfill);
    void selectShader(Light* light);
    void bindParameters();

private:

    FrameBuffer frameBuffer[2];
    Shader  *m_shaderBasic;
    Shader *m_shaderPointLight;
    Shader *m_currentShader;
    int m_currentBuffer;

    int m_kernelSize;
    float m_standardDeviation;
    float m_depthBias;

};


#endif // SHADOW_H
