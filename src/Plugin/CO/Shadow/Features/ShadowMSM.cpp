//
// Created by jb on 11/02/19.
//

#include "ShadowMSM.hpp"

ShadowMSM::ShadowMSM()
{
    m_shaderBasic = new Shader("../Shaders/Shadow/ShadowMap/MSM/BasicLights/vertexShader.vs.glsl","../Shaders/Shadow/ShadowMap/MSM/BasicLights/BasicLight.fs.glsl");
    //m_shaderPoint = new Shader("../Shaders/Shadow/ShadowMap/MSM/PointLights/vertexShader.vs.glsl","../Shaders/Shadow/ShadowMap/MSM/PointLights/BasicLight.fs.glsl");
    //m_shader->setInt("shadowMap", 3);
    m_currentBuffer = 0;
}

void ShadowMSM::use(DrawData &drawData, bool drawfill) {
    m_drawFill = drawfill;
    clearPingPongBuffer(drawfill);
    DataForDrawing dfd = drawData.getDataForDrawing();
    m_currentBuffer = 0;
    uint cptLights = 0;
    for(auto& light : dfd.lightManager->getValues()) {
        selectShader(light);
        m_currentShader->use();
        frameBuffer[m_currentBuffer].bind();
        glClear(GL_DEPTH_BUFFER_BIT);
        light->bindSimple(m_currentShader);

        bindShadowMap(light, cptLights);

        m_currentShader->setFloat("momentBias", m_momentBias);
        m_currentShader->setFloat("depthBias", m_depthBias);

        for (auto& obj : drawData.objectManager.getValues()) {
            for (auto& elem : obj->getElements()) {
                glActiveTexture(GL_TEXTURE9);
                glBindTexture(GL_TEXTURE_2D, frameBuffer[(m_currentBuffer + 1)%2].texture);
                m_currentShader->setInt("previousDrawCall", 9);
                Material *material = dfd.materialManager->getValue(elem->m_materialName);
                bindLightSpaceLights(light);
                m_currentShader->setMat4("model",obj->getModel());
                m_currentShader->setMat4("proj",dfd.camera->GetProjMatrix());
                m_currentShader->setMat4("view",dfd.camera->GetViewMatrix());
                m_currentShader->setVec3("viewPos",dfd.camera->m_position);
                m_currentShader->setFloat("far_plane", 20.f);
                material->bind(m_currentShader, dfd);
                elem->m_mesh.draw();
            }
        }
        frameBuffer[m_currentBuffer].unbind();
        m_currentBuffer = (m_currentBuffer + 1) % 2;
        cptLights++;
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void ShadowMSM::resize(int w, int h) {
    frameBuffer[0].resize(w,h);
    frameBuffer[1].resize(w,h);
}

int ShadowMSM::getTexture() {
    return casti(frameBuffer[(m_currentBuffer+1) % 2].texture);
}

void ShadowMSM::clearPingPongBuffer(bool drawfill){
    for(int i = 0; i < 2; ++i){
        frameBuffer[i].bind();
        if(i==0) glClearColor(0.05f, 0.1f, 0.1f, 0);
        else glClearColor(0,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
    }

    if (drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void ShadowMSM::clearShadowMaps() {
    for (MomentShadowMapBuffer* sm : m_MSMBuffers)
        delete sm;
    m_MSMBuffers.clear();

    for (MomentShadowMapBuffer* sm : m_MSMCubeMaps)
            delete[] sm;
    m_MSMCubeMaps.clear();
}

void ShadowMSM::addShadowMap() {
    m_MSMBuffers.emplace_back(new MomentShadowMapBuffer());
}

void ShadowMSM::addCubeMap() {
    m_MSMCubeMaps.emplace_back(new MomentShadowMapBuffer[6]);
}

void ShadowMSM::setMomentBias(float val) {
    m_momentBias = val;
    //use(m_drawData, m_drawFill);
}
void ShadowMSM::setDepthBias(float val) {
    m_depthBias = val;
    //use(m_drawData, m_drawFill);
}

void ShadowMSM::bindShadowMap(Light* light, uint lightNum) {
    if (light->m_type == Light::POINT_LIGHT) {
        for (uint i = 0; i < 6; i++) {
            glActiveTexture(GL_TEXTURE3 + i);
            glBindTexture(GL_TEXTURE_2D, m_MSMCubeMaps[lightNum][i].texture);
            m_currentShader->setInt("shadowMap["+std::to_string(i)+"]", 3 + i);
        }
    }
    else if (light->m_type == Light::DIR_LIGHT || light->m_type == Light::SPOT_LIGHT) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, m_MSMBuffers[lightNum]->texture);
        m_currentShader->setInt("shadowMap", 3);
    }
    else {
        Log(logError) << "Error::Light not compatible yet for shadowMapping";
    }
}

void ShadowMSM::bindLightSpaceLights(Light *light) {
    if (light->m_type == Light::SPOT_LIGHT || light->m_type == Light::DIR_LIGHT) {
        m_currentShader->setMat4("lightSpaceMatrix", *(light->getLightSpace()));
    }
    else if (light->m_type == Light::POINT_LIGHT) {
        PointLight* pointLight = dynamic_cast<PointLight*>(light);
        for (uint i = 0; i < 6; i++) {
            m_currentShader->setMat4("lightSpaceMatCube[" + std::to_string(i) + "]", pointLight->m_lightSpaceMats[i]);
        }
    }
    else {
        Log(logError) << "Error::Light not compatible yet for shadowMapping";
    }
}

void ShadowMSM::selectShader(Light* light) {
    if (light->m_type == Light::SPOT_LIGHT || light->m_type == Light::DIR_LIGHT)
        m_currentShader = m_shaderBasic;
    else if (light->m_type == Light::POINT_LIGHT)
        m_currentShader = m_shaderPoint;
    else
        Log(logError) << "Error::Light not compatible yet for shadowMapping";
    m_currentShader = m_shaderBasic;

}