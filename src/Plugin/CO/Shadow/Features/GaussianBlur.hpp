//
// Created by jb on 17/02/19.
//

#ifndef ROGUE_GAUSSIANBLUR_HPP
#define ROGUE_GAUSSIANBLUR_HPP
#include <Plugin/CO/Shadow/FrameBuffer/BlurMSMTexture.hpp>
#include "./src/Engine/Shader/Shader.hpp"
#include "./src/Engine/Object/Object.hpp"
#include <src/Core/Shapes/TriangleMesh.hpp>

/**
 * @file GaussianBlur.hpp
 * @brief The GaussianBlur class
 */

/**
 * @class GaussianBlur
 * @brief A GaussianBlur to Gaussiannly blur your texture
 */

class GaussianBlur {

public:
    GaussianBlur();
    ~GaussianBlur();

    /**
     * @brief Setup the rendering and render the scene bluring the texture with a gaussian kernel
     * @param texture : texture to blur
     * @param kernelSize : size of the gaussien kernel
     * @param standardDeviation
     */
    void use(unsigned int texture, int kernelSize, float standardDeviation);

    /**
     * @brief Return the texture from the second (vertical) pass : DON4T FORGET TO CALL USE BEFORE
     * @return
     */
    unsigned int getTexture();

    /**
     * @brief Resize the framebuffers of this renderer
     * @param w
     * @param h
     */
    void resize(int w, int h);

private:

    BlurMSMTexture* horizontalPass;
    BlurMSMTexture* verticalPass;
    Shader* shader;
    Object *screenObj;


};


#endif //ROGUE_GAUSSIANBLUR_HPP
