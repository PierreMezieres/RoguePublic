#ifndef FORWARDSM_HPP
#define FORWARDSM_HPP

#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Engine/Texture/FrameBuffer.hpp>
#include <src/Engine/Renderer/Features/Forward.hpp>
#include <src/Engine/Texture/ShadowMapBuffer.hpp>
#include <./Engine/Texture/FrameBuffers/ShadowMap.hpp>
#include <src/Engine/Object/Element.hpp>


/**
 * \file ForwardSM.hpp
 * \brief Basic forward algorithm module
 */

/**
 * \class ForwardSM
 * \brief do not use this
 */
class ForwardSM : public Forward {
public:
    ForwardSM();
    /**
     * @brief Setup and render the scene using basic forward algorithm
     * @param drawData
     * @param skybox
     * @param shadowMap
     * @param drawfill
     */
    void use(DrawData &drawData, Texture* skybox, ShadowMap shadowMap, bool drawfill = true);

    /**
     * @brief Render one element of the scene
     * @param data
     * @param model
     * @param shadowMap
     * @param e
     */
    void drawElement(DataForDrawing data, Matrix4 model, ShadowMap shadowMap, Element *e);

private:

    FrameBuffer frameBuffer;
    Shader *shader;
};

#endif // FORWARDSM_HPP

