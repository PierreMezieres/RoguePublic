//
// Created by jb on 11/02/19.
//

#include "ShadowVSM.hpp"

ShadowVSM::ShadowVSM()
{
    m_shaderBasic = new Shader("../Shaders/Shadow/ShadowMap/VSM/BasicLights/vertexShader.vs.glsl","../Shaders/Shadow/ShadowMap/VSM/BasicLights/BasicLight.fs.glsl");
    //m_shaderPoint = new Shader("../Shaders/Shadow/ShadowMap/VSM/PointLights/vertexShader.vs.glsl","../Shaders/Shadow/ShadowMap/VSM/PointLights/BasicLight.fs.glsl");
    //m_shader->setInt("shadowMap", 3);
    m_currentBuffer = 0;
}

void ShadowVSM::use(DrawData &drawData, bool drawfill) {
    m_drawFill = drawfill;
    clearPingPongBuffer(drawfill);
    DataForDrawing dfd = drawData.getDataForDrawing();
    m_currentBuffer = 0;
    uint cptLights = 0;
    for(auto& light : dfd.lightManager->getValues()) {
        selectShader(light);
        m_currentShader->use();
        frameBuffer[m_currentBuffer].bind();
        glClear(GL_DEPTH_BUFFER_BIT);
        light->bindSimple(m_currentShader);
        bindShadowMap(light, cptLights);
        m_currentShader->setFloat("minVar", m_minVar);

        for (auto& obj : drawData.objectManager.getValues()) {
            for (auto& elem : obj->getElements()) {
                glActiveTexture(GL_TEXTURE9);
                glBindTexture(GL_TEXTURE_2D, frameBuffer[(m_currentBuffer + 1)%2].texture);
                m_currentShader->setInt("previousDrawCall", 9);
                Material *material = dfd.materialManager->getValue(elem->m_materialName);
                bindLightSpaceLights(light);
                m_currentShader->setMat4("model",obj->getModel());
                m_currentShader->setMat4("proj",dfd.camera->GetProjMatrix());
                m_currentShader->setMat4("view",dfd.camera->GetViewMatrix());
                m_currentShader->setVec3("viewPos",dfd.camera->m_position);
                m_currentShader->setFloat("far_plane", 20.f);
                material->bind(m_currentShader, dfd);
                elem->m_mesh.draw();
            }
        }
        frameBuffer[m_currentBuffer].unbind();
        m_currentBuffer = (m_currentBuffer + 1) % 2;
        cptLights++;
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void ShadowVSM::resize(int w, int h) {
    frameBuffer[0].resize(w,h);
    frameBuffer[1].resize(w,h);
}

int ShadowVSM::getTexture() {
    return casti(frameBuffer[(m_currentBuffer+1) % 2].texture);
}

void ShadowVSM::clearPingPongBuffer(bool drawfill){
    for(int i = 0; i < 2; ++i){
        frameBuffer[i].bind();
        if(i==0) glClearColor(0.05f, 0.1f, 0.1f, 0);
        else glClearColor(0,0,0,0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
    }

    if (drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void ShadowVSM::clearShadowMaps() {
    for (VSMBuffer* sm : m_VSMBuffers)
        delete sm;
    m_VSMBuffers.clear();

    for (VSMBuffer* sm : m_VSMCubeMaps)
            delete[] sm;
    m_VSMCubeMaps.clear();
}

void ShadowVSM::addShadowMap() {
    m_VSMBuffers.emplace_back(new VSMBuffer());
}

void ShadowVSM::addCubeMap() {
    m_VSMCubeMaps.emplace_back(new VSMBuffer[6]);
}

void ShadowVSM::setMinVar(float val) {
    m_minVar = val;
}
void ShadowVSM::setKernelSize(int val) {
    m_kernelSize = val;
}



void ShadowVSM::bindShadowMap(Light* light, uint lightNum) {
    if (light->m_type == Light::POINT_LIGHT) {
        for (uint i = 0; i < 6; i++) {
            glActiveTexture(GL_TEXTURE3 + i);
            glBindTexture(GL_TEXTURE_2D, m_VSMCubeMaps[lightNum][i].texture);
            m_currentShader->setInt("shadowMap["+std::to_string(i)+"]", 3 + i);
        }
    }
    else if (light->m_type == Light::DIR_LIGHT || light->m_type == Light::SPOT_LIGHT) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, m_VSMBuffers[lightNum]->texture);
        m_currentShader->setInt("shadowMap", 3);
    }
    else {
        Log(logError) << "Error::Light not compatible yet for shadowMapping";
    }
}

void ShadowVSM::bindLightSpaceLights(Light *light) {
    if (light->m_type == Light::SPOT_LIGHT || light->m_type == Light::DIR_LIGHT) {
        m_currentShader->setMat4("lightSpaceMatrix", *(light->getLightSpace()));
    }
    else if (light->m_type == Light::POINT_LIGHT) {
        PointLight* pointLight = dynamic_cast<PointLight*>(light);
        for (uint i = 0; i < 6; i++) {
            m_currentShader->setMat4("lightSpaceMatCube[" + std::to_string(i) + "]", pointLight->m_lightSpaceMats[i]);
        }
    }
    else {
        Log(logError) << "Error::Light not compatible yet for shadowMapping";
    }
}

void ShadowVSM::selectShader(Light* light) {
    if (light->m_type == Light::SPOT_LIGHT || light->m_type == Light::DIR_LIGHT)
        m_currentShader = m_shaderBasic;
    else if (light->m_type == Light::POINT_LIGHT)
        m_currentShader = m_shaderPoint;
    else
        Log(logError) << "Error::Light not compatible yet for shadowMapping";
    m_currentShader = m_shaderBasic;

}
