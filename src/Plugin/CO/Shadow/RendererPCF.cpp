//
// Created by pierre on 09/01/19.
//

#include "RendererPCF.hpp"
#include <Engine/Light/DirLight.hpp>
#include <Engine/Light/PointLight.hpp>
#include <Engine/Light/SpotLight.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>

RendererPCF::RendererPCF()
  : Renderer("PCF")
{

}

void RendererPCF::draw(DrawData &drawData) {

  m_screenShader = drawData.shaderManager.getValue("screenDepthDebug");
  m_shadowMapShader = drawData.shaderManager.getValue("mapDepthLight");
  //m_shadowMapPointShader = drawData.shaderManager.getValue("cubeMapDepthLight");

  GLint qt_buffer;
  glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
  int vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /*
   ==============================================================
                            Shadow pass
   ==============================================================
  */
  computeShadowMaps(drawData);

  /*
   ==============================================================
                            Light pass
   ==============================================================
  */
  m_shadow.use(drawData,m_drawfill);

  /*
   ==============================================================
                            Post process pass
   ==============================================================
  */
  glViewport(vp[0], vp[1], vp[2], vp[3]);
  m_postProcess.use(qt_buffer, m_shadow.getTexture());
  //debugPostProcess(&drawData, qt_buffer);
}

void RendererPCF::resize(int w, int h){
  m_shadow.resize(w,h);
  m_prepass.resize(w,h);
  m_shadowMap.resize(w,h);
}

void RendererPCF::computeShadowMaps(DrawData& drawData) {
  glCullFace(GL_FRONT);
  Matrix4 lightProjection, lightView;
    std::vector<Matrix4> lightViews;


  for(const auto& light : drawData.lightManager.getValues()) {
      if (light->m_type == Light::POINT_LIGHT)
          continue;
      lightViews.clear();
      lightView = glm::lookAt(Vector3(0.5f,2,2), Vector3(0.f), Vector3(0.f, 1.f, 0.f));
      Vector3 position;

      switch(light->m_type){
        case Light::POINT_LIGHT:{
            auto *pLight = dynamic_cast<PointLight*>(light);
            pLight->m_shadowMap.resize(m_sm_width, m_sm_height);
            pLight->m_shadowMap.bind();

            glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
            glClear(GL_DEPTH_BUFFER_BIT);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);

            position = pLight->getPosition();
            lightProjection = glm::perspective<float>(glm::radians(90.f), float(m_sm_width)/float(m_sm_height), 1.f, 20.f);

            lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3( 1.f, 0.f, 0.f), Vector3(0.f, -1.f, 0.f)));
            lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(-1.f, 0.f, 0.f), Vector3(0.f, -1.f, 0.f)));
            lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 1.f, 0.f), Vector3(0.f, 0.f, 1.f)));
            lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, -1.f, 0.f), Vector3(0.f, 0.f, -1.f)));
            lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 0.f, 1.f), Vector3(0.f, -1.f, 0.f)));
            lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 0.f,-1.f), Vector3(0.f,-1.f, 0.f)));

        }
          break;
        case Light::SPOT_LIGHT:{
            auto *sLight = dynamic_cast<SpotLight*>(light);
            sLight->m_shadowMap.resize(m_sm_width,m_sm_height);
            sLight->m_shadowMap.bind();

            glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
            glClear(GL_DEPTH_BUFFER_BIT);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);

            position = sLight->getPosition();
            lightProjection = glm::perspective<float>(glm::radians(sLight->getOuterAngle()*2.f), 1.0f, 1.f, 20.f);
            lightView = glm::lookAt(position, position + glm::normalize(sLight->getDirection()), drawData.camera->m_worldUp);
          }
          break;
        case Light::DIR_LIGHT:{
            auto *dLight = dynamic_cast<DirLight*>(light);
            dLight->m_shadowMap.resize(m_sm_width,m_sm_height);
            dLight->m_shadowMap.bind();

            glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
            glClear(GL_DEPTH_BUFFER_BIT);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);

            position = -10.f * glm::normalize(dLight->getDirection());
            lightProjection = glm::ortho(-10.f, 10.f, -10.f, 10.f, 1.f, 20.f);
            lightView = glm::lookAt(position, position + glm::normalize(dLight->getDirection()), drawData.camera->m_worldUp);
          }
          break;
        }
      //TODO Rajouter les cubemap
      if (light->m_type == Light::DIR_LIGHT || light->m_type == Light::SPOT_LIGHT) {
          m_shadowMapShader->use();
          Matrix4 lsm = lightProjection * lightView;
          light->setLightSpace(lsm);
          m_shadowMapShader->setMat4("lightSpaceMatrix", lsm);

          for(auto& obj : drawData.objectManager.getValues()) {
              m_shadowMapShader->setMat4("model",obj->getModel());
              for (auto& e : obj->getElements()) {
                  e->m_mesh.draw();
              }
          }
      }
      else if (light->m_type == Light::POINT_LIGHT) {
          m_shadowMapPointShader->use();
          glUniformMatrix4fv(glGetUniformLocation(m_shadowMapPointShader->getProgram(), "shadowMatrices"), 6, GL_FALSE, &lightViews[0][0][0]);
          m_shadowMapPointShader->setVec3("lightPos", position);
          m_shadowMapPointShader->setFloat("far_plane", 20.f);

          for(auto& obj : drawData.objectManager.getValues()) {
              m_shadowMapPointShader->setMat4("model",obj->getModel());
              for (auto& e : obj->getElements()) {
                  e->m_mesh.draw();
              }
          }
      }

    }
  glCullFace(GL_BACK);
}

void RendererPCF::debugPostProcess(DrawData* drawData, int bufferId) {

  glBindFramebuffer(GL_FRAMEBUFFER, castui(bufferId));

    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
  glDisable(GL_DEPTH_TEST);
  glClear(GL_COLOR_BUFFER_BIT );

  m_screenShader->use();
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, static_cast<DirLight *>(drawData->lightManager.getValue(1))->m_shadowMap.texture);
  m_screenShader->setInt("screenTexture", 0);
  m_screenShader->setFloat("near_plane", drawData->camera->getNear());
  m_screenShader->setFloat("far_plane", drawData->camera->getFar());
  drawScreen();
}

void RendererPCF::setStandardDeviation(float sd) {
    m_shadow.setStandardDeviation(sd);
}

void RendererPCF::setKernelSize(int ks) {
    m_shadow.setKernelSize(ks);
}

void RendererPCF::setDepthBias(float db) {
    m_shadow.setDepthBias(db);
}
