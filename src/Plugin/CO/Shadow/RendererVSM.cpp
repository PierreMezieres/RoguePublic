//
// Created by jb on 11/02/19.
//

#include "RendererVSM.hpp"
#include <Engine/Light/DirLight.hpp>
#include <Engine/Light/PointLight.hpp>
#include <Engine/Light/SpotLight.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>

RendererVSM::RendererVSM()
        : Renderer("VSM") {
    m_varMin = 0.000002f;
    m_kernelSize = 11;

    m_shadow.setMinVar(m_varMin);
    m_kernelSize = 9;
    m_standardDeviation = 2.4f;
}

void RendererVSM::draw(DrawData &drawData) {

    m_screenShader = drawData.shaderManager.getValue("screenDepthDebug");
    m_shadowMapShader = drawData.shaderManager.getValue("VSMMapDepthLight");
    //m_shadowMapPointShader = drawData.shaderManager.getValue("cubeMapDepthLight");

    GLint qt_buffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);
    int vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    /*
     ==============================================================
                              Shadow pass
     ==============================================================
    */
    computeShadowMaps(drawData);

    /*
     ==============================================================
                              Light pass
     ==============================================================
    */
    m_shadow.use(drawData,m_drawfill);

    /*
     ==============================================================
                              Post process pass
     ==============================================================
    */
    glViewport(vp[0], vp[1], vp[2], vp[3]);
    m_postProcess.use(qt_buffer, m_shadow.getTexture());
    //debugPostProcess(&drawData, qt_buffer);

    for (GaussianBlurVSMOpt* g : gaussianBlurPostProcess)
        delete g;

    gaussianBlurPostProcess.clear();
    m_shadow.clearShadowMaps();
}

void RendererVSM::resize(int w, int h){
    m_shadow.resize(w,h);
    m_prepass.resize(w,h);
    m_shadowMap.resize(w,h);
}

void RendererVSM::computeShadowMaps(DrawData& drawData) {
    //glCullFace(GL_FRONT);
    glEnable(GL_MULTISAMPLE);

    Matrix4 lightProjection, lightView;
    std::vector<Matrix4> lightViews;

    for(const auto& light : drawData.lightManager.getValues()) {
        lightViews.clear();
        lightView = glm::lookAt(Vector3(0.5f,2,2), Vector3(0.f), Vector3(0.f, 1.f, 0.f));
        Vector3 position;

        switch(light->m_type){
            case Light::POINT_LIGHT:{
                auto *pLight = dynamic_cast<PointLight*>(light);
                //pLight->m_shadowMap.resize(m_sm_width, m_sm_height);
                //pLight->m_shadowMap.bind();
                m_shadow.addCubeMap();

                position = pLight->getPosition();
                lightProjection = glm::perspective<float>(glm::radians(90.f), float(m_sm_width)/float(m_sm_height), 1.f, 20.f);

                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3( 1.f, 0.f, 0.f), Vector3(0.f, -1.f, 0.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(-1.f, 0.f, 0.f), Vector3(0.f, -1.f, 0.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 1.f, 0.f), Vector3(0.f, 0.f, 1.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, -1.f, 0.f), Vector3(0.f, 0.f, -1.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 0.f, 1.f), Vector3(0.f, -1.f, 0.f)));
                lightViews.emplace_back(lightProjection * glm::lookAt(position, position + Vector3(0.f, 0.f,-1.f), Vector3(0.f,-1.f, 0.f)));

                for (uint i = 0; i < 6; i++)
                    pLight->m_lightSpaceMats[i] = lightViews[i];

            }
                break;
            case Light::SPOT_LIGHT:{
                auto *sLight = dynamic_cast<SpotLight*>(light);
                m_shadow.addShadowMap();
                m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->resize(m_sm_width,m_sm_height);
                m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->bind();

                position = sLight->getPosition();
                lightProjection = glm::perspective<float>(glm::radians(sLight->getOuterAngle()*2.f), 1.0f, 1.f, 20.f);
                lightView = glm::lookAt(position, position + glm::normalize(sLight->getDirection()), drawData.camera->m_worldUp);
            }
                break;
            case Light::DIR_LIGHT:{
                auto *dLight = dynamic_cast<DirLight*>(light);
                m_shadow.addShadowMap();
                m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->resize(m_sm_width, m_sm_height);
                m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->bind();

                position = -10.f * glm::normalize(dLight->getDirection());
                lightProjection = glm::ortho(-10.f, 10.f, -10.f, 10.f, 1.f, 20.f);
                lightView = glm::lookAt(position, position + glm::normalize(dLight->getDirection()), drawData.camera->m_worldUp);
            }
                break;
        }
        if (light->m_type == Light::DIR_LIGHT || light->m_type == Light::SPOT_LIGHT) {
            m_shadowMapShader->use();
            Matrix4 lsm = lightProjection * lightView;
            light->setLightSpace(lsm);
            m_shadowMapShader->setMat4("lightSpaceMatrix", lsm);

            glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
            glClear(GL_DEPTH_BUFFER_BIT);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glEnable(GL_DEPTH_TEST);
            glClear(GL_COLOR_BUFFER_BIT);
            glEnable(GL_MULTISAMPLE);

            for(auto& obj : drawData.objectManager.getValues()) {
                m_shadowMapShader->setMat4("model",obj->getModel());
                for (auto& e : obj->getElements()) {
                    e->m_mesh.draw();
                }
            }
            m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->blit();
            gaussianBlurPostProcess.emplace_back(new GaussianBlurVSMOpt());
            gaussianBlurPostProcess[gaussianBlurPostProcess.size()-1]->resize(m_sm_width, m_sm_height);
            gaussianBlurPostProcess[gaussianBlurPostProcess.size()-1]->use(m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->texture, m_kernelSize, m_standardDeviation);
            m_shadow.m_VSMBuffers[m_shadow.m_VSMBuffers.size()-1]->changeTexture(gaussianBlurPostProcess[gaussianBlurPostProcess.size()-1]->getTexture());

        }
        else if (light->m_type == Light::POINT_LIGHT) {
            m_shadowMapShader->use();
            m_shadowMapShader->setVec3("lightPos", position);
            m_shadowMapShader->setFloat("far_plane", 20.f);
            for (uint i = 0; i < 6; i++) {
                m_shadow.m_VSMCubeMaps[m_shadow.m_VSMCubeMaps.size()-1][i].resize(m_sm_width, m_sm_height);
                m_shadow.m_VSMCubeMaps[m_shadow.m_VSMCubeMaps.size()-1][i].bind();
                glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
                glClear(GL_DEPTH_BUFFER_BIT);
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                glEnable(GL_DEPTH_TEST);
                glClear(GL_COLOR_BUFFER_BIT);
                glEnable(GL_MULTISAMPLE);
                m_shadowMapShader->setMat4("lightSpaceMatrix", lightViews[i]);
                for (auto &obj : drawData.objectManager.getValues()) {
                    m_shadowMapShader->setMat4("model", obj->getModel());
                    for (auto &e : obj->getElements()) {
                        e->m_mesh.draw();
                    }
                }
                m_shadow.m_VSMCubeMaps[m_shadow.m_VSMCubeMaps.size()-1][i].blit();
            }
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    //glCullFace(GL_BACK);
    glDisable(GL_MULTISAMPLE);
}

void RendererVSM::debugPostProcess(DrawData*, int bufferId) {

    glBindFramebuffer(GL_FRAMEBUFFER, castui(bufferId));

    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);

    m_screenShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_shadow.m_VSMBuffers[0]->texture);
    m_screenShader->setInt("screenTexture", 0);
    m_screenShader->setFloat("near_plane", 1.f);
    m_screenShader->setFloat("far_plane", 20.f);
    drawScreen();
}

void RendererVSM::setVarClamp(float vc) {
    m_varMin = vc;
    m_shadow.setMinVar(vc);
}

void RendererVSM::setKernelSize(uint KS) {
    m_kernelSize= KS;
}

void RendererVSM::setStandardDeviation(float sd) {
    m_standardDeviation = sd;
}
