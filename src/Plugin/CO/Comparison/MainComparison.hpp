//
// Created by pierre on 09/01/19.
//

#ifndef ROGUE_MAINCOMPARISON_HPP
#define ROGUE_MAINCOMPARISON_HPP

#include <QtGui/QImage>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include "OperatorsComparison.hpp"

/**
 * \file MainComparison.hpp
 * \brief Manage comparisons module
 */

/**
 * \class MainComparison
 * \brief Manage comparisons module
 */
class MainComparison {
public:
    MainComparison(QComboBox *img1, QComboBox *img2);
    ~MainComparison();

    void LaunchComparison();

    /**
     * @brief Set the image one given by the relativ path to the first image for comparision
     * @param id : Relativ path of the image
     */
    void setImg1(std::string id);
    /**
     * @brief Set the image two given by the relativ path to the first image for comparision
     * @param id : Relativ path of the image
     */
    void setImg2(std::string id);

    /**
     * @brief Set the operator to apply
     * @param number : ID of the operator
     */
    void setNumberOperator(int number);

    /**
     * @brief Set the multiplicator corresponding to the brightness of the result grayscale image
     * @param number
     */
    void setMultiplicator(int number);

    /**
     * @brief Saves the current frame of the given renderer as a selectable image
     * @param img
     * @param renderer
     */
    void saveImg(QImage img, std::string renderer);

    /**
     * @brief Saves the current frame of the given renderer as a png file : You will find them in the bin folder
     * @param img
     * @param renderer
     */
    void saveImgPng(QImage img, std::string renderer);

    /**
     * @brief Saves the result of the selected Comparison operator
     */
    void saveResult();

    /**
     * @brief Display image 1
     */
    void displayImg1();

    /**
     * @brief Display image 2
     */
    void displayImg2();
private:
    std::map<std::string, QImage> images;
    OperatorsComparison operatorsComparison;

    QComboBox *img1ComboBox;
    QComboBox *img2ComboBox;

    std::string id1;
    std::string id2;

    int numberOperator = 0;
    bool firstImg1CB = true;
    bool firstImg2CB = true;

    QLabel *label1;
    QLabel *label2;
};


#endif //ROGUE_MAINCOMPARISON_HPP
