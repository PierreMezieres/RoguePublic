//
// Created by pierre on 09/01/19.
//

#include <Core/Log/Log.hpp>
#include <Core/Math/Color.hpp>
#include "OperatorsComparison.hpp"

OperatorsComparison::OperatorsComparison(){

    label = new QLabel();
    label->setWindowTitle(QString::fromStdString("Result"));
}

OperatorsComparison::~OperatorsComparison() {
    delete label;
}

void OperatorsComparison::setImg1(QImage img){
    img1 = img;
}

void OperatorsComparison::setImg2(QImage img){
    img2 = img;
}

QImage OperatorsComparison::getResult() {
    return result;
}

void OperatorsComparison::imageComparison(int mode){
    if(!verifySize()){
        Log(logWarning) << "Can't do the comparisons with 2 differents size for images";
    }else{
        QSize size = img1.size();
        QImage res(size,QImage::Format_RGB32);

        int X = size.width();
        int Y = size.height();

        switch(mode){
            case 0: imageDifferencesLuminosity(res,X,Y); break;
            case 1: imageDifferencesLightness(res,X,Y); break;
            case 2: imageDifferencesAverage(res,X,Y); break;
            case 3: imageDifferencesColorLAB(res,X,Y); break;
            case 4: imageDifferencesColorHSV(res,X,Y); break;
            case 5: imageDifferencesColorHSVhue(res,X,Y); break;
            case 6: imageDifferencesColorHSVsaturation(res,X,Y); break;
            case 7: imageDifferencesColorHSVvalue(res,X,Y); break;
        }

        result = res;
        label->setPixmap(QPixmap::fromImage(res));
        label->hide();
        label->show();
    }
}

bool OperatorsComparison::verifySize() {
    QSize s1 = img1.size();
    QSize s2 = img2.size();
    if(s1.height()!=s2.height()) return false;
    if(s1.width()!=s2.width()) return false;
    return true;
}

void OperatorsComparison::setMultiplicator(int number) {
    multiplicator = number;
}

QColor OperatorsComparison::mapResult(int result) {
    int res = multiplicator * result;
    if(res>=255) res=255;
    return QColor(res,res,res);
}

void OperatorsComparison::imageDifferencesLightness(QImage &res, int &X, int &Y){

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            int gray1 = toGrayLightness(img1.pixel(i,j));
            int gray2 = toGrayLightness(img2.pixel(i,j));
            float t = std::abs(gray1 - gray2);
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}

void OperatorsComparison::imageDifferencesAverage(QImage &res, int &X, int &Y){

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            int gray1 = toGrayAverage(img1.pixel(i,j));
            int gray2 = toGrayAverage(img2.pixel(i,j));
            float t = std::abs(gray1 - gray2);
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}

void OperatorsComparison::imageDifferencesLuminosity(QImage &res, int &X, int &Y){

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            int gray1 = toGrayLuminosity(img1.pixel(i,j));
            int gray2 = toGrayLuminosity(img2.pixel(i,j));

            float t = std::abs(gray1 - gray2);
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}

void OperatorsComparison::imageDifferencesColorLAB(QImage &res, int &X, int &Y) {

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            QColor c1 = img1.pixel(i,j);
            QColor c2 = img2.pixel(i,j);
            float t = deltaE76lab(c1,c2) *255.0;
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}

void OperatorsComparison::imageDifferencesColorHSVhue(QImage &res, int &X, int &Y) {

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            QColor c1 = img1.pixel(i,j);
            QColor c2 = img2.pixel(i,j);
            float t = gapColorHSVhue(c1,c2) *255.0;
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}

void OperatorsComparison::imageDifferencesColorHSVsaturation(QImage &res, int &X, int &Y) {

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            QColor c1 = img1.pixel(i,j);
            QColor c2 = img2.pixel(i,j);
            float t = gapColorHSVsaturation(c1,c2) *255.0;
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}

void OperatorsComparison::imageDifferencesColorHSVvalue(QImage &res, int &X, int &Y) {

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            QColor c1 = img1.pixel(i,j);
            QColor c2 = img2.pixel(i,j);
            float t = gapColorHSVvalue(c1,c2) * 255.0;
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}


void OperatorsComparison::imageDifferencesColorHSV(QImage &res, int &X, int &Y) {

    for(auto i=0;i<X;i++){
        for(auto j=0; j<Y; j++){
            QColor c1 = img1.pixel(i,j);
            QColor c2 = img2.pixel(i,j);
            float t = gapColorHSV(c1,c2) *255.0;
            QColor q = mapResult(t);
            res.setPixelColor(i,j,q);
        }
    }
}
