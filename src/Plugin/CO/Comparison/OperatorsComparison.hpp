//
// Created by pierre on 09/01/19.
//

#ifndef ROGUE_OPERATORSCOMPARISON_HPP
#define ROGUE_OPERATORSCOMPARISON_HPP


#include <QtGui/QImage>
#include <QtWidgets/QLabel>

/**
 * \file OperatorComparison.hpp
 * \brief Available operator for the comparisons module
 */

/**
 * \class OperatorComparison
 * \brief Implement and compute comparision
 */
class OperatorsComparison {
public:
    OperatorsComparison();
    ~OperatorsComparison();

    /**
     * @brief Set the QImage as the first operand
     * @param img
     */
    void setImg1(QImage img);

    /**
     * @brief Set the QImage as the second operand
     * @param img
     */
    void setImg2(QImage img);

    /**
     * @brief Set the multiplicator coefficient
     * @param number
     */
    void setMultiplicator(int number);

    /**
     * @brief Pull the result from the current operation state
     * @return
     */
    QImage getResult();

    /**
     * @brief Launch the comparision with the operator given as parameter
     * @param mode
     */
    void imageComparison(int mode);

private:
    QImage img1;
    QImage img2;
    QImage result;
    int multiplicator = 1;

    bool verifySize();

    QLabel *label;

    QColor mapResult(int result);
    void imageDifferencesLightness(QImage &res, int &X, int &Y);
    void imageDifferencesAverage(QImage &res, int &X, int &Y);
    void imageDifferencesLuminosity(QImage &res, int &X, int &Y);
    void imageDifferencesColorLAB(QImage &res, int &X, int &Y);
    void imageDifferencesColorHSV(QImage &res, int &X, int &Y);
    void imageDifferencesColorHSVhue(QImage &res, int &X, int &Y);
    void imageDifferencesColorHSVsaturation(QImage &res, int &X, int &Y);
    void imageDifferencesColorHSVvalue(QImage &res, int &X, int &Y);
};


#endif //ROGUE_OPERATORSCOMPARISON_HPP
