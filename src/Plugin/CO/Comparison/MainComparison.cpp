//
// Created by pierre on 09/01/19.
//

#include <Core/Log/Log.hpp>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>
#include <Core/Util/FileUtil.hpp>
#include "MainComparison.hpp"

#define ID "[ComparisonModule] "

MainComparison::MainComparison(QComboBox *img1, QComboBox *img2) {
    img1ComboBox = img1;
    img2ComboBox = img2;

    label1 = new QLabel();
    label1->setWindowTitle(QString::fromStdString("Image 1"));
    label2 = new QLabel();
    label2->setWindowTitle(QString::fromStdString("Image 2"));
}

MainComparison::~MainComparison() {
    delete label1;
    delete label2;
}

void MainComparison::LaunchComparison(){
    operatorsComparison.imageComparison(numberOperator);
}

void MainComparison::setImg1(std::string id){
    id1 = id;
    operatorsComparison.setImg1(images[id]);
}

void MainComparison::setImg2(std::string id){
    id2 = id;
    operatorsComparison.setImg2(images[id]);
}

void MainComparison::setNumberOperator(int number) {
    numberOperator = number;
}

void MainComparison::setMultiplicator(int number) {
    operatorsComparison.setMultiplicator(number);
}

std::string getNameFromRenderer(std::string renderer){
    if(renderer == "MBOIT") return "OIT-MBOIT-";
    if(renderer == "WBOIT") return "OIT-WBOIT-";
    if(renderer == "Depth Peeling") return "OIT-DepthPeeling-";
    if(renderer == "PCF") return "SM-PCF-";
    if(renderer == "MSM") return "SM-MSM-";
    if(renderer == "VSM") return "SM-VSM-";
    return renderer + "-";
}


void MainComparison::saveImg(QImage img, std::string renderer){
    // current date/time based on current system
    time_t now = time(0);

    tm *ltm = localtime(&now);

    std::string name =  getNameFromRenderer(renderer) + std::to_string(ltm->tm_hour) + "-" + std::to_string(ltm->tm_min) + "-" + std::to_string(ltm->tm_sec);

    Log(logInfo) << ID << "Save Actual Image : " << name;
    images[name] = img;

    img1ComboBox->addItem(QString::fromStdString(name));
    img2ComboBox->addItem(QString::fromStdString(name));

    if(firstImg1CB){
        firstImg1CB = false;
        setImg1(name);
    }
    if(firstImg2CB){
        firstImg2CB = false;
        setImg2(name);
    }
}

void MainComparison::saveImgPng(QImage img, std::string renderer) {
    time_t now = time(0);

    tm *ltm = localtime(&now);

    std::string name =  getNameFromRenderer(renderer) + std::to_string(ltm->tm_mday) + "-" + std::to_string(ltm->tm_mon+1) + "-" + std::to_string(ltm->tm_year-100) + "-" +
            std::to_string(ltm->tm_hour) + "-" + std::to_string(ltm->tm_min) + "-" + std::to_string(ltm->tm_sec) + ".png";
    Log(logInfo) << ID << "Save Actual Image : " << name;
    img.save(QString::fromStdString(name));
}

void MainComparison::saveResult() {
    QImage img = operatorsComparison.getResult();
    int cpt = 0;
    while(FileUtil::fileExist(QString::fromStdString("Result" + std::to_string(cpt) + ".png"))) cpt++;
    std::string str ="Result" + std::to_string(cpt) + ".png";
    img.save(str.data());
}

void MainComparison::displayImg1() {
    if(!firstImg1CB){
        label1->setPixmap(QPixmap::fromImage(images[id1]));
        label1->show();
    }
}

void MainComparison::displayImg2() {
    if(!firstImg2CB){
        label2->setPixmap(QPixmap::fromImage(images[id2]));
        label2->show();
    }
}