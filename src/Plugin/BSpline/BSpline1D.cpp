//
// Created by pierre on 22/09/18.
//

#include <src/Core/Shapes/Sphere.hpp>
#include "BSpline1D.hpp"

BSpline1D::BSpline1D(VectorPoint3 pt, float k, std::string nodal ): BSpline(k, pt.size(), nodal),
        m_pt{pt}{

    m_k = k;
}

BSpline1D::BSpline1D(VectorPoint3 pt, float k, VectorFloat nodal): BSpline(k, pt.size(), nodal),
                                                                    m_pt{pt}{

    m_k = k;
}

Point3 BSpline1D::compute(float u){

    int dec=0;
    int i=m_k;
    int k=m_k;
    VectorPoint3 ptCalc;

    while(u>m_U[i]){
        dec = dec+1;
        i=i+1;
    }

    for(int j =0;j<m_k;j++) ptCalc.push_back(m_pt[dec+j]);

    while(k>1){
        for(int j=0;j<k-1;j++){
            float max = m_U[dec+k+j];
            float min = m_U[dec+j+1];
            float u1 = (max-u)/(max-min);
            float u2 = (u-min)/(max-min);
            ptCalc[j] = u1 * ptCalc[j] + u2 * ptCalc[j+1];
        }
        dec++;
        k--;
    }

    return ptCalc[0];


}

Mesh BSpline1D::getMesh(float step){
    Mesh m;
    int i=0;
    for(auto u = m_U[m_k-1]; u< m_U[m_pt.size()];u+=step){

        m.m_vertices.push_back(compute(u));

        if(i) m.setLine( Line( i-1,i ) );
        i++;
        m.m_texCoord.push_back(Vector3(0));
        m.m_normals.push_back(Vector3(0,1,0));

    }

    m.setMeshTypes(GL_LINES);
    return m;
}

VectorPoint3 BSpline1D::getPoints(float step){
    VectorPoint3 res;
    for(auto u = m_U[m_k-1]; u< m_U[m_pt.size()];u+=step){
        res.push_back(compute(u));
    }
    return res;
}


std::vector<Object*> BSpline1D::getObjectPoints() {
    std::vector<Object*> objs;
    for(auto i:m_pt){
        Object *obj = new Object(createSphereShape(0.1));
        obj->translate(i);
        objs.push_back(obj);
    }
    return objs;
}