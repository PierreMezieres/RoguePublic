//
// Created by pierre on 09/10/18.
//

#ifndef ROGUE_NODALS_HPP
#define ROGUE_NODALS_HPP

#include <src/Core/Macros.hpp>

/** \class Nodals
 * \brief [Plugin BSpline]
 */
class Nodals {
public:

    Nodals(int k, int pointsNumber, std::string nodal);

    VectorFloat getVector();

private:
    int m_k;
    VectorFloat m_U;

};


#endif //ROGUE_NODALS_HPP
