//
// Created by pierre on 12/09/18.
//

#include "BSpline.hpp"
#include "Nodals.hpp"

BSpline::BSpline(float k, int pointsNumber, std::string nodal): m_k{k}{

    Nodals nod(k,pointsNumber,nodal);
    m_U = nod.getVector();
}

BSpline::BSpline(float k, int, VectorFloat nodal): m_k{k}, m_U{nodal}{

}
