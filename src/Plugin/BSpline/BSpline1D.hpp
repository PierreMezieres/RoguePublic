//
// Created by pierre on 22/09/18.
//

#ifndef ROGUE_BSPLINE2D_HPP
#define ROGUE_BSPLINE2D_HPP


#include <src/Engine/Object/Object.hpp>
#include "BSpline.hpp"

/** \class BSpline1D
 * \brief [Plugin BSpline]
 */
class BSpline1D : public BSpline {
public:
    // Math
    BSpline1D(VectorPoint3 pt, float k, std::string nodal = "uniform");
    BSpline1D(VectorPoint3 pt, float k, VectorFloat nodal);
    Point3 compute(float u); //Floraison

    // Link with ogre
    Mesh getMesh(float step = 0.1) override; // Obtenir le maillage 1D
    VectorPoint3 getPoints(float step = 0.1); // Obtenir juste les vertices du maillage
    std::vector<Object*> getObjectPoints(); // Obtenir les points de contrôle
private:
    VectorPoint3 m_pt; // Points de contrôle
};


#endif //ROGUE_BSPLINE2D_HPP
