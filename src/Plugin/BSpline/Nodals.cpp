//
// Created by pierre on 09/10/18.
//

#include "Nodals.hpp"
Nodals::Nodals(int k, int pointsNumber, std::string nodal): m_k{k}{

    if(nodal == "ouvertUniforme"){
        for(int i=0;i<k;i++) m_U.push_back(0);
        for(int i=1;i<pointsNumber-k+1;i++) m_U.push_back(i);
        for(int i=0;i<k;i++) m_U.push_back(pointsNumber-k+1);

    }else if(nodal=="uniform"){
        for(int i=0;i<k+pointsNumber+1;i++){
            m_U.push_back(i);
        }
    }else{
        int number = 0;
        for(int i=0;i<k+pointsNumber+1;i++){
            number += std::rand()%10;
            m_U.push_back(number);
        }
    }
}

VectorFloat Nodals::getVector() {
    return m_U;
}