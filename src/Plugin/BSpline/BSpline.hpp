//
// Created by pierre on 12/09/18.
//

#ifndef ROGUE_BSPLINE_HPP
#define ROGUE_BSPLINE_HPP


#include <src/Engine/Object/Mesh.hpp>
#include <src/Engine/Object/Object.hpp>

/** \class BSpline
 * \brief [Plugin BSpline]
 */
class BSpline {
public:
    // Math
    BSpline(float k, int pointsNumber, std::string nodal);
    BSpline(float k, int pointsNumber, VectorFloat nodal);
    virtual ~BSpline(){};

    // Link with ogre
    virtual Mesh getMesh(float step = 0.1) = 0;


    float m_k;
    VectorFloat m_U; //Vecteur nodal

};


#endif //ROGUE_BSPLINE_HPP
