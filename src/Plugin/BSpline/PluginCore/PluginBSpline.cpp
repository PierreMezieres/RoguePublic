//
// Created by pierre on 16/09/18.
//

#include "PluginBSpline.hpp"
#include "BSplineWidget.hpp"
#include "BSplineShortcut.hpp"

PluginBSpline::PluginBSpline() {
    m_name = "PluginBSpline";
}

void PluginBSpline::loadData() {

    m2Widget = new M2Widget(m_context);
    m2Shortcut = new M2Shortcut(m2Widget);
    registerWidget(m2Widget,"BSpline");
    registerShortcut(m2Shortcut);
}