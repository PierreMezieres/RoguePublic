//
// Created by pierre on 16/09/18.
//

#ifndef ROGUE_PLUGINM2_HPP
#define ROGUE_PLUGINM2_HPP

#include <Plugin/PluginInterface.hpp>
#include "BSplineShortcut.hpp"

/** \class PluginBSpline
 * \brief [Plugin BSpline]
 */
class PluginBSpline : public PluginInterface {
public:

    PluginBSpline();
    void loadData();

    M2Widget * m2Widget;
    M2Shortcut *m2Shortcut;

};


#endif //ROGUE_PLUGINM2_HPP
