//
// Created by pierre on 17/09/18.
//

#include <src/Core/Log/Log.hpp>
#include "BSplineShortcut.hpp"
M2Shortcut::M2Shortcut(M2Widget *m2Widget) {
    id_shortcut = "BSpline shortcut";
    m_m2Widget = m2Widget;
}

bool M2Shortcut::keyboard(unsigned char k) {
    switch(k) {
        case 'q': m_mode_move = FREE; Log(logInfo) << "FREE MODE"; return true;
        case 's': m_mode_move = XY; Log(logInfo) << "XY MODE"; return true;
        case 'd': m_mode_move = YZ; Log(logInfo) << "YZ MODE"; return true;
        case 'f': m_mode_move = XZ; Log(logInfo) << "XZ MODE"; return true;
        default:
            return false;
    }
}

void M2Shortcut::mouseclick(ButtonMouse button, float xpos, float ypos) {
    if(button==LEFT_BUTTON){
        objClicked=m_m2Widget->m_context.scene->drawData.rayCast(xpos,ypos);
        if(objClicked){
            for(auto i:m_m2Widget->m_pointsObjects2D){
                if(i->getName()==objClicked->getName()){
                    posObjClicked = objClicked->getPositionModel();
                    return;
                }
            }
            for(auto i:m_m2Widget->m_pointsObjects3D){
                for(auto j:i){
                    if(j->getName()==objClicked->getName()){
                        posObjClicked = objClicked->getPositionModel();
                        return;
                    }
                }
            }
            objClicked = nullptr;
        }
    }
}

void M2Shortcut::mousemove(ButtonMouse button, float xpos, float ypos) {
    if(objClicked && button==LEFT_BUTTON){
        Normal normalPlan;
        Vector3 X(1,0,0);
        Vector3 Y(0,1,0);
        Vector3 Z(0,0,1);
        Vector3 right = m_m2Widget->m_context.scene->drawData.camera->m_right;
        Vector3 up = m_m2Widget->m_context.scene->drawData.camera->m_up;
        right = glm::normalize(right);
        up = glm::normalize(up);
        switch(m_mode_move){
            case FREE: normalPlan = glm::cross(right,up); break;
            case XY:

                normalPlan = Z;break;
            case YZ: normalPlan = X;break;
            case XZ: normalPlan = Y; break;
        }

        Ray r = m_m2Widget->m_context.scene->drawData.camera->getRayFromScreen(xpos,ypos);

        Point3 newPos;
        if(r.intersectionPlane(posObjClicked, normalPlan, newPos )){
            objClicked->setPositionModel(newPos);
        }else{
            if(r.intersectionPlane(posObjClicked, -normalPlan, newPos )){
                objClicked->setPositionModel(newPos);
            }
        }

        m_m2Widget->reloadBSpline();
    }

}