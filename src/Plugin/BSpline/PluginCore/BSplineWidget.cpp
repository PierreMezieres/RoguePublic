#include <src/Core/Log/Log.hpp>
#include <src/Plugin/PluginInterface.hpp>
#include <src/Plugin/BSpline/BSpline.hpp>
#include <src/Core/Shapes/Grid.hpp>
#include <src/Plugin/BSpline/BSpline1D.hpp>
#include <src/Plugin/BSpline/BSpline2D.hpp>
#include "BSplineWidget.hpp"
#include "ui_BSplineWidget.h"

M2Widget::M2Widget(ContextPlugin context,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::M2Widget)
{
    ui->setupUi(this);

    m_context = context;
    objectManager = &m_context.scene->drawData.objectManager;
    spinBoxK = ui->spinBoxK;
}

M2Widget::~M2Widget()
{
    delete ui;
}

void M2Widget::on_buttonDemo1_clicked()
{
    Log(logInfo) << "Demo1";
    m_context.scene->cleanScene();
    VectorPoint3 pt;
    pt.push_back(Vector3(-3,0,0));
    pt.push_back(Vector3(-1,2,0));
    pt.push_back(Vector3(1,2,0));
    pt.push_back(Vector3(2,0,0));
    pt.push_back(Vector3(3,1,0));
    m_k=spinBoxK->value();
    loadBSpline2D(pt,m_k);
    //activatingDemo(1);
}

void M2Widget::reloadBSpline() {
    if(mode2d){
        VectorPoint3 pts;
        for(auto i:m_pointsObjects2D){
            pts.push_back(getPositionMat4(i->getModel()));
        }
        BSpline1D bspline(pts,m_k, nodal);
        m_bSpline->setMesh(bspline.getMesh(step));
    }else{
        std::vector<VectorPoint3> all_pts;
        VectorPoint3 pts;
        for(auto i:m_pointsObjects3D){
            pts.clear();
            for(auto j:i){
                pts.push_back(getPositionMat4(j->getModel()));
            }
            all_pts.push_back(pts);
        }
        BSpline2D bspline(all_pts,m_k, nodal);
        m_bSpline->setMesh(bspline.getMesh(step));
    }
    m_context.openGLWidget->update();
}

void M2Widget::loadBSpline() {
    objectManager->addValue(m_bSpline,"BSPLINE");


    Object *o = new Object(Mesh(createGridShape(10,10,10,10)));
    o->setMaterial("blank");
    objectManager->addValue(o,"grid");

    m_context.openGLWidget->update();

}

void M2Widget::loadBSpline2D(std::vector<Vector3> pts, int k){
    mode2d = true;
    BSpline1D bspline(pts,k, nodal);
    m_bSpline = new Object(bspline.getMesh(step));
    m_pointsObjects2D=bspline.getObjectPoints();
    m_bSpline->setMaterial("red");
    for(auto i:m_pointsObjects2D){
        objectManager->addValue(i,"POINT");
    }
    loadBSpline();
}

void M2Widget::loadBSpline3D(std::vector<VectorPoint3> pts, int k){
    mode2d = false;
    BSpline2D bspline(pts,k,nodal);
    m_bSpline = new Object(bspline.getMesh(step));
    m_pointsObjects3D=bspline.getObjectPoints();
    for(auto i:m_pointsObjects3D){
        for(auto j:i){
            objectManager->addValue(j,"POINT");
        }
    }
    loadBSpline();

}

void M2Widget::activatingDemo(int i) {
    switch(i){
        case 1: m_context.openGLWidget->loadFile("../Assets/BSpline/demo1.ogre"); break;
        default: break;
    }
}
void M2Widget::on_buttonDemo2_clicked()
{
    Log(logInfo) << "Demo2";
    m_context.scene->cleanScene();
    std::vector<VectorPoint3> pts;
    VectorPoint3 pt;
    pt.push_back(Vector3(-3,0,-3));
    pt.push_back(Vector3(-1,2,-3));
    pt.push_back(Vector3(1,2,-3));
    pt.push_back(Vector3(2,0,-3));
    pt.push_back(Vector3(3,1,-3));
    pts.push_back(pt);
    pt.clear();
    pt.push_back(Vector3(-3,0,-2));
    pt.push_back(Vector3(-1,2,-2));
    pt.push_back(Vector3(1,2,-2));
    pt.push_back(Vector3(2,0,-2));
    pt.push_back(Vector3(3,1,-2));
    pts.push_back(pt);
    pt.clear();
    pt.push_back(Vector3(-3,0,-1));
    pt.push_back(Vector3(-1,2,-1));
    pt.push_back(Vector3(1,2,-1));
    pt.push_back(Vector3(2,0,-1));
    pt.push_back(Vector3(3,1,-1));
    pts.push_back(pt);
    pt.clear();
    pt.push_back(Vector3(-3,0,0));
    pt.push_back(Vector3(-1,2,0));
    pt.push_back(Vector3(1,2,0));
    pt.push_back(Vector3(2,0,0));
    pt.push_back(Vector3(3,1,0));
    pts.push_back(pt);
    pt.clear();
    pt.push_back(Vector3(-3,0,1));
    pt.push_back(Vector3(-1,2,1));
    pt.push_back(Vector3(1,2,1));
    pt.push_back(Vector3(2,0,1));
    pt.push_back(Vector3(3,1,1));
    pts.push_back(pt);
    pt.clear();
    pt.push_back(Vector3(-3,0,2));
    pt.push_back(Vector3(-1,2,2));
    pt.push_back(Vector3(1,2,2));
    pt.push_back(Vector3(2,0,2));
    pt.push_back(Vector3(3,1,2));
    pts.push_back(pt);
    pt.clear();
    pt.push_back(Vector3(-3,0,3));
    pt.push_back(Vector3(-1,2,3));
    pt.push_back(Vector3(1,2,3));
    pt.push_back(Vector3(2,0,3));
    pt.push_back(Vector3(3,1,3));
    pts.push_back(pt);

    m_k=spinBoxK->value();
    loadBSpline3D(pts,m_k);


}

void M2Widget::on_spinBoxK_valueChanged(int arg1)
{
    m_k = arg1;
    reloadBSpline();
}

void M2Widget::on_comboBox_activated(int index)
{
    switch(index){
        case 1: nodal = "ouvertUniforme"; break;
        case 0: nodal = "uniform"; break;
        default : nodal = "random"; break;
    }
    reloadBSpline();
}

void M2Widget::on_buttonVisible_clicked()
{
    visible = !visible;
    if(!visible){
        for(auto i:m_pointsObjects2D){
            m_context.scene->drawData.objectManager.remValue(i);
        }
        for(auto i:m_pointsObjects3D){
            for(auto j:i){
                m_context.scene->drawData.objectManager.remValue(j);
            }
        }

    }else{
        for(auto i:m_pointsObjects2D){
            m_context.scene->drawData.objectManager.addValue(i);
        }
        for(auto i:m_pointsObjects3D){
            for(auto j:i){
                m_context.scene->drawData.objectManager.addValue(j);
            }
        }
    }
    m_context.openGLWidget->update();
}

void M2Widget::on_doubleSpinBox_valueChanged(double arg1)
{
    step = arg1;
    reloadBSpline();
}
