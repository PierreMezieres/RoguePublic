//
// Created by pierre on 17/09/18.
//

#ifndef ROGUE_M2SHORTCUT_HPP
#define ROGUE_M2SHORTCUT_HPP


#include <src/Gui/ShortcutInterface.hpp>
#include "BSplineWidget.hpp"

/** \class M2Shortcut
 * \brief [Plugin BSpline]
 */
class M2Shortcut : public ShortcutInterface{
public:
    M2Shortcut(M2Widget *m2Widget);

    enum mode_move{FREE = 0, XY = 1, YZ =2, XZ =3};

    bool keyboard(unsigned char k) override;
    void mouseclick(ButtonMouse button, float xpos, float ypos) override;
    void mousemove(ButtonMouse button, float xpos, float ypos) override;

    M2Widget *m_m2Widget;
    Object *objClicked;
    Point3 posObjClicked;
    mode_move m_mode_move = XY;

};


#endif //ROGUE_M2SHORTCUT_HPP
