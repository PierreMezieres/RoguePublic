//
// Created by pierre on 26/08/18.
//

#ifndef ROGUE_PLUGININTERFACE_HPP
#define ROGUE_PLUGININTERFACE_HPP

/**
 * \file PluginInterface.hpp
 * \brief Interface to implement plugin
 */

#include <src/Engine/Loader/FileLoaderManager.hpp>
#include <src/Gui/OpenGLWidget.hpp>
#include <QtWidgets/QMenu>
#include <src/Gui/MainWindow.hpp>
#include <QtWidgets/QComboBox>

struct ContextPlugin{
    FileLoaderManager *fileLoaderManager;
    OpenGLWidget *openGLWidget;
    Scene *scene;
    QTabWidget *tabWidget;
    QComboBox *shortcutComboBox;
    MainWindow *mainWindow;
};


/**
 * \class PluginInterface
 * \brief [Plugin] Interface to implement a plugin
 */
class PluginInterface {
public:

    virtual ~PluginInterface(){};

    std::string m_name = "NO NAME PLUGIN";

    void loadPlugin(ContextPlugin contextPlugin){
        Log(logInfo) << "[" << m_name << "] loading";
        m_context = contextPlugin;
        loadData();
    }

    virtual void loadData() = 0;

    void registerFileLoader(FileLoader *fileLoader){
        m_context.fileLoaderManager->addValue(fileLoader);
    }

    void registerComputeEachDraw(ComputeEachDraw *computeEachDraw){
        m_context.scene->computeEachDraw.push_back(computeEachDraw);
    }

    void registerWidget(QWidget *widget, std::string name){
        m_context.tabWidget->addTab(widget,QString::fromStdString(name));
    }

    void registerShortcut(ShortcutInterface *shortCutInterface){
            m_context.scene->shortCuts[shortCutInterface->id_shortcut]=shortCutInterface;
            m_context.shortcutComboBox->addItem(QString::fromStdString(shortCutInterface->id_shortcut));
    }

    void registerRenderer(Renderer *renderer){
            m_context.mainWindow->addRenderer(renderer);
    }

    void registerShader(std::string name, Shader *shader){
        m_context.scene->drawData.shaderManager.addValue(name,shader);
    }

    ObjectManager *getObjectManager(){
        return &m_context.scene->drawData.objectManager;
    }

    ShaderManager *getShaderManager(){
        return &m_context.scene->drawData.shaderManager;
    }

    MaterialManager *getMaterialManager(){
        return &m_context.scene->drawData.materialManager;
    }

    ContextPlugin m_context;
};

#endif //ROGUE_PLUGININTERFACE_HPP
