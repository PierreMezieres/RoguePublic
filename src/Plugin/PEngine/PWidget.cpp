#include "PWidget.hpp"
#include "ui_PWidget.h"
#include <Core/Log/Log.hpp>

PWidget::PWidget(QWidget *parent, PEngine *pE) :
    QWidget(parent),
    ui(new Ui::PWidget)
{
    ui->setupUi(this);

    pEngine = pE;
}

PWidget::~PWidget()
{
    delete ui;
}

void PWidget::on_PlayButton_clicked()
{
    if(pEngine->playPause()){
        ui->PlayButton->setText("Play");
    }else{
        ui->PlayButton->setText("Pause");
    }
}

void PWidget::on_TraceButton_clicked()
{
    if(pEngine->activeTrace()){
        ui->TraceButton->setText("Trace on");
    }else{
        ui->TraceButton->setText("Trace off");
    }
}
