//
// Created by pierre on 20/08/18.
//

#ifndef ROGUE_PSPRING_HPP
#define ROGUE_PSPRING_HPP

#include <src/Engine/Object/Object.hpp>
#include "PObject.hpp"

/** \class PSpring
 * \brief [Plugin PEngine]
 */
class PSpring : public Object{
public:
    PSpring(PObject * o1, PObject * o2, float stif = 5, float length = 0);

    PObject * obj1;
    PObject * obj2;

    void deformation();
    Vector3 computeForce(PObject *o);

    float m_stifness;
    float m_restLength;

    void displayInfo();

};


#endif //ROGUE_PSPRING_HPP
