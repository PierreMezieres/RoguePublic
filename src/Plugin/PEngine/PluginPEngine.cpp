//
// Created by pierre on 26/08/18.
//

#include <src/Engine/Loader/RogueLoader.hpp>
#include <src/Plugin/PEngine/Loader/PRogueLoader.hpp>
#include "PluginPEngine.hpp"
#include "PhysicShortcut.hpp"
#include "PWidget.hpp"

PluginPEngine::PluginPEngine() {
    m_name = "PEngine";

    pEngine = new PEngine();
}

void PluginPEngine::loadData() {
    registerFileLoader(new PRogueLoader(pEngine));
    registerComputeEachDraw(pEngine);
    registerShortcut(new PhysicShortcut(pEngine));
    registerWidget(new PWidget(m_context.tabWidget,pEngine),"Physic");
}
