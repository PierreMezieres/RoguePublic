//
// Created by pierre on 26/08/18.
//

#ifndef ROGUE_PLUGINPENGINE_HPP
#define ROGUE_PLUGINPENGINE_HPP

#include <Plugin/PluginInterface.hpp>
#include <Plugin/PEngine/PEngine.hpp>

/** \class PluginPEngine
 * \brief [Plugin PEngine]
 */
class PluginPEngine : public PluginInterface {
public:

    PluginPEngine();
    void loadData();

    PEngine *pEngine;


};


#endif //ROGUE_PLUGINPENGINE_HPP
