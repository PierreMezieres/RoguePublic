//
// Created by pierre on 28/08/18.
//

#include "PhysicShortcut.hpp"

PhysicShortcut::PhysicShortcut(PEngine *pEngine) : m_pEngine{pEngine} {
    id_shortcut = "Physic shortcut";
}

bool PhysicShortcut::keyboard(unsigned char k) {
    switch(k) {
        case 'p':
            m_pEngine->playPause();
            if(m_pEngine->active){
                Log(logInfo) << "Play physic";
            }else{
                Log(logInfo) << "Pause physic";
            }
            return true;
        case 'i':
            m_pEngine->utilDataP.boundFactor-=0.05;
            if(m_pEngine->utilDataP.boundFactor<0) m_pEngine->utilDataP.boundFactor=0;
            Log(logInfo) << "Bound Factor : " << m_pEngine->utilDataP.boundFactor;
            return true;
        case 'o':
            m_pEngine->utilDataP.boundFactor+=0.05;
            Log(logInfo) << "Bound Factor : " << m_pEngine->utilDataP.boundFactor;
            return true;
        case 't':
            m_pEngine->activeTrace();
            if(m_pEngine->activeTracebool){
                Log(logInfo) << "Trace on";
            }else{
                Log(logInfo) << "Trace off";
            }
            return true;
        case 'b':
            m_pEngine->displayInfo();
            return true;
        case 'n':
            m_pEngine->displayInfo2();
            return true;
        default:
            return false;
    }
}
