//
// Created by pierre on 20/08/18.
//

#ifndef ROGUE_PENGINE_HPP
#define ROGUE_PENGINE_HPP


#include <src/Core/Math/Chronometer.hpp>
#include <src/Engine/Renderer/ComputeEachDraw.hpp>
#include "PObject.hpp"
#include "PSpring.hpp"

/** \class PEngine
 * \brief [Plugin PEngine]
 */
class PEngine : public ManagerList<PObject*>, public ComputeEachDraw {
public:
    PEngine();
    ~PEngine();

    void addSpring(PSpring * spring);
    void computeDeformationSpring();
    std::vector<PSpring*> springList;

    bool activeTrace();

    void computeEachDraw();

    void cleanUp() override;

    bool playPause();
    bool active = false;
    bool activeTracebool = false;

    UtilDataP utilDataP;

    Chronometer chrono;

    void displayInfo();
    void displayInfo2();

    //TODO Function to save and restart easily the system

};


#endif //ROGUE_PENGINE_HPP
