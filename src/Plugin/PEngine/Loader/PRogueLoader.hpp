//
// Created by pierre on 27/08/18.
//

#ifndef ROGUE_POGRELOADER_HPP
#define ROGUE_POGRELOADER_HPP


#include <src/Engine/Loader/RogueLoader.hpp>
#include <src/Plugin/PEngine/PEngine.hpp>
#include "PParserRogue.hpp"

/** \class PRogueLoader
 * \brief [Plugin PEngine]
 */
class PRogueLoader : public RogueLoader{
public:
    PRogueLoader(PEngine *pEngine);

    std::vector<std::string> getFormatManaged() override;
    virtual void loadCustomData() override;
    bool customToken(std::vector<std::string> line, int n) override ;

    PEngine *m_pEngine;
    PParserRogue *pParserRogue;

};


#endif //ROGUE_POGRELOADER_HPP
