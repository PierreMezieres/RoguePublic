//
// Created by pierre on 27/08/18.
//

#include <src/Engine/Material/MaterialGenerator.hpp>
#include "PRogueLoader.hpp"
#include "PParserRogue.hpp"

PRogueLoader::PRogueLoader(PEngine *pEngine): RogueLoader(), m_pEngine{pEngine} {
}

std::vector<std::string> PRogueLoader::getFormatManaged(){
    std::vector<std::string> formats;
    formats.push_back("*.progue");
    return formats;
}

void PRogueLoader::loadCustomData() {
    pParserRogue=new PParserRogue(m_drawData,m_pEngine);
}


bool PRogueLoader::customToken(std::vector<std::string> line, int n){
    std::string token = line[0];
    bool token_unknown = false;
    switch(token.c_str()[0]){
        case 'p':
            if(token == "pobject") {
                pParserRogue->processPObject(line);
            }else if(token == "pobjectinv"){
                pParserRogue->processPObject(line,true);
            }else if(token == "pspring"){
                std::string err=pParserRogue->processPSpring(line);
                if(err!="") Error(err,n);
            }else if(token == "pDefaultMaterial"){
                pParserRogue->processDefaultMaterial(line);
            }else token_unknown=true;
            break;
        case 'r':
            if(token == "rebound"){
                pParserRogue->processRebound(line);
            }else token_unknown=true;
            break;
        default: token_unknown=true; break;
    }
    return token_unknown;
}
