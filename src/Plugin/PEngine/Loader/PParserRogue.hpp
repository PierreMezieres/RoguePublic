//
// Created by pierre on 27/08/18.
//

#ifndef ROGUE_PPARSEROGRE_HPP
#define ROGUE_PPARSEROGRE_HPP


#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Engine/Loader/ParserRogue.hpp>
#include <src/Plugin/PEngine/PObject.hpp>
#include <src/Plugin/PEngine/PEngine.hpp>

/** \class PParserRogue
 * \brief [Plugin PEngine]
 */
class PParserRogue {
public:
    PParserRogue(DrawData *d, PEngine *p);

    void processPObject(TOKENS list, bool inv = false);
    std::string processPSpring(TOKENS list);
    void  processRebound(TOKENS list);
    void processDefaultMaterial(TOKENS list);

    PEngine *m_pEngine;
    DrawData *m_drawData;
    std::vector<PObject*> listPObject;

    std::string defaultMaterial = "default";

};


#endif //ROGUE_PPARSEROGRE_HPP
