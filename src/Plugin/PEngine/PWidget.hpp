#ifndef PWIDGET_HPP
#define PWIDGET_HPP

#include <QWidget>
#include "PEngine.hpp"

namespace Ui {
class PWidget;
}

/** \class PWidget
 * \brief [Plugin PEngine]
 */
class PWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PWidget(QWidget *parent, PEngine *pE);
    ~PWidget();

    PEngine *pEngine;

private slots:

    void on_PlayButton_clicked();

    void on_TraceButton_clicked();

private:
    Ui::PWidget *ui;
};

#endif // PWIDGET_HPP
