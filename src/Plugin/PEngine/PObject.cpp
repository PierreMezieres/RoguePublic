//
// Created by pierre on 20/08/18.
//

#include "PObject.hpp"
#include <Core/Shapes/Sphere.hpp>
#include <Plugin/PEngine/PSpring.hpp>

#define GRAV Vector3(0,-9.81,0)
#define UPSILON 0.001

PObject::PObject(Vector3 pos, Vector3 speed, float weight, float radius, std::string materialTrace) : Object(createSphereShape(radius)), m_position{pos}, m_speed{speed}, m_weight{weight}, m_radius{radius} {

    translate(pos);

    if(pos.y < radius) setPosition(Vector3(m_position.x, radius, m_position.z));

    Mesh m = Mesh();
    m.setMeshTypes(GL_LINES);
    trace = new Object(m);
    trace->setMaterial(materialTrace);
}


void PObject::setPosition(Vector3 position){
    m_position = position;
    setPositionModel(position);

}

void PObject::setSpeed(Vector3 speed){
    m_speed = speed;
}

Vector3 PObject::getPosition(){
    return m_position;
}

Vector3 PObject::getSpeed(){
    return m_speed;
}

void PObject::computeSpeed(UtilDataP &u){

    Vector3 f;
    for(auto i: m_springList){
        f+=i->computeForce(this);
    }

    f += Vector3(m_weight) * GRAV;
    f *= (u.deltaT/m_weight);

    setSpeed((f + getSpeed())*u.airFactor);
}

void PObject::computePosition(UtilDataP &u){
    auto pos = Vector3(u.deltaT) * getSpeed() + getPosition();
    if(pos.y<=UPSILON+m_radius){
        auto v = getSpeed();
        v= Vector3(u.boundFactor*v.x,-u.boundFactor*v.y,u.boundFactor*v.z);
        setSpeed(v);
        pos.y=m_radius+UPSILON;
    }
    auto oldPos = getPosition();
    if(glm::length(oldPos-pos) > UPSILON && u.traceActive ) trace->getElements()[0]->m_mesh.addLine(oldPos,pos);
    setPosition(pos);
}

void PObject::computeForce(UtilDataP &u){
    computeSpeed(u);
    computePosition(u);
}

void PObject::draw(DataForDrawing data){
    for(auto i:getElements()){
        i->draw(data,getModel());
    }
    trace->draw(data);
}

void PObject::displayInfo(){
    Log(logInfo) << "| --------------- PObject ---------------";
    Log(logInfo) << "| - Name: " << getName();
    Log(logInfo) << "| - Position: " << m_position.x << " " << m_position.y << " " << m_position.z;
    Log(logInfo) << "| - Speed: " << m_speed.x << " " << m_speed.y << " " << m_speed.z;
    Log(logInfo) << "| - Weight: " << m_weight;
}