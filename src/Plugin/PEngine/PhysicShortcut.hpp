//
// Created by pierre on 28/08/18.
//

#ifndef ROGUE_PHYSICSHORTCUT_HPP
#define ROGUE_PHYSICSHORTCUT_HPP


#include <src/Gui/ShortcutInterface.hpp>
#include "PEngine.hpp"

/** \class PhysicShortcut
 * \brief [Plugin PEngine]
 */
class PhysicShortcut : public ShortcutInterface{
public:
    PhysicShortcut(PEngine *pEngine);

    bool keyboard(unsigned char k) override;

    PEngine *m_pEngine;

};


#endif //ROGUE_PHYSICSHORTCUT_HPP
