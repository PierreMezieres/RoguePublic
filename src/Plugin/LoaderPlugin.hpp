//
// Created by pierre on 26/08/18.
//

#ifndef ROGUE_LOADERPLUGIN_HPP
#define ROGUE_LOADERPLUGIN_HPP

/**
 * \file LoaderPlugin.hpp
 * \brief Plugin loader
 */


#include "PluginInterface.hpp"
#include <vector>

//Temporary class to load plugins ...

/** \class LoaderPlugin
 *  \brief [Plugin] Load plugins
 */
class LoaderPlugin {
public:
    LoaderPlugin(ContextPlugin contextPlugin);
    void load();

    std::vector<PluginInterface*> plugins;
    ContextPlugin m_context;

};


#endif //ROGUE_LOADERPLUGIN_HPP
