#include <src/Core/Shapes/Cube.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>
#include <src/Core/Mesh/SubdivLoop.hpp>
#include <src/Core/Mesh/Decimator.hpp>
#include "GeoWidget.hpp"
#include "ui_GeoWidget.h"

#include <OpenMesh/Tools/Subdivider/Uniform/LoopT.hh>
#include <src/Core/Mesh/MarchingCubes.hpp>


GeoWidget::GeoWidget(ContextPlugin context,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GeoWidget)
{
    ui->setupUi(this);

    m_context = context;
    objectManager = &m_context.scene->drawData.objectManager;

    labelObj = ui->label;
    objClicked=nullptr;

    VectorPoint3 vectorPoint {
            {-1,-1,1},{1,-1,1},{1,1,1},{-1,1,1},
            {-1,-1,-1},{1,-1,-1},{1,1,-1},{-1,1,-1}
    };
    VectorUInt indices {
        0,2,1,0,3,2,
        1,6,5,1,2,6,
        5,7,4,5,6,7,
        4,3,0,4,7,3,
        3,6,2,3,7,6,
        4,1,5,4,0,1
    };
    VectorPoint3 vectorNormal {
            {0,0,1},{0,0,1},{0,0,1},{0,0,1},
            {0,0,-1},{0,0,-1},{0,0,-1},{0,0,-1},
    };
    m_mesh = createTriangleMeshShape(vectorPoint, indices);

    pointsMarching.push_back(Point3(NX/2+30,NY/2,NZ/2));
    pointsMarching.push_back(Point3(NX/2,NY/2,NZ/2));
}

GeoWidget::~GeoWidget()
{
    delete ui;
}

void GeoWidget::decalPoint(float x,float y,float z) {
    pointsMarching[0].x += x;
    pointsMarching[0].y += y;
    pointsMarching[0].z += z;
    updateMarchingCubes();
}

void GeoWidget::on_pushButton_clicked()
{
    Log(logInfo) << "Geo";

    m_context.scene->cleanScene();
    m_context.openGLWidget->loadFile("../Assets/Model/bunny.obj");

    Object *bun = objectManager->getValue(0);
    bun->scale(10);


    m_context.openGLWidget->loadFile("../Assets/Model/cat.obj");
    bun = objectManager->getValue(1);
    bun->translate(-4,0,0);
    bun->scale(0.006);


    m_context.openGLWidget->loadFile("../Assets/Model/hand.obj");
    bun = objectManager->getValue(2);
    bun->translate(-8,0,0);
    bun->scale(0.006);
    /*OpenMesh::Subdivider::Uniform::LoopT<OpenMesh::PolyMeshT> loop;
    loop.attach(meshOM);
    loop(1);
    loop.detach();*/
    //subdivLoop(meshOM);

    Object *o = new Object(m_mesh);
    o->translate(4,1,0);
    objectManager->addValue(o,"CUBE");

    m_context.openGLWidget->fitScene();




}

void GeoWidget::on_subdividePushButton_clicked()
{
    if(objClicked){
        Mesh mesh = objClicked->getMesh();

        Log(logInfo) << "SUBDIVIDE";
        subdivLoop(mesh);
        objClicked->setMesh(mesh);
        m_context.openGLWidget->update();
    }else{
        Log(logInfo) << "NO OBJECT SELECTED";
    }
}

void GeoWidget::on_invNormalPushButton_clicked()
{
    if(objClicked){
        Mesh mesh = objClicked->getMesh();
        mesh.invNormals();
        objClicked->setMesh(mesh);
        m_context.openGLWidget->update();
    }
}

void GeoWidget::on_marchingCubesButton_clicked()
{
    Log(logInfo) << "MarchingCubes";

    m_context.scene->cleanScene();
    Mesh m = mc.getMesh(isolevel,pointsMarching);

    objMarching = new Object(m);
    objectManager->addValue(objMarching,"MC");

    Object *o = mc.getCage();
    objectManager->addValue(o,"CAGE");

    m_context.openGLWidget->fitScene();
    m_context.openGLWidget->update();

}

void GeoWidget::updateMarchingCubes() {
    objMarching->setMesh(mc.getMesh(isolevel,pointsMarching));
    m_context.openGLWidget->update();
}

void GeoWidget::on_xpButton_clicked()
{
    pointsMarching[0].x += 1;
    updateMarchingCubes();
}

void GeoWidget::on_xmButton_clicked()
{
    pointsMarching[0].x += -1;
    updateMarchingCubes();
}

void GeoWidget::on_ypButton_clicked()
{
    pointsMarching[0].y += 1;
    updateMarchingCubes();
}

void GeoWidget::on_ymButton_clicked()
{
    pointsMarching[0].y += -1;
    updateMarchingCubes();
}

void GeoWidget::on_zpButton_clicked()
{
    pointsMarching[0].z += 1;
    updateMarchingCubes();
}

void GeoWidget::on_zmButton_clicked()
{
    pointsMarching[0].z += -1;
    updateMarchingCubes();
}

void GeoWidget::on_isovalueSpinBox_valueChanged(double arg1)
{
    isolevel = arg1;
    updateMarchingCubes();
}

void GeoWidget::on_pushButton_2_clicked()
{
    if(objClicked){
        Mesh mesh = objClicked->getMesh();
        Log(logInfo) << "DECIMATE";
        decimate(mesh,edgeCollapse);
        objClicked->setMesh(mesh);
        m_context.openGLWidget->update();
    }else{
        Log(logInfo) << "NO OBJECT SELECTED";
    }
}

void GeoWidget::on_spinBox_valueChanged(int arg1)
{
    edgeCollapse = arg1;
}
