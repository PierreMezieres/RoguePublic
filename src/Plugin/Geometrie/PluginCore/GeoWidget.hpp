#ifndef GEOWIDGET_HPP
#define GEOWIDGET_HPP

#include <QWidget>
#include <src/Plugin/PluginInterface.hpp>
#include <QtWidgets/QLabel>
#include <src/Core/Mesh/MarchingCubes.hpp>

namespace Ui {
class GeoWidget;
}

/** \class GeoWidget
 * \brief [Plugin Geo]
 */
class GeoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GeoWidget(ContextPlugin context, QWidget *parent = 0);
    ~GeoWidget();

    ContextPlugin m_context;
    QLabel * labelObj;
    Object *objClicked;

    void decalPoint(float x, float y, float z);

private slots:
    void on_pushButton_clicked();

    void on_subdividePushButton_clicked();

    void on_invNormalPushButton_clicked();

    void on_marchingCubesButton_clicked();

    void on_xpButton_clicked();

    void on_xmButton_clicked();

    void on_ypButton_clicked();

    void on_ymButton_clicked();

    void on_zpButton_clicked();

    void on_zmButton_clicked();

    void on_isovalueSpinBox_valueChanged(double arg1);

    void on_pushButton_2_clicked();

    void on_spinBox_valueChanged(int arg1);

private:
    Ui::GeoWidget *ui;
    ObjectManager *objectManager;

    // Marching cube
    std::vector<Point3> pointsMarching;
    int NX=100, NY=60,NZ=60;
    MarchingCubes mc = MarchingCubes(true,NX,NY,NZ);
    Object *objMarching;
    double isolevel=0.01;
    void updateMarchingCubes();

    int edgeCollapse = 2000;

    Mesh m_mesh;
    Mesh m_mesh2;
};

#endif // GEOWIDGET_HPP
