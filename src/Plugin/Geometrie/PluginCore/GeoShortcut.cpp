//
// Created by pierre on 22/10/18.
//

#include "GeoShortcut.hpp"
#include <src/Core/Log/Log.hpp>

GeoShortcut::GeoShortcut(GeoWidget *geo) {
    id_shortcut = "Geo shortcut";
    geoWidget = geo;
}

bool GeoShortcut::keyboard(unsigned char k) {
    Log(logInfo) << k;
    if(k=='7'){
        geoWidget->decalPoint(-1,0,0);
        return true;
    }
    if(k=='8'){
        geoWidget->decalPoint(1,0,0);
        return true;
    }
    if(k=='4'){
        geoWidget->decalPoint(0,1,0);
        return true;
    }
    if(k=='5'){
        geoWidget->decalPoint(0,-1,0);
        return true;
    }
    if(k=='9'){
        geoWidget->decalPoint(0,0,1);
        return true;
    }
    if(k=='6'){
        geoWidget->decalPoint(0,0,-1);
        return true;
    }
    return false;
}

void GeoShortcut::mouseclick(ButtonMouse button, float xpos, float ypos) {
    if(button==LEFT_BUTTON){
        objClicked=geoWidget->m_context.scene->drawData.rayCast(xpos,ypos);
        if(objClicked!=nullptr){
            geoWidget->labelObj->setText(QString::fromStdString("Object clicked :  " + objClicked->getName()));
            geoWidget->objClicked=objClicked;
        }else{
            Log(logInfo) << "Ray cast: NO OBJET HIT";
        }
    }
}

void GeoShortcut::mousemove(ButtonMouse, float, float) {

}
