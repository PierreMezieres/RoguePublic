//
// Created by pierre on 22/10/18.
//

#ifndef ROGUE_PLUGINGEO_HPP
#define ROGUE_PLUGINGEO_HPP

#include <Plugin/PluginInterface.hpp>
#include "GeoWidget.hpp"
#include "GeoShortcut.hpp"

/** \class PluginGeo
 * \brief [Plugin Geo]
 */
class PluginGeo : public PluginInterface {
public:

    PluginGeo();
    void loadData();

    GeoWidget *geoWidget;
    GeoShortcut *geoShortcut;

};


#endif //ROGUE_PLUGINGEO_HPP
