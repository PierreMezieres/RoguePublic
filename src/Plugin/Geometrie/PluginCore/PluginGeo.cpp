//
// Created by pierre on 22/10/18.
//

#include "PluginGeo.hpp"
#include "GeoShortcut.hpp"


PluginGeo::PluginGeo() {
    m_name = "PluginGeo";
}

void PluginGeo::loadData() {

    geoWidget = new GeoWidget(m_context);
    geoShortcut = new GeoShortcut(geoWidget);
    registerWidget(geoWidget,"Geo");
    registerShortcut(geoShortcut);
}
