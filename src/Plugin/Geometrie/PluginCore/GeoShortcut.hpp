//
// Created by pierre on 22/10/18.
//

#ifndef ROGUE_GEOSHORTCUT_HPP
#define ROGUE_GEOSHORTCUT_HPP


#include <src/Gui/ShortcutInterface.hpp>
#include <Core/Macros.hpp>
#include <src/Engine/Object/Object.hpp>
#include "GeoWidget.hpp"

/** \class GeoShortcut
 * \brief [Plugin Geo]
 */
class GeoShortcut : public ShortcutInterface {
public:
    GeoShortcut(GeoWidget *geo);

    enum mode_move{FREE = 0, XY = 1, YZ =2, XZ =3};

    bool keyboard(unsigned char k) override;
    void mouseclick(ButtonMouse button, float xpos, float ypos) override;
    void mousemove(ButtonMouse button, float xpos, float ypos) override;

    GeoWidget *geoWidget;
    Object *objClicked;

};

#endif //ROGUE_GEOSHORTCUT_HPP
