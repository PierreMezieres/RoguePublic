//
// Created by pierre on 20/11/18.
//

#include <src/Plugin/Anim/RendererAnim.hpp>
#include "PluginAnim.hpp"

PluginAnim::PluginAnim() {
    m_name = "PluginAnim";

}

void PluginAnim::loadData(){
    geoWidget = new AnimWidget(m_context);
    geoShortcut = new AnimShortcut(geoWidget);
    registerWidget(geoWidget,"Anim");
    registerShortcut(geoShortcut);
    registerRenderer(new RendererAnim(geoWidget));
}