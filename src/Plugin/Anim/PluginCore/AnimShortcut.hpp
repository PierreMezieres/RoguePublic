//
// Created by pierre on 20/11/18.
//

#ifndef ROGUE_ANIMSHORTCUT_HPP
#define ROGUE_ANIMSHORTCUT_HPP


#include <src/Gui/ShortcutInterface.hpp>
#include <src/Engine/Object/Object.hpp>
#include "AnimWidget.hpp"

class AnimShortcut : public ShortcutInterface {
public:
    AnimShortcut(AnimWidget *anim);

    enum mode_move{FREE = 0, XY = 1, YZ =2, XZ =3};

    bool keyboard(unsigned char k) override;
    void mouseclick(ButtonMouse button, float xpos, float ypos) override;
    void mousemove(ButtonMouse button, float xpos, float ypos) override;

    AnimWidget *animWidget;
    Object *objClicked;

};


#endif //ROGUE_ANIMSHORTCUT_HPP
