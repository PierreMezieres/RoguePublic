//
// Created by pierre on 20/11/18.
//

#ifndef ROGUE_PLUGINANIM_HPP
#define ROGUE_PLUGINANIM_HPP


#include <src/Plugin/PluginInterface.hpp>
#include "AnimWidget.hpp"
#include "AnimShortcut.hpp"

/** \class PluginAnim
 * \brief [Plugin Anim]
 */
class PluginAnim : public PluginInterface {
public:

    PluginAnim();
    void loadData();

    AnimWidget *geoWidget;
    AnimShortcut *geoShortcut;

};


#endif //ROGUE_PLUGINANIM_HPP
