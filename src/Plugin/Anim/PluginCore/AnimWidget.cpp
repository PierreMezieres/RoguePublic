#include <src/Core/Shapes/Cylinder.hpp>
#include <src/Core/Shapes/Grid.hpp>
#include <src/Core/Math/Math.hpp>
#include "AnimWidget.hpp"
#include "ui_AnimWidget.h"

AnimWidget::AnimWidget(ContextPlugin contextPlugin, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AnimWidget)
{
    ui->setupUi(this);
    slider = ui->horizontalSlider;
    dial1 = ui->dial;
    dial2 = ui->dialAngle;

    m_context = contextPlugin;
    objectManager = &m_context.scene->drawData.objectManager;
    materialManager = &m_context.scene->drawData.materialManager;
    shaderManager = &m_context.scene->drawData.shaderManager;


    shaderManager->addValue("lbs",new Shader("../Shaders/Anim/lbs.vs.glsl","../Shaders/Anim/lbs.fs.glsl"));
    shaderManager->addValue("dqs",new Shader("../Shaders/Anim/dqs.vs.glsl","../Shaders/Anim/dqs.fs.glsl"));
    shaderManager->addValue("WeightShader",new Shader("../Shaders/Anim/Weight.vs.glsl","../Shaders/Anim/Weight.fs.glsl"));
    Material *mat = new Material(Vector3(1));
    mat->setShader("lbs");
    materialManager->addValue("lbs",mat);
    mat = new Material(Vector3(1));
    mat->setShader("dqs");
    materialManager->addValue("dqs",mat);
    mat = new Material(Vector3(1));
    mat->setShader("WeightShader");
    materialManager->addValue("WeightMaterial", mat);
    mesh_cylinder = createCylinderShape(8,1,64);


    Mesh m = createCylinderShape(8,1,64);
    m.m_vertices_init = m.m_vertices;

    Bone *bone=new Bone(Point3(4,1.5,0),Point3(0,1.5,0));
    bone->setPosition(Vector3(0));
    m.boneList.clear();
    m.boneList.push_back(bone);
    Bone *bone2= new Bone(Point3(0,1.5,0),Point3(-4,1.5,0));
    bone->childs.push_back(bone2);
    bone2->setPosition(Vector3(0,4,0));
    m.boneList.push_back(bone2);
    m.boneWeight.resize(m.m_texCoord.size());

    Object * o = new Object(m);
    o->translate(4,1.5,0);
    o->rotate(90,0,0,1);
    o->setMaterial("WeightMaterial");

    object =o;
    element = o->getElements()[0];
    mesh = m;

    computeWeight();

}

AnimWidget::~AnimWidget()
{
    delete ui;
}

void AnimWidget::computeWeight() {
    float d =(float)(value_d)/200;
    float min = 0.5-d;
    float max = 0.5+d;
    for(auto i=0; i< (int)mesh_cylinder.m_texCoord.size(); i++){
        Vector3 tex = mesh_cylinder.m_texCoord[i];
        float a = tex.y;
        if(a<min) a = 0;
        else{
            if(a>max) a = 1;
            else{
                a = (a - min) / (max - min);
            }
        }
        mesh.boneWeight[i].clear();
        mesh.boneWeight[i].push_back(std::pair<int,float>(0,1-a));
        mesh.boneWeight[i].push_back(std::pair<int,float>(1,a));

        mesh.m_texCoord[i] = Vector3(1-a,a,0);
    }
    mesh.setUpdate();
    element->m_mesh=mesh;
}

void AnimWidget::computeWeightSmooth() {
    float d =(float)(value_d)/200;
    float min = 0.5-d;
    float max = 0.5+d;
    float R = max-min;
    for(auto i=0; i< (int)mesh.m_texCoord.size(); i++){
        Vector3 tex = mesh_cylinder.m_texCoord[i];
        float a = tex.y;
        if(a<min) a = 1;
        else{
            if(a>max) a = 0;
            else{
                a=a-min;
                a = std::pow(R*R -(a*a), 2);
                a= 1/(std::pow(R,4)) * a;
            }
        }
        mesh.boneWeight[i].clear();
        mesh.boneWeight[i].push_back(std::pair<int,float>(0,a));
        mesh.boneWeight[i].push_back(std::pair<int,float>(1,1-a));
        mesh.m_texCoord[i] = Vector3(a,1-a,0);
    }
    mesh.setUpdate();
    element->m_mesh=mesh;
}

void AnimWidget::computeRotation() {
    mesh.boneList[1]->rotateRest(-rot+180,Vector3(0.,0.,1.));
    mesh.boneList[1]->rotate(-rot3+180,Vector3(0.,1.,0.));
    mesh.boneList[0]->rotateRest(-rot2+180,Vector3(0.,0.,1.));
}

void AnimWidget::computeMesh() {
    computeRotation();
    for(auto i=0; i<(int)mesh.m_vertices.size(); i++){
        Matrix4 blend(0);
        for(uint j=0; j<mesh.boneWeight[i].size(); j++){
            std::pair<int,float> pair = mesh.boneWeight[i][j];
            Bone* bone = mesh.boneList[pair.first];
            blend += pair.second * bone->getBlendPose();
        }
        mesh.m_vertices[i]= blend * Vector4(mesh.m_vertices_init[i],1);
    }
    mesh.setUpdate();
    element->m_mesh=mesh;
}


void AnimWidget::computeMeshDQS() {
    computeRotation();
    for(auto i=0; i<(int)mesh.m_vertices.size(); i++){
        DualQuat dq;
        glm::quat q0 = mesh.boneList[1]->getDualQuat().dq.real;
        for(uint j=0; j<mesh.boneWeight[i].size(); j++){
            std::pair<int,float> pair = mesh.boneWeight[i][j];
            Bone* bone = mesh.boneList[pair.first];
            if( glm::dot(dq.dq.real,q0) < 0.f)
                dq.blend(bone->getDualQuat(), -pair.second);
            else
                dq.blend(bone->getDualQuat(), pair.second);
        }
        dq.normalize();
        mesh.m_vertices[i]= dq.transformPositionDQS(mesh.m_vertices_init[i]);
    }
    mesh.setUpdate();
    element->m_mesh=mesh;

}

void AnimWidget::updateWeights() {
    if(mode_weight==0){
        computeWeight();
    }else{
        computeWeightSmooth();
    }
}

void AnimWidget::updateMesh() {
    switch(mode_anim){
        case 0: computeMesh(); break;
        case 1: case 3: computeRotation(); break;
        case 2: computeMeshDQS(); break;
        default: break;
    }
}

void AnimWidget::on_loadButton_clicked()
{
    m_context.scene->cleanScene();

    objectManager->addValue(object,"cylinder");

    Object *grid = new Object(createGridShape());
    objectManager->addValue(grid,"grid");

    m_context.openGLWidget->fitScene();
}

void AnimWidget::on_dSlider_valueChanged(int value)
{
    value_d = value;
    updateWeights();
    updateMesh();
    m_context.openGLWidget->update();
}

void AnimWidget::on_dialAngle_valueChanged(int value)
{
    rot2 = value;
    updateMesh();
    m_context.openGLWidget->update();
}

void AnimWidget::on_horizontalSlider_valueChanged(int value)
{
    rot3 = value;
    updateMesh();
    m_context.openGLWidget->update();

}

void AnimWidget::on_dial_valueChanged(int value)
{
    rot = value;
    updateMesh();
    m_context.openGLWidget->update();
}

void AnimWidget::on_pushButton_clicked()
{
    mode_anim = (mode_anim+1)%4;

    if(mode_anim==0){
        ui->pushButton->setText("LBS - CPU");
    }else{
        if(mode_anim==1){
            ui->pushButton->setText(" LBS - GPU");
        }else{
            if(mode_anim==2){
                ui->pushButton->setText("DQS - CPU");
            }else{
                ui->pushButton->setText("DQS - GPU");
            }
        }
    }
    for(auto i=0; i<(int)mesh.m_vertices.size(); i++){
        mesh.m_vertices[i]= mesh.m_vertices_init[i];
    }
    mesh.setUpdate();
    element->m_mesh=mesh;
    updateMesh();
    m_context.openGLWidget->update();
}

void AnimWidget::on_comboBox_activated(int index)
{
    mode_weight = index;
    updateWeights();
    updateMesh();
    m_context.openGLWidget->update();
}

void AnimWidget::on_resetButton_clicked()
{
    dial1->setValue(180);
    dial2->setValue(180);
    slider->setValue(180);
}
