#ifndef ANIMWIDGET_HPP
#define ANIMWIDGET_HPP

#include <QWidget>
#include <src/Plugin/PluginInterface.hpp>
#include <QtWidgets/QDial>

namespace Ui {
class AnimWidget;
}

class AnimWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AnimWidget(ContextPlugin contextPlugin,QWidget *parent = 0);
    ~AnimWidget();

    void computeRotation();
    void computeMesh();
    void computeMeshDQS();
    void computeWeight();
    void computeWeightSmooth();

    int mode_anim = 0; // 0: LBS - CPU / 1: LBS - GPU / 2: DQS - CPU / 3: DQS - GPU
    int mode_weight = 0;


private slots:
    void on_loadButton_clicked();

    void on_dSlider_valueChanged(int value);

    void on_dialAngle_valueChanged(int value);

    void on_horizontalSlider_valueChanged(int value);

    void on_dial_valueChanged(int value);

    void on_pushButton_clicked();

    void on_comboBox_activated(int index);

    void on_resetButton_clicked();

private:
    Ui::AnimWidget *ui;

    ContextPlugin m_context;
    ObjectManager *objectManager;
    MaterialManager *materialManager;
    ShaderManager *shaderManager;

    Object *object;
    Element *element;
    Mesh mesh;

    void updateWeights();
    void updateMesh();

    Mesh mesh_cylinder;

    int value_d = 50;
    float rot = 180;
    float rot2 = 180;
    float rot3 = 180;
    QDial *dial1;
    QDial *dial2;
    QSlider *slider;
};

#endif // ANIMWIDGET_HPP
