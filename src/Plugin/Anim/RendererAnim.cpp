//
// Created by pierre on 30/11/18.
//

#include "RendererAnim.hpp"

RendererAnim::RendererAnim(AnimWidget *_animWidget) : Renderer("AnimRenderer"), animWidget{_animWidget}{

}

void RendererAnim::draw(DrawData &drawData) {
    DataForDrawing dfd = drawData.getDataForDrawing();

    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    if (getDrawMode())
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);



    for(auto object : drawData.objectManager.getValues()){
        for(auto elt: object->getElements()){


            Material * material;
            int mode = animWidget->mode_anim;
            if(mode == 0 || mode == 1 || mode == 2){
                material = dfd.materialManager->getValue("lbs");
            }else{
                material = dfd.materialManager->getValue("dqs");
            }
            Shader *shader = dfd.shaderManager->getValue(material->getNameShader());

            shader->use();
            shader->setMat4("proj",dfd.camera->GetProjMatrix());
            shader->setMat4("view",dfd.camera->GetViewMatrix());
            shader->setVec3("viewPos",dfd.camera->m_position);
            shader->setMat4("model",object->getModel());


            //data.lightManager->bind(shader);
            material->bind(shader, dfd);

            Mesh m = elt->m_mesh;

            Matrix4 I(1.0);
            switch(animWidget->mode_anim){
                case 0: // LBS - CPU
                    shader->setMat4("bone1",I);
                    shader->setMat4("bone2",I);
                    break;
                case 1: // LBS - GPU
                    if(m.boneList.size()>=1){
                        shader->setMat4("bone1",m.boneList[0]->getBlendPose());
                    }
                    if(m.boneList.size()>=2){
                        shader->setMat4("bone2",m.boneList[1]->getBlendPose());
                    }
                    break;
                case 2: // DQS - CPU
                    shader->setMat4("bone1",I);
                    shader->setMat4("bone2",I); break;
                case 3: // DQS - GPU
                    if(m.boneList.size()>=1){
                        DualQuat dq = m.boneList[0]->getDualQuat();
                        glm::quat q1 = dq.dq.real;
                        glm::quat q2 = dq.dq.dual;
                        shader->setVec4("b1r",{q1.x,q1.y,q1.z,q1.w});
                        shader->setVec4("b1i",{q2.x,q2.y,q2.z,q2.w});
                    }
                    if(m.boneList.size()>=2){
                        DualQuat dq = m.boneList[1]->getDualQuat();
                        glm::quat q1 = dq.dq.real;
                        glm::quat q2 = dq.dq.dual;
                        shader->setVec4("b2r",{q1.x,q1.y,q1.z,q1.w});
                        shader->setVec4("b2i",{q2.x,q2.y,q2.z,q2.w});
                    }
                    break;

                default: break;
            }

            elt->m_mesh.draw();
        }
    }

    /*glDisable(GL_DEPTH_TEST);
    for(auto object : drawData.objectManager.getValues()){
        for(auto bone : object->getMesh().boneList){
            bone->object->draw(dfd);
        }
    }*/

}

void RendererAnim::resize(int, int) {
}
