//
// Created by pierre on 30/11/18.
//

#ifndef ROGUE_RENDERERANIM_HPP
#define ROGUE_RENDERERANIM_HPP


#include <src/Engine/Renderer/Renderer.hpp>
#include <Plugin/Anim/PluginCore/AnimWidget.hpp>

class RendererAnim : public Renderer{
public:
    RendererAnim(AnimWidget *_animWidget);

    void draw(DrawData &drawData);

    void resize(int w, int h);
private:

    AnimWidget *animWidget;
};


#endif //ROGUE_RENDERERANIM_HPP
