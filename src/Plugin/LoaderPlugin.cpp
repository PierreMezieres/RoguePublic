    //
// Created by pierre on 26/08/18.
//

#include <src/Plugin/PEngine/PluginPEngine.hpp>
#include <src/Plugin/BSpline/PluginCore/PluginBSpline.hpp>
#include <src/Plugin/Geometrie/PluginCore/PluginGeo.hpp>
#include <src/Plugin/Anim/PluginCore/PluginAnim.hpp>
#include <Plugin/CO/PluginCore/PluginCO.hpp>
#include <Plugin/Test/PluginCore/PluginTest.hpp>
#include "LoaderPlugin.hpp"

LoaderPlugin::LoaderPlugin(ContextPlugin contextPlugin) {

    Log(logInfo) << "";
    Log(logInfo) << "[LOAD PLUGINS]";

    plugins.push_back(new PluginTest());
    plugins.push_back(new PluginCO());
    plugins.push_back(new PluginAnim());
    plugins.push_back(new PluginGeo());
    plugins.push_back(new PluginPEngine());
    plugins.push_back(new PluginBSpline());

    m_context=contextPlugin;
    load();

    Log(logInfo) << "[END LOAD PLUGINS]";
    Log(logInfo) << "";
}

void LoaderPlugin::load() {
    for(auto i:plugins){
        i->loadPlugin(m_context);
    }
}
