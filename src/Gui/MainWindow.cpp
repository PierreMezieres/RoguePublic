#include "MainWindow.hpp"
#include "ui/ui_mainwindow.h"

#include <QMessageBox>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <src/Core/Log/Log.hpp>
#include <src/Engine/Renderer/DefaultRenderer.hpp>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    QSurfaceFormat format;
    format.setVersion(4, 1);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    ui->setupUi(this);

    //ui->dockWidget->setVisible(false);

    ui->openglWidget->setFocus();

    tabWidget = ui->tabWidget;
    shortcutComboBox = ui->shortcutComboBox;
    rendererComboBox = ui->rendererComboBox;

    ui->openglWidget->labelMS = ui->labelTimeMS;
    ui->openglWidget->labelFPS = ui->labelTimeFPS;
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *) {
    Log(logInfo) << "";
    Log(logSuccess) << "Good bye :)";
}

void MainWindow::on_action_Version_OpenGL_triggered() {
    std::stringstream message;
    message << "Renderer       : " << glGetString(GL_RENDERER) << std::endl;
    message << "Vendor         : " << glGetString(GL_VENDOR) << std::endl;
    message << "Version        : " << glGetString(GL_VERSION) << std::endl;
    message << "GLSL Version   : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    QMessageBox::information(this, "OpenGL Information", message.str().c_str());
}

void MainWindow::on_actionCommande_triggered()
{
    std::stringstream message;
    message << "d : toggle draw mode\n" <<
            "u : toggle update every time \n" <<
            "c : change camera \n" <<
            "s : change default shader \n" <<
            "f : fit scene \n" <<
            "r : clean scene\n" <<
            "l : load file\n" <<
            "y : change shortcut set\n" <<
            "1-3 : See info in terminal\n" << std::endl;
    //messageBox =QMessageBox(QMessageBox::Information, "Global shortcut", message.str().c_str());
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setWindowTitle("Global shortcut");
    messageBox.setText(message.str().c_str());
    messageBox.setModal(false);
    messageBox.show();

    //QMessageBox::information(this, "Global shortcut", message.str().c_str());
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    ui->openglWidget->keyPressEvent(event);

}


void MainWindow::on_actionOptions_triggered()
{
    ui->dockWidget->setVisible(true);
}

void MainWindow::on_updateButton_clicked()
{
    if(ui->openglWidget->updateToggle()){
        ui->updateButton->setText("Update");
    }else{
        ui->updateButton->setText("No Update");
    }
}

void MainWindow::on_fitButton_clicked()
{
    ui->openglWidget->fitScene();
}

void MainWindow::on_clearButton_clicked()
{
    ui->openglWidget->clearScene();
    ui->openglWidget->resetCameraPoints();
}

void MainWindow::on_loadButton_clicked()
{
    ui->openglWidget->loadFile();
}

void MainWindow::on_drawmodeButton_clicked()
{
    ui->openglWidget->changeDrawMode();
}

void MainWindow::on_openglInfoButton_clicked()
{
    ui->openglWidget->displayInfo();
}

void MainWindow::on_drawdataInfoButton_clicked()
{
    ui->openglWidget->getScene()->drawData.displayInfo();
}

void MainWindow::on_rendererInfoButton_clicked()
{
    ui->openglWidget->getScene()->displayInfo();
}

void MainWindow::on_postProcessButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->changePostProcess());
    ui->postProcessButton->setText(t);
}

void MainWindow::on_skyboxButton_clicked()
{

    QString t = QString::fromStdString(ui->openglWidget->changeSkybox());
    ui->skyboxButton->setText(t);
}

void MainWindow::on_ssaoButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->toggleSSAO());
    ui->ssaoButton->setText(t);

}

void MainWindow::on_radiusSsaoSpinBox_valueChanged(double arg1)
{
    getDefaultRenderer()->changeRadiusSSAO(arg1);
    ui->openglWidget->update();
}

void MainWindow::on_biasSpinBox_valueChanged(double arg1)
{
    getDefaultRenderer()->changeBiasSSAO(arg1);
    ui->openglWidget->update();
}

void MainWindow::on_shortcutComboBox_activated(const QString &arg1)
{
    ui->openglWidget->changeShortcut(arg1.toStdString());
}

void MainWindow::on_cameraComboBox_activated(int index)
{
    ui->openglWidget->changeCamera(index);
}

void MainWindow::on_frameBufferComboBox_activated(int index)
{
    ui->openglWidget->defaultShader(index);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->openglWidget->fitElement();
    Log(logWarning) << "Fit Element is not finished ...";
}

void MainWindow::on_fitObjectButton_clicked()
{
    ui->openglWidget->fitObject();
}

void MainWindow::on_fxaaButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->toggleFXAA());
    ui->fxaaButton->setText(t);
}

void MainWindow::on_debugFxaaButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->toggleFXAADebug());
    ui->debugFxaaButton->setText(t);
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    getDefaultRenderer()->changeGammaHDR(arg1);
    ui->openglWidget->update();
}

void MainWindow::on_exposureSpinBox_2_valueChanged(double arg1)
{
    getDefaultRenderer()->changeExposureHDR(arg1);
    ui->openglWidget->update();
}

void MainWindow::on_pushButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->toggleHDR());
    ui->pushButton->setText(t);
}

void MainWindow::on_amountBloomspinBox_valueChanged(int arg1)
{
    getDefaultRenderer()->changeAmountBloom(arg1);
    ui->openglWidget->update();
}

void MainWindow::on_bloompushButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->toggleBLOOM());
    ui->bloompushButton->setText(t);
}

void MainWindow::on_screenshotButton_clicked()
{
    ui->openglWidget->takeScreenshot();
}

void MainWindow::on_minCSpinBox_valueChanged(double arg1)
{
    getDefaultRenderer()->changeMinContrastFXAA(arg1);
    ui->openglWidget->update();
}

void MainWindow::on_minTSpinBox_valueChanged(double arg1)
{
    getDefaultRenderer()->changeMinThresholdFXAA(arg1);
    ui->openglWidget->update();
}

DefaultRenderer * MainWindow::getDefaultRenderer() {
    return (DefaultRenderer*) ui->openglWidget->getScene()->defaultRenderer;
}
void MainWindow::on_rendererComboBox_activated(const QString &arg1)
{
    ui->openglWidget->getScene()->changeRenderer(arg1.toStdString());
    ui->openglWidget->update();
}

void MainWindow::addRenderer(Renderer *renderer) {
    rendererComboBox->addItem(QString::fromStdString(renderer->getID()));
    ui->openglWidget->getScene()->addRenderer(renderer);
}

void MainWindow::on_actionReload_shaders_triggered()
{
    ui->openglWidget->getScene()->drawData.shaderManager.reloadShaders();
}


void MainWindow::on_cameraInfoButton_clicked()
{
    ui->openglWidget->getScene()->drawData.camera->displayInfo();
}
