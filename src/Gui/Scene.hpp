#ifndef SCENE_H
#define SCENE_H

/**
 * \file Scene.hpp
 * \brief A scene represent opengl Widget's core
 */


#include <vector>

#include <Engine/Camera/Camera.hpp>
#include <Engine/Camera/EulerCamera.hpp>
#include <Engine/Renderer/Renderer.hpp>
#include <src/Engine/Renderer/ComputeEachDraw.hpp>
#include <src/Engine/Renderer/DefaultRenderer.hpp>
#include "ShortcutInterface.hpp"

/** Simple classe for managing an OpenGL scene
 */
class Scene {

public:

    explicit Scene(FileLoaderManager *flm);
    virtual ~Scene();

    virtual void cleanScene();

    virtual void resize(int width, int height);
    void draw();

    /* * User event * */
    virtual void mouseclick(ButtonMouse button, float xpos, float ypos);
    virtual void mousemove(float xpos, float ypos);
    virtual void mouserelease(ButtonMouse button);
    virtual void keyboardmove(int key, double time);
    virtual bool keyboard(unsigned char k);
    virtual void wheelEvent(float offset);

    /* * Manage shortcuts * */
    std::map<std::string,ShortcutInterface*> shortCuts;
    ShortcutInterface * shortcut;
    void changeShortcut(std::string id);

    /* * Manage renderer * */
    Renderer *renderer;
    DefaultRenderer *defaultRenderer;
    std::map<std::string, Renderer*> mapRenderer;
    void addRenderer(Renderer * renderer);
    void changeRenderer(std::string idRenderer);
    DrawData drawData;
    void displayInfo();
    /* ******************* */


    std::vector<ComputeEachDraw*> computeEachDraw;
    FileLoaderManager *fileLoaderManager;

    void fitScene();
    void fitObj();
    void fitElt();

protected:
    // Width and heigth of the viewport
    int _width;
    int _height;


    float lastX[3],lastY[3];
    bool firstMouse[3];

private:

    Object *obj = nullptr;
    Element *elementClicked = nullptr;
};


#endif // SCENE_H
