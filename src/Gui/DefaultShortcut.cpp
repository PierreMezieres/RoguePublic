//
// Created by pierre on 28/08/18.
//

#include <src/Core/Log/Log.hpp>
#include "DefaultShortcut.hpp"

DefaultShortcut::DefaultShortcut(DrawData *_drawData) {
    id_shortcut = "Default shortcut";
    drawData=_drawData;
}

bool DefaultShortcut::keyboard(unsigned char k) {
    switch(k) {
        case '4': if(obj) obj->translate(0.1,0,0); return true;
        case '7': if(obj) obj->translate(-0.1,0,0); return true;
        case '5': if(obj) obj->translate(0,0,0.1); return true;
        case '8': if(obj) obj->translate(0,0,-0.1);return true;
        case '9': if(obj) obj->translate(0,0.1,0); return true;
        case '6': if(obj) obj->translate(0,-0.1,0); return true;
        case '+': if(obj) obj->scale(1.1); return true;
        case '-': if(obj) obj->scale(0.9); return true;
        default:
            return false;
    }
}

void DefaultShortcut::mouseclick(ButtonMouse button, float xpos, float ypos) {
    lastX[button] = xpos;
    lastY[button] = ypos;
    if(button==LEFT_BUTTON){
        obj=drawData->rayCast(xpos,ypos);
    }
}

void DefaultShortcut::mouserelease(ButtonMouse){
}

void DefaultShortcut::mousemove(ButtonMouse b, float xpos, float ypos) {

    float xOffset = xpos - lastX[b];
    //float yOffset = lastY[b] - ypos;
    lastX[b] = xpos;
    lastY[b] = ypos;
    switch(b){
        case RIGHT_BUTTON: break;
        case LEFT_BUTTON:
            if(obj) obj->rotate(xOffset/6,0,1,0);
            break;
        case MIDDLE_BUTTON:break;
        case NO_BUTTON: break;
    }
}
