#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 * \file MainWindow.hpp
 * \brief MainWindow's application
 */

#include <QMainWindow>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QComboBox>
#include <src/Engine/Renderer/DefaultRenderer.hpp>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;

    QTabWidget *tabWidget;
    QComboBox *shortcutComboBox;
    QComboBox *rendererComboBox;

    void addRenderer(Renderer *renderer);

private slots:
    void on_cameraInfoButton_clicked();

    void on_actionReload_shaders_triggered();

    void on_action_Version_OpenGL_triggered();

    void on_actionCommande_triggered();

    void on_actionOptions_triggered();

    void on_updateButton_clicked();

    void on_fitButton_clicked();

    void on_clearButton_clicked();

    void on_loadButton_clicked();

    void on_drawmodeButton_clicked();

    void on_openglInfoButton_clicked();

    void on_drawdataInfoButton_clicked();

    void on_rendererInfoButton_clicked();

    void on_postProcessButton_clicked();

    void on_skyboxButton_clicked();

    void on_ssaoButton_clicked();

    void on_radiusSsaoSpinBox_valueChanged(double arg1);

    void on_biasSpinBox_valueChanged(double arg1);

    void on_shortcutComboBox_activated(const QString &arg1);

    void on_cameraComboBox_activated(int index);

    void on_frameBufferComboBox_activated(int index);

    void on_pushButton_2_clicked();

    void on_fitObjectButton_clicked();

    void on_fxaaButton_clicked();

    void on_debugFxaaButton_clicked();

    void on_doubleSpinBox_valueChanged(double arg1);

    void on_exposureSpinBox_2_valueChanged(double arg1);

    void on_pushButton_clicked();

    void on_bloompushButton_clicked();

    void on_amountBloomspinBox_valueChanged(int arg1);

    void on_screenshotButton_clicked();

    void on_minCSpinBox_valueChanged(double arg1);

    void on_minTSpinBox_valueChanged(double arg1);

    void on_rendererComboBox_activated(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QMessageBox messageBox;

    DefaultRenderer * getDefaultRenderer();

};

#endif // MAINWINDOW_H
