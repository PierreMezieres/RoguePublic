//
// Created by pierre on 28/08/18.
//

#ifndef ROGUE_DEFAULTSHORTCUT_HPP
#define ROGUE_DEFAULTSHORTCUT_HPP

/**
 * \file DefaultShortcut.hpp
 * \brief Shortcut use by default
 */


#include <src/Engine/Renderer/Renderer.hpp>
#include <src/Engine/Renderer/DefaultRenderer.hpp>
#include "ShortcutInterface.hpp"

class DefaultShortcut : public ShortcutInterface {
public:
    DefaultShortcut(DrawData *drawData);

    bool keyboard(unsigned char k) override;


    void mouseclick(ButtonMouse button, float xpos, float ypos) override;

    void mousemove(ButtonMouse button, float xpos, float ypos) override;

    void mouserelease(ButtonMouse button);

private:
    float lastX[3],lastY[3];
    Object *obj = nullptr;
    DrawData *drawData;

};


#endif //ROGUE_DEFAULTSHORTCUT_HPP
