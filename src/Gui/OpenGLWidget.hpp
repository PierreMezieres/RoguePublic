#ifndef OPENGLWIDGET_HPP
#define OPENGLWIDGET_HPP

/**
 * \file OpenGLWidget.hpp
 * \brief OpenGL Widget
 */

#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_1_Core>
#include <QKeyEvent>

#include <memory>
#include <functional>
#include <QtWidgets/QMenu>
#include <src/Engine/Renderer/ComputeEachDraw.hpp>
#include <QtWidgets/QLabel>
#include "Scene.hpp"


class OpenGLWidget : public QOpenGLWidget, public QOpenGLFunctions_4_1_Core {

public:
    explicit OpenGLWidget(QWidget *parent = 0);

    ~OpenGLWidget();

    // OpenGL management
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;

    // Event management
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;

    void changeCamera(int i);
    void setCameraPoint(int i);
    void resetCameraPoints();
    bool updateToggle();
    void fitScene();
    void fitObject();
    void fitElement();
    void clearScene();
    void changeShortcut(std::string id);
    void loadFile();
    void loadFile(std::string filename);
    void defaultShader(int i);
    void changeDrawMode();
    std::string changePostProcess();
    std::string changeSkybox();
    std::string toggleSSAO();
    std::string toggleFXAA();
    std::string toggleFXAADebug();
    std::string toggleHDR();
    std::string toggleBLOOM();
    void takeScreenshot();

    Scene* getScene();
    std::int64_t getLastTime();
    QLabel *labelMS, *labelFPS;

    void displayInfo();

private :
    std::unique_ptr<Scene> _scene;
    bool updateEverytime = false;

    FileLoaderManager *fileLoaderManager;

    // for event management
    std::int64_t _lastime;
};

#endif // OpenGLWidget_H
