#include "Scene.hpp"
#include <iostream>

#include "src/Engine/Object/Mesh.hpp"
#include "Engine/Object/Object.hpp"
#include "Core/Shapes/Sphere.hpp"
#include "DefaultShortcut.hpp"
#include <Engine/Shader/Shader.hpp>
#include <Core/Log/Log.hpp>
#include <Core/Math/Ray.hpp>
#include <src/Core/Util/FileWriter.hpp>

Scene::Scene(FileLoaderManager *flm) : fileLoaderManager{flm} {
    glEnable(GL_DEPTH_TEST);
    //glViewport(0, 0, width, height);
    //renderer.drawData.cameraManager.changeSize(width,height);

    for(int i = 0; i!=3; i++) firstMouse[i] = true;

    defaultRenderer = new DefaultRenderer();
    renderer = defaultRenderer;

    DefaultShortcut *defaultShortcut = new DefaultShortcut(&drawData);
    shortCuts[defaultShortcut->id_shortcut]=defaultShortcut;
    shortcut=defaultShortcut;

}

Scene::~Scene() {

}

void Scene::cleanScene() {
    Log(logInfo) << "Clean Scene";
    drawData.cleanScene();
    for(auto i: computeEachDraw){
        i->cleanUp();
    }
}

void Scene::resize(int width, int height) {
   _width = width;
   _height = height;
    drawData.cameraManager.changeSize(width,height);
   defaultRenderer->resize(width, height);
   for(auto i:mapRenderer){
       i.second->resize(width, height);
   }
}

void Scene::draw() {

    for(auto i: computeEachDraw){
        i->computeEachDraw();
    }

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    renderer->draw(drawData);
}

void Scene::mouseclick(ButtonMouse button, float xpos, float ypos) {
    lastX[button] = xpos;
    lastY[button] = ypos;
    firstMouse[button] = false;
    if(button==LEFT_BUTTON){
        Ray r=drawData.camera->getRayFromScreen(xpos,ypos);
        Intersection i = drawData.objectManager.rayIntersectionFull(r);
        if(i.obj!=nullptr){
            elementClicked=i.elt;
            obj = i.obj;
            Log(logInfo) << "Ray cast: " << obj->getName();
        }else{
            Log(logInfo) << "Ray cast: NO OBJET HIT";
        }
    }
    shortcut->mouseclick(button, xpos, ypos);
}

void Scene::mouserelease(ButtonMouse button){
    firstMouse[button] = true;
}

void Scene::mousemove(float xpos, float ypos) {
    ButtonMouse button = NO_BUTTON;
    if(!firstMouse[RIGHT_BUTTON]){
        button=RIGHT_BUTTON;
    }else{
        if(!firstMouse[MIDDLE_BUTTON]){
            button=MIDDLE_BUTTON;
        }else{
            if(!firstMouse[LEFT_BUTTON]){
                button=LEFT_BUTTON;
            }
        }
    }

    float xOffset = xpos - lastX[button];
    float yOffset = lastY[button] - ypos;
    lastX[button] = xpos;
    lastY[button] = ypos;
    switch(button){
        case RIGHT_BUTTON: drawData.camera->mouseMovement(xOffset, yOffset); break;
        case LEFT_BUTTON: break;
        case MIDDLE_BUTTON: drawData.camera->mouseMovementMiddle(xOffset, yOffset);break;
        case NO_BUTTON: break;
    }
    shortcut->mousemove(button,xpos,ypos);
}

void Scene::wheelEvent(float offset){
    drawData.camera->mouseScroll(offset);
}

void Scene::keyboardmove(int key, double time) {
    drawData.camera->keyboard((Movement)key,time);
}

bool Scene::keyboard(unsigned char k) {
    return shortcut->keyboard(k);
}


void Scene::changeShortcut(std::string id){
    shortcut = shortCuts[id];
}


void Scene::addRenderer(Renderer *renderer) {
    mapRenderer[renderer->getID()] = renderer;
}

void Scene::changeRenderer(std::string idRenderer) {
    if(idRenderer == "Default Renderer"){
        renderer=defaultRenderer;
    }else{
        auto i = mapRenderer.find(idRenderer);
        if(i!=mapRenderer.end()){
            renderer = i->second;
        }else{
            Log(logError) << "Renderer : " << idRenderer << " does not exist !";
        }
    }
}

void Scene::fitScene() {
    Log(logInfo) << "Fit Scene";
    drawData.camera->fitScene(drawData.objectManager.getAABB());
}

void Scene::fitObj() {
    if(obj){
        drawData.camera->fitScene(drawData.objectManager.getAABBObject(obj));
    }
}

void Scene::fitElt() {
    if(elementClicked && obj){
        drawData.camera->fitScene(drawData.objectManager.getAABBElement(elementClicked,obj->getModel()));
    }
}

void Scene::displayInfo(){
    Log(logInfo) << "|------------- Renderer info ------------";
    drawData.objectManager.displayInfo2();
    drawData.lightManager.displayInfo2();
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";
}
