#include "OpenGLWidget.hpp"
#include "MainWindow.hpp"

#include <Core/Log/Log.hpp>

#include <QMessageBox>
#include <QApplication>
#include <QDateTime>

#include <iostream>
#include <stdexcept>
#include <QtWidgets/QFileDialog>
#include <src/Plugin/LoaderPlugin.hpp>
#include <src/Core/Util/FileWriter.hpp>
#include <src/Core/Util/FileUtil.hpp>

namespace Ui {
    class MainWindow;
}

OpenGLWidget::OpenGLWidget(QWidget *parent) :QOpenGLWidget(parent), QOpenGLFunctions_4_1_Core(), _scene(nullptr), _lastime(0) {
    // add all demo constructors here

}

OpenGLWidget::~OpenGLWidget() {

}

void OpenGLWidget::initializeGL() {
    if (!initializeOpenGLFunctions()) {
        QMessageBox::critical(this, "OpenGL initialization error", "OpenGLWidget::initializeGL() : Unable to initialize OpenGL functions");
        exit(1);
    }

    // Initialize OpenGL and all OpenGL dependent stuff below
    _scene.reset(new Scene(fileLoaderManager));

    fileLoaderManager = new FileLoaderManager();

    LoaderPlugin loaderPlugin({fileLoaderManager,this,_scene.get(),((MainWindow*)parentWidget())->tabWidget,((MainWindow*)parentWidget())->shortcutComboBox,(MainWindow*) parentWidget()});

    _scene->drawData.fileLoaderManager=fileLoaderManager;
    fileLoaderManager->loadFile(&_scene->drawData, "../Assets/scenes.rogue");
    fitScene();

    displayInfo();
    _scene->drawData.displayInfo();
}

void OpenGLWidget::paintGL() {
    std::int64_t starttime = QDateTime::currentMSecsSinceEpoch();
    _scene->draw();
    glFinish();
    std::int64_t endtime = QDateTime::currentMSecsSinceEpoch();
    _lastime = endtime-starttime;
    if(labelFPS!=nullptr && labelMS!=nullptr){
        labelMS->setText(QString::fromStdString("Time (ms): " + std::to_string(_lastime)));
        if(_lastime!=0) labelFPS->setText(QString::fromStdString("fps ≅ " + std::to_string(1000/_lastime)));
    }
    if(updateEverytime)update();
}

void OpenGLWidget::resizeGL(int width, int height) {
    _scene->resize(width, height);
}

void OpenGLWidget::mousePressEvent(QMouseEvent *event) {
    // buttons are 0(left), 1(right) to 2(middle)
    int b;
    Qt::MouseButton button=event->button();
    if (button & Qt::LeftButton) {
        if ((event->modifiers() & Qt::ControlModifier))
            b = 2;
        else
            b = 0;
    } else if (button & Qt::RightButton)
        b = 1;
    else if (button & Qt::MiddleButton)
        b = 2;
    else
        b=3;
    _scene->mouseclick((ButtonMouse)b, event->x(), event->y());
    _lastime = QDateTime::currentMSecsSinceEpoch();
    this->setFocus();
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent *event){
    int b;
    Qt::MouseButton button=event->button();
    if (button & Qt::LeftButton) {
        if ((event->modifiers() & Qt::ControlModifier))
            b = 2;
        else
            b = 0;
    } else if (button & Qt::RightButton)
        b = 1;
    else if (button & Qt::MiddleButton)
        b = 2;
    else
        b=3;
    _scene->mouserelease((ButtonMouse)b);
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent *event) {
    _scene->mousemove(event->x(), event->y());
    update();
}

void OpenGLWidget::wheelEvent(QWheelEvent *event){
    float delta = event->delta();

    if(delta > 0) delta = 1;
    else delta = -1;
    _scene->wheelEvent(delta);
    update();
}

void OpenGLWidget::keyPressEvent(QKeyEvent *event) {
    bool used = true;
    QImage i;
    switch(event->key()) {
        // Move keys
        case Qt::Key_Left:
        case Qt::Key_Up:
        case Qt::Key_Right:
        case Qt::Key_Down:
            _scene->keyboardmove(event->key()-Qt::Key_Left, 1./100/*double(_lastime)/10.*/);
            update();
        break;
        // Wireframe key
        case Qt::Key_W:
            _scene->renderer->toggleDrawMode();
        break;
        case Qt::Key_X:
            break;
        case Qt::Key_U:
            updateToggle();
        break;
        case Qt::Key_F:
            fitScene();
            break;
        case Qt::Key_C:
            clearScene();
            break;
        case Qt::Key_L:
            loadFile();
            break;
        case Qt::Key_1:
            displayInfo();
            break;
        case Qt::Key_2:
            _scene->drawData.displayInfo();
            break;
        case Qt::Key_3:
            _scene->displayInfo();
            break;
        default :
            used = false;
        break;
    }
    if(_scene->keyboard(event->text().toStdString()[0]) && used)
        Log(logWarning) << "Watchout, shortcut '" << event->text().toStdString()[0] << "' is use in openGlWidget ...";
    update();
}

void OpenGLWidget::changeCamera(int i){
    _scene->drawData.changeCamera(i);
    update();
}

void OpenGLWidget::setCameraPoint(int i){
  if( i >= 0 )
    _scene->drawData.cameraManager.setPoint(i);
  update();
}

void OpenGLWidget::resetCameraPoints(){
  _scene->drawData.cameraManager.resetPoints();
}

bool OpenGLWidget::updateToggle(){
    updateEverytime = ! updateEverytime;
    if(updateEverytime){
        Log(logInfo) << "Update everytime";
    }else{
        Log(logInfo) << "No Update everytime";
    }
    update();
    return updateEverytime;
}

void OpenGLWidget::fitScene(){
    _scene->fitScene();
    update();
}

void OpenGLWidget::fitObject(){
    _scene->fitObj();
    update();
}

void OpenGLWidget::fitElement(){
    _scene->fitElt();
    update();
}

void OpenGLWidget::clearScene(){
    _scene->cleanScene();
    update();
}

void OpenGLWidget::changeShortcut(std::string id){
    _scene->changeShortcut(id);
}

void OpenGLWidget::loadFile(){
    QString filename;
    bool save;
    save = updateEverytime;
    updateEverytime=false;
    filename = QFileDialog::getOpenFileName(parentWidget(), QObject::tr("File loader"), "../Assets", QObject::tr("File loader (*)"));
    if(filename != "") {
        fileLoaderManager->loadFile(&_scene->drawData, filename.toStdString());
        _scene->fitScene();
    }
    updateEverytime = save;
    update();
}

void OpenGLWidget::loadFile(std::string filename) {
    fileLoaderManager->loadFile(&_scene->drawData, filename);
    _scene->fitScene();
    update();
}

void OpenGLWidget::defaultShader(int i){
    _scene->defaultRenderer->changeBufferFinal(i);
    update();
}

void OpenGLWidget::changeDrawMode(){
    _scene->renderer->toggleDrawMode();
    update();
}

Scene* OpenGLWidget::getScene(){
    return _scene.get();
}

std::int64_t OpenGLWidget::getLastTime() {
    return _lastime;
}

std::string OpenGLWidget::changePostProcess() {
    std::string t =_scene->defaultRenderer->changeShaderPost();
    update();
    return t;
}

std::string OpenGLWidget::changeSkybox() {
    std::string t =_scene->defaultRenderer->changeSkybox();
    update();
    return t;
}

std::string OpenGLWidget::toggleSSAO() {
    std::string t=_scene->defaultRenderer->toggleSSAO();
    update();
    return t;
}

std::string OpenGLWidget::toggleFXAA() {
    bool tog = _scene->defaultRenderer->toggleFXAA();
    update();
    if(tog) return "FXAA ON";
    else return "FXAA OFF";
}

std::string OpenGLWidget::toggleFXAADebug() {
    bool tog = _scene->defaultRenderer->toggleFXAADebug();
    update();
    if(tog) return "Debug FXAA ON";
    else return "Debug FXAA OFF";
}

std::string OpenGLWidget::toggleHDR() {
    bool tog = _scene->defaultRenderer->toggleHDR();
    update();
    if(tog) return "HDR ON";
    else return "HDR OFF";
}

std::string OpenGLWidget::toggleBLOOM() {
    bool tog = _scene->defaultRenderer->toggleBLOOM();
    update();
    if(tog) return "BLOOM ON";
    else return "BLOOM OFF";
}

void OpenGLWidget::takeScreenshot() {
    int cpt = 1;
    auto i = grabFramebuffer();
    "screenshot" + std::to_string(cpt) + ".png";
    while(FileUtil::fileExist(QString::fromStdString("screenshot" + std::to_string(cpt) + ".png"))) cpt++;
    std::string str ="screenshot" + std::to_string(cpt) + ".png";
    i.save(str.data());
}

void OpenGLWidget::displayInfo(){
    Log(logInfo) << "";
    Log(logInfo) << "|---------- Info OpenGL Widget ----------";
    Log(logInfo) << "| - Actual shortcut: " << _scene->shortcut->id_shortcut;
    Log(logInfo) << "| - Shortcut set number: "<< _scene->shortCuts.size();
    Log(logInfo) << "| - Update every time: " << (updateEverytime?"true":"false");
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";

}
