#include "Gui/MainWindow.hpp"

#include <QApplication>
#include <Core/Log/Log.hpp>

/** \mainpage Welcome on Rogue's documentation
 *
 * <i>last update : 16/11/2018</i>
 *
 * Rogue stands for <b>R</b>ender <b>O</b>bject <b>G</b>raphic <b>U</b>seless <b>E</b>ngine.
 *
 * ______
 *
 * It's a project of a simple 3D Render Engine.
 *
 * It's mainly inspired and designed from 2 other projects:
 *
 * [Learn Opengl](https://learnopengl.com): a very good tutorial to learn how to use the OpenGL API.
 *
 * [Radium-Engine](https://github.com/STORM-IRIT/Radium-Engine): a 3D Render Engine from the STORM team from [IRIT](https://www.irit.fr/).
 *
 * _______
 *
 * <b>Note</b>: Only the items judged to be the most important are documented ...
 *
 */

int main(int argc, char *argv[]) {

    Log(logInfo) << " <<< Rogue main launch >>>";

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
