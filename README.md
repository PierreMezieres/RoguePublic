# Rogue

*last update : 21/04/2019*

Rogue stands for **R**ender **O**bject **G**raphic **U**seless **E**ngine.

It's a project of a simple 3D Render Engine.

It's mainly inspired and designed from 2 other projects:

* [Learn Opengl](https://learnopengl.com/): a very good tutorial to learn how to use the OpenGL API.
* [Radium-Engine](https://github.com/STORM-IRIT/Radium-Engine): a 3D Render Engine from the [STORM](https://www.irit.fr/STORM/site/) team from [IRIT](https://www.irit.fr/).

## Table of contents 

* [Supported compiler and platforms](#supported-compiler-and-platforms)
* [Build instructions](#build-instructions)
* [Developers](#developers)
* [Assets](#assets)
* [RogueLoader](#rogue-loader)
* [Docs](#docs)
* [Plugin](#plugin)
* [Masterpiece Master 2](#masterpiece-master-2)
* [Extra](#extra)

## Supported compiler and platforms

The following platforms and tool chains have been tested and should work :

* *Linux* : gcc 5 or higher
* *Mac OSX* : gcc 5 or higher, Apple clang

## Developers

**:bangbang: For developers, read absolutely this before commiting/pushing :bangbang:**

* Before doing a *git push* do not forget to do a *git pull*
* Don't push on *master* branch
* Check the branch you are on before pushing
* Don't push Assets

**Please follow these rules ! (Accidents arrive quickly ...)** :skull:

## Build instructions

After cloning the project:

### Getting submodules
Do not forget to run these two commands to be sure to have submodules
```bash
$ git submodule init
$ git submodule update
```

### Configure build

It's recommanded to follow the usual sequence:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
```

### Run the project

You'll find the *executable* in bin folder. :sunglasses:

```bash
$ cd bin
$ ./rogue
```

## Assets

If you want to download some assets, [you can find a few here](https://gitlab.com/PierreMezieres/RogueAssets).

For the moment, it's recommanded to download it ! Over time, small changes may lead to small file path management issues. Feel free to report any problems you may have with the Assets ! :smiley:

To use Assets put the directory *Assets* directly in the directory where you have *src/Shaders/...*.

## Rogue Loader

The engine propose its own file system. If you want more information to use it, you can refer [here](Docs/rogueLoader.md).

## Docs

This project use doxyfile, at compilation time some documentation is automaticaly produced. You can find aditionnal documentation [here](Docs).

## Plugin

The concept of plugin has begun to be implemented.

For the moment, plugin have to be with the source (src/Plugin directory).
 
A small tutorial will be added later, when adding plugin will be more advanced.

For the moment, follow the plugin interface (src/Plugin/PluginInterface) and don't to forget to add the plugin in the plugin vector (src/Plugin/LoaderPlugin) if you want it to load !

## Masterpiece Master 2

This version of the engine also has the final version of the project of Master 2 Computer Graphics and Image Analysis (Paul Sabatier University 2018-2019).

This project is named "Moment based rendering". As the name suggests, the main idea is to implement moment-based rendering techniques. Specifically, transparency calculation or shadow calculation techniques.

Some screnshots of final result are available [here](https://argodark.artstation.com/)

### References

The work for moment is based on 2 articles

- [Moment-Based Order-Independent Transparency](http://cg.cs.uni-bonn.de/en/publications/paper-details/muenstermann2018-mboit/)
- [Moment Shadow Mapping](http://cg.cs.uni-bonn.de/en/publications/paper-details/peters-2015-msm/)

But to be able to compare these techniques, we have implemented other rendering techniques.

- [Interactive Order-Independent Transparency](https://pubweb.eng.utah.edu/~cs5610/handouts/order_independent_transparency.pdf)
- [Weighted Blended Order-Independent Transparency](http://jcgt.org/published/0002/02/09/paper.pdf)
- [Rendering Antialiased Shadows with Depth Maps](https://graphics.pixar.com/library/ShadowMaps/paper.pdf)
- [Variance Shadow Maps](http://www.punkuser.net/vsm/)


### Students

Students who mainly worked on shadow mapping

- Alban Odot
- Jean-Baptiste Sarazin

Students who mainly worken on order independant transparency

- Baptiste Delos
- Pierre Mézières


## Extra

Enjoy ! :smile:

Do not hesitate to report some bugs or any indication you find useful !
