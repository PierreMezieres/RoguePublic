
vec4 getKdA()
{
    if(material.hasKdTexture){
        return texture(material.kdTexture, texCoord);
    }
    return material.kd;
}

vec4 getKsA()
{
    if(material.hasKsTexture){
         return vec4(texture(material.ksTexture, texCoord));
     }
    return vec4(material.ks,1);
}

float getNsA()
{
    if(material.hasNsTexture){
         return vec3(texture(material.nsTexture, texCoord)).r;
     }
    return material.ns;
}

float getAmbientA()
{
    return material.ambient;
}