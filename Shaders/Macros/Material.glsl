vec4 getKd()
{
    if(material.hasKdTexture){
        return texture(material.kdTexture, texCoord);
    }
    return material.kd;
}

vec3 getKs()
{
    if(material.hasKsTexture){
         return vec3(texture(material.ksTexture, texCoord));
     }
    return material.ks;
}

float getNs()
{
    if(material.hasNsTexture){
         return vec3(texture(material.nsTexture, texCoord)).r;
     }
    return material.ns;
}

float getAmbient()
{
    return material.ambient;
}