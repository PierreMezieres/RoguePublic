#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

uniform bool horizontal;
uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

void main()
{
    vec2 texelStep = 1.0 / textureSize(screenTexture, 0);
    vec3 col = texture(screenTexture, texCoord).rgb * weight[0];
    if(horizontal)
    {
        for(int i=1; i < 5; ++i)
        {
            col += texture(screenTexture, texCoord + vec2(texelStep.x * i, 0.0)).rgb * weight[i];
            col += texture(screenTexture, texCoord - vec2(texelStep.x * i, 0.0)).rgb * weight[i];
        }
    }
    else
    {
        for(int i=1; i < 5; ++i)
        {
            col += texture(screenTexture, texCoord + vec2(texelStep.y * i, 0.0)).rgb * weight[i];
            col += texture(screenTexture, texCoord - vec2(texelStep.y * i, 0.0)).rgb * weight[i];
        }
    }
    color = vec4(col, 1.0);
}