#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

void main()
{
    vec3 col = texture(screenTexture, texCoord).rgb;
    vec3 mapped = vec3(1.0) - exp(-col);
    color = vec4(pow(mapped,vec3(1.0)),1);
}
