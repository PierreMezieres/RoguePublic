
vec3 gammaFunction(vec3 color){
    vec3 mapped = vec3(1.0) - exp(-color);
    return pow(mapped,vec3(1));
}