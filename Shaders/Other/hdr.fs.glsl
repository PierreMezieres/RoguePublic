#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;
uniform sampler2D brightTexture;
uniform float gamma;
uniform float exposure;
uniform bool togHDR;
uniform bool togBLOOM;

void main()
{
    vec3 hdrColor = texture(screenTexture, texCoord).rgb;
    if(!togHDR){
        color = vec4(hdrColor,1.0);
        return;
    }

    vec3 bri = texture(brightTexture, texCoord).rgb;
    if(togBLOOM) hdrColor += bri;
    vec3 mapped =vec3(1.0) - exp(-hdrColor * exposure);

    mapped = pow(mapped,vec3(1.0/gamma));
    color = vec4(mapped, 1.0);
}