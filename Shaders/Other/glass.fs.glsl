#version 410 core
out vec4 color;

in vec3 normal;
in vec3 fragPos;

uniform vec3 viewPos;
uniform samplerCube skybox;

void main()
{
    float ratio = 1.00 / 1.52;
    vec3 I = normalize(fragPos - viewPos);
    vec3 R = refract(I, normalize(normal), ratio);
    color = vec4(texture(skybox, R).rgb, 1.0);
}