#version 410 core

#include "../Macros/Data.glsl"
in vec3 normal;

uniform Material material;
in vec2 texCoord;

out vec4 color;

#include "../Macros/MaterialAlpha.glsl"

void main()
{
    color = getKdA();
}
