#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

uniform bool fxaaON;
uniform bool debug;

uniform float lumaContrastMin;
uniform float lumaThreshold;

const float subpix_trim =2;
const float subpix_trim_scale=0;
const float subpix_cap=3./4.;

const float search_steps = 10;
const float search_acceleration = 1;
const float search_threshold = 1./4.;

uniform vec2 texelStep;

float toLuma(vec3 rgb){
    return rgb.r * 0.299 + rgb.g * 0.587 + rgb.b * 0.114;
}

void main()
{
    vec3 col = textureOffset(screenTexture, texCoord, ivec2(0,0)).rgb;

    if(!fxaaON)
    {
        color = vec4(col, 1.0);
        return;
    }

    /* ******************************************************** */
    /* ***************** LOCAL CONTRAST CHECK ***************** */
    /* ******************************************************** */
    float lumaM = toLuma(col);
    float lumaN = toLuma(textureOffset(screenTexture, texCoord, ivec2(0,-1)).rgb);
    float lumaS = toLuma(textureOffset(screenTexture, texCoord, ivec2(0,1)).rgb);
    float lumaE = toLuma(textureOffset(screenTexture, texCoord, ivec2(1,0)).rgb);
    float lumaW = toLuma(textureOffset(screenTexture, texCoord, ivec2(-1,0)).rgb);

    float lumaMin = min(lumaM, min(min(lumaN, lumaW), min(lumaS, lumaE)));
    float lumaMax = max(lumaM, max(max(lumaN, lumaW), max(lumaS, lumaE)));
    float lumaRange = lumaMax - lumaMin;

    // No AA needed
    if(lumaRange <= max(lumaThreshold, lumaRange * lumaContrastMin))
    {
        color = vec4(col, 1.0);
        return;
    }


    /* ******************************************************** */
    /* ********* GRADIENT AND CHOOSING EDGE DIRECTION ********* */
    /* ******************************************************** */
    float lumaNW = toLuma(textureOffset(screenTexture, texCoord, ivec2(-1,-1)).rgb);
    float lumaNE = toLuma(textureOffset(screenTexture, texCoord, ivec2(1,-1)).rgb);
    float lumaSW = toLuma(textureOffset(screenTexture, texCoord, ivec2(-1,1)).rgb);
    float lumaSE = toLuma(textureOffset(screenTexture, texCoord, ivec2(1,1)).rgb);

    float lumaNS = lumaS + lumaN;
    float lumaEW = lumaE + lumaW;

    float lumaWC = lumaSW + lumaNW;
    float lumaSC = lumaSW + lumaSE;
    float lumaEC = lumaSE + lumaNE;
    float lumaNC = lumaNE + lumaNW;

    // Gradient estimation
    float edgeHorizontal = abs(-2.0 * lumaW + lumaWC) + abs(-2.0 * lumaM + lumaNS)*2.0 + abs(-2.0 * lumaE + lumaEC);
    float edgeVertical =   abs(-2.0 * lumaN + lumaNC) + abs(-2.0 * lumaM + lumaEW)*2.0 + abs(-2.0 * lumaS + lumaSC);

    bool isHorizontal = (edgeHorizontal >= edgeVertical);


    /* ******************************************************** */
    /* *************** CHOOSING EDGE ORIENTATION ************** */
    /* ******************************************************** */
    float luma1 = isHorizontal ? lumaS : lumaW;
    float luma2 = isHorizontal ? lumaN : lumaE;

    float grad1 = luma1 - lumaM;
    float grad2 = luma2 - lumaM;

    bool is1Steepest = abs(grad1) >= abs(grad2);

    float gradScaled = 0.25*max(abs(grad1),abs(grad2));

    float stepLength = isHorizontal ? texelStep.y : texelStep.x;

    float lumaLocalAverage = 0.0;
    if(is1Steepest){
        stepLength = -stepLength;
        lumaLocalAverage = 0.5*(luma1 +lumaM);
    }else{
        lumaLocalAverage = 0.5*(luma2 + lumaM);
    }

    vec2 currentUv = texCoord;
    if(isHorizontal){
        currentUv.y +=stepLength * 0.5;
    }else{
        currentUv.x +=stepLength *0.5;
    }


    /* ******************************************************** */
    /* ***************** ITERATION EXPLORATION **************** */
    /* ******************************************************** */
    vec2 offset = isHorizontal ? vec2(texelStep.x,0.0) : vec2(0.0,texelStep.y);
    vec2 pos1 = currentUv;
    vec2 pos2 = currentUv;
    float lumaEnd1, lumaEnd2;
    bool done1 = false, done2 = false;

    for(int i = 0; i < search_steps; i++){
        if(!done1) lumaEnd1 = toLuma(texture(screenTexture, pos1).rgb) - lumaLocalAverage;
        if(!done2) lumaEnd2 = toLuma(texture(screenTexture, pos2).rgb) - lumaLocalAverage;
        done1 = abs(lumaEnd1) >= gradScaled; // Do we have reached the side of the edge
        done2 = abs(lumaEnd2) >= gradScaled;
        if(!done1) pos1 -= offset;
        if(!done2) pos2 += offset;
        if(done1 && done2){ break;}
    }


    /* ******************************************************** */
    /* ******************* ESTIMATING OFFSET ****************** */
    /* ******************************************************** */
    float distance1 = isHorizontal ? (texCoord.x - pos1.x) : (texCoord.y - pos1.y);
    float distance2 = isHorizontal ? (pos2.x - texCoord.x) : (pos2.y - texCoord.y);
    bool isDirection1 = distance1 < distance2;
    float distanceFinal = min(distance1, distance2);
    float edgeThickness = (distance1 + distance2);
    float pixelOffset = - distanceFinal / edgeThickness + 0.5;
    bool isLumaCenterSmaller = lumaM < lumaLocalAverage;
    // If the luma at center is smaller than at its neighbour, the delta luma at each end should be positive (same variation).
    // (in the direction of the closer side of the edge.)
    bool correctVariation = ((isDirection1 ? lumaEnd1 : lumaEnd2) < 0.0) != isLumaCenterSmaller;
    float finalOffset = correctVariation ? pixelOffset : 0.0;

    /* ******************************************************** */
    /* ***************** SUBPIXEL ANTIALIASING **************** */
    /* ******************************************************** */
    float lumaAverage = (1.0/12.0) * (2.0 * (lumaNS + lumaEW) + lumaWC + lumaEC);
    float subPixelOffset1 = clamp(abs(lumaAverage - lumaM)/lumaRange,0.0,1.0);
    float subPixelOffset2 = (-2.0 * subPixelOffset1 + 3.0) * subPixelOffset1 * subPixelOffset1;
    float subPixelOffsetFinal = subPixelOffset2 * subPixelOffset2 * 0.75;

    finalOffset = max(finalOffset,subPixelOffsetFinal);

    /* ******************************************************** */
    /* ********************* FINAL READ *********************** */
    /* ******************************************************** */
    vec2 finalUv = texCoord;
    if(isHorizontal){
        finalUv.y += finalOffset * stepLength;
    } else {
        finalUv.x += finalOffset * stepLength;
    }
    color = vec4(texture(screenTexture,finalUv).rgb, 1);

    if(debug)
    {
            color.r = 1.0;
    }
}