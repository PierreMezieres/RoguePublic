#version 410 core

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

#include "../Macros/Data.glsl"

uniform Material material;

out vec4 color;


#include "../Macros/Material.glsl"

void main()
{
    //color = vec4(vec3(clamp(dot(normalize(normal), vec3(0,0,1)), 0, 1)), 1.0);
    color = vec4(getKd().xyz,0.5);
}
