#version 410 core
out vec4 color;

in vec3 normal;
in vec3 fragPos;

uniform vec3 viewPos;
uniform samplerCube skybox;

void main()
{
    vec3 I = normalize(fragPos - viewPos);
    vec3 R = reflect(I, normalize(normal));
    color = vec4(texture(skybox, R).rgb, 1.0);
}