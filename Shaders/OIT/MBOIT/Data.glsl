
struct MomentOIT
{
	vec4 wrapping_zone_parameters;
	float overestimation;
	float moment_bias;
};