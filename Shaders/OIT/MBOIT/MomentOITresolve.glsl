
#include "MomentMath.glsl"

/*! This function is to be called from the shader that composites the
	transparent fragments. It reads the moments and calls the appropriate
	function to reconstruct the transmittance at the specified depth.*/
void resolveMoments4(out float transmittance_at_depth, out float total_transmittance, float depth, vec4 b, float b0)
{

	transmittance_at_depth = 1;
	total_transmittance = 1;

	float b_0 = b0;
	if(b_0 < 0.00100050033f) discard;
	total_transmittance = exp(-b_0);

	vec4 b_1234 = b;
	vec2 b_even = b_1234.yw;
	vec2 b_odd = b_1234.xz;

	b_even /= b_0;
	b_odd /= b_0;

	const vec4 bias_vector = vec4(0, 0.375, 0, 0.375);
	transmittance_at_depth = computeTransmittanceAtDepthFrom4PowerMoments(b_0, b_even, b_odd, depth, momentOIT.moment_bias, momentOIT.overestimation, bias_vector);


}

void resolveMoments6(out float transmittance_at_depth, out float total_transmittance, float depth, vec4 b12, vec4 b3456, float b0)
{
	transmittance_at_depth = 1;
	total_transmittance = 1;

	float b_0 = b0;
	if(b_0 < 0.00100050033f) discard;
	total_transmittance = exp(-b_0);

	vec2 b_12 = b12.xy;
    vec2 b_34 = b3456.xy;
    vec2 b_56 = b3456.zw;
    vec3 b_even = vec3(b_12.y, b_34.y, b_56.y);
    vec3 b_odd = vec3(b_12.x, b_34.x, b_56.x);

    b_even /= b_0;
    b_odd /= b_0;

    const float bias_vector[6] = float[6]( 0, 0.48, 0, 0.451, 0, 0.45 );
    transmittance_at_depth = computeTransmittanceAtDepthFrom6PowerMoments(b_0, b_even, b_odd, depth, momentOIT.moment_bias, momentOIT.overestimation, bias_vector);

}

void resolveMoments8(out float transmittance_at_depth, out float total_transmittance, float depth, vec4 bEven, vec4 bOdd, float b0)
{
	transmittance_at_depth = 1;
	total_transmittance = 1;

	float b_0 = b0;
	if(b_0 < 0.00100050033f) discard;
	total_transmittance = exp(-b_0);

	vec4 b_even = bEven;
	vec4 b_odd = bOdd;

	b_even /= b_0;
	b_odd /= b_0;
	const float bias_vector[8] = float[8]( 0, 0.75, 0, 0.67666666666666664, 0, 0.63, 0, 0.60030303030303034 );
	transmittance_at_depth = computeTransmittanceAtDepthFrom8PowerMoments(b_0, b_even, b_odd, depth, momentOIT.moment_bias, momentOIT.overestimation, bias_vector);

}

void resolveTrigoMoments2(out float transmittance_at_depth, out float total_transmittance, float depth, vec4 b, float b0)
{
	transmittance_at_depth = 1;
	total_transmittance = 1;

	float b_0 = b0;
	if(b_0 < 0.00100050033f) discard;
	total_transmittance = exp(-b_0);

    vec4 b_tmp = b;
   	vec2 trig_b[2];
   	trig_b[0] = b_tmp.xy;
   	trig_b[1] = b_tmp.zw;
   	trig_b[0] /= b_0;
   	trig_b[1] /= b_0;
   	transmittance_at_depth = computeTransmittanceAtDepthFrom2TrigonometricMoments(b_0, trig_b, depth, momentOIT.moment_bias, momentOIT.overestimation, momentOIT.wrapping_zone_parameters);

}

void resolveTrigoMoments3(out float transmittance_at_depth, out float total_transmittance, float depth, vec4 b12, vec4 b3456, float b0)
{
	transmittance_at_depth = 1;
	total_transmittance = 1;

	float b_0 = b0;
	if(b_0 < 0.00100050033f) discard;
	total_transmittance = exp(-b_0);

    vec2 trig_b[3];
    trig_b[0] = b12.xy;
    vec4 tmp = b3456;
    trig_b[1] = tmp.xy;
    trig_b[2] = tmp.zw;
    trig_b[0] /= b_0;
    trig_b[1] /= b_0;
    trig_b[2] /= b_0;
    transmittance_at_depth = computeTransmittanceAtDepthFrom3TrigonometricMoments(b_0, trig_b, depth, momentOIT.moment_bias, momentOIT.overestimation, momentOIT.wrapping_zone_parameters);



}

void resolveTrigoMoments4(out float transmittance_at_depth, out float total_transmittance, float depth, vec4 b1, vec4 b2, float b0)
{
	transmittance_at_depth = 1;
	total_transmittance = 1;

	float b_0 = b0;
	if(b_0 < 0.00100050033f) discard;
	total_transmittance = exp(-b_0);

    vec4 b_tmp = b1;
	vec4 b_tmp2 = b2;
	vec2 trig_b[4] = vec2[4](
		b_tmp2.xy / b_0,
		b_tmp.xy / b_0,
		b_tmp2.zw / b_0,
		b_tmp.zw / b_0
	);
	transmittance_at_depth = computeTransmittanceAtDepthFrom4TrigonometricMoments(b_0, trig_b, depth, momentOIT.moment_bias, momentOIT.overestimation, momentOIT.wrapping_zone_parameters);


}