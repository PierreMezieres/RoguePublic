#version 410 core
layout (location = 0) out float depth;
layout (location = 1) out vec4 rgba;

#include "../../Macros/Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

#define MAX_LIGHTS 10

uniform int numberLights;

uniform Light lights[MAX_LIGHTS];
uniform vec3 viewPos;
uniform Material material;


#include "../../Macros/Material.glsl"
#include "../../Macros/Lighting.glsl"

void main()
{
    vec3 result = vec3(0);

    for(int i=0; i<numberLights; i++){
        result += compute_lighting(lights[i]);
    }

    rgba = vec4(result, 1.0);
    depth = gl_FragCoord.z;
}