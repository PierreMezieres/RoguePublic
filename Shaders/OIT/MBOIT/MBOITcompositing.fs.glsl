#version 410 core
out vec4 color;

#include "../../Macros/Data.glsl"
#include "Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

#define MAX_LIGHTS 10

uniform int numberLights;
uniform Light lights[MAX_LIGHTS];
uniform vec3 viewPos;
uniform bool trigonometric;
uniform int moment_number;

uniform Material material;
uniform sampler2D momentsTexture1;
uniform sampler2D momentsTexture2;
uniform sampler2D b0Texture;
uniform sampler2D depthOpaque;
uniform MomentOIT momentOIT;

#include "MomentOITresolve.glsl"
#include "../../Macros/Material.glsl"
#include "../../Macros/LightingTransparency.glsl"

void main()
{
    ivec2 fragCoord = ivec2(gl_FragCoord.xy);
    float depth = texelFetch(depthOpaque, fragCoord, 0).r;
    if(depth < gl_FragCoord.z) discard;

    float transmittance_at_depth;
    float total_transmittance;

    vec4 b1 = texelFetch(momentsTexture1, fragCoord, 0);
    vec4 b2 = texelFetch(momentsTexture2, fragCoord, 0);
    float b0 = texelFetch(b0Texture, fragCoord,0).r;

    if(trigonometric){
        switch(moment_number){
        case 2: resolveTrigoMoments2(transmittance_at_depth, total_transmittance, gl_FragCoord.z, b1, b0); break;
        case 3: resolveTrigoMoments3(transmittance_at_depth, total_transmittance, gl_FragCoord.z, b1, b2, b0); break;
        case 4: resolveTrigoMoments4(transmittance_at_depth, total_transmittance, gl_FragCoord.z, b1, b2, b0); break;
        }
    }else{
        switch(moment_number){
        case 4: resolveMoments4(transmittance_at_depth, total_transmittance, gl_FragCoord.z, b1, b0); break;
        case 6: resolveMoments6(transmittance_at_depth, total_transmittance, gl_FragCoord.z, b1, b2, b0); break;
        case 8: resolveMoments8(transmittance_at_depth, total_transmittance, gl_FragCoord.z, b1, b2, b0); break;
        }
    }

    vec4 col = getKd();
    vec3 result = vec3(0);

    for(int i=0; i<numberLights; i++){
        result += compute_lighting(lights[i]);
    }

    col = vec4(result, col.a);

    //color = vec4(col.rgb,transmittance_at_depth);

    //color = col * total_transmittance * transmittance_at_depth;
    color = col * col.a * transmittance_at_depth;
    color.a = col.a * transmittance_at_depth;
    //color.a = 0;

}