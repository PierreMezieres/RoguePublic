#include "ComplexAlgebra.glsl"

/*! This functions relies on fixed function additive blending to compute the
	vector of moments.moment vector. The shader that calls this function must
	provide the required render targets.*/
void generateMoments4(float depth, float transmittance, out float b_0, out vec4 b)
{
	float absorbance = -log(transmittance);

	b_0 = absorbance;
	float depth_pow2 = depth * depth;
	float depth_pow4 = depth_pow2 * depth_pow2;
	b = vec4(depth, depth_pow2, depth_pow2 * depth, depth_pow4) * absorbance;
}

void generateMoments6(float depth, float transmittance, out float b_0, out vec4 b_12, out vec4 b_3456)
{
	float absorbance = -log(transmittance);

	b_0 = absorbance;
	float depth_pow2 = depth * depth;
	float depth_pow4 = depth_pow2 * depth_pow2;
	b_12 = vec4(depth, depth_pow2,0,0) * absorbance;
	b_3456 = vec4(depth_pow2 * depth, depth_pow4, depth_pow4 * depth, depth_pow4 * depth_pow2) * absorbance;
}

void generateMoments8(float depth, float transmittance, out float b_0, out vec4 b_even, out vec4 b_odd)
{
	float absorbance = -log(transmittance);

	b_0 = absorbance;
	float depth_pow2 = depth * depth;
	float depth_pow4 = depth_pow2 * depth_pow2;
	float depth_pow6 = depth_pow4 * depth_pow2;
	b_even = vec4(depth_pow2, depth_pow4, depth_pow6, depth_pow6 * depth_pow2) * absorbance;
	b_odd = vec4(depth, depth_pow2 * depth, depth_pow4 * depth, depth_pow6 * depth) * absorbance;
}


void generateTrigoMoments2(float depth, float transmittance, vec4 wrapping_zone_parameters, out float b_0, out vec4 b)
{
	float absorbance = -log(transmittance);

	b_0 = absorbance;
	float phase = fma(depth, wrapping_zone_parameters.y, wrapping_zone_parameters.y);
	vec2 circle_point;
	circle_point.y = sin(phase);
	circle_point.x = cos(phase);
	vec2 circle_point_pow2 = Multiply(circle_point, circle_point);
	b = vec4(circle_point, circle_point_pow2) * absorbance;
}

void generateTrigoMoments3(float depth, float transmittance, vec4 wrapping_zone_parameters, out float b_0, out vec4 b_12, out vec4 b_3456)
{
	float absorbance = -log(transmittance);

	b_0 = absorbance;
	float phase = fma(depth, wrapping_zone_parameters.y, wrapping_zone_parameters.y);
	vec2 circle_point;
	circle_point.y = sin(phase);
	circle_point.x = cos(phase);
	vec2 circle_point_pow2 = Multiply(circle_point, circle_point);
	b_12 = vec4(circle_point * absorbance,0,0);
	b_3456 = vec4(circle_point_pow2, Multiply(circle_point, circle_point_pow2)) * absorbance;
}

void generateTrigoMoments4(float depth, float transmittance, vec4 wrapping_zone_parameters, out float b_0, out vec4 b_even, out vec4 b_odd)
{
	float absorbance = -log(transmittance);

	b_0 = absorbance;
	float phase = fma(depth, wrapping_zone_parameters.y, wrapping_zone_parameters.y);
	vec2 circle_point;
	circle_point.y = sin(phase);
	circle_point.x = cos(phase);
	vec2 circle_point_pow2 = Multiply(circle_point, circle_point);
	b_even = vec4(circle_point_pow2, Multiply(circle_point_pow2, circle_point_pow2)) * absorbance;
	b_odd = vec4(circle_point, Multiply(circle_point, circle_point_pow2)) * absorbance;
}
