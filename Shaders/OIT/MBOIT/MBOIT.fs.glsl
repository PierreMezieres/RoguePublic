#version 410 core
layout (location = 0) out float b_0;
layout (location = 1) out vec4 b1;
layout (location = 2) out vec4 b2;

#include "../../Macros/Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

uniform bool trigonometric;
uniform int moment_number;
uniform vec4 wrapping_parameters;

uniform Material material;
uniform sampler2D depthOpaque;

#include "MomentOITgeneration.glsl"
#include "../../Macros/MaterialAlpha.glsl"

void main()
{

    ivec2 fragCoord = ivec2(gl_FragCoord.xy);
    float depth = texelFetch(depthOpaque, fragCoord, 0).r;
    if(depth < gl_FragCoord.z) discard;

    vec4 col = getKdA();
    b2 = vec4(0);

    if(trigonometric){
        switch(moment_number){
        case 2: generateTrigoMoments2(gl_FragCoord.z, 1-col.a, wrapping_parameters, b_0, b1); break;
        case 3: generateTrigoMoments3(gl_FragCoord.z, 1-col.a, wrapping_parameters, b_0, b1, b2); break;
        case 4: generateTrigoMoments4(gl_FragCoord.z, 1-col.a, wrapping_parameters, b_0, b1, b2); break;
        }
    }else{
        switch(moment_number){
        case 4: generateMoments4(gl_FragCoord.z, 1-col.a, b_0, b1); break;
        case 6: generateMoments6(gl_FragCoord.z, 1-col.a, b_0, b1, b2); break;
        case 8: generateMoments8(gl_FragCoord.z, 1-col.a, b_0, b1, b2); break;
        }
    }

}