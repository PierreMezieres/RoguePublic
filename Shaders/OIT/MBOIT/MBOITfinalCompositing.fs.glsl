#version 410 core
out vec4 color;

in vec2 texCoord;

#include "../../Other/gamma.glsl"

uniform sampler2D transparentLayer;
uniform sampler2D opaqueLayer;
uniform sampler2D totalTransmittance;

void main()
{
    vec3 O = texture(opaqueLayer, texCoord).rgb;
    vec3 T = texture(transparentLayer, texCoord).rgb;
    float a = texture(totalTransmittance, texCoord).r;
    float b = (1-exp(-a)) / texture(transparentLayer, texCoord).a;

    color = vec4(O*exp(-a)+T, 1.0);

    color.rgb = gammaFunction(color.rgb);

}