#version 410 core
layout (location = 0) out float depth;
layout (location = 1) out vec4 rgba;

#include "../../Macros/Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

#define MAX_LIGHTS 10
#define MAX_DEPTH 99999.0

uniform int numberLights;

uniform sampler2D posZ;
uniform sampler2D color;
uniform sampler2D opaqueDepth;

uniform Light lights[MAX_LIGHTS];
uniform vec3 viewPos;
uniform Material material;

#include "../../Macros/Material.glsl"
#include "../../Macros/LightingTransparency.glsl"

void main()
{


    float fragDepth = gl_FragCoord.z;

    ivec2 fragCoord = ivec2(gl_FragCoord.xy);
    float lastDepth = -texelFetch(posZ, fragCoord, 0).r;
    vec4 lastColor = texelFetch(color, fragCoord, 0);

    depth = -MAX_DEPTH;

    rgba = lastColor;

    if(fragDepth < lastDepth) return;

    if(fragDepth > lastDepth){
        depth = -fragDepth;
    }else{
        //depth = fragDepth;

        float depthOpaque = texelFetch(opaqueDepth, fragCoord,0).r;
        if(fragDepth < depthOpaque){


            vec4 col = getKd();
            vec3 result = vec3(0);

            for(int i=0; i<numberLights; i++){
                result += compute_lighting(lights[i]);
            }

            col = vec4(result, col.a);


           //vec4 col = getKd();


            float alphaMul = 1.0 - lastColor.a;
            rgba.rgb += col.rgb * col.a * alphaMul;
            rgba.a = 1.0 - alphaMul * (1.0 - col.a);

        }

    }



}