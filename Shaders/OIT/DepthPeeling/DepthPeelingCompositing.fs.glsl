#version 410 core
out vec4 color;

in vec2 texCoord;

#include "../../Other/gamma.glsl"

uniform sampler2D transparentLayer;
uniform sampler2D opaqueLayer;

void main()
{
    vec3 O = texture(opaqueLayer, texCoord).rgb;
    vec3 T = texture(transparentLayer, texCoord).rgb;
    float a = texture(transparentLayer, texCoord).a;

    color = vec4(T*a + O*(1-a), 1.0);
    color.rgb = gammaFunction(color.rgb);
}