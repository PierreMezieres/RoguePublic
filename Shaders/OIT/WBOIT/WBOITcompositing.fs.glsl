#version 410 core
out vec4 color;

in vec2 texCoord;

#include "../../Other/gamma.glsl"

uniform sampler2D opaqueLayer;
uniform sampler2D accumTexture;
uniform sampler2D revealTexture;

void main()
{
    vec3 O = texture(opaqueLayer, texCoord).rgb;
    vec4 accum = texture(accumTexture, texCoord);
    float reveal = texture(revealTexture, texCoord).r;

    color = vec4(accum.rgb / clamp(accum.a, 1e-5, 5e4),reveal);

      color.rgb = reveal*O+ color.rgb*(1-reveal);
    //color.rgb += (1-reveal)*O;
    //color = vec4(O, 1.0);

    //color = vec4(reveal,reveal,reveal,1);

    color.rgb = gammaFunction(color.rgb);



}