#version 410 core
layout (location = 0) out float reveal;
layout (location = 1) out vec4 accum;

#include "../../Macros/Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

#define MAX_LIGHTS 10

uniform int numberLights;
uniform Light lights[MAX_LIGHTS];
uniform vec3 viewPos;
uniform int weightFunction;

/* Weight function parameters */
uniform float colorResistance;
uniform float depthRange;
uniform float orderingStrength;

uniform Material material;
uniform sampler2D depthOpaque;

#include "../../Macros/Material.glsl"
#include "../../Macros/LightingTransparency.glsl"

void main()
{

    ivec2 fragCoord = ivec2(gl_FragCoord.xy);
    float depth = texelFetch(depthOpaque, fragCoord, 0).r;
    if(depth < gl_FragCoord.z) discard;

    vec4 col = getKd();
    vec3 result = vec3(0);

    for(int i=0; i<numberLights; i++){
        result += compute_lighting(lights[i]);
    }

    col = vec4(result, col.a);

    float weight;
    float origZ = gl_FragCoord.z / gl_FragCoord.w;

    switch(weightFunction){
    case 0:
        weight = col.a * max(1e-2, 3e3*(1-pow(gl_FragCoord.z,3)));
        break;
    case 1:
        weight = col.a * max(1e-2, min(3e3,10/(1e-5+pow(origZ/5,2)+(pow(origZ/200,6)))));
        break;
    case 2:
        weight = col.a * max(1e-2, min(3e3,10/(1e-5+pow(origZ/10,3)+(pow(origZ/200,6)))));
        break;
    case 3:
        weight = col.a * max(1e-2, min(3e3,0.03/(1e-5+pow(origZ/200,4))));
        break;
    case 4:
        weight = max(min(1.0, max(max(col.r, col.g), col.b) * col.a), col.a) *
                 clamp(0.03 / (1e-5 + pow(origZ / 200, 4.0)), 1e-2, 3e3);
        break;
    case 5:
        weight = pow(col.a, colorResistance) * clamp(0.03/(1e-5+pow(origZ/depthRange,orderingStrength)), 1e-2, 3e3);
        break;
    }
    reveal = col.a;
    accum = vec4(col.rgb * col.a, col.a) * weight;

}