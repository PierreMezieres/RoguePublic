
#include "ShadowMap/PCF.glsl"

const float Pi = 3.14159265;
/*
vec2 poissonDisk4[4] = vec2[](
  vec2( -0.94201624, -0.39906216 ),
  vec2( 0.94558609, -0.76890725 ),
  vec2( -0.094184101, -0.92938870 ),
  vec2( 0.34495938, 0.29387760 )
);


vec2 poissonDisk16[16] = vec2[](
   vec2( -0.94201624, -0.39906216 ),
   vec2( 0.94558609, -0.76890725 ),
   vec2( -0.094184101, -0.92938870 ),
   vec2( 0.34495938, 0.29387760 ),
   vec2( -0.91588581, 0.45771432 ),
   vec2( -0.81544232, -0.87912464 ),
   vec2( -0.38277543, 0.27676845 ),
   vec2( 0.97484398, 0.75648379 ),
   vec2( 0.44323325, -0.97511554 ),
   vec2( 0.53742981, -0.47373420 ),
   vec2( -0.26496911, -0.41893023 ),
   vec2( 0.79197514, 0.19090188 ),
   vec2( -0.24188840, 0.99706507 ),
   vec2( -0.81409955, 0.91437590 ),
   vec2( 0.19984126, 0.78641367 ),
   vec2( 0.14383161, -0.14100790 )
);

float random(vec3 seed, int i){
	vec4 seed4 = vec4(seed,i);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
	return fract(sin(dot_product) * 43758.5453);
}
*/
/*
float ShadowCalcPCF(vec4 fragPosLightSpace, vec3 lightDir) {
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords.xyz * 0.5 + 0.5;
    float currentDepth = projCoords.z;
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);

    if(projCoords.z > 1.0 || projCoords.z < 0.0)
        return 0.f;

    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x) {
        for(int y = -1; y <= 1; ++y) {
            float u = projCoords.x + x * texelSize.x;
            float v = projCoords.y + y * texelSize.y;
            if(u > texelSize.x * 1024 || v > texelSize.y * 1024)
                return 0.f;
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    return shadow;
}
*/

float depthTest(vec4 fragPosLightSpace, vec3 lightDir) {
    switch(light.type) {
    case POINT_LIGHT: return depthTestCube(lightDir, shadowMapPoint, light.pointLight.position);
    case SPOT_LIGHT: return 0;
    case DIR_LIGHT: return 0;
    }
    return 0.f;
}

float shadowCalcPCF(vec4 fragPosLightSpace, vec3 lightDir) {
    switch(light.type) {
    case POINT_LIGHT: return shadowCalcPCFCube(lightDir, shadowMapPoint, light.pointLight.position);
    case SPOT_LIGHT: return 0;
    case DIR_LIGHT: return 0;
    }
    return 0.f;
}

vec3 commune_compute(Light light, vec3 direction, vec3 normal) {
    vec3 lightDir = normalize(direction);
    vec3 norm = normalize(normal);
    vec3 viewDir = normalize(viewPos - fragPos);
    float Ns = getNs();
    vec3 Kd = getKd().xyz;
    float normalization = ((Ns + 2) * (Ns + 4)) / (8 * Pi * (exp2(-Ns * 0.5) + Ns));
    vec3 Ks = getKs() * normalization;
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), Ns);


    vec3 ambient = 0.3 * Kd;
    vec3 diffuse = light.color * diff * Kd;
    vec3 specular = light.color * spec * Ks;
    float shadow = shadowCalcPCF(fragPosLightSpace, direction);

    return (ambient + (1.f - shadow) * (diffuse + specular));
}

float compute_attenuation(float c, float l, float q, float d)
{
    float att = c + l * d + q * d * d;
    return 1.0/ att;
}

vec3 compute_pointLight(Light light)
{
    PointLight l = light.pointLight;

    vec3 lightDir = normalize(l.position - fragPos);

    float distance = length(l.position - fragPos);

    float attenuation = compute_attenuation(l.attenuation.constant, l.attenuation.linear, l.attenuation.quadratic, distance);
    vec3 color = commune_compute(light, lightDir, normal);

    return color * attenuation;

}

vec3 compute_dirLight(Light light)
{
    return commune_compute(light, -light.dirLight.direction, normal);
}

vec3 compute_spotLight(Light light)
{
    SpotLight l = light.spotLight;

    vec3 lightDir = normalize(l.position - fragPos);



    float theta = dot(lightDir, normalize(-l.direction));
    float epsilon = l.innerAngle - l.outerAngle;
    float intensity = clamp((theta - l.outerAngle) / epsilon, 0.0, 1.0);

    float distance = length(l.position - fragPos);
    float attenuation = compute_attenuation(l.attenuation.constant, l.attenuation.linear, l.attenuation.quadratic, distance);
    vec3 color = commune_compute(light, lightDir, normal);

    return color * attenuation * intensity;
}

vec3 compute_lighting(Light light)
{

    switch(light.type){
    case 0: return compute_pointLight(light);
    case 1: return compute_spotLight(light);
    case 2: return compute_dirLight(light);
    }
    return vec3(0);
}















//
