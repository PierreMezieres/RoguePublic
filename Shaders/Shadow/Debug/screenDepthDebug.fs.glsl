#version 410 core

out vec4 FragColor;

in vec2 texCoord;

uniform sampler2D screenTexture;
uniform float near_plane;
uniform float far_plane;


float LinearizeDepth(float depth) {
    float z = depth * 2.0 - 1.0;
    return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));	
}

void main() {             
    vec4 depthValue = texture(screenTexture, texCoord);
    //FragColor = vec4(vec3(LinearizeDepth(depthValue.r) / far_plane), 1.0); // perspective
    //FragColor = vec4(vec3((depthValue) / far_plane), 1.0); // perspective
    FragColor = vec4(vec3(depthValue.r, depthValue.r, depthValue.r), 1.0); // orthographic

}
