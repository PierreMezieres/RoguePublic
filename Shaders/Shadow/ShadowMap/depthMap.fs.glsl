#version 410 core

layout (location = 0) out vec4 depth;

void main() {
    depth = vec4(gl_FragCoord.z, gl_FragCoord.z, gl_FragCoord.z, 1.f);
}
