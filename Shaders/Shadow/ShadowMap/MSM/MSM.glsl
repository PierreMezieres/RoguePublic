
uniform float depthBias;
uniform float momentBias;

void Compute4MomentUnboundedShadowIntensity(out float OutShadowIntensity,
    vec4 _4Moments, float FragmentDepth, float DepthBias, float MomentBias) {
	// Bias input data to avoid artifacts
	vec4 b=mix(_4Moments, vec4(0.5f,0.5f,0.5f,0.5f), MomentBias);
	vec3 z;
	z[0]=FragmentDepth-DepthBias;

	// Compute a Cholesky factorization of the Hankel matrix B storing only non-
	// trivial entries or related products
	//float L32D22=mad(-b[0],b[1],b[2]);
	float L32D22 = (-b[0] * b[1]) + b[2];
	//float D22=mad(-b[0],b[0], b[1]);
	float D22 = (-b[0] * b[0]) + b[1];
	//float SquaredDepthVariance=mad(-b[1],b[1], b[3]);
	float SquaredDepthVariance = (-b[1] * b[1]) + b[3];
	float D33D22 = dot(vec2(SquaredDepthVariance, -L32D22), vec2(D22,L32D22));
	float InvD22 = 1.0f/D22;
	float L32 = L32D22 * InvD22;

	// Obtain a scaled inverse image of bz=(1,z[0],z[0]*z[0])^T
	vec3 c = vec3(1.0f,z[0],z[0]*z[0]);
	// Forward substitution to solve L*c1=bz
	c[1] -= b.x;
	c[2] -= b.y + L32 * c[1];
	// Scaling to solve D*c2=c1
	c[1] *= InvD22;
	c[2] *= D22/D33D22;
	// Backward substitution to solve L^T*c3=c2
	c[1] -= L32 * c[2];
	c[0] -= dot(c.yz, b.xy);
	// Solve the quadratic equation c[0]+c[1]*z+c[2]*z^2 to obtain solutions
	// z[1] and z[2]
	float p = c[1]/c[2];
	float q = c[0]/c[2];
	float D = (p * p * 0.25f) - q;
	float r = sqrt(D);
	z[1] = -p * 0.5f - r;
	z[2] = -p * 0.5f + r;
	// Compute the shadow intensity by summing the appropriate weights
	vec4 Switch=
		(z[2] < z[0]) ? vec4(z[1], z[0], 1.0f, 1.0f):(
		(z[1] < z[0]) ? vec4(z[0], z[1], 0.0f, 1.0f):
		vec4(0.0f,0.0f,0.0f,0.0f));
	float Quotient = (Switch[0] * z[2] -b[0] * (Switch[0] + z[2]) + b[1])/((z[2] - Switch[1]) * (z[0] - z[1]));
	OutShadowIntensity = Switch[2] + Switch[3] * Quotient;
	OutShadowIntensity = clamp(OutShadowIntensity, 0.f, 1.f);

	//OutShadowIntensity = saturate(OutShadowIntensity);
}

//#include "shadow.glsl"
void Compute4MomentShadowIntensity(out float OutShadowIntensity,
    vec4 _4Moments, float FragmentDepth, float DepthBias, float MomentBias) {
	// Bias input data to avoid artifacts
	vec4 b = mix(_4Moments, vec4(0.5f,0.5f,0.5f,0.5f), MomentBias);
	vec3 z;
	z[0] = FragmentDepth - DepthBias;

	// Compute a Cholesky factorization of the Hankel matrix B storing only non-
	// trivial entries or related products
	float L32D22 = (-b[0] * b[1]) + b[2];
	float D22 = (-b[0] * b[0]) + b[1];
	float SquaredDepthVariance = (-b[1] * b[1]) + b[3];
	float D33D22 = dot(vec2(SquaredDepthVariance, -L32D22), vec2(D22, L32D22));
	float InvD22 = 1.0f/D22;
	float L32 = L32D22 * InvD22;

	// Obtain a scaled inverse image of bz=(1,z[0],z[0]*z[0])^T
	vec3 c = vec3(1.0f, z[0], z[0] * z[0]);
	// Forward substitution to solve L*c1=bz
	c[1] -= b.x;
	c[2] -= b.y + L32 * c[1];
	// Scaling to solve D*c2=c1
	c[1] *= InvD22;
	c[2] *= D22/D33D22;
	// Backward substitution to solve L^T*c3=c2
	c[1] -= L32*c[2];
	c[0] -= dot(c.yz,b.xy);
	// Solve the quadratic equation c[0]+c[1]*z+c[2]*z^2 to obtain solutions z[1]
	// and z[2]
	float p = c[1]/c[2];
	float q = c[0]/c[2];
	float D = ((p*p)/4.0f)-q;
	float r = sqrt(D);
	z[1] =- (p/2.0f)-r;
	z[2] =- (p/2.0f)+r;

	// Use a solution made of four deltas if the solution with three deltas is invalid
	if(z[1]<0.0f || z[2]>1.0f) {
		float zFree = ((b[2]-b[1])*z[0]+b[2]-b[3])/((b[1]-b[0])*z[0]+b[1]-b[2]);
		float w1Factor = (z[0]>zFree)?1.0f:0.0f;
		OutShadowIntensity = (b[1]-b[0]+(b[2]-b[0]-(zFree+1.0f)*(b[1]-b[0]))*(zFree-w1Factor-z[0])
		                                        /(z[0]*(z[0]-zFree)))/(zFree-w1Factor)+1.0f-b[0];
	}
	// Use the solution with three deltas
	else {
		vec4 Switch =
			(z[2]<z[0]) ? vec4(z[1],z[0],1.0f,1.0f):(
			(z[1]<z[0]) ? vec4(z[0],z[1],0.0f,1.0f):
			vec4(0.0f,0.0f,0.0f,0.0f));
		float Quotient = (Switch[0]*z[2]-b[0]*(Switch[0]+z[2])+b[1])/((z[2]-Switch[1])*(z[0]-z[1]));
		OutShadowIntensity=Switch[2]+Switch[3]*Quotient;
	}
	OutShadowIntensity = clamp(OutShadowIntensity, 0.f, 1.f);


}

vec4 sampleMomentShadowMap(sampler2DMS sm, vec2 coords, int x, int y) {
    vec4 s1 = texelFetch(sm, ivec2(coords.x * x, coords.y * y), 0);
    vec4 s2 = texelFetch(sm, ivec2(coords.x * x, coords.y * y), 1);
    vec4 s3 = texelFetch(sm, ivec2(coords.x * x, coords.y * y), 2);
    vec4 s4 = texelFetch(sm, ivec2(coords.x * x, coords.y * y), 3);

    return (s1 + s2 + s3 + s4) / 4.f;
}


float fourMomentsShadowCalcBase(vec4 fragPosLightSpace, vec3 lightDir) {
    float shadow = 0.f;
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float fragDepth = projCoords.z;

    float bias = depthBias;

    float depth = texelFetch(shadowMap[0], ivec2(projCoords.x * 1024, projCoords.y * 1024), 0).r;
    vec4 moments = texture(shadowMap[0], projCoords.xy);

    Compute4MomentUnboundedShadowIntensity(shadow, moments, fragDepth, bias, momentBias);

    return shadow;
}

void directionToUV(vec3 direction, out int texId, out vec2 coord) {
    float x = direction.x;
    float y = direction.y;
    float z = direction.z;

    float absX = abs(x);
    float absY = abs(y);
    float absZ = abs(z);

    bool isXPositive = x > 0 ? true : false;
    bool isYPositive = y > 0 ? true : false;
    bool isZPositive = z > 0 ? true : false;

    float maxAxis, uc, vc;

    //Positive X
    if (isXPositive && absX >= absY && absX >= absZ) {
        //u (0 to 1) goes from +z to -z
        //v (0 to 1) goes from -y to +y
        maxAxis = absX;
        uc = -z;
        vc = y;
        texId = 0;
    }
    //Negative X
    if (!isXPositive && absX >= absY && absX >= absZ) {
        //u (0 to 1) goes from -z to +z
        //v (0 to 1) goes from -y to +y
        maxAxis = absX;
        uc = z;
        vc = y;
        texId = 1;
    }
    //Positive Y
    if (isYPositive && absY >= absX && absY >= absZ) {
        //u (0 to 1) goes from -x to +x
        //v (0 to 1) goes from +z to -z
        maxAxis = absY;
        uc = x;
        vc = -z;
        texId = 2;
    }
    //Negative Y
    if (!isYPositive && absY >= absX && absY >= absZ) {
        //u (0 to 1) goes from -x to +x
        //v (0 to 1) goes from -z to +z
        maxAxis = absY;
        uc = x;
        vc = z;
        texId = 3;
    }
    //Positive Z
    if (isZPositive && absZ >= absX && absZ >= absY) {
        // u (0 to 1) goes from -x to +x
        // v (0 to 1) goes from -y to +y
        maxAxis = absZ;
        uc = x;
        vc = y;
        texId = 4;
    }
    //Negative Z
    if (!isZPositive && absZ >= absX && absZ >= absY) {
        //u (0 to 1) goes from +x to -x
        //v (0 to 1) goes from -y to +y
        maxAxis = absZ;
        uc = -x;
        vc = y;
        texId = 5;
    }

    coord = vec2(0.5f * (uc/maxAxis + 1.f), 0.5f * (vc/maxAxis + 1.f));
}


//not work yet
float fourMomentsShadowCalcCube(vec4 fragPosLightSpace, vec3 lightDir, vec3 lightPos) {
    float shadow = 0.f;
    vec3 fragToLight = normalize(fragPos - lightPos);
    int texId;
    vec2 projCoords;
    directionToUV(fragToLight, texId, projCoords);
    vec3 projCoord = fragPosLightSpaces[texId].xyz / fragPosLightSpaces[texId].w;
    projCoord = projCoord * 0.5 + 0.5;
    float fragDepth = projCoord.z;
    //float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    //float bias = 0.005;
    float bias = depthBias;
    //float momentBias = 3.f*pow(10, -5);
    float depth = texelFetch(shadowMap[texId], ivec2(projCoords.x * 1024, projCoords.y * 1024), 0).r;
    vec4 moments = texture(shadowMap[texId], projCoords.xy);
    //vec4 moments = sampleMomentShadowMap(shadowMap, projCoords.xy, 1024, 1024);
    //return fragDepth - bias > depth ? 1.0 : 0.0;
    //Compute4MomentShadowIntensity(shadow, moments, fragDepth, bias, momentBias);
    Compute4MomentUnboundedShadowIntensity(shadow, moments, fragDepth, bias, momentBias);

    return shadow;
}


float fourMomentsShadowCalc(vec4 fragPosLightSpace, vec3 lightDir) {
    switch (light.type) {
        case POINT_LIGHT: return fourMomentsShadowCalcCube(fragPosLightSpace, lightDir, light.pointLight.position);
        case SPOT_LIGHT: return fourMomentsShadowCalcBase(fragPosLightSpace, lightDir);
        case DIR_LIGHT: return fourMomentsShadowCalcBase(fragPosLightSpace, lightDir);
    }
    return 0.f;
}