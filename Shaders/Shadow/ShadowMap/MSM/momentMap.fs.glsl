#version 410 core

layout (location = 0) out vec4 moments;

void main() {
    float depth = gl_FragCoord.z;
    float depthSquare = depth*depth;
    moments = vec4(depth, depthSquare, depthSquare*depth, depthSquare*depthSquare);
}
