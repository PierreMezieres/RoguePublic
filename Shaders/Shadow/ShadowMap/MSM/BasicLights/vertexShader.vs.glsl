#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 itexCoord;

uniform mat4 model;
uniform mat4 proj;
uniform mat4 view;
uniform mat4 lightSpaceMatrix;
//uniform mat4 lightSpaceMatCube[6];

out vec3 normal;
out vec2 texCoord;
out vec3 fragPos;
out vec4 fragPosLightSpace;
//out vec4 fragPosLightSpaces[6];


void main() {
    normal=mat3(transpose(inverse(model))) * inormal;
    gl_Position = proj * view * model * vec4(position, 1.0);
    texCoord = itexCoord;
    fragPos = vec3(model * vec4(position, 1.0));
    fragPosLightSpace = lightSpaceMatrix * model * vec4(position, 1.f);
    /*for (int i = 0; i < 6; i++) {
        fragPosLightSpaces[i] = lightSpaceMatCube[i] * model * vec4(position, 1.f);
    }*/
}
