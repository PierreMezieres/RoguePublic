
uniform float minVar;

float Chebychev(vec2 moments, float FragmentDepth) {
    // Surface is fully lit. as the current fragment is before the light occluder
    if (FragmentDepth <= moments.x)
        return 1.0 ;
    
    // The fragment is either in shadow or penumbra. We now use chebyshev's upperBound to check
    // How likely this pixel is to be lit (p_max)
    float variance = max(moments.y - (moments.x*moments.x), minVar);
    
    float d = FragmentDepth - moments.x;
    float p_max = variance / (variance + d*d);
    
    return clamp(p_max, 0.f, 1.f);//This clamp is normally not needed, still putting it there for later on optimization

    //OutShadowIntensity = clamp(0.7, 0.f, 1.f);
}



float VSMShadowCalc(vec4 fragPosLightSpace, vec3 lightDir) {
    if (light.type == POINT_LIGHT)
        return 0.f;

    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float fragDepth = projCoords.z;

    //float depth = texture(shadowMap, projCoords.xy).r;//ivec2(projCoords.x * 1024, projCoords.y * 1024), 0).r;
    vec2 moments = texture(shadowMap, projCoords.xy).rg;
    return 1.f - Chebychev(moments, fragDepth); // This is the shadow value
}


