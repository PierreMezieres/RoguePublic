#version 410 core

#include "../../../Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;
in vec4 fragPosLightSpace;

out vec4 color;
uniform sampler2D previousDrawCall;
uniform sampler2D shadowMap;
uniform Light light;
uniform float far_plane;

uniform vec3 viewPos;
uniform Material material;


#include "../../../../Other/gamma.glsl"
#include "../../../Material.glsl"
#include "Lighting.glsl"

void main()
{
    color = vec4(compute_lighting(light),1.f) + vec4(texelFetch(previousDrawCall, ivec2(gl_FragCoord.xy), 0).rgb, 1.0);
}
