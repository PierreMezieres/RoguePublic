
#include "VSM.glsl"

const float Pi = 3.14159265;

vec3 commune_compute(Light light, vec3 direction, vec3 normal, float intensity) {
    vec3 lightDir = normalize(direction);
    vec3 norm = normalize(normal);
    vec3 viewDir = normalize(viewPos - fragPos);
    float Ns = getNs();
    vec3 Kd = getKd().xyz;
    float normalization = ((Ns + 2) * (Ns + 4)) / (8 * Pi * (exp2(-Ns * 0.5) + Ns));
    vec3 Ks = getKs() * normalization;
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), Ns);

    vec3 ambient = 0.3 * Kd;
    vec3 diffuse = light.color * diff * Kd * intensity;
    vec3 specular = light.color * spec * Ks * intensity;
    
    float shadow = VSMShadowCalc(fragPosLightSpace, lightDir);

    return (ambient + (1.f - shadow) * (diffuse + specular));
}

float compute_attenuation(float c, float l, float q, float d)
{
    float att = c + l * d + q * d * d;
    return 1.0/ att;
}

vec3 compute_pointLight(Light light)
{
    PointLight l = light.pointLight;

    vec3 lightDir = normalize(l.position - fragPos);

    float distance = length(l.position - fragPos);

    float attenuation = compute_attenuation(l.attenuation.constant, l.attenuation.linear, l.attenuation.quadratic, distance);
    vec3 color = commune_compute(light, lightDir, normal,1);

    return color * attenuation;

}

vec3 compute_dirLight(Light light)
{
    return commune_compute(light, -light.dirLight.direction, normal,1);
}

vec3 compute_spotLight(Light light)
{
    SpotLight l = light.spotLight;

    vec3 lightDir = normalize(l.position - fragPos);



    float theta = dot(lightDir, normalize(-l.direction));
    float epsilon = l.innerAngle - l.outerAngle;
    float intensity = clamp((theta - l.outerAngle) / epsilon, 0.0, 1.0);

    float distance = length(l.position - fragPos);
    float attenuation = compute_attenuation(l.attenuation.constant, l.attenuation.linear, l.attenuation.quadratic, distance);
    vec3 color = commune_compute(light, lightDir, normal, intensity);

    return color * attenuation;
}

vec3 compute_lighting(Light light)
{

    switch(light.type){
    case 0: return compute_pointLight(light);
    case 1: return compute_spotLight(light);
    case 2: return compute_dirLight(light);
    }
    return vec3(0);
}















//
