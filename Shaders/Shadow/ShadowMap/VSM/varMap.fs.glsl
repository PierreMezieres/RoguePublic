#version 410 core

layout (location = 0) out vec2 moments;


void main() {

    float depth = gl_FragCoord.z;
    float depthSquare = depth*depth;
    
    moments = vec2(depth, depthSquare);

}

