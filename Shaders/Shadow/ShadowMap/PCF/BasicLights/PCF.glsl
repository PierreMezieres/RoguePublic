
uniform int u_kernelSize;
uniform float u_standardDeviation;
uniform float u_depthBias;

float depthTest(vec4 fragPosLightSpace, vec3 lightDir) {
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    
    vec2 texelSize = 1.f / textureSize(shadowMap, 0);
    float u = projCoords.x;
    float v = projCoords.y;

    return      (currentDepth - bias <= texture(shadowMap, projCoords.xy).r)
            ||  (u > (texelSize.x*1024) || v > (texelSize.y*1024))
            ?   0.0 : 1.0;
}


void ComputePCFShadowIntensity(out float OutShadowIntensity, sampler2D ShadowMapSampler, vec2 ShadowMapTexCoord, float FragmentDepth, vec4 ShadowMapSize, const float StandardDeviation, const int KernelRadius, float DepthBias) {
	float BiasedDepth = FragmentDepth - DepthBias;
	// Determine the region that needs to be sampled. It is extended by one texel
	// for bilinear filtering
	int iFirstSample = -KernelRadius - 1;
	int iLastSample = KernelRadius;
	// Prepare quantities for computation of filter weights
	float InvVariance = 1.0f / (StandardDeviation*StandardDeviation);
	ShadowMapTexCoord += ShadowMapSize.zw*0.5f;
	vec2 TexelCoord = fract(ShadowMapTexCoord*ShadowMapSize.xy);
	OutShadowIntensity = 0.0f;
	float TotalWeight = 0.0f;
	for (int x = iFirstSample; x <= iLastSample; ++x) {
		// Linearly interpolate between two Gaussian weights to implement bilinear
		// filtering on top of an image-space Gaussian filtering
		float HorizontalWeight = mix((x==iLastSample)?0.0f:exp(-0.5f*(x+1)*(x+1)*InvVariance),(x==iFirstSample)?0.0f:exp(-0.5f*x*x*InvVariance), TexelCoord.x);
		for (int y = iFirstSample; y <= iLastSample; ++y) {
			// Obtain the PCF sample
			vec2 OffsetTexCoord = ShadowMapTexCoord + ShadowMapSize.zw * vec2(x,y);
			float SampledShadowMapDepth = texture(ShadowMapSampler, OffsetTexCoord).r;
			float Summand = (SampledShadowMapDepth<BiasedDepth)?1.0f:0.0f;
			// Compute the vertical weight in analogy to the horizontal weight
			float VerticalWeight = mix((y==iLastSample)?0.0f:exp(-0.5f*(y+1)*(y+1)*InvVariance),(y==iFirstSample)?0.0f:exp(-0.5f*y*y*InvVariance),TexelCoord.y);
			float Weight = HorizontalWeight * VerticalWeight;
			OutShadowIntensity += Weight*Summand;
			TotalWeight += Weight;
		}
	}
	OutShadowIntensity/=TotalWeight;
}


float shadowCalcPCF(vec4 fragPosLightSpace, vec3 lightDir) {

    if (light.type == POINT_LIGHT)
        return 0.f;

    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;
    float shadow = 0.f;

    float bias = u_depthBias;
    float standardDeviation = u_standardDeviation;
    int kernelRadius = u_kernelSize;

    ComputePCFShadowIntensity(shadow, shadowMap, projCoords.xy, currentDepth, vec4(1024, 1024, 1.f/1024.f, 1.f/1024.f), standardDeviation, kernelRadius, bias);

    return shadow;
}







//
