#version 410 core
in vec4 FragPos;
//out float depth;

uniform vec3 lightPos;
uniform float far_plane;

void main() {
    float lightDistance = length(FragPos.xyz - lightPos);
    
    lightDistance = lightDistance / far_plane;
    
    gl_FragDepth = lightDistance;
    //depth = lightDistance;
}  
