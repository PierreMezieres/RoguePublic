#version 410 core


in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

out vec4 color;


float c1 = 0.429043;
float c2 = 0.511664;
float c3 = 0.743125;
float c4 = 0.886227;
float c5 = 0.247708;

/* Grace Cathedral */


float l00r=0.79, l00g=0.44, l00b=0.54;
float l1_1r=0.39, l1_1g=0.35, l1_1b=0.60;
float l10r=-0.34, l10g=-0.18, l10b=-0.27;
float l11r=-0.29, l11g=-0.06, l11b=0.01;
float l2_2r=-0.11, l2_2g=-0.05, l2_2b=-0.12;
float l2_1r=-0.26, l2_1g=-0.22, l2_1b=-0.47;
float l20r=-0.16, l20g=-0.09, l20b=-0.15;
float l21r=0.56, l21g=0.21, l21b=0.14;
float l22r=0.21, l22g=-0.05, l22b=-0.30;

/* Eucalyptus Grove */

/*
float l00r=0.378326, l00g=0.426043, l00b=0.450459;
float l1_1r=0.288781, l1_1g=0.35868, l1_1b=0.414705;
float l10r=0.037903, l10g=0.0295216, l10b=0.00985669;
float l11r=-0.103303, l11g=-0.103169, l11b=-0.0884924;
float l2_2r=-0.062175, l2_2g=-0.0554431, l2_2b=-0.0396779;
float l2_1r=0.007782, l2_1g=-0.0148312, l2_1b=-0.0471301;
float l20r=-0.0935561, l20g=-0.125426, l20b=-0.152563;
float l21r=-0.0572703, l21g=-0.0502193, l21b=-0.036341;
float l22r=0.0203348, l22g=-0.00442008, l22b=-0.045218;
*/

/* stpeters Basilica */
/*
float l00r=0.362392, l00g=0.262413, l00b=0.232626;
float l1_1r=0.175913, l1_1g=0.143627, l1_1b=0.126057;
float l10r=-0.0247311, l10g=-0.0101254, l10b=-0.00107447;
float l11r=0.03465, l11g=0.0223184, l11b=0.010135;
float l2_2r=0.019814, l2_2g=0.0144073, l2_2b=0.00439867;
float l2_1r=-0.0469596, l2_1g=-0.0254485, l2_1b=-0.0117786;
float l20r=-0.0898667, l20g=-0.0760911, l20b=-0.0740964;
float l21r=0.00501938, l21g=0.00388412, l21b=0.000137424;
float l22r=-0.081875, l22g=-0.0321501, l22b=0.00333986;*/

/* ocean */
/*
float l00r=0.588476, l00g=0.713, l00b=0.785731;
float l1_1r=0.371228, l1_1g=0.467491, l1_1b=0.550068;
float l10r=0.139886, l10g=0.170408, l10b=0.206914;
float l11r=0.0329048, l11g=0.0316051, l11b=0.0366522;
float l2_2r=-0.00429078, l2_2g=-0.00552686, l2_2b=-0.00464458;
float l2_1r=0.10062, l2_1g=0.126222, l2_1b=0.152429;
float l20r=0.0548785, l20g=0.0321909, l20b=-0.00161016;
float l21r=-0.0684963, l21g=-0.0740589, l21b=-0.0692563;
float l22r=0.104443, l22g=0.100008, l22b=0.0907901;*/

/* Nebula */
/*
float l00r=0.549724, l00g=0.532071, l00b=0.628692;
float l1_1r=-0.0417591, l1_1g=-0.0414161, l1_1b=-0.0478082;
float l10r=0.0119058, l10g=0.0135727, l10b=-0.107128;
float l11r=0.0374337, l11g=0.0426066, l11b=0.0851692;
float l2_2r=-0.0505125, l2_2g=-0.0422551, l2_2b=-0.0356566;
float l2_1r=0.0102779, l2_1g=-0.0251008, l2_1b=-0.00423167;
float l20r=0.146772, l20g=0.153622, l20b=0.220045;
float l21r=-0.140761, l21g=-0.132183, l21b=-0.211893;
float l22r=0.11228, l22g=0.0994502, l22b=0.166622;*/








float formula(float x, float y, float z, float x2, float y2, float z2, float l00, float l1_1, float l10, float l11, float l2_2, float l2_1, float l20, float l21, float l22){
    return c1*l22*(x2-y2)+c3*l20*z2+c4*l00-c5*l20+2*c1*(l2_2*x*y+l21*x*z+l2_1*y*z)+2*c2*(l11*x+l1_1*y+l10*z);
}

/*float formula_withMat(vec4 n, float l00, float l1_1, float l10, float l11, float l2_2, float l2_1, float l20, float l21, float l22){
    float M = formula(l00,l1_1,l10,l11,l2_2,l2_1,l20,l21,l22);

    return n*M*n;
}

mat4 mat(float l00, float l1_1, float l10, float l11, float l2_2, float l2_1, float l20, float l21, float l22){

    return (c1*l22,c1*l2_2,c1*l21,c2*l11,c1*l2_2,-c1*l22,c1*l2_1,c2*l1_1,c1*l21,c1*l2_1,c3*l20,c2*l10,c2*l11,c2*l1_1,c2*l10,c4*l00-c5*l20);
}*/

void main()
{
    vec3 n = normalize(normal);
    float x = n.x;
    float y = n.y;
    float z = n.z;
    float x2 = x*x;
    float y2 = y*y;
    float z2 = z*z;

    float r = formula(x,y,z,x2,y2,z2,l00r,l1_1r,l10r,l11r,l2_2r,l2_1r,l20r,l21r,l22r);
    float g = formula(x,y,z,x2,y2,z2,l00g,l1_1g,l10g,l11g,l2_2g,l2_1g,l20g,l21g,l22g);
    float b = formula(x,y,z,x2,y2,z2,l00b,l1_1b,l10b,l11b,l2_2b,l2_1b,l20b,l21b,l22b);

    color = vec4(r,g,b, 1.0);
}