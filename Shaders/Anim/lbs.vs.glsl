#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 itexCoord;

uniform mat4 model;
uniform mat4 proj;
uniform mat4 view;

uniform mat4 bone1 = mat4(1);
uniform mat4 bone2 = mat4(1);

out vec3 normal;
out vec2 texCoord;
out vec3 fragPos;

void main()
{
    mat4 blend = itexCoord.x * bone1 + itexCoord.y * bone2;
    vec3 new_pos = vec3(blend * vec4(position,1));
    normal=mat3(transpose(inverse(model))) * inormal;
    gl_Position = proj * view * model * vec4(new_pos, 1.0);
    texCoord = itexCoord;
    fragPos = vec3(model * vec4(new_pos, 1.0));
}
