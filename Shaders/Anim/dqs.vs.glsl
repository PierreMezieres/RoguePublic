#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 itexCoord;

uniform mat4 model;
uniform mat4 proj;
uniform mat4 view;

uniform vec4 b1r = vec4(1,0,0,0);
uniform vec4 b1i = vec4(1,0,0,0);
uniform vec4 b2r = vec4(1,0,0,0);
uniform vec4 b2i = vec4(1,0,0,0);

out vec3 normal;
out vec2 texCoord;
out vec3 fragPos;


vec3 transformPositionDQS( vec3 position, vec4 r, vec4 d )
{
    vec3 tr = (d.xyz*r.w - r.xyz*d.w + cross(r.xyz,d.xyz)) *2.f;
    vec3 rot = position + cross((r.xyz*2.f), cross( r.xyz , position) + position * r.w);
    return tr + rot;
}

void main()
{
    vec4 q0 = b2r;

    float w1 = itexCoord.x;
    float w2 = itexCoord.y;
    if(dot(b1r,q0) < 0) w1 = -w1;
    if(dot(b2r,q0) < 0) w2 = -w2;

    vec4 b1 = w1 * b1r + w2 * b2r;
    vec4 b2 = w1 * b1i + w2 * b2i;
    float norm = length(b1);
    b1 = b1 / norm;
    b2 = b2 / norm;

    vec3 new_pos = transformPositionDQS(position, b1, b2);
    normal=mat3(transpose(inverse(model))) * inormal;
    gl_Position = proj * view * model * vec4(new_pos, 1.0);
    texCoord = itexCoord;
    fragPos = vec3(model * vec4(new_pos, 1.0));
}
