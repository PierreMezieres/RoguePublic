# Rogue Loader

*(This document was created by Jean-Baptiste Sarazin and updated by Pierre Mézières)*

*last update : 17/03/2018*

Rogue loader allow to create scenes with some commands.

# Syntax


Each line is considered as a command, the first word corresponds to a token.

Before we get into the available tokens. Here is a list of interesting things to know about the loader:

* '/' : Allows you to comment a line
* ';' : Allows you to consider a new token. A single line is no longer considered as a command.
* ',' : The next command will use the previous token.

Example of code
```
 / Here is a commented line
 
 / Example for ";"
 Token1 parameters1
 Token2 parameters2
 / Token1 is executed and then Token2
 / It's the same for: 
 Token1 parameters1; Token 2 parameters2
 
 / Example for ","
 Token1 parameters1
 Token1 parameters2
 / It's the same for:
 Token1 parameters1, parameters2

```

# Tokens

Here is the list of tokens you can use:

* [attenuation](#attenuation)
* [createMaterial](#creatematerial)
* [createShader](#createshader)
* [camera](#camera)
* [defaultMaterial](#defaultmaterial)
* [END](#END)
* [GOTO](#GOTO)
* [loadfile](#loadfile)
* [light](#light)
* [material](#material)
* [opaqueObject](#opaqueobject)
* [rotate](#rotate)
* [shape](#shape)
* [scale](#scale)
* [translate](#translate)
* [transparentObject](#transparentobject)

**Note:** You do not have to use all the token settings. They will be automatically replaced by default settings

## attenuation

Set the default attenuation parameters used for light

```
    attenuation
        take : 
            constant (float)
            linear (float)
            quadratic (float)
    ex : attenuation 0.9 1 0.5
```

## createMaterial

Create a material and add it in engine. You can add material with 2 modes (by define kd color or texture)

* Kd with color
```
    material
        take : 
            id material (string)
            color (vec3)
            shader name (string)
    ex : material newMaterial 0.5 0.6 0.4 shaderName
```
* Kd with texture
```
    material
        take : 
            id material (string)
            texture file path (string) 
            shader name (string)
    ex : material newMaterial pathToTexture shaderName
```

## createShader

Create a shader and add it in engine
```
    createShader
        take : VertexShader path to file (string)
               FragmentShader path to file (string)
    ex : attenuation vs.glsl fs.glsl
```


## camera
Rogue allow to add cameras in your scene and switch between them:
```
  camera
    take : 
        position (vec3)
        lookAt (vec3)
        fov in degree (float)
    ex : camera 1 1 1 -1 -1 -1 85
```

## defaultMaterial

Set the default material used for objects    
```
  defaultMaterial
    take : 
        material name (string)
    ex : material granit
```

## END

Force the parser to stop
```
    ex : END
```

## GOTO

You can pass some part of the file

```
example code :

GOTO A

A:

    shape cube 1 1 1
    translate 2 0 0
    GOTO C

B:
    loadfile file.obj
    material container
    light point 1 1 1 0.5 0.5 0.5 0.9 1 0.5
    END

C:
    shape sphere 1
    scale 0.5 0.5 0.5
    rotate 180 0 0 1
    GOTO B
```

In this example, all the code is executed, but in order A-C-B.

## loadfile

You can load other files

```
  loadfile
    take : 
        file name (string)
    ex : loadfile otherfilename
```

## light

Rogue is compatible with some light models, there are :
* directionnal light
    ```
    dir
        take : direction of light (vec3)
               color (vec3)
    ex : light dir 1 1 1 0.5 0.5 0.5
    ```

* point light
    ```
    point
        take : position (vec3)
               color (vec3)
               constant attenuation (float)
               linear attenuation (float)
               quadratic attenuation (float)
    ex : light point 1 1 1 0.5 0.5 0.5 0.9 1 0.5
    ```

* spot light
    ```
    spot
        take : position (vec3)
               direction (vec3)
               inner (float)
               outer (float)
               color (float)
               constant attenuation (float)
               linear attenuation (float)
               quadratic attenuation (float)
    ex : light spot 1 1 1 1 0 0 30 45 0.5 0.5 0.5 0.9 1 0.5
    ```


## material
Rogue allow to add materials to last object create with the loader, basic materials are :
 - emerald
 - gold
 - greenPlastic
 - chrome
 - container
 - earth
 - granite

```
  material
    take : 
        material name (string)
    ex : material emerald
```


## opaqueObject

The next objects will be defined as opaque objects

```
    ex : opaqueObject
```

## rotate

Rotate the last defined object

```
  rotate
    take : 
        angle in degree (float)
        rotation vector (vec3)
    ex : rotate 45 0 1 0
```


## shape

Rogue provide some base shape, there are :
* sphere
```
    take :  
        radius (float)
        
    ex : shape sphere 0.5
```
* cube
```
    take : 
        x length (float)
        y length (float)
        z length (float)
    ex : shape cube 1 1 1
```
* grid
```
    take : 
        width (float)
        height (float)
        number of squares on x axis (int)
        number of squares on y axis (int)
    ex : shape cube 10 10 20 20
```
* tiangleShape

Create your own triangle mesh
```
    take :
        Number of points (int)
        Number of vertices index (int)
        List of points (vec3 * Number of points)
        List of vertices index (vec3 * Number of vertices index)
        Texture coordinate for each point (vec2 * Number of points)
    ex: shape triangleShape 4 2  -2 0 0  -2 4 0  2 4 0  2 0 0  0 1 2  0 2 3
    (Create a simple rectangle and normals are automatically computed)
        

```

## scale

Scale the last defined object

```
  scale
    take : 
        scale vector (vec3)
    ex : scale 0.5 0.3 0.4
```

## translate

Translate the last defined object

```
  translate
    take : 
        translate vector (vec3)
    ex : translate 1 2 3
```

## transparentObject

The next objects will be defined as transparent objects
```
    ex : transparentObject
```


